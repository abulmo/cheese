################################################################################
#
# Cheese, Chess engine.
#
# Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
#
################################################################################

add_library(project_options INTERFACE)

target_compile_features(project_options INTERFACE cxx_std_17)

if (NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
	message(STATUS "Build type not specified, using Release as default")
	set(CMAKE_BUILD_TYPE Release)
endif()

# export compile commands for clang tidy
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if (ENABLE_IPO)
	include(CheckIPOSupported)
 	check_ipo_supported(RESULT result OUTPUT output)
 	if (result)
		set(CMAKE_INTERPROCEDURAL_OPTIMIZATION ON)
	else()
		message(STATUS "Interprocedural optimizations not supported")
	endif()
endif()

if (WIN32)
	target_compile_definitions(project_options INTERFACE WIN32)
endif()

if (MSVC)

	# compiler options for Microsoft Visual Studio

	# doesn't work ?
	#set_property(TARGET project_options PROPERTY MSVC_RUNTIME_LIBRARY
	#	"MultiThreaded$<$<CONFIG:Debug>:Debug>$<$<BOOL:BUILD_DYNAMIC>:DLL>")

	if (BUILD_DYNAMIC)
		target_compile_options(project_options INTERFACE
			$<$<CONFIG:Release>:/MD>
			$<$<CONFIG:Debug>:/MDd>
		)
	else()
		target_compile_options(project_options INTERFACE
			$<$<CONFIG:Release>:/MT>
			$<$<CONFIG:Debug>:/MTd>
		)
	endif()

	target_compile_options(project_options INTERFACE
		/favor:blend /nologo
		/W4 /WX-
		$<$<CONFIG:Release>:
			/Ox /GS- /Gw
			/EHsc /GR- /D_HAS_EXCEPTIONS=0>

		$<$<BOOL:${ENABLE_IPO}>: /GL>
	)

	target_link_options(project_options INTERFACE
		$<$<CONFIG:Release>:
			/OPT:REF>
		/STACK:8192000

		$<$<BOOL:${ENABLE_IPO}>: /INCREMENTAL:NO /LTCG>
	)

elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")

	# compiler options for Clang

	target_compile_options(project_options INTERFACE
		-pipe
		-Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic
		$<$<CONFIG:Release>:
			-O3 -fomit-frame-pointer
			-fno-rtti -fno-exceptions>
		$<$<BOOL:${USE_POPCOUNT}>:
			$<$<NOT:$<BOOL:${ARM}>>:
				-mpopcnt>>
		$<$<BOOL:${USE_PEXT}>:
			-mbmi2>
		$<$<BOOL:${ARM_THUMB}>:
			-mthumb>
		$<$<BOOL:${ARM_PIE}>:
			-fPIE>
	)

	target_link_options(project_options INTERFACE
		$<$<NOT:$<BOOL:${BUILD_DYNAMIC}>>:
			-static -static-libgcc -static-libstdc++
			-Wl,--whole-archive -lpthread -Wl,--no-whole-archive>
		$<$<BOOL:${ARM_PIE}>:
			-fPIE -pie>
	)

elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

	# compiler options for GNU gcc or MinGW gcc

	target_compile_options(project_options INTERFACE
		-pipe
		-Wall -Wextra -pedantic
		$<$<CONFIG:Release>:
			-O3 -fomit-frame-pointer
			-fno-rtti -fno-exceptions>
		$<$<BOOL:${USE_POPCOUNT}>:
			$<$<NOT:$<BOOL:${ARM}>>:
				-mpopcnt>>
		$<$<BOOL:${USE_PEXT}>:
			-mbmi2>
		$<$<BOOL:${ARM_THUMB}>:
			-mthumb>
		$<$<BOOL:${ARM_PIE}>:
			-fPIE>
	)

	target_link_options(project_options INTERFACE
		$<$<NOT:$<BOOL:${BUILD_DYNAMIC}>>:
			-static -static-libgcc -static-libstdc++
			-Wl,--whole-archive -lpthread -Wl,--no-whole-archive>
		$<$<BOOL:${BUILD_DYNAMIC}>:
			-no-pie>
		$<$<PLATFORM_ID:MinGW>:
			-Wl,--stack,8192000>
		$<$<BOOL:${ARM_PIE}>:
			-fPIE -pie>
	)

else()
	message(STATUS "Compiler not detected, using default options")
endif()


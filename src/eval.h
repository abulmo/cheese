////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_EVAL_H_
#define CHEESE_EVAL_H_

#include "config.h"

#include "bitboard.h"
#include "endgame.h"
#include "move.h"
#include "tune.h"

#define USE_LAZY_EVAL

#define USE_OUTPOST_KNIGHT

//#define USE_OUTPOST_BISHOP

//#define USE_PAWN_STORM

// use personality files to initialize evaluation function
#define USE_PERSONALITY

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// evaluation score, depending on middle game and endgame score
class EvalScore {

public:

	// middle game score
	int32_t mid;

	// end game score
	int32_t end;

	EvalScore() = default;

	EvalScore(int32_t m, int32_t e) : mid(m), end(e)
	{ }

	EvalScore & operator=(const int32_t &v)
	{
		mid = v;
		end = v;
		return *this;
	}

	// interpolate mid and end score to the real score
	inline int interpolate(int p, int max) const
	{
		return (mid * p + end * (max - p)) / max;
	}

	inline EvalScore & operator+= (const EvalScore &s)
	{
		mid += s.mid;
		end += s.end;
		return *this;
	}

	inline EvalScore & operator-= (const EvalScore &s)
	{
		mid -= s.mid;
		end -= s.end;
		return *this;
	}

	inline EvalScore & operator+= (int v)
	{
		mid += v;
		end += v;
		return *this;
	}

	inline EvalScore & operator-= (int v)
	{
		mid -= v;
		end -= v;
		return *this;
	}

	inline EvalScore & operator*= (int v)
	{
		mid *= v;
		end *= v;
		return *this;
	}

	inline EvalScore & operator/= (int v)
	{
		mid /= v;
		end /= v;
		return *this;
	}
};

inline EvalScore operator+(const EvalScore &a, const EvalScore &b)
{
	return { a.mid + b.mid, a.end + b.end };
}

inline EvalScore operator-(const EvalScore &a, const EvalScore &b)
{
	return { a.mid - b.mid, a.end - b.end };
}

inline EvalScore operator-(const EvalScore &e)
{
	return { -e.mid, -e.mid };
}

inline EvalScore operator*(const EvalScore &e, int v)
{
	return { e.mid * v, e.end * v };
}

inline EvalScore operator/(const EvalScore &e, int v)
{
	return { e.mid / v, e.end / v };
}

class ChessBoard;
class ChessPawnHashTable;

#ifdef USE_PERSONALITY
////////////////////////////////////////////////////////////////////////////////
// chess evaluation personality parameters
struct ChessEvalPersonality {

	int32_t materialPawn;

	int32_t materialKnight;

	int32_t materialBishop;

	int32_t materialRook;

	int32_t materialQueen;

	int32_t bishopPair;

	int32_t pawnDoubledMid;

	int32_t pawnDoubledEnd;

	int32_t pawnIsolatedMid;

	int32_t pawnIsolatedEnd;

	int32_t pawnBackwardMid;

	int32_t pawnBackwardEnd;

	int32_t rookOpenFileMid;

	int32_t rookOpenFileEnd;

	int32_t rookSemiOpenFileMid;

	int32_t rookSemiOpenFileEnd;

	int32_t rook7thRankMid;

	int32_t rook7thRankEnd;

	int32_t kingSafetyOwn;

	int32_t kingSafetyOpp;

	int32_t mobilityOwn;

	int32_t mobilityOpp;

	int32_t randomEval;

};
#endif

////////////////////////////////////////////////////////////////////////////////
// parameters passed to evaluation functions
struct ChessEvalParam {

	std::array<std::array<BitBoard, nb_pieces_type>, nb_sides> bbAttackByPiece;

	std::array<BitBoard, nb_sides> bbPassed;

	std::array<EvalScore, nb_sides> scoreMaterial;

	std::array<EvalScore, nb_sides> scoremob;

	std::array<int, nb_sides> kingSafety;

};

constexpr int max_mobility_knight = 9;
constexpr int max_mobility_bishop = 14;
constexpr int max_mobility_rook   = 15;
constexpr int max_mobility_queen  = 28;

constexpr int max_game_phase = 24;

// infinity score value
constexpr int eval_infinity = 32767;

// mate value returned by search (+/-)
constexpr int mate_value = 32000;

// default draw value returned by search
constexpr int draw_value = 0;

// minimum mate score : mated in N ply
constexpr int min_score_mate = -mate_value + 256;

// maximum mate score : mate in N ply
constexpr int max_score_mate = mate_value - 256;

////////////////////////////////////////////////////////////////////////////////
// Evaluation class
// need tables initialised in BitBoards::init()
class ChessEval {

private:

	// engine playing side (used for personality)
	Side engineSide;

#ifdef USE_PERSONALITY
	// personality parameters
	ChessEvalPersonality perso;
#endif

	bool usePerso;

#ifdef USE_TUNING
	Tuning tuning;
#endif

	// Contempt factor returned at draw score
	int drawValue;

	// pieces square tables [side][piece][square]
	std::array<std::array<std::array<EvalScore, nb_squares>, nb_pieces_type>,
		nb_sides> evalPSQT;

	// knight outpost
	std::array<std::array<EvalScore, nb_squares>, nb_sides> evalOutpostKnight;

	// bishop outpost
#ifdef USE_OUTPOST_BISHOP
	std::array<std::array<EvalScore, nb_squares>, nb_sides> evalOutpostBishop;
#endif

	// total material value at the begining
	int32_t startMaterialValue;

	// malus for doubled pawns
	EvalScore evalDoubledPawn;

	// isolated pawn
	EvalScore evalIsolatedPawn;

	// backward pawn
	EvalScore evalBackwardPawn;

	// open file
	EvalScore evalRookOpenFile;

	// semi open file
	EvalScore evalRookSemiOpenFile;

	// rook on 7th file
	EvalScore evalRookOn7th;

	// bishop pair (bonus = half a pawn)
	EvalScore evalBishopPair;

	// passed pawns base score middle game
	int32_t evalPassedBaseMid;
	
	// passed pawns base score end game
	int32_t evalPassedBaseEnd;
	
	// passed paws free to advance
	int32_t evalPassedFree;
	
	// passed pawns score distance to oponent king
	int32_t evalPassedKingOpp;
	
	// passed pawns score distance to own king
	int32_t evalPassedKingOwn;

	// mobility tables
	std::array<EvalScore, nb_pieces_type> mobTranslate;

	std::array<EvalScore, nb_pieces_type> mobScale;

	std::array<EvalScore, max_mobility_knight> evalMobilityKnight;

	std::array<EvalScore, max_mobility_bishop> evalMobilityBishop;

	std::array<EvalScore, max_mobility_rook> evalMobilityRook;

	std::array<EvalScore, max_mobility_queen> evalMobilityQueen;

	std::array<int32_t, nb_pieces_type> kingSafetyScale;

	// king safety attacks exponential table
	std::array<int32_t, 32> kingAttackTable;

	// eval for missing or moved pawn shield
	std::array<int32_t, nb_ranks> evalPawnShield;

#ifdef USE_PAWN_STORM
	std::array<int32_t, nb_ranks> evalPawnStorm;
#endif

	// blocked pawns
	EvalScore evalBlockedPawn;

	// blocked bishops
	EvalScore evalTrappedBishop;

	// blocked knights
	EvalScore evalTrappedKnight;

	// blocked rooks
	EvalScore evalBlockedRook;

	// tempo bonus
	EvalScore evalTempo;

public:

	ChessEval();

#ifdef USE_TUNING
	void initTuning();

	Tuning &getTuning();
#endif

	// evaluate position
	int evaluate(const ChessBoard &board, ChessPawnHashTable &htp, int alpha,
		int beta);

	void setEngineSide(Side s);

	// set default parameters
	void initEvalData(bool useperso);

	// initialize evaluation
	void initEval();

	int getDrawValue() const;

	void setDrawValue(int v);

#ifdef USE_PERSONALITY
	bool loadPersonality(const std::string &fname);
#endif

private:

	template<Side side>
	EvalScore evaluateKnights(const ChessBoard &board, ChessEvalParam &ev);

	template<Side side>
	EvalScore evaluateBishops(const ChessBoard &board, ChessEvalParam &ev);

	template<Side side>
	EvalScore evaluateRooks(const ChessBoard &board, ChessEvalParam &ev);

	template<Side side>
	EvalScore evaluateQueens(const ChessBoard &board, ChessEvalParam &ev);

	template<Side side>
	EvalScore evaluateKing(const ChessBoard &board, ChessEvalParam &ev);

	template<Side side>
	EvalScore evaluatePawns(const ChessBoard &board, ChessEvalParam &ev);

	template<Side side>
	EvalScore evaluatePassedPawns(const ChessBoard &board, ChessEvalParam &ev);

	template<Side side>
	int evaluateKingFile(const ChessBoard &board, int f);

};

#ifdef USE_TUNING
////////////////////////////////////////////////////////////////////////////////
inline Tuning &ChessEval::getTuning()
{
	return tuning;
}
#endif

////////////////////////////////////////////////////////////////////////////////
inline void ChessEval::setEngineSide(Side s)
{
	engineSide = s;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEval::getDrawValue() const
{
	return drawValue;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEval::setDrawValue(int v)
{
	drawValue = v;
}

////////////////////////////////////////////////////////////////////////////////
constexpr uint64_t getCountBlackPawn(uint64_t m)
{
	return (m & 0x0000000fULL);
}

constexpr uint64_t getCountBlackKnight(uint64_t m)
{
	
	return ((m >> 4) & 0x0000000fULL);
}

constexpr uint64_t getCountBlackBishop(uint64_t m)
{
	return ((m >> 8) & 0x0000000fULL);
}

constexpr uint64_t getCountBlackRook(uint64_t m)
{
	return ((m >> 12) & 0x0000000fULL);
}

constexpr uint64_t getCountBlackQueen(uint64_t m)
{
	return ((m >> 16) & 0x0000000fULL);
}

constexpr uint64_t getCountWhitePawn(uint64_t m)
{
	return ((m >> 32) & 0x0000000fULL);
}

constexpr uint64_t getCountWhiteKnight(uint64_t m)
{
	return ((m >> 36) & 0x0000000fULL);
}

constexpr uint64_t getCountWhiteBishop(uint64_t m)
{
	return ((m >> 40) & 0x0000000fULL);
}

constexpr uint64_t getCountWhiteRook(uint64_t m)
{
	return ((m >> 44) & 0x0000000fULL);
}

constexpr uint64_t getCountWhiteQueen(uint64_t m)
{
	return ((m >> 48) & 0x0000000fULL);
}

constexpr bool havePawns(uint64_t m)
{
	return ((m & 0x0000000f0000000fULL) != 0ULL);
}

constexpr bool haveOnlyBishopPawns(uint64_t m)
{
	return ((m & 0x000FF0F0000FF0F0ULL) == 0ULL);
}

////////////////////////////////////////////////////////////////////////////////
// material signature (64 bits) for piece count, used to detect endgames
//
// White							                Black
//
// unused		Queens Rooks Bishops Knights Pawns	unused       Queens Rooks Bishops Knights Pawns
// 000000000000 0000   0000  0000    0000    0000   000000000000 0000   0000  0000    0000    0000
//
const std::array<std::array<uint64_t, nb_pieces_type>, nb_sides>
	material_count_mask = {{
	// black
	{
	  0x0000000000000000ULL,
	  0x0000000000000001ULL,	// pawn
	  0x0000000000000010ULL, 	// knight
	  0x0000000000000100ULL, 	// bishop
	  0x0000000000001000ULL, 	// rook
	  0x0000000000010000ULL, 	// queen
	  0x0000000000000000ULL, 	// king (not used)
	  0x0000000000000000ULL
	},
	// white
	{
	  0x0000000000000000ULL,
	  0x0000000100000000ULL, 	// pawn
	  0x0000001000000000ULL, 	// knight
	  0x0000010000000000ULL, 	// bishop
	  0x0000100000000000ULL, 	// rook
	  0x0001000000000000ULL, 	// queen
	  0x0000000000000000ULL,	// king (not used)
	  0x0000000000000000ULL
	}
}};

// material evaluation for game phase
// max = 4 + 2*2 + 1*2 + 1*2 = 12 * 2 = 24
const std::array<int, nb_pieces_type> piece_game_phase = {
	0, 0, 1, 1, 2, 4, 0, 0
};

extern std::array<int32_t, nb_pieces_type> pieces_value;

} // namespace Cheese

#endif //CHEESE_EVAL_H_

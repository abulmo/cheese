////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "endgame.h"

#include "board.h"
#include "engine.h"
#include "util/logfile.h"

namespace Cheese::ChessEndGames {

// endgames signatures		       :      White   Black
//							  	   : --...QRBNP...QRBNP---

// insuficient material
const uint64_t material_endgame_KNKN      = 0x0000001000000010ULL;
const uint64_t material_endgame_KBKB      = 0x0000010000000100ULL;
const uint64_t material_endgame_KBKN      = 0x0000010000000010ULL;
const uint64_t material_endgame_KNKB      = 0x0000001000000100ULL;
const uint64_t material_endgame_KKNN      = 0x0000000000000020ULL;
const uint64_t material_endgame_KNNK      = 0x0000002000000000ULL;

const uint64_t material_endgame_KBKNN 	  = 0x0000010000000020ULL;
const uint64_t material_endgame_KNKNN     = 0x0000001000000020ULL;
const uint64_t material_endgame_KNNKB     = 0x0000002000000100ULL;
const uint64_t material_endgame_KNNKN     = 0x0000002000000010ULL;
const uint64_t material_endgame_KNNKNN    = 0x0000002000000020ULL;

const uint64_t material_endgame_KBKNB 	  = 0x0000010000000110ULL;
const uint64_t material_endgame_KNKNB     = 0x0000001000000110ULL;
const uint64_t material_endgame_KNBKB     = 0x0000011000000100ULL;
const uint64_t material_endgame_KNBKN     = 0x0000011000000010ULL;

const uint64_t material_endgame_KBKBB 	  = 0x0000010000000200ULL;
const uint64_t material_endgame_KNKBB     = 0x0000001000000200ULL;
const uint64_t material_endgame_KBBKB     = 0x0000020000000100ULL;
const uint64_t material_endgame_KBBKN     = 0x0000020000000010ULL;

const uint64_t material_endgame_KKP       = 0x0000000000000001ULL;
const uint64_t material_endgame_KPK       = 0x0000000100000000ULL;
const uint64_t material_endgame_KKR       = 0x0000000000001000ULL;
const uint64_t material_endgame_KRK       = 0x0000100000000000ULL;
const uint64_t material_endgame_KKQ       = 0x0000000000010000ULL;
const uint64_t material_endgame_KQK       = 0x0001000000000000ULL;

// drawish endgames
const uint64_t material_endgame_KNKR      = 0x0000001000001000ULL;
const uint64_t material_endgame_KRKN      = 0x0000100000000010ULL;
const uint64_t material_endgame_KBKR      = 0x0000010000001000ULL;
const uint64_t material_endgame_KRKB      = 0x0000100000000100ULL;

const uint64_t material_endgame_KBKP      = 0x0000010000000001ULL;
const uint64_t material_endgame_KPKB      = 0x0000000100000100ULL;
const uint64_t material_endgame_KNKP      = 0x0000001000000001ULL;
const uint64_t material_endgame_KPKN      = 0x0000000100000010ULL;

// bonus to push our king near opponent king
const std::array<int32_t, max_square_distance> eval_king_to_king = {
	0, 0, 25, 20, 15, 10, 5, 0
};

// push ennemy king to border in endgames
const std::array<int32_t, nb_squares> king_to_border = {
   80,  70,  60,  50,  50,  60,  70,  80,
   70,  60,  40,  30,  30,  40,  60,  70,
   60,  40,  20,  10,  10,  20,  40,  60,
   50,  30,  10,   0,   0,  10,  30,  50,
   50,  30,  10,   0,   0,  10,  30,  50,
   60,  40,  20,  10,  10,  20,  40,  60,
   70,  60,  40,  30,  30,  40,  60,  70,
   80,  70,  60,  50,  50,  60,  70,  80
};

#ifdef USE_BITBASE
////////////////////////////////////////////////////////////////////////////////
template<Side side>
int evalKPK(const ChessBoard &board)
{
	uint64_t bb = board.bitboardPiece(side, pawn);
	if (bb) {
		int sq = BitBoards::getFirstBit(bb);

		// use only draw information from KPK table
		int kw;
		int kb;
		int pw;
		Side s;
		if constexpr (side == white) {
			kw = board.kingPosition(side);
			kb = board.kingPosition(~side);
			pw = sq;
			s = board.side();
		} else {
			kw = flip_square[board.kingPosition(side)];
			kb = flip_square[board.kingPosition(~side)];
			pw = flip_square[sq];
			s = ~board.side();
		}

		if (BitBases::readKPK(s, kw, kb, pw) == 0) {
			// draw
			return 0;
		}
		// win				
		int v = pieces_value[pawn] + 
			    relSquareRank<side>(sq) * 8;
				
		return (board.side() == side) ? v : -v;
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
int evalKPK(Side side, const ChessBoard &board)
{
	return (side == white) ? evalKPK<white>(board) : evalKPK<black>(board);
}
#endif

////////////////////////////////////////////////////////////////////////////////
template<Side side>
int evalKRK(const ChessBoard &board)
{
	int v = pieces_value[rook] +
			king_to_border[board.kingPosition(~side)] +
			eval_king_to_king[squareDistance(board.kingPosition(side),
				board.kingPosition(~side))];
	return (board.side() == side) ? v : -v;
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
int evalKQK(const ChessBoard &board)
{
	int v = pieces_value[queen] +
			king_to_border[board.kingPosition(~side)] +
			eval_king_to_king[squareDistance(board.kingPosition(side),
				board.kingPosition(~side))];
	return (board.side() == side) ? v : -v;
}

////////////////////////////////////////////////////////////////////////////////
// evaluate known endgames
int evalEndGames(const ChessBoard &board, int &eval, int &scale)
{
	scale = max_draw_scale;

	// draw by insufficient material
	if (board.evaluateDraw()) {
		eval = draw_value;
		return 1;
	}

	// TODO : wrong color bishop

	// known endgames
	switch (board.materialCount()) {

#ifdef USE_BITBASE
		// KPK
		case	material_endgame_KPK:
				eval = evalKPK<white>(board);
				return 1;
				break;

		case	material_endgame_KKP:
				eval = evalKPK<black>(board);
				return 1;
				break;
#endif

		// KRK
		case	material_endgame_KRK:
				eval = evalKRK<white>(board);
				return 1;
				break;

		case	material_endgame_KKR:
				eval = evalKRK<black>(board);
				return 1;
				break;

		// KQK
		case	material_endgame_KQK:
				eval = evalKQK<white>(board);
				return 1;
				break;

		case	material_endgame_KKQ:
				eval = evalKQK<black>(board);
				return 1;
				break;

		// easy draw
		case	material_endgame_KNKN:
		case	material_endgame_KBKB:
		case	material_endgame_KNKB:
		case	material_endgame_KBKN:
		case	material_endgame_KNNK:
		case	material_endgame_KKNN:
				eval = draw_value;
				return 1;
				break;

		case	material_endgame_KRKN:
		case	material_endgame_KRKB:
		case	material_endgame_KBKR:
		case	material_endgame_KNKR:
				scale = 8;
				return 0;
				break;

		case	material_endgame_KBKNN:
		case	material_endgame_KNKNN:
		case	material_endgame_KNNKB:
		case	material_endgame_KNNKN:

		case	material_endgame_KNBKN:
		case	material_endgame_KNBKB:
		case	material_endgame_KNKNB:
		case	material_endgame_KBKNB:

		case	material_endgame_KNNKNN:
				scale = 2;
				return 0;
				break;

		case	material_endgame_KBBKN:
		case	material_endgame_KBBKB:
		case	material_endgame_KNKBB:
		case	material_endgame_KBKBB:
				scale = 4;
				return 0;
				break;

		case	material_endgame_KBKP:
		case	material_endgame_KNKP:
		case	material_endgame_KPKB:
		case	material_endgame_KPKN:
				scale = 8;
				return 0;
				break;

		default:
				return 0;
				break;
	}
}

} // namespace Cheese::ChessEndGames

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "eval.h"

#include "board.h"
#include "util/configfile.h"
#include "search.h"
#include "evalparams.h"
#include "hash.h"
#include "util/logfile.h"

#include <cassert>

#include <iomanip>
#include <iostream>
#include <string>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// lazy eval cutoff (minor piece value)
constexpr int lazy_eval_margin = 325;

// material value
std::array<int32_t, nb_pieces_type> pieces_value;

////////////////////////////////////////////////////////////////////////////////
ChessEval::ChessEval() : engineSide(white)
#ifdef USE_PERSONALITY
						 , perso()
#endif
						 , usePerso(false), drawValue(draw_value)
{
}

#ifdef USE_TUNING
////////////////////////////////////////////////////////////////////////////////
void ChessEval::initTuning()
{
	//tuning.add("PawnValue", &pieces_value[pawn], 100, 1, 200);
	//tuning.add("KnightValue", &pieces_value[knight], 300, 1, 500);
	//tuning.add("BishopValue", &pieces_value[bishop], 300, 1, 500);
	//tuning.add("RookValue", &pieces_value[rook], 500, 1, 1000);
	//tuning.add("QueenValue", &pieces_value[queen], 900, 1, 1500);
}
#endif

////////////////////////////////////////////////////////////////////////////////
template<Side side>
 EvalScore ChessEval::evaluateKnights(const ChessBoard &board,
	ChessEvalParam &ev)
{
	EvalScore score(0, 0);

	const BitBoard bbAttacks = ev.bbAttackByPiece[~side][pawn];

	ev.bbAttackByPiece[side][knight] = 0;

	BitBoard bbSrc = board.bitboardPiece(side, knight);
	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);

		// psqt
		score += evalPSQT[side][knight][src];

		BitBoard bAtt = BitBoards::attackKnight(src);

		ev.bbAttackByPiece[side][knight] |= bAtt;

		// mobility
		bAtt = bAtt & ~board.bitboardColor(side) & ~bbAttacks;
		int mb = BitBoards::bitCount(bAtt);
		ev.scoremob[side] += evalMobilityKnight[mb];

		// outpost
#ifdef USE_OUTPOST_KNIGHT
		if ((!(BitBoards::attackPawn(side, src) &
				board.bitboardPiece(~side, pawn))) &&
			  (BitBoards::attackPawn(~side, src) &
				board.bitboardPiece(side, pawn))) {
			score += evalOutpostKnight[side][src];
		}
#endif

		// trapped knight
		if constexpr (side == white) {
			if ((src == a7) &&
				(board.bitboardPiece(black, pawn) & BitBoards::square[b7]) &&
				(board.bitboardPiece(black, pawn) & BitBoards::square[a6])) {
				score -= evalTrappedKnight;
			}
			if ((src == h7) &&
				(board.bitboardPiece(black, pawn) & BitBoards::square[g7]) &&
				(board.bitboardPiece(black, pawn) & BitBoards::square[h6])) {
				score -= evalTrappedKnight;
			}
		} else {
			if ((src == a2) &&
				(board.bitboardPiece(white, pawn) & BitBoards::square[b2]) &&
				(board.bitboardPiece(white, pawn) & BitBoards::square[a3])) {
				score -= evalTrappedKnight;
			}
			if ((src == h2) &&
				(board.bitboardPiece(white, pawn) & BitBoards::square[g2]) &&
				(board.bitboardPiece(white, pawn) & BitBoards::square[h3])) {
				score -= evalTrappedKnight;
			}
		}

	}
	return score;
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
 EvalScore ChessEval::evaluateBishops(const ChessBoard &board,
			ChessEvalParam &ev)
{
	EvalScore score(0, 0);

	const BitBoard bbAttacks = ev.bbAttackByPiece[~side][pawn];

	ev.bbAttackByPiece[side][bishop] = 0;

	BitBoard bbSrc = board.bitboardPiece(side, bishop);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);

		// psqt
		score += evalPSQT[side][bishop][src];

		BitBoard bAtt = BitBoards::attackBishop(src, board.bitboardAll());

		ev.bbAttackByPiece[side][bishop] |= bAtt;

		// mobility
		bAtt = bAtt & ~board.bitboardColor(side) & ~bbAttacks;
		int mb = BitBoards::bitCount(bAtt);
		ev.scoremob[side] += evalMobilityBishop[mb];

		// outpost
#ifdef USE_OUTPOST_BISHOP
		if ((!(BitBoards::attackPawn(side, src) &
				board.bitboardPiece(~side, pawn))) &&
			  (BitBoards::attackPawn(~side, src) &
				board.bitboardPiece(side, pawn))) {
			score += evalOutpostBishop[side][src];
		}
#endif

		// trapped bishop
		if constexpr (side == white) {
			if ((src == a7) &&
				(board.bitboardPiece(black, pawn) & BitBoards::square[b6])) {
				score -= evalTrappedBishop;
			}
			if ((src == h7) &&
				(board.bitboardPiece(black, pawn) & BitBoards::square[g6])) {
				score -= evalTrappedBishop;
			}
		} else {
			if ((src == a2) &&
				(board.bitboardPiece(white, pawn) & BitBoards::square[b3])) {
				score -= evalTrappedBishop;
			}
			if ((src == h2) &&
				(board.bitboardPiece(white, pawn) & BitBoards::square[g3])) {
				score -= evalTrappedBishop;
			}
		}
	}

	return score;
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
 EvalScore ChessEval::evaluateRooks(const ChessBoard &board,
			ChessEvalParam &ev)
{
	constexpr int r7 = relRank<side>(rank_7);
	constexpr int r8 = relRank<side>(rank_8);

	EvalScore score(0, 0);

	const BitBoard bbAttacks = ev.bbAttackByPiece[~side][pawn];

	ev.bbAttackByPiece[side][rook] = 0;

	BitBoard bbSrc = board.bitboardPiece(side, rook);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);

		// psqt
		score += evalPSQT[side][rook][src];

		BitBoard bAtt = BitBoards::attackRook(src, board.bitboardAll());

		ev.bbAttackByPiece[side][rook] |= bAtt;

		// mobility
		bAtt = bAtt & ~board.bitboardColor(side) & ~bbAttacks;
		int mb = BitBoards::bitCount(bAtt);
		ev.scoremob[side] += evalMobilityRook[mb];

		int f = squareFile(src);

		// rook on an open/semi-open file
		if (!(board.bitboardPiece(side, pawn) & BitBoards::file[f])) {
			if (!(board.bitboardPiece(~side, pawn) & BitBoards::file[f])) {
				score += evalRookOpenFile;
			} else {
				score += evalRookSemiOpenFile;
			}
		}

		// rook on 7th rank, with king on 8th or ennemi pawns on 7th
		if (squareRank(src) == r7) {
			if ((squareRank(board.kingPosition(~side)) == r8) ||
				(board.bitboardPiece(~side, pawn) & BitBoards::rank[r7])) {
				score += evalRookOn7th;
			}
		}

		// trapped rook
		if constexpr (side == white) {
			if (((src == g1) || (src == h1)) &&
				(board.kingPosition(white) <= h1) &&
				(board.kingPosition(white) > e1) &&
				(board.kingPosition(white) < src)) {
				score -= evalBlockedRook;
			}
			if (((src == a1) || (src == b1) || (src == c1)) &&
				(board.kingPosition(white) <= h1) &&
				(board.kingPosition(white) < d1) &&
				(board.kingPosition(white) > src)) {
				score -= evalBlockedRook;
			}
		} else {
			if (((src == g8) || (src == h8)) &&
				(board.kingPosition(black) >= a8) &&
				(board.kingPosition(black) > e8) &&
				(board.kingPosition(black) < src)) {
				score -= evalBlockedRook;
			}
			if (((src == a8) || (src == b8) || (src == c8)) &&
				(board.kingPosition(black) >= a8) &&
				(board.kingPosition(black) < d8) &&
				(board.kingPosition(black) > src)) {
				score -= evalBlockedRook;
			}
		}

	}
	return score;
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
 EvalScore ChessEval::evaluateQueens(const ChessBoard &board,
			ChessEvalParam &ev)
{
	EvalScore score(0, 0);

	const BitBoard bbAttacks = ev.bbAttackByPiece[~side][pawn];

	ev.bbAttackByPiece[side][queen] = 0;

	BitBoard bbSrc = board.bitboardPiece(side, queen);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);

		// psqt
		score += evalPSQT[side][queen][src];

		BitBoard bAtt = BitBoards::attackQueen(src, board.bitboardAll());

		ev.bbAttackByPiece[side][queen] |= bAtt;

		// mobility
		bAtt = bAtt & ~board.bitboardColor(side) & ~bbAttacks;
		int mb = BitBoards::bitCount(bAtt);
		ev.scoremob[side] += evalMobilityQueen[mb];
	}
	return score;
}

////////////////////////////////////////////////////////////////////////////////
// evaluate a King file (pawn shelter + pawn storm)
template<Side side>
int ChessEval::evaluateKingFile(const ChessBoard &board, int f)
{
	int value = 0;

	// pawn shelter
	int r = 0;
	BitBoard bbp = board.bitboardPiece(side, pawn) & BitBoards::file[f];
	if (bbp) {
		r = (side == white) ?
			squareRank(BitBoards::getFirstBit(bbp)) :
			(rank_8 - squareRank(BitBoards::getLastBit(bbp)));
	}

	value += evalPawnShield[r];

	// pawn storm
#ifdef USE_PAWN_STORM
	r = 0;
	bbp = board.bitboardPiece(~side, pawn) & BitBoards::file[f];
	if (bbp) {
		r = (side == white) ?
			squareRank(BitBoards::getFirstBit(bbp)) :
			(rank_8 - squareRank(BitBoards::getLastBit(bbp)));
	}
	value -= evalPawnStorm[r];
#endif

	return value;
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
 EvalScore ChessEval::evaluateKing(const ChessBoard &board, ChessEvalParam &ev)
{
	int32_t value = 0;

	const int pk = board.kingPosition(side);

	// pawn shelter + pawn storm
	const int b = (side == white) ? (squareRank(pk) <= rank_2) :
								    (squareRank(pk) >= rank_7);
	if (b) {
		int f = squareFile(pk);
		for (int n=std::max<int>(file_a, f - 1);
			n<=std::min<int>(file_h, f + 1); n++) {
			value += evaluateKingFile<side>(board, n);
		}
	}

	// king safety
	int32_t v = 0;

	BitBoard bbAtt = BitBoards::attackKing(pk) &
		ev.bbAttackByPiece[~side][knight];
	v += BitBoards::bitCount(bbAtt) * kingSafetyScale[knight];
	bbAtt = BitBoards::attackKing(pk) & ev.bbAttackByPiece[~side][bishop];
	v += BitBoards::bitCount(bbAtt) * kingSafetyScale[bishop];
	bbAtt = BitBoards::attackKing(pk) & ev.bbAttackByPiece[~side][rook];
	v += BitBoards::bitCount(bbAtt) * kingSafetyScale[rook];
	bbAtt = BitBoards::attackKing(pk) & ev.bbAttackByPiece[~side][queen];
	v += BitBoards::bitCount(bbAtt) * kingSafetyScale[queen];

	v = std::min<int32_t>(31, v);
	value -= kingAttackTable[v];

	// scale king safety with opponent material
	value = (value * ev.scoreMaterial[~side].mid) / startMaterialValue;

	ev.kingSafety[side] = value;

	EvalScore score(0, 0);

	score += evalPSQT[side][king][pk];

	return score;
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
 EvalScore ChessEval::evaluatePawns(const ChessBoard &board,
			ChessEvalParam &ev)
{
	EvalScore score(0, 0);

	BitBoard bbSrc = board.bitboardPiece(side, pawn);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);

		// psqt
		score += evalPSQT[side][pawn][src];

		// doubled pawns
		bool doubled = false;

		int dst = relSquareUp<side>(src);

		BitBoard bd = (side == white) ?
			(BitBoards::fillUp(BitBoards::square[dst]) &
							   board.bitboardPiece(side, pawn)) :
			(BitBoards::fillDown(BitBoards::square[dst]) &
							   board.bitboardPiece(side, pawn));
		if (bd) {
			doubled = true;
		}

		// isolated pawn
		bool isolated = false;
		int f = squareFile(src);
		if (!(board.bitboardPiece(side, pawn) & BitBoards::files_around[f])) {
			isolated = true;
		}

		// passed pawn
		if constexpr (side == white) {
			bd = BitBoards::fillDown(board.bitboardPiece(black, pawn)) &
				(BitBoards::square[dst] | BitBoards::attackPawn(white, src));
		} else {
			bd = BitBoards::fillUp(board.bitboardPiece(white, pawn)) &
				(BitBoards::square[dst] | BitBoards::attackPawn(black, src));
		}

		bool backward = false;
		if (!bd) {
			if (!doubled) {
				// passed pawn
				ev.bbPassed[side] |= BitBoards::square[src];
			}
		} else {
			// backward pawn
			if (!isolated) {
				// + front square without black pawn ? or empty ?
				if constexpr (side == white) {
					if ((!(BitBoards::fillDown(BitBoards::attackPawn(black,
						dst)) & board.bitboardPiece(white, pawn))) &&
						((BitBoards::attackPawn(white, dst) &
							board.bitboardPiece(black, pawn)))) {
						backward = true;
					}
				} else {
					if ((!(BitBoards::fillUp(BitBoards::attackPawn(white,
						dst)) & board.bitboardPiece(black, pawn))) &&
						((BitBoards::attackPawn(black, dst) &
							board.bitboardPiece(white, pawn)))) {
						backward = true;
					}
				}
			}
		}

		if (doubled) {
			score -= evalDoubledPawn;
		}
		if (isolated) {
			score -= evalIsolatedPawn;
		}
		if (backward) {
			score -= evalBackwardPawn;
		}
	}
	return score;
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
EvalScore ChessEval::evaluatePassedPawns(const ChessBoard &board,
	ChessEvalParam &ev)
{
	EvalScore score(0, 0);

	BitBoard bbSrc = ev.bbPassed[side];

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);

		// distance to promotion
		int r = relSquareRank<side>(src);

		// base score
		score += EvalScore((r - 1) * evalPassedBaseMid,
						   (r - 1) * evalPassedBaseEnd);

		if (r > rank_3) {

			// front square
			int dst = relSquareUp<side>(src);

			// bonus if the front square is empty
			if (board.piece(dst) == no_piece) {
				int rr = ((r - 2) * (r - 2));
				score += rr * evalPassedFree;
			}

			// distance to ennemy king
			score.end += (squareDistance(src, board.kingPosition(~side)) - 1) *
				r * evalPassedKingOpp;

			// protected by king
			score.end -= (squareDistance(src, board.kingPosition(side)) - 1) *
				r * evalPassedKingOwn;
		}
	}

#ifdef EVAL_INFO
	scorePassedPawns[side] = score;
#endif

	return score;
}

////////////////////////////////////////////////////////////////////////////////
int ChessEval::evaluate(const ChessBoard &board, ChessPawnHashTable &htp,
	int alpha, int beta)
{
	// disable lazy evaluation for tuning
#ifndef USE_TUNING
#ifdef USE_LAZY_EVAL
	// Lazy evaluation
	int lazyeval = board.materialScore(white) - board.materialScore(black);
	if (board.side() == black) {
		lazyeval = -lazyeval;
	}

	int lm = lazy_eval_margin;

	if (((lazyeval + lm) < alpha) ||
		((lazyeval - lm) > beta)) {
		return lazyeval;
	}
#endif //USE_LAZAY_EVAL
#endif // USE_TUNING

	int drawScale = ChessEndGames::max_draw_scale;

	// evaluate known endgames
	if (isGamePhaseAfterMiddle(board.gamePhase())) {
		int v = 0;
		if (ChessEndGames::evalEndGames(board, v, drawScale)) {
			return v;
		}
	}

	ChessEvalParam ev;

	// material
	ev.scoreMaterial[black] = board.materialScore(black);
	ev.scoreMaterial[white] = board.materialScore(white);

	// bishop pair
	if (getCountBlackBishop(board.materialCount()) >= 2) {
		ev.scoreMaterial[black] += evalBishopPair;
	}
	if (getCountWhiteBishop(board.materialCount()) >= 2) {
		ev.scoreMaterial[white] += evalBishopPair;
	}

	EvalScore score = ev.scoreMaterial[white] - ev.scoreMaterial[black];

	// tempo : time advantage for the side to play
	if (board.side() == white) {
		score += evalTempo;
	} else {
		score -= evalTempo;
	}

	// blocked pawns at D2,E2 / D7,E7
	if ((board.bitboardPiece(white, pawn) & BitBoards::square[d2]) &&
		(board.piece(d3) != no_piece)) {
		score -= evalBlockedPawn;
	}

	if ((board.bitboardPiece(white, pawn) & BitBoards::square[e2]) &&
		(board.piece(e3) != no_piece)) {
		score -= evalBlockedPawn;
	}

	if ((board.bitboardPiece(black, pawn) & BitBoards::square[d7]) &&
		(board.piece(d6) != no_piece)) {
		score += evalBlockedPawn;
	}

	if ((board.bitboardPiece(black, pawn) & BitBoards::square[e7]) &&
		(board.piece(e6) != no_piece)) {
		score += evalBlockedPawn;
	}

	ev.bbPassed[black] = 0;
	ev.bbPassed[white] = 0;

	if (board.getHashKeyPawns() != 0ULL) {

		// read pawn hash table
		auto *hashp = htp.getHashNode(board.getHashKeyPawns());

		if (hashp->key == board.getHashKeyPawns()) {

#ifdef HASH_STATS
			htp.addStatFound();
#endif

			ev.bbPassed[black] = hashp->passed[black];
			ev.bbPassed[white] = hashp->passed[white];
			score.mid += hashp->evalmid;
			score.end += hashp->evalend;

		} else {

#ifdef HASH_STATS
			htp.addStatMiss();
#endif

			EvalScore vp(0, 0);

			vp += evaluatePawns<white>(board, ev);
			vp -= evaluatePawns<black>(board, ev);

			score += vp;

			// save to hash table
			hashp->key = board.getHashKeyPawns();
			hashp->passed[black] = ev.bbPassed[black];
			hashp->passed[white] = ev.bbPassed[white];
			hashp->evalmid = vp.mid;
			hashp->evalend = vp.end;

#ifdef HASH_STATS
			htp.addStatStore();
#endif
		}
	}

	// generate pawn attacks bitboards
	ev.bbAttackByPiece[black][pawn] =
		(BitBoards::shiftUpLeft<black>(board.bitboardPiece(black, pawn)) &
			~BitBoards::file[file_a]) |
		(BitBoards::shiftUpRight<black>(board.bitboardPiece(black, pawn)) &
			~BitBoards::file[file_h]);

	ev.bbAttackByPiece[white][pawn] =
		(BitBoards::shiftUpLeft<white>(board.bitboardPiece(white, pawn)) &
			~BitBoards::file[file_h]) |
		(BitBoards::shiftUpRight<white>(board.bitboardPiece(white, pawn)) &
			~BitBoards::file[file_a]);

	ev.scoremob[black] = 0;
	ev.scoremob[white] = 0;

	// evaluate knights
	score += evaluateKnights<white>(board, ev);
	score -= evaluateKnights<black>(board, ev);

	// evaluate bishops
	score += evaluateBishops<white>(board, ev);
	score -= evaluateBishops<black>(board, ev);

	// evaluate rooks
	score += evaluateRooks<white>(board, ev);
	score -= evaluateRooks<black>(board, ev);

	// evaluate queens
	score += evaluateQueens<white>(board, ev);
	score -= evaluateQueens<black>(board, ev);

	// evaluate kings
	score += evaluateKing<white>(board, ev);
	score -= evaluateKing<black>(board, ev);

	// evaluate passed pawns
	score += evaluatePassedPawns<white>(board, ev);
	score -= evaluatePassedPawns<black>(board, ev);

	// add mobility
#ifdef USE_PERSONALITY
	if (usePerso) {
		int32_t pw;
		int32_t pb;
		if (engineSide == white) {
			pw = perso.mobilityOwn;
			pb = perso.mobilityOpp;
		} else {
			pw = perso.mobilityOpp;
			pb = perso.mobilityOwn;
		}
		score += (ev.scoremob[white] * pw - ev.scoremob[black] * pb) / 100;
	} else {
		score += ev.scoremob[white] - ev.scoremob[black];
	}
#else
	score += ev.scoremob[white] - ev.scoremob[black];
#endif

	// add king safety
#ifdef USE_PERSONALITY
	if (usePerso) {
		if (engineSide == white) {
			score.mid += (ev.kingSafety[white] * perso.kingSafetyOwn) / 100;
			score.mid -= (ev.kingSafety[black] * perso.kingSafetyOpp) / 100;
		} else {
			score.mid += (ev.kingSafety[white] * perso.kingSafetyOpp) / 100;
			score.mid -= (ev.kingSafety[black] * perso.kingSafetyOwn) / 100;
		}
	} else {
		score.mid += ev.kingSafety[white];
		score.mid -= ev.kingSafety[black];
	}
#else
	score.mid += ev.kingSafety[white];
	score.mid -= ev.kingSafety[black];
#endif

	// tappered evaluation
	int p = std::min<int>(board.gamePhase(), max_game_phase);
	int v = score.interpolate(p, max_game_phase);

	// scale drawish
	if (drawScale != ChessEndGames::max_draw_scale) {
		v = (v * drawScale) / ChessEndGames::max_draw_scale;
	}

#ifdef USE_PERSONALITY
	if ((usePerso) && (perso.randomEval != 0)) {
		// r = 1.0 => maximum strength, r = 0.0 => random eval
		int r = (100 - perso.randomEval) / 100;
		// TODO : use real random numbers + seed
		int rval = static_cast<int>(board.getHashKey() % 200) - 100;
		v = ((v * r) + rval * (100 - r)) / 100;
	}
#endif

	return (board.side() == white) ? v : -v;
}

#ifdef USE_PERSONALITY
////////////////////////////////////////////////////////////////////////////////
bool ChessEval::loadPersonality(const std::string &fname)
{
	ConfigFile conf;
	if (conf.load(fname)) {

		LOG_INFO << "load personality " << fname;

		perso.materialPawn = conf.getInt("PawnValue", 100);
		perso.materialKnight = conf.getInt("KnightValue", 325);
		perso.materialBishop = conf.getInt("BishopValue", 325);
		perso.materialRook = conf.getInt("RookValue", 500);
		perso.materialQueen = conf.getInt("QueenValue", 975);
		perso.bishopPair = conf.getInt("BishopPair", 50);
		perso.pawnDoubledMid = conf.getInt("PawnDoubledMid", 10);
		perso.pawnDoubledEnd = conf.getInt("PawnDoubledEnd", 10);
		perso.pawnIsolatedMid = conf.getInt("PawnIsolatedMid", 20);
		perso.pawnIsolatedEnd = conf.getInt("PawnIsolatedEnd", 20);
		perso.pawnBackwardMid = conf.getInt("PawnBackwardMid", 10);
		perso.pawnBackwardEnd = conf.getInt("PawnBackwardEnd", 10);
		perso.rookOpenFileMid = conf.getInt("RookOpenFileMid", 20);
		perso.rookOpenFileEnd = conf.getInt("RookOpenFileEnd", 20);
		perso.rookSemiOpenFileMid = conf.getInt("RookSemiOpenFileMid", 10);
		perso.rookSemiOpenFileEnd = conf.getInt("RookSemiOpenFileEnd", 10);
		perso.rook7thRankMid = conf.getInt("Rook7thRankMid", 20);
		perso.rook7thRankEnd = conf.getInt("Rook7thRankEnd", 20);
		perso.kingSafetyOwn = conf.getInt("KingSafetyOwn", 100);
		perso.kingSafetyOpp = conf.getInt("KingSafetyOpp", 100);
		perso.mobilityOwn = conf.getInt("MobilityOwn", 100);
		perso.mobilityOpp = conf.getInt("MobilityOpp", 100);
		perso.randomEval = conf.getInt("Randomness", 0);

		initEvalData(true);
		usePerso = true;

	} else {
		initEvalData(false);
		usePerso = false;
	}
	return usePerso;
}
#endif

////////////////////////////////////////////////////////////////////////////////
void ChessEval::initEvalData(bool useperso)
{
	// PSQT symmetry
	for (int p=0; p<nb_pieces_type; p++) {
		for (int s=0; s<nb_squares; s++) {
			evalPSQT[black][p][s].mid = param_psqt_mid[p][s];
			evalPSQT[black][p][s].end = param_psqt_end[p][s];
			evalPSQT[white][p][flip_square[s]].mid = param_psqt_mid[p][s];
			evalPSQT[white][p][flip_square[s]].end = param_psqt_end[p][s];
		}
	}

	// outpost symmetry
	for (int s=0; s<nb_squares; s++) {
		evalOutpostKnight[black][s].mid = param_eval_outpost_knight[s];
		evalOutpostKnight[black][s].end = param_eval_outpost_knight[s];
		evalOutpostKnight[white][flip_square[s]].mid =
			param_eval_outpost_knight[s];
		evalOutpostKnight[white][flip_square[s]].end =
			param_eval_outpost_knight[s];
	}

#ifdef USE_OUTPOST_BISHOP
	for (int s=0; s<nb_squares; s++) {
		evalOutpostBishop[black][s].mid = param_eval_outpost_bishop[s];
		evalOutpostBishop[black][s].end = param_eval_outpost_bishop[s];
		evalOutpostBishop[white][flip_square[s]].mid =
			param_eval_outpost_bishop[s];
		evalOutpostBishop[white][flip_square[s]].end =
			param_eval_outpost_bishop[s];
	}
#endif

	pieces_value = param_piece_capture_value;

	evalDoubledPawn.mid = param_eval_doubled_pawn[0];
	evalDoubledPawn.end = param_eval_doubled_pawn[1];
	evalIsolatedPawn.mid = param_eval_isolated_pawn[0];
	evalIsolatedPawn.end = param_eval_isolated_pawn[1];
	evalBackwardPawn.mid = param_eval_backward_pawn[0];
	evalBackwardPawn.end = param_eval_backward_pawn[1];
	evalRookOpenFile.mid = param_eval_rook_open_file[0];
	evalRookOpenFile.end = param_eval_rook_open_file[1];
	evalRookSemiOpenFile.mid = param_eval_rook_semi_open_file[0];
	evalRookSemiOpenFile.end = param_eval_rook_semi_open_file[1];
	evalRookOn7th.mid = param_eval_rook_on_7th[0];
	evalRookOn7th.end = param_eval_rook_on_7th[1];
	evalBishopPair.mid = param_eval_bishop_pair[0];
	evalBishopPair.end = param_eval_bishop_pair[1];

	evalPassedBaseMid = param_eval_passed_base_mid;
	evalPassedBaseEnd = param_eval_passed_base_end;
	evalPassedFree = param_eval_passed_free;
	evalPassedKingOpp = param_eval_passed_king_opp;
	evalPassedKingOwn = param_eval_passed_king_own;

	for (int i=0; i<nb_pieces_type; i++) {
		mobTranslate[i].mid = param_mob_translate[0][i];
		mobTranslate[i].end = param_mob_translate[1][i];
		mobScale[i].mid = param_mob_scale[0][i];
		mobScale[i].end = param_mob_scale[1][i];
	}

	kingSafetyScale = param_king_safety_scale;
	kingAttackTable = param_king_attack_table;

	evalPawnShield = param_eval_pawn_shield;
#ifdef USE_PAWN_STORM
	evalPawnStorm = param_eval_pawn_storm;
#endif
	evalBlockedPawn.mid = param_eval_blocked_pawn[0];
	evalBlockedPawn.end = param_eval_blocked_pawn[1];
	evalTrappedBishop.mid = param_eval_trapped_bishop[0];
	evalTrappedBishop.end = param_eval_trapped_bishop[1];
	evalTrappedKnight.mid = param_eval_trapped_knight[0];
	evalTrappedKnight.end = param_eval_trapped_knight[1];
	evalBlockedRook.mid = param_eval_blocked_rook[0];
	evalBlockedRook.end = param_eval_blocked_rook[1];
	evalTempo.mid = param_eval_tempo[0];
	evalTempo.end = param_eval_tempo[1];

#ifdef USE_PERSONALITY
	// apply personality
	if (useperso) {

		LOG_INFO << "init personality";

		pieces_value[pawn] = perso.materialPawn;
		pieces_value[knight] = perso.materialKnight;
		pieces_value[bishop] = perso.materialBishop;
		pieces_value[rook] = perso.materialRook;
		pieces_value[queen] = perso.materialQueen;

		evalBishopPair.mid = perso.bishopPair;
		evalBishopPair.end = perso.bishopPair;

		evalDoubledPawn.mid = perso.pawnDoubledMid;
		evalDoubledPawn.end = perso.pawnDoubledEnd;
		evalIsolatedPawn.mid = perso.pawnIsolatedMid;
		evalIsolatedPawn.end = perso.pawnIsolatedEnd;
		evalBackwardPawn.mid = perso.pawnBackwardMid;
		evalBackwardPawn.end = perso.pawnBackwardEnd;

		evalRookOpenFile.mid = perso.rookOpenFileMid;
		evalRookOpenFile.end = perso.rookOpenFileEnd;
		evalRookSemiOpenFile.mid = perso.rookSemiOpenFileMid;
		evalRookSemiOpenFile.end = perso.rookSemiOpenFileEnd;
		evalRookOn7th.mid = perso.rook7thRankMid;
		evalRookOn7th.end = perso.rook7thRankEnd;
	}
#endif

	initEval();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEval::initEval()
{
	startMaterialValue = pieces_value[pawn]   * 8 +
						 pieces_value[knight] * 2 +
						 pieces_value[bishop] * 2 +
						 pieces_value[rook]   * 2 +
						 pieces_value[queen];

	// init mobility tables
	for (int n=0; n<max_mobility_knight; n++) {
		evalMobilityKnight[n] = (mobScale[knight] * n) / 8 -
		                        mobTranslate[knight];
	}

	for (int n=0; n<max_mobility_bishop; n++) {
		evalMobilityBishop[n] = (mobScale[bishop] * n) / 8 -
		                        mobTranslate[bishop];
	}

	for (int n=0; n<max_mobility_rook; n++) {
		evalMobilityRook[n] = (mobScale[rook] * n) / 8 -
		                        mobTranslate[rook];
	}

	for (int n=0; n<max_mobility_queen; n++) {
		evalMobilityQueen[n] = (mobScale[queen] * n) / 8 -
		                        mobTranslate[queen];
	}
}

} // namespace Cheese

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "lazysmp.h"

#include "engine.h"
#include "search.h"

#include <map>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
ChessSearchThreadLazy::ChessSearchThreadLazy(int id, ChessSearchThreadList &th,
	ChessEngine &e) : ChessSearchThread(id, SMPMode::lazy, th, e),
	moveOrderRoot(history)
{
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThreadLazy::func()
{
	if (isMainThread()) {

		while (!terminate) {

			std::unique_lock<std::mutex> lk(mutex);
			thinking = false;

			threads.notify();
			cond.wait(lk, [&]{ return thinking || terminate; });
			lk.unlock();

			if (!terminate) {

				searching = true;

				resetNodeCount();
				clearHistory();

				// generate legal moves and check book + tablebases
				if (engine.search(moveOrderRoot)) {

					// copy root moves
					for (auto *thread : threads) {
						if (!thread->isMainThread()) {
							auto *t = static_cast<ChessSearchThreadLazy *>
								(thread);
							t->moveOrderRoot = moveOrderRoot;
						}
					}

					// wake up other threads
					for (auto *thread : threads) {
						if (!thread->isMainThread()) {
							thread->startSearching();
						}
					}

					// start search

					// TODO : don't allocate in stack
					ChessSearch searchMain(engine, threads, engine.getEval(),
						engine.getHashTable(), this, engine.getBoardRef(),
						engine.getRepetitions(),
						isMainThread() ? engine.mustCheckNodes() : false);

					// start iterative deepening search
					searchMain.startSearch(moveOrderRoot);

					// wait other threads to finish the search
					for (auto *thread : threads) {
						if (!thread->isMainThread()) {
							thread->waitEndSearching();
						}
					}

					// minimum score
					int minScore = 0;
					for (auto *thread : threads) {
						auto *t = static_cast<ChessSearchThreadLazy *>(thread);
						if (t->bestScore < minScore) {
							minScore = t->bestScore;
						}
					}

					std::map<uint32_t, uint64_t> scores;

					// keep best result, based on depth, score and occurence
					// of the best move
					ChessSearchThreadLazy *bestThread = this;
					for (auto *thread : threads) {
						auto *t = static_cast<ChessSearchThreadLazy *>(thread);
						scores[t->bestMove.data()] += 
							static_cast<uint64_t>((t->bestScore - minScore
							+ 1) * t->searchDepth);
						if (scores[t->bestMove.data()] >
							scores[bestMove.data()]) {
							bestThread = t;
						}
					}

					// send result
					engine.onEndSearch(bestThread->bestMove, 
						bestThread->ponderMove, bestThread->searchDepth, 
						threads.getMaxPly(), threads.getNodeCount(), 
						engine.getSearchTime());
				}

				searching = false;
			}
		}

	} else {

		while (!terminate) {

			std::unique_lock<std::mutex> lk(mutex);
			searching = false;
			cond.notify_one();
			cond.wait(lk, [&]{ return searching || terminate; });
			lk.unlock();

			if (searching) {

				resetNodeCount();
				clearHistory();

				// TODO : don't allocate in stack
				ChessSearch searchMain(engine, threads, engine.getEval(),
					engine.getHashTable(), this, engine.getBoardRef(),
					engine.getRepetitions(),
					isMainThread() ? engine.mustCheckNodes() : false);

				// start iterative deepening search
				searchMain.startSearch(moveOrderRoot);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThreadLazy::run()
{
}

} // namespace Cheese

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "engine.h"

#include "moveorder.h"
#include "util/logfile.h"

#ifdef USE_TABLEBASES
#include "syzygy/tbprobe.h"
#endif

#include <cmath>
#include <limits>

namespace Cheese {

// default maximum number of moves in a game
constexpr int max_moves = 256;

// maximum number of moves in an opening book
constexpr int max_book_moves = 20;

// check time every N nodes
constexpr uint32_t check_time_interval = 4096;

constexpr double elo_nodes_param_a = 242.573714;
constexpr double elo_nodes_param_b = 749.87557;

// fix classic chess castling before sending to GUI
// the engine moves king to rook square for castling,
// but GUI wants normal castling moves
constexpr std::array<int, nb_squares> castlefix = {
	c1, b1, c1, d1, e1, f1, g1, g1,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a7, b7, c7, d7, e7, f7, g7, h7,
	c8, b8, c8, d8, e8, f8, g8, g8
};

////////////////////////////////////////////////////////////////////////////////
ChessEngine::ChessEngine()
{
	board.clear();
	board.setChessRules(ChessRules::classic);
	gameMoves.reserve(max_moves);
	undoMoves.reserve(max_moves);
	moveNumberFEN = 0;
	engineSide = white;
	setMultiPV(1);
	searchMode = SearchMode::fixed_time;
	ideepTimeByMove = 20;
	ideepMaxDepth = max_depth - 1;
	ideepNodeCheckTime = check_time_interval;
	ideepNodeCountTime = 0;
	eloNodesLimit = 0;
	eloLimitMode = ELOLimitMode::speed;
	nodeCountSleep = 0;
	reduceSpeedNodes = 0;
	fixedNodesValue = 2000;
	fixedTimeValue = 1000 * 5;
	remainingmoves = 30;
	timeIncrement[black] = 0;
	timeIncrement[white] = 0;
	movesOutOfBook = 0;
	thinkDepth = 0;
	timeGameLength = 0;
	maxThread = 1;
	smpmode = SMPMode::ybwc;
	minSplitDepth = 4 * one_ply;
	timeGame[black] = 0;
	timeGame[white] = 0;
	pondering = false;
	useNullMoves = true;
	useLMR = true;
	runtestsuite = false;
	useOpeningBook = true;
#ifdef USE_LIMIT_STRENGTH
	setStrengthELO(max_strength_elo);
#endif
#ifdef USE_TABLEBASES
	TBProbeDepth = 1;
	TBProbeLimit = 5;
	TB50MoveRule = true;
#endif
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::free()
{
#ifdef USELOG_DEBUG
	LOG_DEBUG << "Engine free";
#endif

	// stop search
	pondering = false;

	if ((threads.size() != 0) &&
		(isThinking())) {
		stopThinking();
	}

	threads.destroy();
	hashTable.release();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::precalcTables()
{
#ifdef GENERATE_ZOBRIST_KEYS
	Zobrist::generate();
#endif

#ifdef GENERATE_BITBASE
	BitBases::generate();
#endif

#ifdef PRECALC_MVVLVA
	ChessMoveOrder::precalcMVVLVA();
#endif

	// precalc bitboards tables
	BitBoards::init();

	// precalc LMR & LMP tables
	ChessSearch::init();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::init(int hashsize)
{
#ifdef USELOG_DEBUG
	LOG_DEBUG << "Engine init : " << hashsize;
#endif

	// set default parameters
	eval.initEvalData(false);

	repetition.clear();

	// init hash table
	hashTable.init(hashsize);

	LOG_INFO << "Pawns hash table size : " << hashtable_pawn_size
			 << " Kb by thread";

	threads.create(*this);

	setStartPosition();
}

#ifdef USE_TUNING
////////////////////////////////////////////////////////////////////////////////
void ChessEngine::initTuning()
{
	Tuning &tuning = eval.getTuning();
	tuning.clear();
	eval.initTuning();
}
#endif

#ifdef USE_LIMIT_STRENGTH
////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setStrengthELO(int e)
{
	setCustomStrength(e);

	eloNodesLimit = 0;
	reduceSpeedNodes = 0;

	int elo = getStrengthELO();
	if (elo < max_strength_elo) {

		if (eloLimitMode == ELOLimitMode::nodes) {

			// limit nodes
			eloNodesLimit = static_cast<uint64_t>(floor(exp((elo +
				elo_nodes_param_b) / elo_nodes_param_a)));
			if (eloNodesLimit < 250) {
				eloNodesLimit = 250;
			}

			LOG_INFO << "set engine strength to " << elo
					 << " ELO (" << eloNodesLimit << " nodes)";
		} else {

			// init reduce speed
			reduceSpeedNodes = static_cast<uint64_t>(floor(exp((elo +
				elo_nodes_param_b) / elo_nodes_param_a)));
			if (reduceSpeedNodes < 50) {
				reduceSpeedNodes = 50;
			}

			LOG_INFO << "set engine strength to " << elo
					 << " ELO (" << reduceSpeedNodes << " node/s)";
		}

	} else {
		LOG_INFO << "set engine strength to maximum";
	}
}
#endif

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setEngineSearchMode(SearchMode type, int value1,
	[[maybe_unused]] int value2)
{
	searchMode = type;
	switch (type) {

		case	SearchMode::fixed_depth:
				thinkDepth = std::clamp<int>(value1, 1, max_depth - 1);
				break;

		case	SearchMode::fixed_nodes:
				fixedNodesValue = std::max(0, value1);
				break;

		case 	SearchMode::fixed_time:
				fixedTimeValue = static_cast<uint64_t>(std::max(0, value1));
				break;

		default:
				break;
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::newGame()
{
	board.clear();
	gameMoves.clear();
	undoMoves.clear();
	repetition.clear();
	movesOutOfBook = 0;
	moveNumberFEN = 0;
	pondering = false;
	timeGameLength = 5 * 60 * 1000;
	searchMode = SearchMode::fixed_time;
	thinkDepth = 8;
	ideepNodeCountTime = 0;
	remainingmoves = 30;
	timeIncrement[white] = 0;
	timeIncrement[black] = 0;
	ideepTimeByMove = timeGameLength / remainingmoves;
	fixedNodesValue = 2000;
	fixedTimeValue = 1000 * 5;

	setStartPosition();
	hashTable.clear();
	resetTime();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::errorMoveFromGUI(const std::string &text)
{
	std::string str = "Illegal move: " + text;
	onSendInfoMessage(str);
	LOG_ERROR << str;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::setMoveSquareList(const std::string &text)
{
	std::size_t p = 0;
	while (p < text.size()) {

		int sx = 0;
		int sy = 0;
		int dx = 0;
		int dy = 0;;
		int promotion = 0;

		// 'O-O' & 'O-O-O'
		if (((p + 2) < text.size()) &&
			(text[p] == 'O') &&
			(text[p + 1] == '-') &&
			(text[p + 2] == 'O')) {

			sx = squareFile(board.squareKingStart(board.side()));
			sy = (board.side() == white) ? rank_1 : rank_8;
			dy = sy;

			if (((p + 4) < text.size()) &&
				(text[p + 3] == '-') &&
				(text[p + 4] == 'O')) {
				dx =  squareFile(board.squareRookLong(board.side()));
				p += 5;
			} else {
				dx =  squareFile(board.squareRookShort(board.side()));
				p += 3;
			}

		} else
		if ((p + 3) < text.size()) {

			sx = text[p++] - 'a';
			sy = text[p++] - '1';
			dx = text[p++] - 'a';
			dy = text[p++] - '1';

			if ((sx < file_a) || (sx > file_h) ||
				(sy < rank_1) || (sy > rank_8) ||
				(dx < file_a) || (dx > file_h) ||
				(dy < rank_1) || (dy > rank_8)) {
				errorMoveFromGUI(text);
				return false;
			}

			// promotion
			if (p < text.size()) {

				char c = text[p++];

				if (c == 'q') {
					promotion = queen;
					++p;
				} else {
					if (c == 'r') {
						promotion = rook;
						++p;
					} else {
						if (c == 'b') {
							promotion = bishop;
							++p;
						} else {
							if (c == 'n') {
								promotion = knight;
								++p;
							} else {
								if (c != ' ') {
									errorMoveFromGUI(text);
									return false;
								}
							}
						}
					}
				}
			}
		}

		// skip other spaces (sdhould not happen)
		while ((p < text.size()) && (text[p] == ' ')) {
			++p;
		}

		int src = makeSquare(sx, sy);
		int dst = makeSquare(dx, dy);
		int mv = board.piece(src);
		int ep = ((mv == pawn) && (board.piece(dst) == no_piece) &&
				  (sx != dx) && board.isEnPassant());
		int cap = board.piece(dst) | ep;

		int cast = 0;
		if (mv == king) {

			// classic rules : change castle move to "king capture rook"
			if (board.getChessRules() == ChessRules::classic) {
				if (board.side() == white) {
					if ((src == board.squareKingStart(white)) && (dst == g1)) {
						dst = board.squareRookShort(white);
					}
					if ((src == board.squareKingStart(white)) && (dst == c1)) {
						dst = board.squareRookLong(white);
					}
				} else {
					if ((src == board.squareKingStart(black)) && (dst == g8)) {
						dst = board.squareRookShort(black);
					}
					if ((src == board.squareKingStart(black)) && (dst == c8)) {
						dst = board.squareRookLong(black);
					}
				}
			}

			if ((src == board.squareKingStart(board.side())) &&
				(board.bitboardPiece(board.side(), rook) &
					BitBoards::square[dst]) &&
				((dst == board.squareRookShort(board.side())) ||
				 (dst == board.squareRookLong(board.side())))) {
				cast = 1;
				cap = 0;
			}
		}

		ChessMove move(src, dst, mv, cap, promotion, cast, ep);

		if (!playMove(move)) {
			errorMoveFromGUI(text);
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::playMove(ChessMove move)
{
	const HashKey key = board.getHashKey();

	if (!board.validMove(move)) {
		return false;
	}

	ChessMoveRec rec;
	board.makeMove(move, rec);
	bool ok = board.isPositionLegal();

	if (ok) {
		repetition.push(key);
		if (!move.isReversible()) {
			repetition.clear();
		}
		gameMoves.emplace_back(move);
		undoMoves.emplace_back(rec);
	} else {
		board.unMakeMove(move, rec);
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::takeBack()
{
	if (getGameMoveCount() > 0) {
		ChessMove &move = gameMoves.back();
		ChessMoveRec &rec = undoMoves.back();

		board.unMakeMove(move, rec);

		gameMoves.pop_back();
		repetition.pop();

		// rebuild repetitions
		repetition.clear();

		for (int n=0; n<getGameMoveCount(); n++) {
			repetition.push(undoMoves[n].hashKey);
			if (!gameMoves[n].isReversible()) {
				repetition.clear();
			}
		}

		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
// calculate mate value in number of full moves
int ChessEngine::calcMateValue(int value)
{
	if (value <= min_score_mate) {
		return (mate_value + value) / 2;
	}
	if (value >= max_score_mate) {
		return (mate_value - value) / 2 + 1;
	}
	return value;
}

////////////////////////////////////////////////////////////////////////////////
// test : extract PV from hash table
void ChessEngine::extractPV(int depth)
{
	std::array<ChessMove, 32> pvmove;
	std::array<ChessMoveRec, max_depth> moveUndo;
	ChessMove cm;
	int pvdepth = depth - 1;
	int nbpv = 0;

	while (pvdepth >= 0) {

		cm.clear();

		if (!board.validMove(cm)) {
			break;
		}

		ChessMoveRec &rec = moveUndo[nbpv];

		board.makeMove(cm, rec);
		if (!board.isPositionLegal()) {
			break;
		}

		pvmove[nbpv++] = cm;

		--pvdepth;
	}

	for (pvdepth = (nbpv - 1); pvdepth>=0; pvdepth--) {
		ChessMoveRec &rec = moveUndo[pvdepth];
		board.unMakeMove(pvmove[pvdepth], rec);
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::initSearch()
{
	ideepMaxDepth = max_depth - 1;
	ideepNodeCheckTime = check_time_interval;
	ideepNodeCountTime = 0;
	ideepTimeByMove = fixedTimeValue;

	int tm = getTime(board.side());

	// init time management and stop conditions
	switch (searchMode) {

		case	SearchMode::tournament:

				if (remainingmoves != 0) {
					ideepTimeByMove = tm / std::max(remainingmoves, 2);
				} else {
					ideepTimeByMove = tm / 30;
				}

				ideepTimeByMove += timeIncrement[board.side()];

				// give more time for the 10 first moves out of book
				// (first move = x2 time)
				if (movesOutOfBook <= 9) {
					double r = 2.0 - movesOutOfBook / 10.0;
					ideepTimeByMove = static_cast<uint64_t>(
						ideepTimeByMove * r);
				}
				break;

		case	SearchMode::fixed_depth:
				ideepMaxDepth = thinkDepth;
				break;

		case	SearchMode::fixed_nodes:
				if (ideepNodeCheckTime > fixedNodesValue) {
					ideepNodeCheckTime = fixedNodesValue;
				}
				break;

		case	SearchMode::fixed_time:
				ideepTimeByMove = fixedTimeValue;
				break;

		case	SearchMode::infinite_search:
				ideepTimeByMove = std::numeric_limits<uint64_t>::max();
				break;

		default:
			ideepTimeByMove = fixedTimeValue;
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::startSearch(bool ponder)
{
	LOG_DEBUG << "startSearch : move " << getGameMoveCount() <<
		" remaining " << remainingmoves;

	threads.waitEndSearch();

	// search start time
	timerSearch.start();

	pondering = ponder;

	threads.resetNodeCount();

	// wake up the main thread to start the search
	threads.getMainThread()->thinking = true;
	threads.getMainThread()->notify();
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::search(ChessMoveOrder &moveOrderRoot)
{
	// engine side
	engineSide = board.side();
	eval.setEngineSide(engineSide);

	// generate legal moves in current position
	bool incheck = board.inCheck();

	int countMovesRoot = ChessMoveGen::generateLegalMoves(board,
			moveOrderRoot.getMoveList(), incheck);
	moveOrderRoot.setMoveCount(countMovesRoot);

	// no legal moves : should never happen
	if (countMovesRoot == 0) {
		onEndSearch(ChessMove(0), ChessMove(0),
			0, 0, 0ULL, 0);
		return false;
	}

	ChessMove bestmove(0);
	ChessMove ponderMove(0);

	// search in opening book
	if ((useOpeningBook) &&
		(!pondering) &&
		(getGameMoveCount() < max_book_moves) &&
		(searchMode != SearchMode::infinite_search)) {

		bestmove = openingBook.findMove(board);

		if (!bestmove.isEmpty()) {
			// test if move is valid
			bool tstbook = false;
			for (int n=0; n<countMovesRoot; n++) {
				if (moveOrderRoot.getMove(n) == bestmove) {
					tstbook = true;
					break;
				}
			}

			if (tstbook) {

#ifdef USELOG_DEBUG
				LOG_DEBUG << "opening book move : " << bestmove.textMove();
#endif
				onEndSearch(bestmove, ponderMove,
					1, 1, 0ULL, 0);
				pondering = false;
				return false;
			} else {
				LOG_WARNING << "invalid book move : " << bestmove.text();
				bestmove.clear();
			}
		}
	}

#ifdef USE_TABLEBASES
	TBCardinality = std::min(TBProbeLimit, TableBases::TBlargest);
	bool isdtz = true;
	if (TBCardinality > 0) {
		int success = 0;
		int nbp = BitBoards::bitCount(board.bitboardAll());
		if ((nbp <= TBCardinality) &&
			(!board.canCastle(castle_init))) {

			int count = 0;
			int score = 0;

			success = TableBases::root_probe(board, score,
				moveOrderRoot.getMoveList(), moveOrderRoot.getMoveCount(),
				repetition, &count);

			if (!success) {
				isdtz = false;
				success = TableBases::root_probe_wdl(board, score,
					moveOrderRoot.getMoveList(), moveOrderRoot.getMoveCount(),
					&count);
			}
		}

		if (success) {

			threads.getMainThread()->tbhits += moveOrderRoot.getMoveCount();

			moveOrderRoot.getMoveList().sort(0, countMovesRoot);

			if (isdtz) {
				TBCardinality = 0;
			}
		}
	}
#endif

	if (!pondering) {
		++movesOutOfBook;
	}

	// init time management and stop conditions
	initSearch();

	// update main hashtable age
	hashTable.initSearch();

	threads.initSearch();

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::reduceSpeed()
{
	if (threads.mustStop()) {
		return;
	}

	++nodeCountSleep;
	if (nodeCountSleep >= 32) {

		nodeCountSleep = 0;
		uint64_t elapsed = getSearchElapsedTime();
		uint64_t sp = (getSearchNodeCount() * 1000) / (elapsed + 1);

		if (sp > getReduceSpeedNodes()) {

			// * sleep if we have enough time
			if ((elapsed + (20 + 16)) < ideepTimeByMove) {
				// wait 20 ms
				Timer::sleep(20);
			}

			// force to check time
			ideepNodeCountTime = ideepNodeCheckTime;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::checkSearchTime()
{
	if (threads.mustStop()) {
		return false;
	}

	++ideepNodeCountTime;
	if (ideepNodeCountTime < ideepNodeCheckTime) {
		return true;
	}

	ideepNodeCountTime = 0;

	// in pondering : don't stop searching
	if ((!pondering) &&
		(searchMode != SearchMode::fixed_depth)) {

		if (searchMode != SearchMode::fixed_nodes) {
			// compare to time by move
			uint64_t diff = timerSearch.getElapsedTime();
			if (diff >= ideepTimeByMove) {
				threads.stopSearch();
				return false;
			}
		} else {
			uint64_t nodes = threads.getNodeCount();
			if (nodes >= fixedNodesValue) {
				if ((nodes + ideepNodeCheckTime) > fixedNodesValue) {
					ideepNodeCheckTime = fixedNodesValue - nodes;
				}
				threads.stopSearch();
				return false;
			}
		}
	}

	return true;
}

#ifdef USE_PERSONALITY
////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setPersonality(const std::string &fname)
{
	if (eval.loadPersonality(fname)) {
		//...
	}
}
#endif

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::stopThinking()
{
	threads.stopSearch();

	// force to check time
	ideepNodeCountTime = ideepNodeCheckTime;

	threads.waitEndSearch();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::ponderHit()
{
	pondering = false;
	ideepNodeCountTime = ideepNodeCheckTime;
	checkSearchTime();
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::checkNodes()
{
	uint64_t nodes = threads.getNodeCount();
	uint64_t maxnodes = (eloNodesLimit != 0) ? eloNodesLimit : fixedNodesValue;
	if (nodes >= maxnodes) {
		threads.stopSearch();
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::searchingRootMove(ChessMove &mv, int num)
{
	onSearchRootMove(mv, num);
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setStartPosition()
{
	gameMoves.clear();
	undoMoves.clear();
	repetition.clear();
	board.setStartPosition();
}

////////////////////////////////////////////////////////////////////////////////
// set FEN (Forsythe-Edwards Notation)
bool ChessEngine::setFENPosition(const std::string &str)
{
	gameMoves.clear();
	undoMoves.clear();
	repetition.clear();

	ChessFENParser fen(board);
	return fen.set(str);
}

////////////////////////////////////////////////////////////////////////////////
// get FEN (Forsythe-Edwards Notation)
std::string ChessEngine::getFENPosition(ChessBoard &pos)
{
	ChessFENParser fen(pos);
	return fen.get();
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessEngine::getTextMove(const ChessMove &move)
{
	int src = move.src();
	int dst = move.dst();

	if ((move.isCastle()) &&
		(board.getChessRules() == ChessRules::classic)) {
		dst = castlefix[dst];
	}

	std::string str = ChessMove::strSquare(src) + ChessMove::strSquare(dst);

	int pp = move.promotion();
	if (pp != no_piece) {
		str += pieces_name[pp];
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessEngine::getTextPV(const ChessMove *mlist, int count)
{
	std::string str;
	for (int n=0; n<count; n++) {

		str += getTextMove(mlist[n]);

		if (n < (count - 1)) {
			str += ' ';
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
// set parameter, or call evaluation tuning function
#ifdef USE_TUNING
void ChessEngine::setTuneValue(const std::string &name, const std::string &v)
{
	Tuning &tuning = eval.getTuning();
	tuning.set(name, std::stoi(v));
	eval.initEval();
	ChessSearch::init();
}
#endif

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::loadOpeningBook(const std::string &filebook)
{
	useOpeningBook = openingBook.open(filebook);
	return useOpeningBook;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::unloadOpeningBook()
{
    useOpeningBook = false;
	openingBook.close();
}

////////////////////////////////////////////////////////////////////////////////
int ChessEngine::countLegalMoves(ChessBoard &pos)
{
	return ChessMoveGen::countLegalMoves(pos);
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setSMPMode(SMPMode m)
{
	smpmode = m;
	switch (smpmode) {
		case SMPMode::lazy:
			LOG_INFO << "using Lazy SMP";
			break;
		case SMPMode::ybwc:
			LOG_INFO << "using YBWC";
			break;
		default:
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::resetTime()
{
	timeGame[white] = timeGameLength;
	timeGame[black] = timeGameLength;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::showBookMoves()
{
	if (useOpeningBook) {
		openingBook.showMoves(board);
	} else {
		std::cout << "book not loaded" << std::endl;
	}
}

} // namespace Cheese

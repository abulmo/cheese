////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_SEARCH_H_
#define CHEESE_SEARCH_H_

#include "config.h"

#include "board.h"
#include "eval.h"
#include "history.h"
#include "pv.h"
#include "repetitions.h"
#include "searchthread.h"

#include <array>

// define to use null moves pruning
#define USE_NULLMOVES

// null move verification
//#define USE_NULLMOVES_VERIFICATION

// use Razoring
#define USE_RAZORING

// reverse futility prunning, or static null moves pruning
#define USE_REVERSE_FUTILITY

// Late move pruning (LMP)
#define USE_LATE_MOVE_PRUNING

// "delta pruning" = futility pruning in quiescence search
//#define USE_DELTA_PRUNING

// use checks in quiescence for 1 ply
#define USE_QUIESCENCE_CHECKS

// use late move reduction (LMR)
#define USE_LATE_MOVE_REDUCTION

// use Internal Iterative Deepening
#define USE_INTERNAL_ITERATIVE_DEEPENING

// use Futility Pruning
#define USE_FUTILITY_PRUNING

// extend search when in check
#define USE_EXTENSION_CHECK

// extend search when a pawn is on 7th rank
#define USE_EXTENSION_PAWNON7TH

// used only in root
#define USE_EXTENSION_SINGLERESPONSE

// test if PV moves are valid
//#define USE_TEST_PV

namespace Cheese {

// search modes
enum class SearchMode {

	// search to a fixed depth
	fixed_depth,

	// search a fixed number of nodes
	fixed_nodes,

	// search for a fixed time by move
	fixed_time,

	// tournament time or time + increment
	tournament,

	// infinite search
	infinite_search
};

// maximum depth the engine can search
constexpr int max_depth = 128;

// fractional ply resolution
// note : fractional ply is not used, now all extensions are 1 ply
constexpr int one_ply = 2;

// depth for checks in quiescence search
// -1 => 1 ply , -2 => 2 ply ...
constexpr int depth_checks_quiescence = -1;

constexpr int extend_check = one_ply * 1;

constexpr int extend_pawnon7th = one_ply * 1;

// Late Move Reduction : minimum depth
constexpr int lmr_min_depth = 3;

constexpr int lmr_max_depth = 64;

// Late Move Reduction : minimum move count
constexpr int lmr_min_moves = 3;

constexpr int lmr_max_moves = 64;

constexpr int lmp_max_depth = 16;

class ChessEngine;
class ChessMoveOrder;
class ChessSearchThread;
class ChessSearchThreadList;

////////////////////////////////////////////////////////////////////////////////
// search functions
class ChessSearch {

	friend class ChessSearchYBWC;

private:

	// pointer to chess engine
	ChessEngine &engine;

	// reference to thread list
	ChessSearchThreadList &threads;

	// reference to engine evaluation
	ChessEval &eval;

	// reference to engine main hashtable
	ChessHashTable &hashTable;

	// thread associated with this search
	ChessSearchThread *thread;

	// chess board current position in search
	ChessBoard board;

	// repetition table
	ChessRepetitions repetition;

	// searched moves stack
	ChessSearchStackArray searchStack;

	// principal variation
	ChessPV pv;

	// if we need to test nodes in search move loop
	// (only in main thread)
	bool needCheckNodes;

#ifdef USE_LATE_MOVE_REDUCTION
	// lmr reduction [depth][movecount]
	static std::array<std::array<uint32_t, lmr_max_depth>, lmr_max_moves>
		lmr_reduction;
#endif

#ifdef USE_LATE_MOVE_PRUNING
	// lmp limit [depth]
	static std::array<int, lmp_max_depth> lmp_count;
#endif

public:

	ChessSearch(ChessEngine &e, ChessSearchThreadList &ths,	ChessEval &ev,
		ChessHashTable &ht, ChessSearchThread *th, const ChessBoard &pos,
		const ChessRepetitions &rep, bool chknodes);

	void clearPV(int ply);

	ChessPV &getPV();

	// note : only used by move order at root when using quiescence
	ChessRepetitions &getRepetitions();

	// init search (precalc tables)
	static void init();

	// evaluate current position
	int evaluate(int alpha, int beta);

	// start iterative deepening search (main thread)
	void startSearch(ChessMoveOrder &mo);

	// alpha-beta search at root
	int searchRoot(int alpha, int beta, int depth, ChessMoveOrder &moveorder,
		bool sendRootMove);

	// alpha-beta search
	int search(int alpha, int beta, int depth, int ply, bool nodepv,
		bool incheck);

	// alpha-beta quiescence search
	int searchQuiescent(int alpha, int beta, int depth, int ply, bool nodepv,
		bool incheck);

#ifdef USE_TEST_PV
	void testPV(ChessMove *pv, int sz);
#endif
};

////////////////////////////////////////////////////////////////////////////////
inline void ChessSearch::clearPV(int ply)
{
	pv.clear(ply);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessSearch::evaluate(int alpha, int beta)
{
	return eval.evaluate(board, thread->hashTablePawn, alpha, beta);
}

////////////////////////////////////////////////////////////////////////////////
inline ChessPV &ChessSearch::getPV()
{
	return pv;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessRepetitions &ChessSearch::getRepetitions()
{
	return repetition;
}

} // namespace Cheese

#endif // CHEESE_SEARCH_H_

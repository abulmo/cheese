////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_HISTORY_H_
#define CHEESE_HISTORY_H_

#include "config.h"
#include "move.h"

#include <array>

// use counter moves
#define USE_COUNTER_MOVE

namespace Cheese {

// an entry in history heuristic table
using HistoryEntry = uint16_t;

// maximum value in history heuristic table
constexpr HistoryEntry max_history_value = 64000;

// maximum quiet moves used when reducing history
constexpr int max_history_quiets = 64;

// maximum depth the engine can search
constexpr int max_killer_depth = 128;

////////////////////////////////////////////////////////////////////////////////
// killer moves
struct ChessMoveKiller {

	ChessMove killer1;

	ChessMove killer2;
};

////////////////////////////////////////////////////////////////////////////////
// history heuristic, killer moves, and counter moves
class ChessHistory {

private:

	// history heuristic [side][piece][dst]
	std::array<std::array<std::array<HistoryEntry, nb_squares>, nb_pieces_type>,
		nb_sides> history;

#ifdef USE_COUNTER_MOVE
	// counter moves [side][src][dst]
	std::array<std::array<std::array<ChessMove, nb_squares>, nb_squares>,
		nb_sides> counterMoves;
#endif

	// killer moves [ply]
	std::array<ChessMoveKiller, max_killer_depth> killers;

public:

	ChessHistory();

	void clear();

#ifdef USE_COUNTER_MOVE
	void setHistoryMove(Side side, ChessMove lastmove, ChessMove move);

	ChessMove getHistoryMove(Side side, ChessMove lastmove) const;
#endif

	// add a move to history table
	void add(ChessMove move, Side side,  int depth);

	// * test : sub history for some move ?...
	void sub(ChessMove move, Side side,  int depth);

	// get history heuristic value
	HistoryEntry get(ChessMove move, Side side) const;

	// clear all killers
	void clearKillers();

	// add a move to killer table
	void addKillerMove(ChessMove move, int ply);

	ChessMove getKiller1(int ply) const;

	ChessMove getKiller2(int ply) const;

	void copyKillers(ChessHistory &hist, int ply, int count);
};

#ifdef USE_COUNTER_MOVE
////////////////////////////////////////////////////////////////////////////////
inline void ChessHistory::setHistoryMove(Side side, ChessMove lastmove,
	ChessMove move)
{
	int src = lastmove.src();
	int dst = lastmove.dst();

	counterMoves[side][src][dst] = move;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessHistory::getHistoryMove(Side side,
	ChessMove lastmove) const
{
	int src = lastmove.src();
	int dst = lastmove.dst();

	return counterMoves[side][src][dst];
}
#endif

////////////////////////////////////////////////////////////////////////////////
inline void ChessHistory::add(ChessMove move, Side side,  int depth)
{
	int pc = move.piece();
	int dst = move.dst();

	auto v = static_cast<HistoryEntry>(depth * depth);
	if ((history[side][pc][dst] + v) < max_history_value) {
		history[side][pc][dst] += v;
	}
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHistory::sub(ChessMove move, Side side,  int depth)
{
	int pc = move.piece();
	int dst = move.dst();

	auto v = static_cast<HistoryEntry>(depth * depth);
	if ((history[side][pc][dst] - v) >= 0) {
		history[side][pc][dst] -= v;
	}
}

////////////////////////////////////////////////////////////////////////////////
inline HistoryEntry ChessHistory::get(ChessMove move, Side side) const
{
	int pc = move.piece();
	int dst = move.dst();

	return history[side][pc][dst];
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHistory::addKillerMove(ChessMove move, int ply)
{
	if (killers[ply].killer1 != move) {
		killers[ply].killer2 = killers[ply].killer1;
		killers[ply].killer1 = move;
	}
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessHistory::getKiller1(int ply) const
{
	return killers[ply].killer1;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessHistory::getKiller2(int ply) const
{
	return killers[ply].killer2;
}

} // namespace Cheese

#endif //CHEESE_HISTORY_H_

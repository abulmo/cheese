////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "logfile.h"

#include <chrono>
#include <ctime>
#include <iomanip>

namespace Cheese {

#ifdef USELOG

// global logfile
LogFile gLog;

///////////////////////////////////////////////////////////////////////////////
LogFile::LogFile()
{
	levels.emplace_back(LogLevel("INFO",	true));
	levels.emplace_back(LogLevel("WARNING",	true));
	levels.emplace_back(LogLevel("ERROR", 	true));
	levels.emplace_back(LogLevel("DEBUG",	true));
}

///////////////////////////////////////////////////////////////////////////////
LogFile::~LogFile()
{
    close();
}

///////////////////////////////////////////////////////////////////////////////
bool LogFile::open(const std::string &name)
{
	ofs.open(name, std::ofstream::out);
    uselog = ofs.is_open();
	return uselog;
}

///////////////////////////////////////////////////////////////////////////////
void LogFile::close()
{
	if (ofs.is_open()) {
		ofs << std::endl;
		ofs.close();
	}
	uselog = false;
}

///////////////////////////////////////////////////////////////////////////////
LogLevelId LogFile::addCustomLevel(const std::string &name)
{
	levels.emplace_back(LogLevel(name, true));
	return static_cast<LogLevelId>(levels.size() - 1);
}

///////////////////////////////////////////////////////////////////////////////
void LogFile::write(LogLevelId level, const std::string &str)
{
	std::lock_guard<std::mutex> lk(mutex);

	auto now = std::chrono::system_clock::now();
	auto te = now.time_since_epoch();
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(te -
			  std::chrono::duration_cast<std::chrono::seconds>(te));

	auto tc = std::chrono::system_clock::to_time_t(now);

	std::tm ltm{};
	#ifdef WIN32
	localtime_s(&ltm, &tc);
	#else
	localtime_r(&tc, &ltm);
	#endif

	ofs << std::put_time(&ltm, "%Y/%m/%d %H:%M:%S")
		<< '.' << std::setfill('0') << std::setw(3) << ms.count()
		<< " ["
        << levels[static_cast<int>(level)].name
        << "] "
        << str
        << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
LogStream::LogStream(LogFile &log, LogLevelId lev) : logger(log), level(lev)
{
}

////////////////////////////////////////////////////////////////////////////////
LogStream::~LogStream()
{
	// write to log before deleting stream
	logger.write(level, sstr.str());
}

} // namespace Cheese

#endif // USELOG

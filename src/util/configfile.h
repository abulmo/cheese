////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_CONFIGFILE_H_
#define CHEESE_CONFIGFILE_H_

#include <string>
#include <vector>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// configuration file
// format :
//			# comment
//			; another comment
//			key = value
class ConfigFile {

private:

	// note: why use a map when a vector do the job
	std::vector<std::pair<std::string, std::string>> config;

	auto trimString(const std::string &str) const;

public:

	ConfigFile() = default;

	void clear();

	auto find(const std::string &key);

	bool load(const std::string &filename);

	bool save(const std::string &filename);

	std::string get(const std::string &key,
		const std::string &def = std::string());

	int getInt(const std::string &key, int def=0);

	double getDouble(const std::string &key, double def=0.0);

	void set(const std::string &key, const std::string &value);

	void debug();

};

} // namespace Cheese

#endif // CHEESE_CONFIGFILE_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_LOG_H_
#define CHEESE_LOG_H_

#include "config.h"

#include <fstream>
#include <iostream>
#include <mutex>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace Cheese {

// enable/disable logging (at compile time)
// * cf config.h
//#define USELOG

// enable/disable debug logging level (at compile time)
// * cf config.h
//#define USELOG_DEBUG

////////////////////////////////////////////////////////////////////////////////
#ifdef USELOG

#define LOG(level) if (!gLog.isLevelEnable(level)) {} \
        else LogStream(gLog, level).stream()

#define LOG_IF(cond, level) if ((!gLog.isLevelEnable(level)) || \
        (!(cond))) {} else LogStream(gLog, level).stream()

#define LOG_INFO if (!gLog.isLevelEnable(LogLevelId::level_info)) {} \
        else LogStream(gLog, LogLevelId::level_info).stream()

#define LOG_WARNING if (!gLog.isLevelEnable(LogLevelId::level_warning)) \
        {} else LogStream(gLog, LogLevelId::level_warning).stream()

#define LOG_ERROR if (!gLog.isLevelEnable(LogLevelId::level_error)) {} \
        else LogStream(gLog, LogLevelId::level_error).stream()

#ifdef USELOG_DEBUG
#define LOG_DEBUG if (!gLog.isLevelEnable(LogLevelId::level_debug)) {} \
        else LogStream(gLog, LogLevelId::level_debug).stream()
#else
#define LOG_DEBUG if (true) {} else std::cout
#endif

#define LOG_WRITE(level, str) if (!gLog.isLevelEnable(level)) {} \
        else gLog.write(level, str)

#else
#define LOG(level) if (true) {} else std::cout
#define LOG_IF(cond, level) if (true) {} else std::cout
#define LOG_INFO if (true) {} else std::cout
#define LOG_WARNING if (true) {} else std::cout
#define LOG_ERROR if (true) {} else std::cout
#define LOG_DEBUG if (true) {} else std::cout
#define LOG_WRITE(level, str) if (true) {} else std::cout
#endif

#define LOG_CODE_POSITION "line " << __LINE__     \
                                  << " : "        \
                                  << __FUNCTION__ \
                                  << "() : "      \
                                  << __FILE__

////////////////////////////////////////////////////////////////////////////////
enum class LogLevelId {

	level_info,
	level_warning,
	level_error,
    level_debug,

	// start of custom levels
	level_custom
};

#ifdef USELOG
////////////////////////////////////////////////////////////////////////////////
struct LogLevel {

	// level name
	std::string name;

	// enable for output
	bool enable {false};

	LogLevel() = default;

	LogLevel(std::string n, bool e) : name(std::move(n)), enable(e)
	{ }
};

class LogStream;

////////////////////////////////////////////////////////////////////////////////
class LogFile {

private:

    std::ofstream ofs;

    std::vector<LogLevel> levels;

    bool uselog {false};

	std::mutex mutex;

public:

    LogFile();

    ~LogFile();

	auto &getStream();

    void write(LogLevelId level, const std::string &str);

    void enable(bool e);

	bool isEnable() const;

    bool isLevelEnable(LogLevelId level) const;

	void enableLevel(LogLevelId level, bool b);

    // open log file
    bool open(const std::string &name);

    // close logfile
    void close();

    LogLevelId addCustomLevel(const std::string &name);

};

////////////////////////////////////////////////////////////////////////////////
// Temporary stream for LogFile
// to be able to catch the end of the stream when using multiple <<
class LogStream {

private:

    std::ostringstream sstr;

    LogFile &logger;

    LogLevelId level;

public:

    explicit LogStream(LogFile &log, LogLevelId lev = LogLevelId::level_info);

    ~LogStream();

    auto &stream();
};

////////////////////////////////////////////////////////////////////////////////
inline auto &LogFile::getStream()
{
	return ofs;
}

////////////////////////////////////////////////////////////////////////////////
inline void LogFile::enable(bool e)
{
	uselog = e;
}

////////////////////////////////////////////////////////////////////////////////
inline bool LogFile::isEnable() const
{
	return uselog;
}

////////////////////////////////////////////////////////////////////////////////
inline bool LogFile::isLevelEnable(LogLevelId level) const
{
	return uselog &&
		   levels[static_cast<int>(level)].enable;
}

////////////////////////////////////////////////////////////////////////////////
inline void LogFile::enableLevel(LogLevelId level, bool b)
{
	levels[static_cast<int>(level)].enable = b;
}

////////////////////////////////////////////////////////////////////////////////
inline auto &LogStream::stream()
{
	return sstr;
}

extern LogFile gLog;

#endif // USELOG

} // namespace Cheese

#endif // CHEESE_LOG_H_

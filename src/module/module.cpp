////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "module.h"

#include "util/logfile.h"
#include "move.h"
#include "search.h"
#include "thread.h"
#include "tools.h"

#ifdef USE_TABLEBASES
#include "syzygy/tbprobe.h"
#endif

#include <fcntl.h>
#include <iomanip>
#include <iostream>

namespace Cheese {

// custom log level for GUI input/output
constexpr auto level_gui = LogLevelId::level_custom;

const std::string ChessModule::name_author = "Patrice Duhamel";

const std::string ChessModule::name_program = "Cheese";

#ifdef SYSTEM64BIT
const std::string ChessModule::name_archi = "64 bits";
#else
const std::string ChessModule::name_archi = "32 bits";
#endif

const std::string nameFileConfig = "cheese.ini";

const std::string defaultOpeningBook = "cheesebook-30.bin";

const std::string logfilename = "logfile.txt";

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::parseDefault(std::istringstream &stream)
{
	std::string token;
	if (!std::getline(stream, token, ' ')) {
		return true;
	}

	if (token == "uci") {
		setProtocol(GuiProtocol::uci);
		engine.registerListener(this);
		return true;
	} else
	if (token == "xboard") {
		setProtocol(GuiProtocol::cecp);
		engine.registerListener(this);
		return true;
	} else
	if (token == "setvalue") {
		#ifdef USE_TUNING
		if (std::getline(stream, token, ' ')) {
			std::string name = token;
			if (std::getline(stream, token, ' ')) {
				engine.setTuneValue(name, token);
			}
		}
		#endif
		return true;
	} else
	if (token == "quit") {
		return false;
	} else {
		LOG_ERROR << "unknown protocol '" << token << "'";
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////
ChessModule::ChessModule() : protocol(nullptr), guiMode(GuiProtocol::none),
	remainingMoves(0), maxGameMoves(0), quit(false), ponderingActive(false),
	pondernextmove(false)
{
}

////////////////////////////////////////////////////////////////////////////////
// parse command line
bool ChessModule::parseArguments(const std::vector<std::string> &args,
	bool &err)
{
	err = false;
	if (!args.empty()) {

		if (args[0] == "-help") {

			std::cout << name_program << " "
					  << PROJECT_VER << " by "
					  << name_author << "\n\n"
					  << name_program
					  << " -testsuite HASH_SIZE NB_THREADS {depth|time} VALUE"
				      << " FILENAME.epd" << '\n'
					  << name_program
					  << " -perft DEPTH [-fen \"FEN_STRING\"]" << '\n'
					  << name_program
					  << " -perftd DEPTH [-fen \"FEN_STRING\"]" << '\n'
					  << name_program
					  << " -divide DEPTH [-fen \"FEN_STRING\"]" << '\n'
					  << name_program
					  << " -benchmark HASH_SIZE NB_THREADS {depth|time} VALUE"
					  << " [FILENAME.epd]" << '\n'
#ifdef USE_TABLEBASES
					  << name_program
					  << " -probetb \"FEN_STRING\"" << '\n'
#endif
					  << name_program
					  << " -probebook \"FEN_STRING\""
					  << std::endl;

			return true;

		} else
		if (args[0] == "-testsuite") {

			int h = 256;
			int d = 12;
			int c = 1;
			int m = 0;
			if (args.size() > 1) {
				h = std::stoi(args[1]);
				if (args.size() > 2) {
					c = std::stoi(args[2]);
					if (args.size() > 3) {
						if (args[3] == "depth") {
							m = 0;
						} else
						if (args[3] == "time") {
							m = 1;
						}
						if (args.size() > 4) {
							d = std::stoi(args[4]);
						}
					}
				}
			}
			err = cmdRunTestSuite(h, c, m, d, args[5]);
			return true;

        } else
		if (args[0] == "-perftd") {
			if (args.size() > 1) {
				if (args.size() == 2) {
					err = cmdPerftd(std::stoi(args[1]), "");
					return true;
                } else
				if (args.size() == 4) {
					if (args[2] == "-fen") {
						err = cmdPerftd(std::stoi(args[1]), args[3]);
						return true;
                    }
                }
            }
        } else // perft
		if (args[0] == "-perft") {
			if (args.size() > 1) {
				if (args.size() == 2) {
					err = cmdPerft(std::stoi(args[1]), "");
					return true;
                } else
				if (args.size() == 4) {
					if (args[2] == "-fen") {
						err = cmdPerft(std::stoi(args[1]), args[3]);
						return true;
                    }
                }
            }

        } else
		if (args[0] == "-divide") {
			if (args.size() > 1) {
				if (args.size() == 2) {
					err = cmdDivide(std::stoi(args[1]), "");
					return true;
                } else
				if (args.size() == 4) {
					if (args[2] == "-fen") {
						err = cmdDivide(std::stoi(args[1]), args[3]);
						return true;
                    }
                }
            }
        } else
#ifdef USE_TABLEBASES
		if (args[0] == "-probetb") {
			if (args.size() > 1) {
				err = cmdProbeTB(args[1]);
				return true;
			}
		} else
#endif
		if (args[0] == "-probebook") {
			if (args.size() > 1) {
				err = cmdProbeBook(args[1]);
				return true;
			}
		} else
		if (args[0] == "-benchmark") {
			int h = 256;
			int d = 12;
			int	c = 1;
			int m = 0;
			if (args.size() > 1) {
				h = std::stoi(args[1]);
				if (args.size() > 2) {
					c = std::stoi(args[2]);
					if (args.size() > 3) {
						if (args[3] == "depth") {
							m = 0;
						} else
						if (args[3] == "time") {
							m = 1;
						}
						if (args.size() > 4) {
							d = std::stoi(args[4]);
						}
					}
				}
			}
			if (args.size() > 5) {
				err = cmdBenchmark(h, c, m, d, args[5]);
			} else {
				err = cmdBenchmark(h, c, m, d, "");
			}
			return true;
		} else {
			LOG_WARNING << "Unknown command line parameter '"
					    << args[0] << "'";
		}
    }

	return false;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::loadConfig()
{
	// default values
	cfg.hashSize = hashtable_size_default;
	cfg.useBook = 0;
	cfg.bookFile.assign(defaultOpeningBook);
	cfg.useStrength = 0;
	cfg.elo = max_strength_elo;
	cfg.personalityFile.assign("");
	cfg.threads = 1;
	cfg.minSplitDepth = 4;
	cfg.usePersonality = 0;
	cfg.logLevel = 7; // default = info + warning + error
	cfg.nullMoves = 1;
	cfg.lmr = 1;
	cfg.drawScore = 0;
	cfg.uciLimitMode = 0;
	cfg.usePersonalityOptions = 0;
	cfg.tb50MoveRule = 1;
	cfg.tbProbeLimit = 1;
	cfg.tbProbeDepth = 6;
	cfg.smpMode = "ybwc";

	ConfigFile conf;
	if (!conf.load(nameFileConfig)) {
		return false;
	}

	cfg.hashSize = conf.getInt("Hash");
	cfg.useBook = conf.getInt("Book", 0);
	cfg.bookFile = conf.get("BookFile", defaultOpeningBook);
	cfg.useStrength = conf.getInt("UseStrength", 0);

	if (cfg.useStrength) {
		cfg.elo = conf.getInt("StrengthELO", max_strength_elo);
	} else {
		cfg.elo = max_strength_elo;
	}

	cfg.uciLimitMode = conf.getInt("UCILimitMode", 0);
	cfg.threads = conf.getInt("MaxThreads", 1);

	cfg.minSplitDepth = conf.getInt("MinSplitDepth", 4);
	if (cfg.minSplitDepth < 1) {
		cfg.minSplitDepth = 1;
	}

	cfg.pathTableBase = conf.get("SyzygyPath", "<empty>");
	cfg.tb50MoveRule = conf.getInt("Syzygy50MoveRule", 1);
	cfg.tbProbeLimit = conf.getInt("SyzygyProbeDepth", 1);
	cfg.tbProbeDepth = conf.getInt("SyzygyProbeLimit", 6);

#ifdef USE_PERSONALITY
	cfg.usePersonality = conf.getInt("UsePersonality", 0);
	cfg.personalityFile = conf.get("PersonalityFile", "");
	cfg.usePerso = (cfg.usePersonality == 1);

	if (cfg.usePersonality) {
		cfg.elo = max_strength_elo;
	}
#endif

	cfg.smpMode = conf.get("SMPMode", "ybwc");
	cfg.logLevel = conf.getInt("LogLevel", 0);
	cfg.nullMoves = conf.getInt("UseNullMovePruning", 1);
	cfg.lmr = conf.getInt("UseLMR", 1);
	cfg.drawScore = conf.getInt("DrawScore", 0);
	cfg.usePersonalityOptions = conf.getInt("UsePersonalityOptions", 0);

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::init(const std::vector<std::string> &args)
{
	// read config file
	bool cfgFound = loadConfig();

#ifdef USELOG
	gLog.addCustomLevel("GUI");
	gLog.enable((cfg.logLevel != 0));
	if (cfg.logLevel != 0) {
		gLog.open(logfilename);
	}
#endif

	if (!cfgFound) {
		LOG_WARNING << "Config file " << nameFileConfig
					<< "not found, using default parameters";
	}

	// logfile levels
	//   0 = disabled
	//  +1 = info
	//  +2 = warning
	//  +4 = error
	//  +8 = debug
	// +16 = gui
	//  31 = all
#ifdef USELOG
	if ((cfg.logLevel & 1) == 0) {
		gLog.enableLevel(LogLevelId::level_info, false);
	}
	if ((cfg.logLevel & 2) == 0) {
		gLog.enableLevel(LogLevelId::level_warning, false);
	}
	if ((cfg.logLevel & 4) == 0) {
		gLog.enableLevel(LogLevelId::level_error, false);
	}
	if ((cfg.logLevel & 8) == 0) {
		gLog.enableLevel(LogLevelId::level_debug, false);
	}
	if ((cfg.logLevel & 16) == 0) {
		gLog.enableLevel(static_cast<LogLevelId>(level_gui), false);
	}
#endif

	// program name + version + architecture
	LOG_INFO << name_program << " " << PROJECT_VER
#ifdef _DEBUG
							<< " debug"
#endif
#ifdef USE_POPCOUNT
							<< " " << name_archi << " popcount";
#elif USE_PEXT
							<< " " << name_archi << " pext";
#else
							<< " " << name_archi;
#endif
	LOG_INFO << "Compiled on " __DATE__ " at " __TIME__;

	// compiler
	LOG_INFO << "Compiled by "
#ifdef _MSC_VER
		"MSVC " << _MSC_FULL_VER << '.' << _MSC_BUILD;
#elif __clang__
		"Clang " << __clang_major__ << '.' << __clang_minor__ << '.'
				 << __clang_patchlevel__;
#elif __GNUC__
#if defined(__MINGW64__) || defined(__MINGW32__)
		"MINGW " <<
#endif
		"GCC " << __GNUC__ << '.' << __GNUC_MINOR__ << '.'
			   << __GNUC_PATCHLEVEL__;
#else
		"unknonwn";
#endif

	quit = false;

	// precalc some tables for bitboards, search
	ChessEngine::precalcTables();

	if (cfg.smpMode == "ybwc") {
		engine.setSMPMode(SMPMode::ybwc);
	} else
	if (cfg.smpMode == "lazy") {
		engine.setSMPMode(SMPMode::lazy);
	} else {
		cfg.smpMode = "ybwc";
		engine.setSMPMode(SMPMode::ybwc);
	}

	engine.setELOLimitMode((cfg.uciLimitMode == 0) ? ELOLimitMode::speed :
		ELOLimitMode::nodes);

	// parse command line
	bool err = false;
	if (parseArguments(args, err)) {
		// quit after command line tools
		quit = true;
		return err;
	}

	sendCommand(name_program + ' ' + std::string(PROJECT_VER) +
		" by " + name_author);

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessModule::free()
{
	// stop and release engine
	ponderingActive = false;

	if (engine.getThreads().size() != 0) {
		if (engine.isThinking()) {
			engine.stopThinking();
		}
	}

	engine.unregisterListener(this);
	engine.free();

#ifdef USELOG
	LOG_INFO << "End.";
	gLog.close();
#endif
}

////////////////////////////////////////////////////////////////////////////////
void ChessModule::setProtocol(GuiProtocol p)
{
	guiMode = p;

	switch (p) {
		case	GuiProtocol::uci:
				LOG_INFO << "using protocol UCI";
				protocol = std::make_unique<ChessProtocolUCI>(engine, *this);
				protocol->init();
				break;

		case	GuiProtocol::cecp:
				LOG_INFO << "using protocol CECP";
				protocol = std::make_unique<ChessProtocolCECP>(engine, *this);
				protocol->init();
				break;

		default:
				protocol = nullptr;
				break;
	}
}

////////////////////////////////////////////////////////////////////////////////
// main loop
void ChessModule::run()
{
	while (!quit) {
		if (parse()) {
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::parse()
{
	std::string cmd;
	std::getline(std::cin, cmd);

	LOG(static_cast<LogLevelId>(level_gui)) << "<< " << cmd;

	if (!cmd.empty()) {
		std::istringstream stream(cmd);
		if (guiMode != GuiProtocol::none) {
			if (!protocol->parse(stream)) {
				quit = true;
			}
		} else {
			if (!parseDefault(stream)) {
				quit = true;
			}
		}
		return true;
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////
void ChessModule::sendCommand(const std::string &str)
{
	std::cout << str << std::endl;

	LOG(static_cast<LogLevelId>(level_gui)) << ">> " << str;
}

////////////////////////////////////////////////////////////////////////////////
void ChessModule::startSearch(bool ponder)
{
	engine.startSearch(ponder);
}

////////////////////////////////////////////////////////////////////////////////
void ChessModule::doPerftd(int depth)
{
	ChessTools tools(engine, engine.getBoardRef());

	Timer timer;
	timer.start();

	tools.countnode = 0;
	tools.countcapture = 0;
	tools.countep = 0;
	tools.countpromotion = 0;
	tools.countcastle = 0;
	tools.countcheck = 0;
	tools.countmate = 0;

	tools.perftd(depth);

	uint64_t timesearch = timer.getElapsedTime();

	std::string str = Timer::timeToString(timesearch);

	uint64_t nps = (tools.countnode * 1000) / (timesearch + 1);

	sendCommand("Depth " + std::to_string(depth));
	sendCommand("Nodes           = " +
		ChessTools::formatNumber(tools.countnode));
	sendCommand("Nodes captures  = " +
		ChessTools::formatNumber(tools.countcapture));
	sendCommand("Nodes e.p       = " +
		ChessTools::formatNumber(tools.countep));
	sendCommand("Nodes check     = " +
		ChessTools::formatNumber(tools.countcheck));
	sendCommand("Nodes mate      = " +
		ChessTools::formatNumber(tools.countmate));
	sendCommand("Nodes promotion = " +
		ChessTools::formatNumber(tools.countpromotion));
	sendCommand("Nodes castle    = " +
		ChessTools::formatNumber(tools.countcastle));
	sendCommand("Time            = " + str);
	sendCommand("Speed           = " + ChessTools::formatNumber(nps));
}

////////////////////////////////////////////////////////////////////////////////
void ChessModule::doPerft(int depth)
{
	ChessTools tools(engine, engine.getBoardRef());

	Timer timer;
	timer.start();

	tools.countnode = 0;

	tools.perft(depth);

	uint64_t timesearch = timer.getElapsedTime();

	std::string str = Timer::timeToString(timesearch);
	uint64_t nps = (tools.countnode * 1000) / (timesearch + 1);

	sendCommand("Depth "  + std::to_string(depth));
	sendCommand("Nodes = " + ChessTools::formatNumber(tools.countnode));
	sendCommand("Time  = " + str);
	sendCommand("Speed = " + ChessTools::formatNumber(nps));
}

////////////////////////////////////////////////////////////////////////////////
void ChessModule::doDivide(int depth)
{
	ChessTools tools(engine, engine.getBoardRef());

	Timer timer;
	timer.start();

	tools.divide(depth);

	uint64_t timesearch = timer.getElapsedTime();

	uint64_t total = 0ULL;

	std::stringstream ss;
	for (int n=0; n<tools.nbDivide; n++) {

		ss << std::setw(10) << tools.listDivide[n].name << " : "
		   << ChessTools::formatNumber(tools.listDivide[n].count) << '\n';

		total += tools.listDivide[n].count;
	}

	ss << "\ntotal : " << ChessTools::formatNumber(total);
	sendCommand(ss.str());

	std::string str = Timer::timeToString(timesearch);

	uint64_t nps = (total * 1000) / (timesearch + 1);

	sendCommand("Time  = " + str);
	sendCommand("Speed = " + ChessTools::formatNumber(nps));
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::cmdRunTestSuite(int htsize, int nbcpu, int mode, int value,
	const std::string &fname)
{
	ChessTools tools(engine, engine.getBoardRef());

	engine.init(htsize);
	engine.setMaxThreads(nbcpu);

#ifdef USE_TABLEBASES
	engine.setTBProbeDepth(cfg.tbProbeDepth);
	engine.setTB50MoveRule((cfg.tb50MoveRule) ? true : false);
	engine.setTBProbeLimit(cfg.tbProbeLimit);
	if (cfg.pathTableBase != "<empty>") {
		TableBases::init_tablebases(cfg.pathTableBase.c_str());
	}
#endif

	engine.newGame();

	engine.setModeRunTestSuite(true);
	if (mode == 0) {
		engine.setEngineSearchMode(SearchMode::fixed_depth, value, 0);
	} else {
		engine.setEngineSearchMode(SearchMode::fixed_time, value * 1000, 0);
	}

    tools.runTestSuite(fname);

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::cmdPerftd(int depth, const std::string &txtfen)
{
	engine.init(hashtable_size_min);

    if (!txtfen.empty()) {
        if (!engine.setFENPosition(txtfen)) {
            std::cout << "Bad FEN string !" << std::endl;
            return false;
        }
    }
    doPerftd(depth);
    return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::cmdPerft(int depth, const std::string &txtfen)
{
    engine.init(hashtable_size_min);

    if (!txtfen.empty()) {
		if (!engine.setFENPosition(txtfen)) {
            std::cout << "Bad FEN string !" << std::endl;
            return false;
        }
    }
    doPerft(depth);
    return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::cmdDivide(int depth, const std::string &txtfen)
{
    engine.init(hashtable_size_min);

    if (!txtfen.empty()) {
        if (!engine.setFENPosition(txtfen)) {
            std::cout << "Bad FEN string !" << std::endl;
            return false;
        }
    }
    doDivide(depth);
    return true;
}

#ifdef USE_TABLEBASES
////////////////////////////////////////////////////////////////////////////////
bool ChessModule::cmdProbeTB(const std::string &fen)
{
	engine.init(1);
	engine.setMaxThreads(1);
	engine.newGame();

	if (!engine.setFENPosition(fen)) {
           std::cout << "Bad FEN string !" << std::endl;
           return false;
    }

	engine.setTBProbeDepth(cfg.tbProbeDepth);
	engine.setTB50MoveRule((cfg.tb50MoveRule) ? true : false);
	engine.setTBProbeLimit(cfg.tbProbeLimit);
	if (cfg.pathTableBase != "<empty>") {
		TableBases::init_tablebases(cfg.pathTableBase.c_str());
	}

	if ((TableBases::TBnum_piece + TableBases::TBnum_pawn) == 0) {
		std::cout << "Tablebases not loaded." << std::endl;
		return false;
	}

	std::cout << "Tablebases loaded = " <<
				TableBases::TBnum_piece + TableBases::TBnum_pawn <<
				", largest = " << TableBases::TBlargest << std::endl;
	std::cout << "FEN : " << fen << std::endl;

	int success = 0;
	int r = TableBases::probe_wdl(engine.getBoardRef(), &success);
	std::string str = "no result";
	if (success != 0) {
		switch (r) {
			case -2:
				str = "loss";
				break;
			case -1:
				str = "blessed loss";
				break;
			case 0:
				str = "draw";
				break;
			case 1:
				str = "cursed win";
				break;
			case 2:
				str = "win";
				break;
			default:
				str = "unknown";
		}
	}
	std::cout << "WDL = " << str << std::endl;

	success = 0;
	r = TableBases::probe_dtz(engine.getBoardRef(), &success);
	if (success != 0) {
		std::cout << "DTZ = " << r << std::endl;
	} else {
		std::cout << "DTZ = no result"<< std::endl;
	}

	ChessMoveSortList mvlist;
	int count = 0;

	ChessBoard &board = engine.getBoardRef();
	count = ChessMoveGen::generateLegalMoves(board, mvlist, board.inCheck());
	ChessMoveRec rec;
	for (int n=0; n<count; n++) {
		board.makeMove(mvlist[n].move, rec);
		r = -TableBases::probe_wdl(engine.getBoardRef(), &success);
		mvlist[n].value = r * 1000 +
			TableBases::probe_dtz(engine.getBoardRef(), &success);
		board.unMakeMove(mvlist[n].move, rec);
	}
	mvlist.sort(0, count);

	for (int n=0; n<count; n++) {
		board.makeMove(mvlist[n].move, rec);
		r = -TableBases::probe_wdl(engine.getBoardRef(), &success);
		switch (r) {
			case -2:
				str = "loss";
				break;
			case -1:
				str = "blessed loss";
				break;
			case 0:
				str = "draw";
				break;
			case 1:
				str = "cursed win";
				break;
			case 2:
				str = "win";
				break;
			default:
				str = "unknown";
		}
		r = -TableBases::probe_dtz(engine.getBoardRef(), &success);
		board.unMakeMove(mvlist[n].move, rec);
		std::cout << mvlist[n].move.text() << " : " <<
			str << ", DTZ = " << std::abs(r) << std::endl;
	}

	return true;
}
#endif

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::cmdProbeBook(const std::string &fen)
{
	engine.init(1);
	engine.setMaxThreads(1);
	engine.newGame();

	if (!engine.setFENPosition(fen)) {
           std::cout << "Bad FEN string !" << std::endl;
           return false;
    }

	if (cfg.useBook) {
		if (!engine.loadOpeningBook(cfg.bookFile)) {
			std::cout << "book fiole not found" << std::endl;
			return false;
		}
	}

	engine.showBookMoves();

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessModule::cmdBenchmark(int htsize, int nbcpu, int mode, int value,
	const std::string &fname)
{
	const std::vector<std::string> benchmarkFens = {
		"r2qkbnr/ppp2p1p/2n5/3P4/2BP1pb1/2N2p2/PPPQ2PP/R1B2RK1 b kq -  ",
		"r2qkbnr/ppp2p1p/8/nB1P4/3P1pb1/2N2p2/PPPQ2PP/R1B2RK1 b kq - ",
		"r2qkbnr/pp3p1p/2p5/nB1P4/3P1Qb1/2N2p2/PPP3PP/R1B2RK1 b kq - ",
		"r2qkb1r/pp3p1p/2p2n2/nB1P4/3P1Qb1/2N2p2/PPP3PP/R1B1R1K1 b kq - ",
		"r2q1b1r/pp1k1p1p/2P2n2/nB6/3P1Qb1/2N2p2/PPP3PP/R1B1R1K1 b - - ",
		"r2q1b1r/p2k1p1p/2p2n2/nB6/3PNQb1/5p2/PPP3PP/R1B1R1K1 b - - ",
		"r2q1b1r/p2k1p1p/2p5/nB6/3Pn1Q1/5p2/PPP3PP/R1B1R1K1 b - - ",
		"r2q1b1r/p1k2p1p/2p5/nB6/3PR1Q1/5p2/PPP3PP/R1B3K1 b - - ",
		"r2q1b1r/p1k2p1p/8/np6/3PR3/5Q2/PPP3PP/R1B3K1 b - - ",
		"r4b1r/p1kq1p1p/8/np6/3P1R2/5Q2/PPP3PP/R1B3K1 b - - ",
		"r6r/p1kqbR1p/8/np6/3P4/5Q2/PPP3PP/R1B3K1 b - - ",
		"5r1r/p1kqbR1p/8/np6/3P1B2/5Q2/PPP3PP/R5K1 b - - ",
		"5r1r/p2qbR1p/1k6/np2B3/3P4/5Q2/PPP3PP/R5K1 b - - ",
		"5rr1/p2qbR1p/1k6/np2B3/3P4/2P2Q2/PP4PP/R5K1 b - - ",
		"5rr1/p2qbR1p/1kn5/1p2B3/3P4/2P2Q2/PP4PP/4R1K1 b - - ",
		"4qRr1/p3b2p/1kn5/1p2B3/3P4/2P2Q2/PP4PP/4R1K1 b - - ",
		"5qr1/p3b2p/1kn5/1p1QB3/3P4/2P5/PP4PP/4R1K1 b - - ",
		"5q2/p3b2p/1kn5/1p1QB1r1/P2P4/2P5/1P4PP/4R1K1 b - - ",
		"5q2/p3b2p/1kn5/3QB1r1/p1PP4/8/1P4PP/4R1K1 b - - ",
		"5q2/p3b2p/1k6/3QR1r1/p1PP4/8/1P4PP/6K1 b - - ",
		"5q2/p3b2p/1k6/4Q3/p1PP4/8/1P4PP/6K1 b - - ",
		"3q4/p3b2p/1k6/2P1Q3/p2P4/8/1P4PP/6K1 b - - ",
		"3q4/p3b2p/8/1kP5/p2P4/8/1P2Q1PP/6K1 b - - ",
		"3q4/p3b2p/8/2P5/pk1P4/3Q4/1P4PP/6K1 b - - "
	};

	std::vector<std::string> fens;

	ChessFENParser parser(engine.getBoardRef());
	bool usefile = parser.load(fname, fens);

	ChessTools tools(engine, engine.getBoardRef());

	engine.init(htsize);
	engine.setMaxThreads(nbcpu);
	engine.newGame();

#ifdef USE_TABLEBASES
	engine.setTBProbeDepth(cfg.tbProbeDepth);
	engine.setTB50MoveRule((cfg.tb50MoveRule) ? true : false);
	engine.setTBProbeLimit(cfg.tbProbeLimit);
	if (cfg.pathTableBase != "<empty>") {
		TableBases::init_tablebases(cfg.pathTableBase.c_str());
	}
#endif

	engine.setModeRunTestSuite(false);
	if (mode == 0) {
		engine.setEngineSearchMode(SearchMode::fixed_depth, value, 0);
	} else {
		engine.setEngineSearchMode(SearchMode::fixed_time, value * 1000, 0);
	}

	tools.runBenchmark((usefile) ? fens : benchmarkFens, mode);

	std::cout << "Hash table size : " << htsize << " Mb" << std::endl;
	if (mode == 0) {
		std::cout << "Search depth : " << value << std::endl;
	} else {
		std::cout << "Search time : " << value << " s" << std::endl;
	}
	std::cout << "Number of threads : " << nbcpu << std::endl;

	return true;
}

} // namespace Cheese

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "protocolcecp.h"

#include "util/logfile.h"
#include "module.h"

#include <iomanip>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
ChessProtocolCECP::ChessProtocolCECP(ChessEngine &e, ChessModule &m) :
	ChessProtocol(e, m), protocolversion(0), countHalfMove(0), forcemode(false),
	firstThink(false), showthinking(true)
{
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::init()
{
	parseInit();
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::initFeatures()
{
	module.sendCommand("feature myname=\"" +
					   ChessModule::name_program + ' ' +
					   std::string(PROJECT_VER));

	module.sendCommand("feature setboard=1");
	module.sendCommand("feature analyze=1");
	module.sendCommand("feature ping=1");
	module.sendCommand("feature draw=0");
	module.sendCommand("feature sigint=0");
	module.sendCommand("feature sigterm=0");
	module.sendCommand("feature colors=0");
	module.sendCommand("feature smp=1");

	// supported chess variants
	module.sendCommand("feature variants=\"normal,fischerandom\"");

	// gui options
	module.sendCommand("feature option=\"Clear Hash -button\"");

	module.sendCommand("feature done=1");
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolCECP::parseInit()
{
	engine.init(module.cfg.hashSize);
	engine.newGame();
	module.remainingMoves = 30;
	module.maxGameMoves = 30;
	countHalfMove = 0;
	firstThink = false;
	engine.setRemainingMoves(module.remainingMoves);

	// load opening book, if one is used
	if (module.cfg.useBook == 1) {
		if (!engine.loadOpeningBook(module.cfg.bookFile)) {
			module.cfg.useBook = 0;
		}
	}

#ifdef USE_PERSONALITY
	if (module.cfg.usePersonality) {
		engine.setPersonality(module.cfg.personalityFile);
	}
#endif

#ifdef USE_LIMIT_STRENGTH
	if (module.cfg.useStrength) {
		engine.setStrengthELO(module.cfg.elo);
	}
#endif

	LOG_INFO << "Using " << module.cfg.threads << " threads";

	engine.setMaxThreads(module.cfg.threads);
	engine.setMinSplitDepth(module.cfg.minSplitDepth);

	engine.setDrawValue(module.cfg.drawScore);

	forcemode = false;

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolCECP::parse(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return true;
	}

	// move
	if (((token.size() == 4) || (token.size() == 5)) &&
		(token[0] >= 'a') && (token[0] <= 'h') &&
		(token[1] >= '1') && (token[1] <= '8') &&
		(token[2] >= 'a') && (token[2] <= 'h') &&
		(token[3] >= '1') && (token[3] <= '8')) {

		parseMove(token);

	} else
	if (token == "time") {

		if (nextToken(stream, token)) {
            // we are 1-player, because we have not played the opponent move
			engine.setPlayerTime(~engine.side(), std::stoi(token) * 10);
		}

	} else
	if (token == "otim") {

	    if (nextToken(stream, token)) {
			engine.setPlayerTime(engine.side(), std::stoi(token) * 10);
		}

	} else
	if (token == "go") {

        // first think
        if (!firstThink) {

            firstThink = true;

            // if number of moves is fixed : count moves already played in
			// the opening book
            if (module.remainingMoves != 0) {
                module.remainingMoves -= (countHalfMove / 2);
                engine.setRemainingMoves(module.remainingMoves);
            }
        }

        if (forcemode) {
            forcemode = false;
        }

		module.startSearch(false);

	} else
	if (token == "quit") {
		module.ponderingActive = false;
		if (engine.isThinking()) {
			engine.stopThinking();
		}
		return false;

	} else
	if (token == "undo") {

		engine.takeBack();

	} else
	if (token == "force") {

		forcemode = true;

	} else
	if (token == "result") {

	    if (engine.isThinking()) {
			engine.stopThinking();
		}

	} else
	if (token == "analyze") {
		engine.setEngineSearchMode(SearchMode::infinite_search, 0, 0);
		module.startSearch(false);
	}
	else
	if (token == "exit") {
	    // stop thinking (if we are pondering, the player played the wrong move)
		if (engine.isThinking()) {
			engine.stopThinking();
		}
	} else
	if (token == "setboard") {
        if (streamString(stream, token)) {
			// warning : if sent in game, look at current game move count
			// (used by chessgui for book moves...)
            if (!engine.setFENPosition(token)) {
				LOG_DEBUG << "Bad FEN string !";
				return false;
			}
		}
	} else
	if (token == "?") {

		// stop thinking (if we are pondering, the player played the wrong move)
		if (engine.isThinking()) {
			engine.stopThinking();
		}

	} else
	if (token == "new") {

		module.ponderingActive = false;
		module.movePonder.clear();

		if (engine.isThinking()) {
			engine.stopThinking();
		}
		engine.newGame();
        module.remainingMoves = 30;
        module.maxGameMoves = 30;
        countHalfMove = 0;
        firstThink = false;
        engine.setRemainingMoves(module.remainingMoves);

		forcemode = false;

	} else
	if (token == "easy") {

		// stop current search
        if (engine.isThinking()) {
            engine.stopThinking();
        }

        // turn off pondering
		module.ponderingActive = false;
		module.movePonder.clear();

	} else
	if (token == "hard") {

		// turn on pondering
		module.ponderingActive = true;

	} else
	if (token == "post") {

		// turn on pondering output
		showthinking = true;

	} else
	if (token == "nopost") {

		// turn off pondering output
		showthinking = false;

	} else
	if (token == "protover") {

	    if (nextToken(stream, token)) {
			protocolversion = std::stoi(token);
			// send features commands
			initFeatures();
		}

	} else
	if (token == "ping") {

		if (nextToken(stream, token)) {
			module.sendCommand("pong " + token);
		}
	} else
	if (token == "st") {

	    if (nextToken(stream, token)) {
            engine.setEngineSearchMode(SearchMode::fixed_time,
				std::stoi(token) * 1000, 0);
		}

	} else
	if (token == "sd") {

	    if (nextToken(stream, token)) {
            engine.setEngineSearchMode(SearchMode::fixed_depth,
				std::stoi(token), 0);
		}

	} else
	if (token == "level") {

	    if (nextToken(stream, token)) {

			int p1 = std::stoi(token);
			int p2 = 0;
			int p20 = 0;
			int p21 = 0;
			double p3 = 0.0;

			if (nextToken(stream, token)) {

				auto p = token.find_first_of(':');
				if (p != std::string::npos) {
					p20 = std::stoi(token.substr(0, p));
					p21 = std::stoi(token.substr(p + 1));
				} else {
					p20 = std::stoi(token);
				}

				if (nextToken(stream, token)) {
					p3 = std::stof(token);
				}
			}

			module.maxGameMoves = p1;
			module.remainingMoves = p1;
            engine.setRemainingMoves(p1);

			p2 = p20 * 60 + p21;

            engine.setPlayerTime(engine.side(), p2 * 1000);
            engine.setTimeIncrement(white, static_cast<int>(p3 * 1000));
            engine.setTimeIncrement(black, static_cast<int>(p3 * 1000));
			engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
		}

	} else
	if (token == "printBoard") {
		engine.getBoardRef().print(std::cout);
	} else
	if (token == "eval") {

		int ev = engine.evaluate(-mate_value, mate_value);

		std::stringstream ss;
		ss << std::fixed << std::setprecision(2) << (ev / 100.0);
		module.sendCommand(ss.str());

	} else
	if (token == "printHashKey") {
		module.sendCommand(Zobrist::hashKeyToString(
			engine.getBoardRef().getHashKey()));
	} else
	if (token == "printFEN") {
		std::string str = engine.getFENPosition(engine.getBoardRef());
		module.sendCommand(str);
	} else
	if (token == "perftd") {
	    if (nextToken(stream, token)) {
            module.doPerftd(std::stoi(token));
		}
	} else
	if (token == "perft") {
	    if (nextToken(stream, token)) {
            module.doPerft(std::stoi(token));
		}
	} else
	if (token == "divide") {
	    if (nextToken(stream, token)) {
            module.doDivide(std::stoi(token));
		}
	} else
	if (token == "option") {

		parseOption(stream);

	} else {
		LOG_DEBUG << "bad move command !!! : " << token;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolCECP::parseOption(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return false;
	}

	if (token == "Clear") {

		if (!nextToken(stream, token)) {
			return false;
		}

		if (token == "Hash") {
			engine.clearHashTable();
		}

	} else {
		LOG_DEBUG << "unknown option : " << token;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolCECP::parseMove(const std::string &command)
{
	++countHalfMove;

	if ((module.ponderingActive) &&
		(module.pondernextmove) &&
		(!module.movePonder.isEmpty())) {

		int fs = static_cast<int>(command[0] - 'a');
		int rs = static_cast<int>(command[1] - '1');
		int fd = static_cast<int>(command[2] - 'a');
		int rd = static_cast<int>(command[3] - '1');

		// same move : play
		if ((fs == squareFile(module.movePonder.src())) &&
			(rs == squareRank(module.movePonder.src())) &&
			(fd == squareFile(module.movePonder.dst())) &&
			(rd == squareRank(module.movePonder.dst()))) {

			if (engine.isThinking()) {
				module.pondernextmove = false;
				engine.ponderHit();
			} else {
				if (!engine.setMoveSquareList(command)) {
					return false;
				}
				module.pondernextmove = true;
				module.startSearch(false);
			}
			return true;

		} else {

			pondernewmove = command;

			if (engine.isThinking()) {
				engine.stopThinking();
			} else {
				if (!engine.setMoveSquareList(command)) {
					return false;
				}
				module.pondernextmove = false;
				module.startSearch(false);
			}
			return true;
		}

	} else {
		// play move
		if (!engine.setMoveSquareList(command)) {
			return true;
		}
	}

	// return if no moves possibles (mate)
	if (engine.countLegalMoves(engine.getBoardRef()) == 0) {
		return false;
	}

	// computer must play
	if (!forcemode) {
		engine.setRemainingMoves(module.remainingMoves);
		module.startSearch(false);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::onEndSearch(ChessMove bm,
	ChessMove pm, [[maybe_unused]] int depth, [[maybe_unused]] int ply,
	[[maybe_unused]] uint64_t cntnodes, [[maybe_unused]] uint64_t searchtime)
{
	if (module.pondernextmove) {
		// take back if we were pondering
		engine.takeBack();
	} else {
		// play the move (if not in analyze mode)
		if ((engine.getSearchMode() != SearchMode::infinite_search) &&
			(!bm.isEmpty())) {
			engine.playMove(bm);
		}
	}

	// pondering canceled (stopped search or disable pondering)
	if (module.pondernextmove) {

		module.pondernextmove = false;

		if (module.ponderingActive) {

			// play the move
			if (!engine.setMoveSquareList(pondernewmove)) {
				return;
			}

			// restart search if opponent played the wrong move
			module.startSearch(false);
		}

	} else {

		if (!engine.isRunningTestSuite()) {

			module.movePonder = pm;

			// update number of moves
			if (module.maxGameMoves != 0) {
				--module.remainingMoves;
				if (module.remainingMoves == 0) {
					module.remainingMoves += module.maxGameMoves;
				}
			}


			module.sendCommand("move " + engine.getTextMove(bm));

			if (module.ponderingActive) {

				module.pondernextmove = false;

				if (!module.movePonder.isEmpty()) {

					std::string strmove = engine.getTextMove(module.movePonder);
					module.sendCommand("Hint: " + strmove);

					if (engine.setMoveSquareList(strmove)) {
						module.pondernextmove = true;
						module.startSearch(true);
					}
				}
			}
        }
	}

}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::onSendMultiPV([[maybe_unused]] int num, int depth,
	int score, ChessPVMoveList &mlist, int count, uint64_t cntnodes,
	uint64_t searchtime, [[maybe_unused]] int maxply,
	[[maybe_unused]] int bound, [[maybe_unused]] uint64_t tbhits)
{
	if (showthinking) {
		module.sendCommand(std::to_string(depth) + ' ' +
						   std::to_string(score) + ' ' +
						   std::to_string(static_cast<int>(searchtime / 10)) +
						   ' ' + std::to_string(cntnodes) + ' ' +
						   engine.getTextPV(mlist.data(), count));
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::onSendInfoMessage(const std::string &msg)
{
	module.sendCommand(msg);
}

} // namespace Cheese

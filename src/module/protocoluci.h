////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_PROTOCOLUCI_H_
#define CHEESE_PROTOCOLUCI_H_

#include "config.h"

#include "board.h"
#include "engine.h"
#include "protocol.h"

namespace Cheese {

class ChessModule;
class ChessProtocolUCI;

////////////////////////////////////////////////////////////////////////////////
class ChessProtocolUCI : public ChessProtocol {

public:

	ChessProtocolUCI(ChessEngine &e, ChessModule &m);

	~ChessProtocolUCI() override = default;

	void init() override;

	bool parse(std::istringstream &stream) override;

private:

	bool parseInit();

	bool parseOption(std::istringstream &stream);

	bool parsePosition(std::istringstream &stream);

	bool parseGo(std::istringstream &stream);

	void onEndSearch(ChessMove bm, ChessMove pm, int depth,
		int ply, uint64_t cntnodes, uint64_t searchtime) override;

	void onSendMultiPV(int num, int depth, int score, ChessPVMoveList &mlist,
		int count, uint64_t cntnodes, uint64_t searchtime, int maxply,
		int bound, uint64_t tbhits) override;

	void onSendHashFull(uint64_t v) override;

	void onSendInfoDepth(int d) override;

	void onSearchRootMove(ChessMove move, int num) override;

	void onSendInfoMessage(const std::string &msg) override;

};

} // namespace Cheese

#endif //CHEESE_PROTOCOLUCI_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_MODULE_H_
#define CHEESE_MODULE_H_

#include "config.h"

#include "util/configfile.h"
#include "engine.h"
#include "protocol.h"
#include "protocoluci.h"
#include "protocolcecp.h"
#include "thread.h"

#include <memory>
#include <string>
#include <vector>

namespace Cheese {

// GUI protocols
enum class GuiProtocol {

	// unknown protocol (default)
	none,

	// UCI protocol
	uci,

	// CECP protocol (xboard)
	cecp
};

class ChessModule;
class ChessProtocolDefault;

////////////////////////////////////////////////////////////////////////////////
// options loaded from config file
struct ChessModuleConfig {

	// default hash size
	int hashSize;

	// default use of opening book
	int useBook;

	// default use of strength
	int useStrength;

	// default strength in ELO
	int elo;

	// default max number of threads
	int threads;

	// minimum split depth
	int minSplitDepth;

	int usePersonality;

	// default log levels
	int logLevel;

	// use null moves pruning
	int nullMoves;

	// use LMR
	int lmr;

	// contempt factor
	int drawScore;

	// using personality
	bool usePerso;

	// 0 = reduce speed, 1 = recuce nodes
	int uciLimitMode;

	int usePersonalityOptions;

	int tb50MoveRule;

	int tbProbeLimit;

	int tbProbeDepth;

	// SMP algorithm : ybwc or lazy
	std::string smpMode;

	std::string bookFile;

	std::string personalityFile;

	std::string pathTableBase;

};

////////////////////////////////////////////////////////////////////////////////
// Chess module is the main object, to use chess engine with command line
// or gui
class ChessModule : public ChessEngineListener {

public:

	ChessEngine engine;

	std::unique_ptr<ChessProtocol> protocol;

	// configuration options
	ChessModuleConfig cfg;

	// current ponder move
	ChessMove movePonder;

	GuiProtocol guiMode;

	int remainingMoves;

	int maxGameMoves;

	bool quit;

	// if pondering option is enable
	bool ponderingActive;

	// if we are pondering
	bool pondernextmove;

	static const std::string name_archi;

	static const std::string name_author;

	static const std::string name_program;

public:

	ChessModule();

	bool init(const std::vector<std::string> &args);

	// main loop
	void run();

	void free();

	void setProtocol(GuiProtocol p);

	// send a command string to gui
	void sendCommand(const std::string &str);

	bool parse();

	void doPerftd(int depth);

	void doPerft(int depth);

	void doDivide(int depth);

	void startSearch(bool ponder);

private:

	bool parseDefault(std::istringstream &stream);

	bool parseArguments(const std::vector<std::string> &args, bool &err);

	bool loadConfig();

	bool cmdRunTestSuite(int htsize, int nbcpu, int mode, int value,
		const std::string &fname);

	bool cmdPerftd(int depth, const std::string &txtfen);

	bool cmdPerft(int depth, const std::string &txtfen);

	bool cmdDivide(int depth, const std::string &txtfen);

	#ifdef USE_TABLEBASES
	bool cmdProbeTB(const std::string &fen);
	#endif

	bool cmdProbeBook(const std::string &fen);

	bool cmdBenchmark(int htsize, int nbcpu, int mode, int value,
			const std::string &fname);

	void onEndSearch(ChessEngineNotifier &notifier,
		ChessMove bm, ChessMove pm, int depth, int ply, uint64_t cntnodes,
		uint64_t searchtime) override;

	void onSendMultiPV(ChessEngineNotifier &notifier, int num, int depth,
		int score, ChessPVMoveList &mlist, int count, uint64_t cntnodes,
		uint64_t searchtime, int maxply, int bound, uint64_t tbhits) override;

	void onSendHashFull(ChessEngineNotifier &notifier, uint64_t v) override;

	void onSendInfoDepth(ChessEngineNotifier &notifier, int d) override;

	void onSearchRootMove(ChessEngineNotifier &notifier, ChessMove move,
		int num) override;

	void onSendInfoMessage(ChessEngineNotifier &notifier,
		const std::string &msg) override;
};

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onEndSearch(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	ChessMove bm, ChessMove pm, int depth, int ply,
	uint64_t cntnodes, uint64_t searchtime)
{
	protocol->onEndSearch(bm, pm, depth, ply, cntnodes, searchtime);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendMultiPV(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	int num, int depth, int score, ChessPVMoveList &mlist, int count,
	uint64_t cntnodes, uint64_t searchtime, int maxply, int bound,
	uint64_t tbhits)
{
	protocol->onSendMultiPV(num, depth, score, mlist, count, cntnodes,
		searchtime, maxply, bound, tbhits);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendHashFull(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	uint64_t v)
{
	protocol->onSendHashFull(v);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendInfoDepth(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	int d)
{
	protocol->onSendInfoDepth(d);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSearchRootMove(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	ChessMove move, int num)
{
	protocol->onSearchRootMove(move, num);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendInfoMessage(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	const std::string &msg)
{
	protocol->onSendInfoMessage(msg);
}

} // namespace Cheese

#endif //CHEESE_MODULE_H_

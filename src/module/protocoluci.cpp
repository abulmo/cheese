////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "protocoluci.h"

#include "util/logfile.h"
#include "module.h"

#ifdef USE_TABLEBASES
#include "syzygy/tbprobe.h"
#endif

#include <iomanip>
#include <string>

namespace Cheese {

const std::array<std::string, 3> strBound = { "", "upperbound ",
	"lowerbound " };

const std::array<std::string, 2> strMate = { "cp", "mate" };

////////////////////////////////////////////////////////////////////////////////
ChessProtocolUCI::ChessProtocolUCI(ChessEngine &e, ChessModule &m) :
	ChessProtocol(e, m)
{
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::init()
{
	parseInit();
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolUCI::parseInit()
{
	module.sendCommand("id name " + ChessModule::name_program + ' ' +
						std::string(PROJECT_VER));
	module.sendCommand("id author " + ChessModule::name_author);

	int maxhash = hashtable_size_max;

	module.sendCommand("option name Hash type spin default "
					   + std::to_string(module.cfg.hashSize) +
					   " min " + std::to_string(hashtable_size_min) +
					   " max " + std::to_string(maxhash));
	module.sendCommand("option name Ponder type check default false");
	module.sendCommand("option name OwnBook type check default " +
					   std::string((module.cfg.useBook) ? "true" : "false"));
	module.sendCommand("option name Book type string default " +
						module.cfg.bookFile);
	module.sendCommand("option name Clear Hash type button");

	// personality
#ifdef USE_PERSONALITY
	module.sendCommand("option name UsePersonality type check default " +
		std::string((module.cfg.usePersonality) ? "true" : "false"));
	module.sendCommand("option name PersonalityFile type string default " +
		module.cfg.personalityFile);
#endif

	module.sendCommand("option name UCI_Chess960 type check default false");

	module.sendCommand("option name MultiPV type spin default 1 min 1 max " +
		std::to_string(max_multipv));

	module.sendCommand("option name NullMovePruning type check default " +
			std::string((module.cfg.nullMoves) ? "true" : "false"));

	module.sendCommand("option name LateMoveReduction type check default " +
			std::string((module.cfg.lmr) ? "true" : "false"));

	module.sendCommand("option name Threads type spin default " +
			std::to_string(module.cfg.threads) + " min 1 max " +
			std::to_string((module.cfg.smpMode == "ybwc") ? max_threads_ybwc :
				max_threads));

	module.sendCommand("option name SMPMode type combo default " +
		module.cfg.smpMode + " var ybwc var lazy");

#ifdef USE_LIMIT_STRENGTH
	module.sendCommand("option name UCI_LimitStrength type check default " +
			std::string((module.cfg.useStrength) ? "true" : "false"));
	module.sendCommand("option name UCI_Elo type spin default " +
			std::to_string(module.cfg.elo) + " min " +
			std::to_string(min_strength_elo) + " max " +
			std::to_string(max_strength_elo));
#endif

	module.sendCommand("option name DrawScore type spin default " +
						std::to_string(module.cfg.drawScore) +
						" min -100 max 100");

#ifdef USE_TABLEBASES
	module.sendCommand("option name SyzygyPath type string default " +
		module.cfg.pathTableBase);
	module.sendCommand("option name SyzygyProbeDepth type spin default " +
		std::to_string(module.cfg.tbProbeDepth) +
		" min 1 max 100");
	module.sendCommand("option name Syzygy50MoveRule type check default " +
		std::string((module.cfg.tb50MoveRule == 1) ? "true" : "false"));
	module.sendCommand("option name SyzygyProbeLimit type spin default " +
		std::to_string(module.cfg.tbProbeLimit) +
		" min 0 max 6");
#endif

#ifdef USE_TUNING
	engine.initTuning();
	for (const Tunable &p : engine.getTuningParams()) {
		module.sendCommand("option name " + p.name +
						   " type spin default " + std::to_string(p.def) +
						   " min " + std::to_string(p.min) +
						   " max " + std::to_string(p.max));
	}
#endif

	engine.init(module.cfg.hashSize);

#ifdef USE_PERSONALITY
	if (module.cfg.usePersonality) {
		engine.setPersonality(module.cfg.personalityFile);
	}
#endif

#ifdef USE_LIMIT_STRENGTH
	if (module.cfg.useStrength) {
		engine.setStrengthELO(module.cfg.elo);
	} else {
		engine.setStrengthELO(max_strength_elo);
	}
#endif

	engine.newGame();

	// load opening book, if one is used
	if (module.cfg.useBook == 1) {
		if (!engine.loadOpeningBook(module.cfg.bookFile)) {
			module.cfg.useBook = 0;
		}
	}

	LOG_INFO << "Using " << module.cfg.threads << " threads";

	engine.setMaxThreads(module.cfg.threads);
	engine.setMinSplitDepth(module.cfg.minSplitDepth);

#ifdef USE_TABLEBASES
	engine.setTBProbeDepth(module.cfg.tbProbeDepth);
	engine.setTB50MoveRule((module.cfg.tb50MoveRule) ? true : false);
	engine.setTBProbeLimit(module.cfg.tbProbeLimit);
	if (module.cfg.pathTableBase != "<empty>") {
		TableBases::init_tablebases(module.cfg.pathTableBase.c_str());
	}
#endif

	module.sendCommand("uciok");

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolUCI::parse(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return true;
	}

	if (token == "go") {

		parseGo(stream);
		module.startSearch(module.pondernextmove);

	} else
	if (token == "position") {

		parsePosition(stream);

	} else
	if (token == "isready") {

		module.sendCommand("readyok");

	} else
	if (token == "ponderhit") {

		// the user has played the expected move
		if (engine.isThinking()) {
			// => continue the search or play
			engine.ponderHit();
		}

	} else
	if (token == "setoption") {
	    parseOption(stream);
	} else
	if (token == "quit") {
		// turn off pondering
		module.ponderingActive = false;
		module.movePonder.clear();
		if (engine.isThinking()) {
			engine.stopThinking();
		}
		return false;

	} else
	if (token == "ucinewgame") {

		// turn off pondering
		module.movePonder.clear();

		if (engine.isThinking()) {
			engine.stopThinking();

			engine.newGame();

            module.remainingMoves = 30;
            module.maxGameMoves = 30;
            engine.setRemainingMoves(module.remainingMoves);

		} else {
			engine.newGame();
		}

	} else
	if (token == "stop") {
		// stop thinking
		// if we are pondering, the opponent played an unexpected move
		if (engine.isThinking()) {
			engine.stopThinking();
		}
	} else
	if (token == "perftd") {
		if (nextToken(stream, token)) {
			module.doPerftd(std::stoi(token));
		}
	} else
	if (token == "perft") {
		if (nextToken(stream, token)) {
            module.doPerft(std::stoi(token));
		}
	} else
	if (token == "divide") {
		if (nextToken(stream, token)) {
            module.doDivide(std::stoi(token));
		}
	} else
	if (token == "printBoard") {
		engine.getBoardRef().print(std::cout);
	} else
	if (token == "eval") {
		int ev = engine.evaluate(-mate_value, mate_value);
		std::stringstream ss;
		ss << "eval = "
		   << std::fixed << std::setprecision(2) << (ev / 100.0);
		module.sendCommand(ss.str());

	} else
	if (token == "printHashKey") {
		module.sendCommand(Zobrist::hashKeyToString(
			engine.getBoardRef().getHashKey()));
	} else
	if (token == "printFEN") {
		std::string str = engine.getFENPosition(engine.getBoardRef());
		module.sendCommand(str);
	} else
	if (token == "showBookMoves") {
		engine.showBookMoves();
	} else {
		LOG_DEBUG << "unknown parse command " << token;
    }

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolUCI::parseOption(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return false;
	}

    if (token == "name") {

		if (!nextToken(stream, token)) {
			return false;
		}

		std::string name = token;
		if (!nextToken(stream, token)) {
			return false;
		}

		if (token != "value") {
			return false;
		}

		if (!nextToken(stream, token)) {
			return false;
		}

		if (name == "Hash") {

			engine.setHashTableSize(std::stoi(token));

		} else
		if (name == "Ponder") {

			bool b = (token == "true");
			module.ponderingActive = b;

		} else
		if (name == "NullMovePruning") {

			engine.enableNullMoves((token == "true"));

		} else
		if (name == "LateMoveReduction") {

			engine.enableLMR((token == "true"));

		} else
		if (name == "OwnBook") {

			if (token == "true") {
				module.cfg.useBook = 1;
				if (!engine.loadOpeningBook(module.cfg.bookFile)) {
					module.sendCommand("info string book file not found");
					module.cfg.useBook = 0;
				}
			} else {
				module.cfg.useBook = 0;
				engine.unloadOpeningBook();
			}

		} else
		if (name == "Book") {

			module.cfg.bookFile.assign(token);
			if (module.cfg.useBook) {
				if (!engine.loadOpeningBook(module.cfg.bookFile)) {
					module.sendCommand("info string book file not found");
					module.cfg.useBook = 0;
				}
			}

		} else
		if (name == "Clear") {

			if (token == "Hash") {
				engine.clearHashTable();
			}

		} else
		if (name == "MultiPV") {

			engine.setMultiPV(std::stoi(token));

		} else
#ifdef USE_LIMIT_STRENGTH
		if (name == "UCI_Elo") {

			int ve = std::stoi(token);
			if (module.cfg.useStrength) {
				engine.setStrengthELO(ve);
			}
			module.cfg.elo = ve;

		} else
		if (name == "UCI_LimitStrength") {

			if (token == "true") {
				engine.setStrengthELO(module.cfg.elo);
				module.cfg.useStrength = true;
			} else {
				engine.setStrengthELO(max_strength_elo);
				module.cfg.useStrength = false;
			}

		} else
#endif
#ifdef USE_TABLEBASES
		if (name == "SyzygyPath") {
			LOG_INFO << token;
			TableBases::init_tablebases(token.c_str());
		} else
		if (name == "SyzygyProbeDepth") {
			engine.setTBProbeDepth(std::stoi(token));
		} else
		if (name == "Syzygy50MoveRule") {
			engine.setTB50MoveRule((token == "true") ? true : false);
		} else
		if (name == "SyzygyProbeLimit") {
			engine.setTBProbeLimit(std::stoi(token));
		} else
#endif
		if (name == "DrawScore") {

			engine.setDrawValue(std::stoi(token));

		} else
		if (name == "UCI_Chess960") {

			engine.getBoardRef().setChessRules((token == "true") ?
				ChessRules::frc : ChessRules::classic);

		} else
		if (name == "SMPMode") {

			if (token != module.cfg.smpMode) {

				if (token == "ybwc") {
					engine.setSMPMode(SMPMode::ybwc);
				} else
				if (token == "lazy") {
					engine.setSMPMode(SMPMode::lazy);
				} else {
					return false;
				}

				module.cfg.smpMode = token;
				engine.setMaxThreads(module.cfg.threads);

				LOG_INFO << "SMPMode = " << token;
			}
		} else
		if (name == "Threads") {

			int ve = std::stoi(token);
			if (module.cfg.threads != ve) {
				engine.setMaxThreads(ve);
				module.cfg.threads = ve;
				LOG_INFO << "Using " << ve << " threads";
			}

		} else
#ifdef USE_PERSONALITY
		if (name == "UsePersonality") {

			if (token == "true") {
				module.cfg.usePerso = true;
				engine.setPersonality(module.cfg.personalityFile);
			} else {
				module.cfg.usePerso = false;
				engine.setPersonality("");
			}

		} else
		if (name == "personalityFile") {

			module.cfg.personalityFile.assign(token);
			if (module.cfg.usePerso) {
				engine.setPersonality(module.cfg.personalityFile);
			} else {
				engine.setPersonality("");
			}

		} else
#endif
		{
			#ifdef USE_TUNING
			engine.setTuneValue(name, token);
			#endif
		}

    } else {
		LOG_DEBUG << "unknown parse option " << token;
        return false;
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolUCI::parsePosition(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return false;
	}

	if (token == "startpos") {

		// start position
		engine.setStartPosition();

        if (nextToken(stream, token)) {
			if (token == "moves") {
				if (streamString(stream, token)) {
					if (!engine.setMoveSquareList(token)) {
						LOG_DEBUG << "bad move !?";
					}
				}
			}
		}

	} else
	if (token == "fen") {

        if (!streamString(stream, token)) {
			return false;
		}

		auto p = token.find("moves");
		if (p != std::string::npos) {

			std::string strfen = token.substr(0, p);
			auto p2 = token.find_first_of(' ', p);
			auto p3 = token.find_first_not_of(' ', p2);

			std::string strmoves = token.substr(p3);

			if (!engine.setFENPosition(strfen)) {
				LOG_DEBUG << "Bad FEN string !";
				return false;
			}

			if (!engine.setMoveSquareList(strmoves)) {
				LOG_DEBUG << "bad move !?";
			}

		} else {
			if (!engine.setFENPosition(token)) {
				LOG_DEBUG << "Bad FEN string !";
				return false;
			}
		}

	} else {
		LOG_DEBUG << "unknown parse position " << token;
    }

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// parse 'go' command
//
// go wtime 300000 btime 300000 winc 2 binc 2
// go movestogo 40 btime 300000 wtime 300000 winc 0 binc 0
//
bool ChessProtocolUCI::parseGo(std::istringstream &stream)
{
	std::string token;

	module.pondernextmove = false;
    engine.setRemainingMoves(0);
	engine.setTimeIncrement(white, 0);
	engine.setTimeIncrement(black, 0);

    while (nextToken(stream, token)) {

		if (token == "ponder") {
			module.pondernextmove = true;
		} else
        if (token == "wtime") {
            if (nextToken(stream, token)) {
				engine.setPlayerTime(white, std::stoi(token));
				engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
			}
        } else
        if (token == "btime") {
            if (nextToken(stream, token)) {
				engine.setPlayerTime(black, std::stoi(token));
				engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
			}
        } else
        if (token == "winc") {
            if (nextToken(stream, token)) {
				engine.setTimeIncrement(white, std::stoi(token));
			}
        } else
        if (token == "binc") {
            if (nextToken(stream, token)) {
				engine.setTimeIncrement(black, std::stoi(token));
			}
        } else
		if (token == "movetime") {
			if (nextToken(stream, token)) {
				engine.setEngineSearchMode(SearchMode::fixed_time,
					std::stoi(token), 0);
			}
        } else
        if (token == "movestogo") {
            if (nextToken(stream, token)) {
				engine.setRemainingMoves(std::stoi(token));
				engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
			}
        } else
		if (token == "infinite") {
            engine.setEngineSearchMode(SearchMode::infinite_search, 0, 0);
        } else
		if (token == "depth") {
            if (nextToken(stream, token)) {
				engine.setEngineSearchMode(SearchMode::fixed_depth,
					std::stoi(token), 0);
			}
        } else
        if (token == "nodes") {
            if (nextToken(stream, token)) {
				engine.setEngineSearchMode(SearchMode::fixed_nodes,
					std::stoi(token), 0);
			}
        }
    }

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onEndSearch(ChessMove bm,
	ChessMove pm, [[maybe_unused]] int depth, [[maybe_unused]] int ply,
	[[maybe_unused]] uint64_t cntnodes, [[maybe_unused]] uint64_t searchtime)
{
	// don't need to play moves with UCI ?
	if (module.pondernextmove) {
		engine.takeBack();
	} else {
		if (engine.getSearchMode() != SearchMode::infinite_search) {
			engine.playMove(bm);
		}
	}

	// pondering canceled (stopped search or disable pondering)
	if (module.pondernextmove) {

		if (module.ponderingActive) {
			std::string str = "bestmove " + engine.getTextMove(bm);
			if (!pm.isEmpty()) {
				str += " ponder " + engine.getTextMove(pm);
			}
			module.sendCommand(str);
		} else {
			module.pondernextmove = false;
		}

	} else {

		module.movePonder = pm;

		std::string str = "bestmove " + engine.getTextMove(bm);
		if ((module.ponderingActive) && (!pm.isEmpty())) {
			str += " ponder " + engine.getTextMove(pm);
		}
		module.sendCommand(str);
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendMultiPV(int num, int depth, int score,
	ChessPVMoveList &mlist, int count, uint64_t cntnodes, uint64_t searchtime,
	int maxply, int bound,
	[[maybe_unused]] uint64_t tbhits)
{
	uint64_t nps = (cntnodes * 1000) / (searchtime + 1);

	int sc = score;
	int im = 0;

	if (score <= min_score_mate) {
		im = 1;
		sc = -engine.calcMateValue(score);
	} else
	if (score >= max_score_mate) {
		im = 1;
		sc = engine.calcMateValue(score);
	}

	std::string str = "info";

	if (num != 0) {
		str += " multipv " + std::to_string(num);
	}

	str += " depth " + std::to_string(depth) +
		   " seldepth " + std::to_string(maxply) +
		   " score " + strMate[im] + ' ' + std::to_string(sc) +
		   ' ' + strBound[bound] +
		   "time " + std::to_string(searchtime) +
		   " nodes " + std::to_string(cntnodes) +
		   " nps " + std::to_string(nps) +
#ifdef USE_TABLEBASES
		   " tbhits " + std::to_string(tbhits) +
#endif
		   " pv " + engine.getTextPV(mlist.data(), count);

	module.sendCommand(str);
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendHashFull(uint64_t v)
{
	module.sendCommand("info hashfull " + std::to_string(v));
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendInfoDepth(int d)
{
	module.sendCommand("info depth " +  std::to_string(d));
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSearchRootMove(ChessMove move, int num)
{
	module.sendCommand("info currmove " + engine.getTextMove(move) +
					    " currmovenumber " + std::to_string(num));
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendInfoMessage(const std::string &msg)
{
	module.sendCommand("info string " + msg);
}

} // namespace Cheese

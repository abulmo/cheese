////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "ybwc.h"

#include "search.h"
#include "moveorder.h"
#include "repetitions.h"
#include "engine.h"

#include <cassert>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
ChessSearchYBWC::ChessSearchYBWC(ChessEngine &e, ChessSearchThreadList &ths,
	ChessEval &ev, ChessHashTable &ht, ChessSearchThread *th, const
	ChessBoard &pos, const ChessRepetitions &rep, bool chknodes) :
	ChessSearch(e, ths, ev, ht,th, pos, rep, chknodes)
{
}

////////////////////////////////////////////////////////////////////////////////
// a thread start the search at a splitpoint, at Root
void ChessSearchYBWC::searchSMPRoot(int alpha, int beta, int depth)
{
	assert(thread->smp == SMPMode::ybwc);
	auto *th = static_cast<ChessSearchThreadYBWC *>(thread);
	ChessSearchSplitPoint *sp = th->splitPoint;
	ChessMoveOrder &moveorder = *sp->moveorder;

	int	ply = 0;

	ChessMove bestMove(0);
	bestMove.set(sp->bestMove);
	alpha = sp->bestValue;

	// legal moves count
	int nm = sp->numMove;

	clearPV(ply);

	ChessMoveRec rec;

	while (nm < moveorder.getMoveCount()) {

		ChessMove move = moveorder.getMove(nm);

		repetition.push(board.getHashKey());

		// note : legality test already done in orderRoot
		board.makeMove(move, rec);

		searchStack[ply].move = move;

		++thread->countNodes;

		// remember current move num to send to gui
		int cnum = nm;

		++sp->numMove;

		if (sp->bestValue > alpha) {
			alpha = sp->bestValue;
		}

		// send currmove to GUI
		engine.onSearchRootMove(move, cnum + 1);

		sp->mutex.unlock();

		bool chk = board.inCheck();

		int ext = 0;

#ifdef USE_EXTENSION_CHECK
		if (chk) {
			ext += extend_check;
		}
#endif

#ifdef USE_EXTENSION_PAWNON7TH
		if (move.isMovePawnOn7th()) {
			ext += extend_pawnon7th;
		}
#endif

		// limit extension to 1 ply
		ext = std::min<int>(ext, (one_ply * 1));

		int newdepth = depth + ext - one_ply;

		int	value = 0;

		// Principal Variation Search (PVS)

		if (sp->bestValue > alpha) {
			alpha = sp->bestValue;
		}

		// PVS search
		value = -search(-alpha - 1, -alpha, newdepth, ply + 1, false, chk);

		// pvs failed : research
		if ((value > alpha) && (value < beta)) {

			if (sp->bestValue > alpha) {
				alpha = sp->bestValue;
			}

			// PVS re-search
			value = -search(-beta, -alpha, newdepth, ply + 1, true, chk);
		}

		board.unMakeMove(move, rec);

		repetition.pop();

		sp->mutex.lock();

		if ((threads.mustStop()) || (thread->cutoff())) {
			return;
		}

		// get alpha & bestmove modified by other threads
		if (sp->bestValue > alpha) {
			alpha = sp->bestValue;
		}

		// num may change between the unlock / lock
		nm = sp->numMove;

		if (value > alpha) {

			bestMove = move;
			sp->bestMove = move.data();

			// update pv
			sp->pv->insertPV(ply, move, pv);

			if (value >= beta) {
				sp->bestValue = value;
				sp->cutoff = true;
				return;
			}

			alpha = value;
			sp->bestValue = alpha;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// a thread start the search at a splitpoint
void ChessSearchYBWC::searchSMP(int alpha, int beta, int depth, int ply,
	bool nodepv, bool incheck)
{
	assert(alpha < beta);
	assert((nodepv) || (alpha == beta - 1));
	assert(depth < max_depth * one_ply);
	assert(alpha >= -mate_value - 1);
	assert(beta <= mate_value + 1);
	assert(incheck == board.inCheck());

	assert(thread->smp == SMPMode::ybwc);
	auto *th = static_cast<ChessSearchThreadYBWC *>(thread);
	ChessSearchSplitPoint *sp = th->splitPoint;
	ChessMoveOrder &moveorder = *sp->moveorder;

	// real depth
	int rdepth = depth / one_ply;

	clearPV(ply);

	// init futility pruning
#ifdef USE_FUTILITY_PRUNING
	bool futilityPruning = sp->fpruning;
	int futilityMargin = sp->fmargin;
#endif

	bool condLMR = ((depth >= (lmr_min_depth * one_ply))
					&& (!incheck)
					&& (engine.usingLMR()));

	ChessMove bestMove(0);

	bestMove.set(sp->bestMove);
	alpha = sp->bestValue;

	int num = sp->numMove;

	const HashKey hk = board.getHashKey();

	ChessMoveRec rec;
	ChessMove move;

	int value = 0;
	int movePhase = 0;

	while ((movePhase = moveorder.getNextMove(board, move)) != ordermove_end) {

		board.makeMove(move, rec);
		if (!board.isPositionLegal()) {
			board.unMakeMove(move, rec);
			continue;
		}

		searchStack[ply].move = move;

		++thread->countNodes;

		int cnum = num;

		num = ++sp->numMove;

		if (sp->bestValue > alpha) {
			alpha = sp->bestValue;
		}

		sp->mutex.unlock();

		bool chk = board.inCheck();

		// search extensions
		int ext = 0;

#ifdef USE_EXTENSION_CHECK
		if (chk) {
			ext += extend_check;
		}
#endif

#ifdef USE_EXTENSION_PAWNON7TH
		if (move.isMovePawnOn7th()) {
			ext += extend_pawnon7th;
		}
#endif

		// limit extension to 1 ply
		ext = std::min<int>(ext, (one_ply * 1));

		int newdepth = depth + ext - one_ply;

		// Futility Prunning
#ifdef USE_FUTILITY_PRUNING

		bool pruning = false;

		if (futilityPruning) {

			if ((cnum > 0)
				&& (!chk)
				&& (ext == 0)
				&& (!move.isMovePawnPast4th(~board.side()))
				&& (!move.isCaptureOrPromotion())
				) {

				if (futilityMargin <= alpha) {
					pruning = true;
				}
			}
		}

		// late moves pruning (LMP)
#ifdef USE_LATE_MOVE_PRUNING
		if ((movePhase == ordermove_next_quiet)
			&& (!incheck)
			&& (ext == 0)
			&& (!chk)
			&& (!nodepv)
			&& (!move.isMovePawnPast4th(~board.side()))
			&& (depth < lmp_max_depth * one_ply)
		) {
			if (cnum > lmp_count[rdepth]) {
				pruning = true;
			}
		}
#endif

		if (pruning) {
			board.unMakeMove(move, rec);
			sp->mutex.lock();
			continue;
		}

#endif

		repetition.push(hk);

		// Principal Variation Search (PVS)

		if (sp->bestValue > alpha) {
			alpha = sp->bestValue;
		}

		// Late Move Reduction (LMR)
#ifdef USE_LATE_MOVE_REDUCTION
		value = alpha + 1;
		if ((condLMR)
			&& (cnum >= lmr_min_moves)
			&& (!chk)
			&& (movePhase == ordermove_next_quiet)
			&& (ext == 0)) {

			uint32_t lr = lmr_reduction[std::min(rdepth,
				lmr_max_depth - 1)][std::min(num, lmr_max_moves - 1)];
			if (!nodepv) {
				lr++;
			}			

			if (lr) {

				int ld = newdepth - lr * one_ply;

				// LMR search
				value = -search(-alpha - 1, -alpha, ld, ply + 1, false, false);
			}	
		}

		if (value > alpha) {

#endif //USE_LATE_MOVE_REDUCTION

			// PVS search
			value = -search(-alpha - 1, -alpha, newdepth, ply + 1, false, chk);

			if ((value > alpha) && (value < beta)) {

				if ((sp->bestValue > alpha) && (sp->bestValue < beta)) {
					alpha = sp->bestValue;
				}

				// PVS re-search
				value = -search(-beta, -alpha, newdepth, ply + 1, true, chk);
			}

#ifdef USE_LATE_MOVE_REDUCTION
		}
#endif

		board.unMakeMove(move, rec);

		repetition.pop();

		sp->mutex.lock();

		if ((threads.mustStop()) || (thread->cutoff())) {
			return;
		}

		// get alpha modified by other threads
		if (sp->bestValue > alpha) {
			alpha = sp->bestValue;
		}

		// smp : num may change between the unlock / lock
		num = sp->numMove;

		if (value > alpha) {

			bestMove = move;
			sp->bestMove = move.data();

			if (value >= beta) {
				sp->bestValue = value;
				sp->cutoff = true;
				return;
			}

			// update pv
			if (nodepv) {
				sp->pv->insertPV(ply, move, pv);
			}

			alpha = value;
			sp->bestValue = alpha;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
bool ChessSearchYBWC::splitSearch(ChessSearch *search, 
	ChessMoveOrder *moveorder, int alpha, int beta, int depth, int ply, 
	int nummove, bool nodepv, bool incheck, bool rootnode, bool fpruning, 
	int fmargin, int *bestvalue, ChessMove *bestmove)
{
	assert(search->thread->smp == SMPMode::ybwc);
	auto *threadybwc = static_cast<ChessSearchThreadYBWC *>(search->thread);
	auto &threads = search->threads;

	// create a splitpoint
	ChessSearchSplitPoint *sp =
		&threadybwc->listSplitPoint[threadybwc->countSplit];

	sp->parentSplitPoint = threadybwc->splitPoint;
	sp->parentThread = threadybwc;
	sp->board = &search->board;
	sp->moveorder = moveorder;
	sp->repetition = &search->repetition;
	sp->searchStack = &search->searchStack;
	sp->pv = &search->pv;
	sp->depth = depth;
	sp->ply = ply;
	sp->fmargin = fmargin;
	sp->alpha = alpha;
	sp->beta = beta;
	sp->nodepv = nodepv;
	sp->incheck = incheck;
	sp->rootnode = rootnode;
	sp->fpruning = fpruning;
	sp->cutoff = false;
	sp->numMove = nummove;
	sp->bestMove = bestmove->data();
	sp->bestValue = *bestvalue;
	sp->childMask = 1ULL << threadybwc->getSearchId();
	sp->countChildren = 1;

	threads.lock();
	sp->mutex.lock();

	// we also search at this splitpoint
	threadybwc->splitPoint = sp;
	++threadybwc->countSplit;

	// if threads are available, associate splitpoint and start searching
	ChessSearchThreadYBWC *th = nullptr;
	while ((th = static_cast<ChessSearchThreadYBWC *>(
			threads.findAvailableThread(threadybwc))) != nullptr) {
		sp->childMask |= (1ULL << th->getSearchId());
		++sp->countChildren;
		th->splitPoint = sp;
		th->searching = true;
		th->notify();
	}

	sp->mutex.unlock();
	threads.unlock();

	// start searching
	threadybwc->run(sp);

	threads.lock();
	sp->mutex.lock();

	threadybwc->searching = true;

	// remove splitpoint
	--threadybwc->countSplit;
	threadybwc->splitPoint = sp->parentSplitPoint;

	*bestvalue = sp->bestValue;
	bestmove->set(sp->bestMove);

	sp->mutex.unlock();
	threads.unlock();

	// return to the search that called split, and all moves was searched
	return true;
}

////////////////////////////////////////////////////////////////////////////////
ChessSearchThreadYBWC::ChessSearchThreadYBWC(int id, ChessSearchThreadList &th,
	ChessEngine &e) : ChessSearchThread(id, SMPMode::ybwc, th, e),
	splitPoint(nullptr), countSplit(0)
{
	hashTablePawn.clear();
}

////////////////////////////////////////////////////////////////////////////////
bool ChessSearchThreadYBWC::cutoff() const
{
	ChessSearchSplitPoint *sp = splitPoint;
	while (sp != nullptr) {
		if (sp->cutoff) {
			return true;
		}
		sp = sp->parentSplitPoint;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessSearchThreadYBWC::isAvailable(ChessSearchThread *mainthread)
{
	// the thread is already searching
	if (searching) {
		return false;
	}

	const int count = countSplit;

	// we have no splitpoints, we wan help other threads
	if (count == 0) {
		return true;
	}

	// the helpful parent concept :
	// we must only help the threads working on the last splitpoint
	// we created because we want them to finish as fast as possible
	return ((listSplitPoint[count - 1].childMask &
			 (1ULL << mainthread->getSearchId())) != 0ULL);
}

////////////////////////////////////////////////////////////////////////////////
// thread main function
void ChessSearchThreadYBWC::func()
{
	if (isMainThread()) {

		// main YBWC thread
		while (!terminate) {

			std::unique_lock<std::mutex> lk(mutex);
			thinking = false;

			threads.notify();
			cond.wait(lk, [&]{ return thinking || terminate; });
			lk.unlock();

			if (!terminate) {

				searching = true;

				threads.resetNodeCount();
				threads.clearHistory();

				ChessMoveOrder moveOrderRoot(this->history);

				// generate legal moves and check book + tablebases
				if (engine.search(moveOrderRoot)) {

					// TODO : don't allocate in stack
					ChessSearch searchMain(engine, threads, engine.getEval(),
						engine.getHashTable(), this, engine.getBoardRef(),
						engine.getRepetitions(), engine.mustCheckNodes());

					// start iterative deepening search
					searchMain.startSearch(moveOrderRoot);

					// send result
					engine.onEndSearch(bestMove, ponderMove, searchDepth, 
						threads.getMaxPly(), threads.getNodeCount(), 
						engine.getSearchTime());
				}

				searching = false;
			}
		}

	} else {

		// YBWC child threads
		run(nullptr);
	}
}

////////////////////////////////////////////////////////////////////////////////
// split != 0 when this function is called manually
void ChessSearchThreadYBWC::run(ChessSearchSplitPoint *split)
{
	while (!terminate) {

		while (searching) {

			threads.lock();
			ChessSearchSplitPoint *sp = splitPoint;
			threads.unlock();

			// create a new search for this split
			// TODO : don't allocate in stack 
			ChessSearchYBWC search(engine, threads, engine.getEval(),
				engine.getHashTable(), this, *(sp->board), *sp->repetition,
				false);

			search.copySearchStack(*(sp->searchStack), sp->ply);

			// copy killer moves
			int p = std::max(sp->ply - 2, 0);

			history.copyKillers(sp->parentThread->history, p,
							    max_killer_depth - p);

			sp->mutex.lock();

			// start searching at the splitpoint
			if (sp->rootnode) {
				search.searchSMPRoot(sp->alpha, sp->beta, sp->depth);
			} else {
				search.searchSMP(sp->alpha, sp->beta, sp->depth,
								 sp->ply, sp->nodepv, sp->incheck);
			}

			searching = false;

			sp->childMask &= ~(1ULL << stid);
			--sp->countChildren;

			// if the main thread finished before the children
			// and all children have finish, wake up the parent thread
			if ((sp->parentThread != this) &&
				(sp->childMask == 0ULL)) {
				sp->parentThread->notify();
			}

			sp->mutex.unlock();

			// TODO : "late join"
			// after search, try to find another available splitpoint to help

		}

		std::unique_lock<std::mutex> lk(mutex);

		// if we created the splitpoint and all children have finished, return
		// (will return to splitsearch to end the split)
		// else wait until another thread need our help
		if ((split != nullptr) &&
			(split->parentThread == this) &&
			(split->childMask == 0ULL)) {
			// break will release the mutex and free unique_lock
			break;
		}

		// wait until a thread need our help at splitpoint
		// or when the function is called manually
		cond.wait(lk, [&]{
			if ((split != nullptr) &&
				(split->parentThread == this) &&
				(split->childMask == 0ULL)) {
				return true;
			}
			return (searching || terminate);
		});
	}
}

} // namespace Cheese

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_HASH_H_
#define CHEESE_HASH_H_

#include "config.h"

// * note : need eval.h only for mate_value
#include "eval.h"
#include "move.h"
#include "zobrist.h"

#include <array>
#include <memory>

// use the lockless hash idea
#define USE_LOCKLESS_HASH

// use hashfull stat
#define USE_HASHFULL

// cache alignemnt
#define USE_CACHE_ALIGNMENT

// define to use stats on hash table
//#define HASH_STATS

namespace Cheese {

constexpr int cache_aligned_value = 64;

// number of hash cluster
constexpr int nb_hash_cluster = 4;

// hash table default size (in Mb)
constexpr int hashtable_size_default = 32;

// hash table minimum size (in Mb)
constexpr int hashtable_size_min = 1;

// hash table maximum size (in Mb)
// in 32 bits, max allocated size < 4 Gb
#ifdef SYSTEM64BIT
constexpr int hashtable_size_max = 65536;
#else
constexpr int hashtable_size_max = 2048;
#endif

// default size for pawn hash table (in Kb)
constexpr int hashtable_pawn_size = 2048;

// maximum value for full hash table
constexpr int hashfull_max = 1000;

////////////////////////////////////////////////////////////////////////////////
// hash node type
enum class HashNodeType {

	// exact value
	hash_exact,

	// lower bound
	hash_alpha,

	// upper bound
	hash_beta,

	// hash entry not found
	hash_notfound
};

class ChessHashTable;

////////////////////////////////////////////////////////////////////////////////
// size = 64 bits key + 64 bits data = 128 bits = 16 bytes
struct ChessHashNode {

private:

	enum {
		shift_depth = 0,
		shift_age   = 8,
		shift_score = 16,
		shift_move  = 32,
		shift_type  = 56,

		mask_depth = 0xff,
		mask_age   = 0xff,
		mask_score = 0xffff,
		mask_move  = 0x7fffff,
		mask_type  = 0x03
	};

	// hash key (64 bits)
	HashKey key;

	// data (64 bits)

	// high 32 bits part :
	// 00000000 0xxxxxxx xxxxxxxx xxxxxxxx ...  = move   (23 bits)
	// 000000xx 00000000 00000000 00000000 ...  = type   (2 bits)
	// ??????00 ?0000000 00000000 00000000 ...  = unused (7 bits)

	// low 32 bits part :
	// ... 00000000 00000000 00000000 xxxxxxxx  = depth (8 bits) in 1/2 PLY
	// ... 00000000 00000000 xxxxxxxx 00000000  = age   (8 bits)
	// ... xxxxxxxx xxxxxxxx 00000000 00000000  = score (16 bits)

	uint64_t data;

public:

	friend class ChessHashTable;

	ChessHashNode();

	ChessHashNode(HashKey k, ChessMove mv, HashNodeType type, int depth,
		int score, int age);

	void clear();

	void setData(ChessMove mv, HashNodeType type, int depth,
		int score, int age);

	// set hash key
	void setKey(HashKey k);

	// get type of node
	HashNodeType getType() const;

	// get search depth
	int getDepth() const;

	// get age
	int getAge() const;

	// set age
	void setAge(int age);

	// get score
	int getScore() const;

	// get hash key
	HashKey getKey() const;

	// get move
	ChessMove getMove() const;

	// read entry key (support lockless)
	HashKey readKey() const;

	// read entry (support lockless)
	void read(ChessHashNode &node);

	// write entry (support lockless)
	void write(const ChessHashNode &node);

};

////////////////////////////////////////////////////////////////////////////////
class ChessHashTable
{
private:

	// hash table (aligned)
	ChessHashNode *hashtable {nullptr};

	// hash table size
	std::size_t size {0};

	// hash table modulo
	std::size_t sizeModulo {0};

	// age
	int age {0};

	// stats
#ifdef HASH_STATS
	std::atomic_uint64_t statExact {};
	std::atomic_uint64_t statAlpha {};
	std::atomic_uint64_t statBeta {};
	std::atomic_uint64_t statMiss {};
	std::atomic_uint64_t statMissKey {};
	std::atomic_uint64_t statMissDepth {};
	std::atomic_uint64_t statStore {};
	std::array<std::atomic_uint64_t, nb_hash_cluster> statStoreBucket {};
#endif

public:

	ChessHashTable() = default;

	~ChessHashTable();

	// create hash keys, and init hash table
	bool init(int sizehash);

	// free hash table and keys
	void release();

	// clear hash table
	void clear();

	// must be called at initialisation of the search if using
	// the age parameter in the hash
	void initSearch();

	// adjust score for mate after reading from the hashtable
	int adjustScoreFrom(int score, int ply) const;

	// adjust score for mate before writing to the hashtable
	int adjustScoreTo(int score, int ply) const;

#ifdef USE_HASHFULL
	// hash full approximation
	uint64_t getHashFull() const;
#endif

	// get hash node
	ChessHashNode *getHashNode(HashKey key) const;

	// probe hash table
	HashNodeType findNode(HashKey key, int depth, int ply, int alpha, int beta,
		ChessMove &cm, int &v);

	// add new hash entry in the hash table
	void addNode(HashKey key, ChessMove move, int score, int depth,
		int ply, HashNodeType type);

	// get best move from hash table
	ChessMove getBestMove(HashKey key) const;

};

////////////////////////////////////////////////////////////////////////////////
// Pawn hashtable entry :
//
// key      =  8 bytes
// passed   = 16 bytes
// evalmid  =  4 bytes
// evalend  =  4 bytes
//
//  Total   = 32 bytes
//
struct ChessPawnHashNode {

public:

	// hashkey (64 bits)
	HashKey key;

	// passed pawns
	std::array<uint64_t, nb_sides> passed;

	// pawn evaluation (without passed pawns eval)
	int32_t evalmid;

	int32_t evalend;

	ChessPawnHashNode() = default;

	void setKey(HashKey k);

	HashKey getKey() const;

};

// 2048 Kb / 32 bytes => 65536 elements
constexpr int hashtable_pawn_nb_entry = ((hashtable_pawn_size * 1024) /
	sizeof(ChessPawnHashNode));

////////////////////////////////////////////////////////////////////////////////
class ChessPawnHashTable {

private:

	// hash table
	std::array<ChessPawnHashNode, hashtable_pawn_nb_entry> hashtable;

#ifdef HASH_STATS
	std::atomic_uint64_t statFound{};
	std::atomic_uint64_t statMiss{};
	std::atomic_uint64_t statStore{};
#endif

public:

	ChessPawnHashTable() = default;

	~ChessPawnHashTable() = default;

#ifdef HASH_STATS
	void printStat();
#endif

	// clear hash table
	void clear();

#ifdef HASH_STATS
	void addStatFound();

	void addStatMiss();

	void addStatStore();
#endif

	// get hash node
	ChessPawnHashNode *getHashNode(HashKey key);
};

////////////////////////////////////////////////////////////////////////////////
inline void ChessHashNode::clear()
{
	key = 0ULL;
	data = 0ULL;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHashNode::setData(ChessMove mv, HashNodeType type, int depth,
	int score, int age)
{
	const auto sc = static_cast<uint16_t>(score);
	data = (static_cast<uint64_t>(mv.data()) << shift_move) |
		   (static_cast<uint64_t>(sc) << shift_score) |
		   (static_cast<uint64_t>(age) << shift_age) |
		   (static_cast<uint64_t>(depth) << shift_depth) |
		   (static_cast<uint64_t>(type) << shift_type);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHashNode::setKey(HashKey k)
{
	key = k;
}

////////////////////////////////////////////////////////////////////////////////
inline HashNodeType ChessHashNode::getType() const
{
	return static_cast<HashNodeType>((data >> shift_type) & mask_type);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessHashNode::getDepth() const
{
	return static_cast<int>((data >> shift_depth) & mask_depth);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessHashNode::getAge() const
{
	return static_cast<int>((data >> shift_age) & mask_age);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHashNode::setAge(int age)
{
	data = (data & ~(static_cast<uint64_t>(mask_age) << shift_age)) |
		   (static_cast<uint64_t>(age) << shift_age);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessHashNode::getScore() const
{
	auto sc = static_cast<int16_t>((data >> shift_score) & mask_score);
	return static_cast<int>(sc);
}

////////////////////////////////////////////////////////////////////////////////
inline HashKey ChessHashNode::getKey() const
{
	return key;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessHashNode::getMove() const
{
	//return ChessMove(static_cast<uint32_t>((data >> shift_move) & mask_move));
	return { static_cast<uint32_t>((data >> shift_move) & mask_move) };
}

////////////////////////////////////////////////////////////////////////////////
inline HashKey ChessHashNode::readKey() const
{
#ifdef USE_LOCKLESS_HASH
	return (key ^ data);
#else
	return key;
#endif
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHashNode::read(ChessHashNode &node)
{
#ifdef USE_LOCKLESS_HASH
	node.key = key ^ data;
	node.data = data;
#else
	node.key = key;
	node.data = data;
#endif
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHashNode::write(const ChessHashNode &node)
{
#ifdef USE_LOCKLESS_HASH
	key = node.key ^ node.data;
	data = node.data;
#else
	key = node.key;
	data = node.data;
#endif
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessHashTable::initSearch()
{
	age = (age + 1) & ChessHashNode::mask_age;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessHashTable::adjustScoreFrom(int score, int ply) const
{
	if (score > max_score_mate) {
		return (score - ply);
	}

	if (score < min_score_mate) {
		return (score + ply);
	}

	return score;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessHashTable::adjustScoreTo(int score, int ply) const
{
	if (score > max_score_mate) {
		return (score + ply);
	}

	if (score < min_score_mate) {
		return (score - ply);
	}

	return score;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessHashNode *ChessHashTable::getHashNode(HashKey key) const
{
	return hashtable + static_cast<uintptr_t>(key & sizeModulo);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessPawnHashNode::setKey(HashKey k)
{
	key = k;
}

////////////////////////////////////////////////////////////////////////////////
inline HashKey ChessPawnHashNode::getKey() const
{
	return key;
}

#ifdef HASH_STATS
////////////////////////////////////////////////////////////////////////////////
inline void ChessPawnHashTable::addStatFound()
{
	++statFound;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessPawnHashTable::addStatMiss()
{
	++statMiss;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessPawnHashTable::addStatStore()
{
	++statStore;
}
#endif

////////////////////////////////////////////////////////////////////////////////
inline ChessPawnHashNode *ChessPawnHashTable::getHashNode(HashKey key)
{
	const auto i = static_cast<uintptr_t>(key & (hashtable_pawn_nb_entry - 1));
	return &hashtable[i];
}

} // namespace Cheese

#endif //CHEESE_HASH_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "search.h"

#include "engine.h"
#include "util/logfile.h"
#include "moveorder.h"
#include "ybwc.h"

#ifdef USE_TABLEBASES
#include "syzygy/tbprobe.h"
#endif

#include <cassert>
#include <cmath>
#include <iostream>

namespace Cheese {

// default value for aspiration window
constexpr int search_window = 30;

// maximum window value before using full window
constexpr int max_search_window = 640;

// time before sending currmove to gui (ms)
constexpr uint64_t wait_send_current_move = 3000;

#ifdef USE_NULLMOVES_VERIFICATION
constexpr int nullmoves_verification_depth = 12 * one_ply;
#endif

// maximum depth for razoring
constexpr int razoring_depth = 4;

#ifdef USE_DELTA_PRUNING
constexpr int delta_pruning_margin = 200;
#endif

// min depth for internal iterative deepening
constexpr int internal_iterative_deepening_depth = 5;

constexpr int futility_pruning_depth = 4;

constexpr int extend_singleresponse = one_ply * 1;

// futility prunning margins (and reverse futility)
const std::array<int, 8> futility_margin =
	{ 0, 100, 200, 300, 975, 975, 975, 975 };

// razoring margins
const std::array<int, 8> razor_margin =
	{ 0, 200, 400, 600, 975, 975, 975, 975 };

#ifdef USE_LATE_MOVE_REDUCTION
// LMR reduction table [depth][moveposition]
std::array<std::array<uint32_t, lmr_max_depth>, lmr_max_moves>
	ChessSearch::lmr_reduction;
#endif

#ifdef USE_LATE_MOVE_PRUNING
std::array<int, lmp_max_depth> ChessSearch::lmp_count;
#endif

////////////////////////////////////////////////////////////////////////////////
ChessSearch::ChessSearch(ChessEngine &e, ChessSearchThreadList &ths,
	ChessEval &ev, ChessHashTable &ht, ChessSearchThread *th,
	const ChessBoard &pos, const ChessRepetitions &rep, bool chknodes) :
	engine(e), threads(ths), eval(ev), hashTable(ht), thread(th), board(pos), 
	repetition(rep), needCheckNodes(chknodes)
{
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearch::init()
{
#ifdef USE_LATE_MOVE_REDUCTION
	// init lmr reduction table
	for (int d=0; d<lmr_max_depth; d++) {
		for (int n=0; n<lmr_max_moves; n++) {
			int v = 0;
			if ((d > 0) && (n > 0)) {
				v = static_cast<int>(floor((log(d) * log(n)) / 2.30));
				v = std::clamp(v, 0, 10);
			}
			lmr_reduction[d][n] = v;
		}
	}
#endif

#ifdef USE_LATE_MOVE_PRUNING
	for (int n=0; n<lmp_max_depth; n++) {
		lmp_count[n] = static_cast<int>(floor(4.0 + (n * n) * 0.9));
	}
#endif
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearch::startSearch(ChessMoveOrder &mo)
{
	ChessMoveOrder &moveOrderRoot = mo;

	// sort moves at root
	pv.clear(0);
	moveOrderRoot.initSearchRoot(this, board);

	// reset node count again because initSearchRoot call quiescent search
	thread->resetNodeCount();

	int maxmpv = std::min<int>(engine.multipv, moveOrderRoot.getMoveCount());
	maxmpv = std::min<int>(maxmpv, max_multipv);

	// force multipv to 1 in tournament
	if ((!thread->isMainThread()) ||
		(engine.getSearchMode() == SearchMode::tournament)) {
		maxmpv = 1;
	}

	ChessMultiPV mpv;
	mpv.init(maxmpv);

	ChessMoveOrder moveorder(thread->history);
	uint64_t searchtime = 0;

	ChessMove bestmove(0);
	ChessMove ponderMove(0);

	thread->initSearch();

	bool allowSendCurrMove = false;

	Timer timerIteration(engine.timerSearch);

	// iterative deepening
	int	depth = 1;
	int maxdepth = (thread->isMainThread()) ? engine.ideepMaxDepth : max_depth;

	for (depth=1; depth<=maxdepth; depth++) {

		moveorder.copyMoves(moveOrderRoot);
		mpv.updateScores();

		int value = 0;

		// multi-pv loop
		for (int ipv=0; ipv<maxmpv; ipv++) {

			value = mpv.getInfo(ipv).score;

			int alpha = -mate_value - 1;
			int beta  =  mate_value + 1;

			// aspiration window after depth 5
			int dw = search_window;
			if (depth > 5) {
				dw = search_window;
				alpha = std::max(value - dw, -mate_value - 1);
				beta  = std::min(value + dw,  mate_value + 1);
			}

			// aspiration window search
			int failtype = 0;
			do {

				failtype = 0;
				pv.clear(0);

				if ((thread->isMainThread()) &&
					(allowSendCurrMove)) {
					engine.onSendInfoDepth(depth);
				}

				// start search at root
				int v = searchRoot(alpha, beta, depth * one_ply, moveorder,
					allowSendCurrMove);

				searchtime += timerIteration.getElapsedTime();
				timerIteration.start();

				// update multipv
				mpv.setScorePrev(ipv, v);
				if (!pv.isEmpty(0)) {
					mpv.copyPV(ipv, pv.getPV(0), pv.size(0), depth);
				}

				if (threads.mustStop()) {
					value = v;
					break;
				}

				// wait 2 sec to allow sending currmove to uci
				if ((thread->isMainThread()) &&
					(searchtime > wait_send_current_move)) {
					allowSendCurrMove = true;
#ifdef USE_HASHFULL
					engine.onSendHashFull(hashTable.getHashFull());
#endif
				}

				// aspiration window
				if (v <= alpha) {
					failtype = 1;
					alpha = std::max(alpha - dw, -mate_value - 1);
					dw = dw * 2;
					if (dw > max_search_window) {
						alpha = -mate_value - 1;
					}
				} else if (v >= beta) {
					failtype = 2;
					beta  = std::min(beta + dw,  mate_value + 1);
					dw = dw * 2;
					if (dw > max_search_window) {
						beta = mate_value + 1;
					}
				} else {
					value = v;
				}

				// mate => research full window ?
				if (std::abs(v) >= max_score_mate) {
					alpha = -mate_value - 1;
					beta  =  mate_value + 1;
					dw = max_search_window + 1;
				}

				// send current line to gui if aspiration window failed
				if ((thread->isMainThread()) &&
					(failtype) &&
					(!threads.mustStop()) &&
					((allowSendCurrMove) ||
					 (engine.runtestsuite))) {

					for (int i=0; i<maxmpv; i++) {
						engine.onSendMultiPV((maxmpv > 1) ? i + 1 : 0,
							mpv.getInfo(i).depth, mpv.getInfo(i).scorePrev,
							mpv.getInfo(i).move,
							mpv.getInfo(i).nbmove,
							threads.getNodeCount(), searchtime,
							threads.getMaxPly(), failtype,
#ifdef USE_TABLEBASES
							threads.getTBHits());
#else
							0);
#endif
					}
				}

			} while ((!threads.mustStop()) &&
					 (failtype));

			// send current line to gui
			if ((thread->isMainThread()) &&
				(!pv.isEmpty(0))) {

#ifdef USE_TEST_PV
				testPV(mpv.getInfo(0).move.data(), mpv.getInfo(0).nbmove);
#endif
				if (maxmpv > 1) {
					mpv.sortMultiPVLines();
				}

				if ((maxmpv == 1) ||
					(ipv == (maxmpv - 1)) ||
					(allowSendCurrMove)) {
					for (int i=0; i<maxmpv; i++) {
						engine.onSendMultiPV((maxmpv > 1) ? i + 1 : 0,
							mpv.getInfo(i).depth, mpv.getInfo(i).scorePrev,
							mpv.getInfo(i).move,
							mpv.getInfo(i).nbmove,
							threads.getNodeCount(), searchtime,
							threads.getMaxPly(), 0,
#ifdef USE_TABLEBASES
							threads.getTBHits());
#else
							0);
#endif
					}
				}
			}

			if (threads.mustStop()) {
				break;
			}

			// exclude the current best move in next multi-pv search
			if ((thread->isMainThread()) &&
				((ipv + 1) < maxmpv)) {
				moveorder.excludeMove(pv.getMove(0, 0));
			}

		} // end multipv loop

		// special stop conditions in tournament
		if ((thread->isMainThread()) &&
			(engine.getSearchMode() == SearchMode::tournament) &&
			(!engine.pondering) &&
			(!threads.mustStop())) {

			// only one move possible => stop searching
			if (moveOrderRoot.getMoveCount() == 1) {
				threads.stopSearch();
			}

			// if found mate  : stop searching
			if ((depth >= 6) &&
				((value <= min_score_mate) ||
				 (value >= max_score_mate))) {
				threads.stopSearch();
			}

			// try to guess if we have enough time to end the next search
			if ((depth > 1)
				&& (searchtime > static_cast<uint64_t>(engine.ideepTimeByMove
					* 0.60))) {
				threads.stopSearch();
			}
		}

		if (mpv.getInfo(0).size() != 0) {

			thread->updateSearch(depth, mpv.getInfo(0).getMove(0),
				mpv.getInfo(0).getMove(1), mpv.getInfo(0).scorePrev);

			// update best move & ponder move
			bestmove = mpv.getInfo(0).getMove(0);

			if (mpv.getInfo(0).size() > 1) {
				ponderMove = mpv.getInfo(0).getMove(1);
			} else {
				ponderMove.clear();
			}

			// push best move on top of root moves list
			moveOrderRoot.updateRootMove(mpv.getInfo(0).getMove(0));
		}

		if (threads.mustStop()) {
			break;
		}

		// let the main thread finish the search if we reach max depth
		if ((!thread->isMainThread()) &&
			(depth == maxdepth)) {
			--depth;
		}

	} // end depth loop

	// wait when pondering and reaching max depth
	while ((thread->isMainThread()) &&
			(engine.pondering) &&
			(!threads.mustStop())) {
	}
	engine.pondering = false;

	threads.stopSearch();

	--depth;

	// no bestmove take the first move in the list (should never happens)
	if (bestmove.isEmpty()) {
		bestmove = moveOrderRoot.getMove(0);
#ifdef USELOG_DEBUG
		LOG_DEBUG << "no best move after search !";
#endif
	}
}

////////////////////////////////////////////////////////////////////////////////
// search at root node
int ChessSearch::searchRoot(int alpha, int beta, int depth,
	ChessMoveOrder &moveorder, bool sendRootMove)
{
	int	ply = 0;
	bool incheck = board.inCheck();
	int nm = 0;
	int	value = 0;

	// real depth
	int rdepth = depth / one_ply;

	std::array<ChessMove, max_history_quiets> searchedMoves;
	int countSearched = 0;

	ChessMove bestMove(0);
	ChessMoveRec rec;

	clearPV(ply);

	while (nm < moveorder.getMoveCount()) {

		if (needCheckNodes && (!engine.checkNodes())) {
			return 0;
		}

		ChessMove move = moveorder.getMove(nm);

		repetition.push(board.getHashKey());

		// make move (legality test done in orderRoot)
		board.makeMove(move, rec);

		searchStack[ply].move = move;

		// increase number of nodes searched
		++thread->countNodes;

		// send currmove to GUI
		if (sendRootMove) {
			engine.searchingRootMove(move, nm + 1);
		}

		bool chk = board.inCheck();

		int ext = 0;

#ifdef USE_EXTENSION_CHECK
		if (chk) {
			ext += extend_check;
		}
#endif

#ifdef USE_EXTENSION_PAWNON7TH
		if (move.isMovePawnOn7th()) {
			ext += extend_pawnon7th;
		}
#endif

#ifdef USE_EXTENSION_SINGLERESPONSE
		if (moveorder.getMoveCount() == 1) {
			ext += extend_singleresponse;
		}
#endif

		// limit extension to 1 ply
		ext = std::min<int>(ext, (one_ply * 1));

		int newdepth = depth + ext - one_ply;

		// Principal Variation Search (PVS)
		// note : no LMR or LMP at root
		if (nm > 0) {
			value = -search(-alpha - 1, -alpha, newdepth, ply + 1,
							false, chk);
		}

		// pvs research, or full window search for first move
		if ((nm == 0) ||
			((value > alpha) && (value < beta))) {
			value = -search(-beta, -alpha, newdepth, ply + 1, true, chk);
		}

		board.unMakeMove(move, rec);

		repetition.pop();

		if ((threads.mustStop()) || (thread->cutoff())) {
			return alpha;
		}

		++nm;

		if (value > alpha) {

			alpha = value;
			bestMove = move;

			// update pv (before beta cut or we have empty pv)
			pv.insertPV(ply, move, pv);

			if (value >= beta) {
				break;
			}
		}

		if ((!move.isCaptureOrPromotion())
			&& (countSearched < max_history_quiets)) {
				searchedMoves[countSearched++] = move;
		}

		// YBWC : split the search
		if ((threads.getMaxThreads() > 1)
			&& (thread->smp == SMPMode::ybwc)
			&& (depth > engine.getMinSplitDepth())
			&& (nm < (moveorder.getMoveCount() - 1))
			&& (thread->canSplit())
			&& (threads.isThreadAvailable(thread))) {

			if (ChessSearchYBWC::splitSearch(this, &moveorder,
					alpha, beta, depth, ply, nm, true, incheck, true,
					false, 0, &alpha, &bestMove)) {
				break;
			}
		}
	}

	// killer move + history heuristic
	if (alpha >= beta) {
		if ((!bestMove.isCaptureOrPromotion())
			&& (!incheck)) {

			thread->history.addKillerMove(bestMove, ply);

			thread->history.add(bestMove, board.side(), rdepth);

			for (int i=0; i<countSearched; i++) {
				thread->history.sub(searchedMoves[i], board.side(), rdepth);
			}
		}
	}

	// no moves possible : mate or pat
	if (nm == 0) {
		clearPV(ply);
		return (incheck) ? (-mate_value + ply) : engine.getDrawValue(board);
	}

	hashTable.addNode(board.getHashKey(), bestMove, alpha, depth, ply,
		(alpha >= beta) ? HashNodeType::hash_beta :
		bestMove.isEmpty() ? HashNodeType::hash_alpha :
							 HashNodeType::hash_exact);

	return alpha;
}

////////////////////////////////////////////////////////////////////////////////
// alpha-beta search function
int ChessSearch::search(int alpha, int beta, int depth, int ply,
						bool nodepv, bool incheck)
{
	assert(alpha < beta);
	assert((nodepv) || (alpha == beta - 1));
	assert(alpha >= -mate_value - 1);
	assert(beta <= mate_value + 1);
	assert(incheck == board.inCheck());

	if (depth < one_ply) {
		return searchQuiescent(alpha, beta, 0, ply, nodepv, incheck);
	}

	// fix problem with wrong moves in pv
	clearPV(ply);

	// reduce engine strength by reducing search speed
	if (thread->isMainThread() && engine.isReducingSpeed()) {
		engine.reduceSpeed();
	}

	// check time and stop conditions
	if (thread->isMainThread()) {
		if (!engine.checkSearchTime()) {
			return 0;
		}
	} else {
		//if (engine.mustStopSearch()) {
		if (threads.mustStop()) {
			return 0;
		}
	}

	// maximum depth : return eval, beta or draw ?
	if (ply >= (max_depth - 1)) {
		return eval.evaluate(board, thread->hashTablePawn, alpha, beta);
	}

	// insufficient material
	if (board.evaluateDraw()) {
		return engine.getDrawValue(board);
	}

	// fifty moves rule
	if (board.fiftyRule() >= 100) {
		// mate is possible
		if ((!incheck) ||
			(!engine.isMate(board))) {
			return engine.getDrawValue(board);
		}
	}

	// check repetitions
	if (repetition.check(board.getHashKey(), board.fiftyRule())) {
		return engine.getDrawValue(board);
	}

	// maximum ply for uci seldepth
	if (ply > thread->maxPly) {
		thread->maxPly = ply;
	}

	// mate distance pruning
	alpha = std::max<int>(alpha, -mate_value + ply);
	beta = std::min<int>(beta, mate_value - ply);

	if (alpha >= beta) {
		return alpha;
	}

	// search in hashtable
	ChessMove hashmove(0);

	int v = 0;
	HashNodeType r = hashTable.findNode(board.getHashKey(), depth, ply,
							   alpha, beta, hashmove, v);

	if (r != HashNodeType::hash_notfound) {
		return v;
	}

	// real depth
	int rdepth = depth / one_ply;

	// probe tablebases
#ifdef USE_TABLEBASES
	if ((engine.getTBCardinality() > 0) &&
		(board.fiftyRule() == 0) &&
		(!board.canCastle(castle_init))) {

		int nbp = BitBoards::bitCount(board.bitboardAll());
		if ((nbp <= TableBases::TBlargest) &&
			((nbp < TableBases::TBlargest) ||
				(rdepth >= engine.getTBProbeDepth()))) {

			// force to check time, tablebases access is slow
			if (thread->isMainThread()) {
				engine.forceCheckTime();
			}

			int success = 0;
			int wdl = TableBases::probe_wdl(board, &success);

			if (success) {

				++thread->tbhits;

				int drawScore = engine.getTB50MoveRule() ? 1 : 0;
				if (wdl < -drawScore) {
					v = min_score_mate + ply + 1;
				} else if (wdl > drawScore) {
					v = max_score_mate - ply - 1;
				} else {
					v = v * 2 + engine.getDrawValue(board);
				}

				HashNodeType ht = HashNodeType::hash_exact;
				if (wdl < -drawScore) {
					ht = HashNodeType::hash_alpha;
				} else
				if (wdl > drawScore) {
					ht = HashNodeType::hash_beta;
				}

				if ((ht == HashNodeType::hash_exact) ||
					((ht == HashNodeType::hash_alpha) && (v <= alpha)) ||
					((ht == HashNodeType::hash_beta) && (v >= beta))) {

					// save in hashtable
					hashTable.addNode(board.getHashKey(), ChessMove(0), v,
						std::min<int>((max_depth - 1) * one_ply,
						depth + 6 * one_ply), ply, ht);
				}

				return v;
			}
		}
	}
#endif

	// evaluate called once	when needed
	int evalpos = eval_infinity;
	int	value = 0;

	// Razoring
#ifdef USE_RAZORING
	if ((!nodepv)
		&& (!incheck)
		&& (depth < (razoring_depth * one_ply))) {

		evalpos = eval.evaluate(board, thread->hashTablePawn, alpha, beta);

		if ((evalpos + razor_margin[rdepth]) < beta) {

			int rbeta = beta - razor_margin[rdepth];
			value = searchQuiescent(rbeta - 1, rbeta, 0, ply + 1,
				false, incheck);

			if (value < rbeta) {
				return value;
			}
		}
	}
#endif

	// "Reverse futility pruning" / Futility pruning at child node
#ifdef USE_REVERSE_FUTILITY
	if ((!nodepv)
		&& (!incheck)
		&& (depth < (futility_pruning_depth * one_ply))
		&& (std::abs(beta) < max_score_mate)) {

		if (evalpos == eval_infinity) {
			evalpos = eval.evaluate(board, thread->hashTablePawn, alpha, beta);
		}

		if ((evalpos - futility_margin[rdepth]) >= beta) {
			return evalpos;
		}
	}
#endif

	// Null moves pruning
#ifdef USE_NULLMOVES
	if ((!nodepv)
		&& (!incheck)
		&& (!searchStack[ply - 1].move.isEmpty())
		&& (board.gamePhaseCount(board.side()) != 0)
		&& (depth >= (2 * one_ply))
		&& (engine.usingNullMoves())) {

		int depthNullMoves = (3 * one_ply) + (rdepth / 8) * one_ply;

		repetition.push(board.getHashKey());

		ChessMoveRec rec;
		board.makeNullMove(rec);

		searchStack[ply].move.clear();

		int ndepth = depth - depthNullMoves - one_ply;
		value = -search(-beta, -beta + 1, ndepth, ply + 1, false, false);

		board.unMakeNullMove(rec);

		repetition.pop();

		if (threads.mustStop()) {
			return 0;
		}

		if (value >= beta) {

			// verification at high depth
#ifdef USE_NULLMOVES_VERIFICATION
			if ((depth >= nullmoves_verification_depth)) {
				int vf = search(beta - 1, beta, depth - depthNullMoves
					- one_ply, ply, false, false);
				if (vf >= beta) {
#endif
				return value;

#ifdef USE_NULLMOVES_VERIFICATION
				}
			}
#endif
		}
	}
#endif

	// Internal iterative deepening (IID)
#ifdef USE_INTERNAL_ITERATIVE_DEEPENING
	if ((nodepv)
		&& (!incheck)
		&& (hashmove.isEmpty())
		&& (depth >= (internal_iterative_deepening_depth * one_ply))) {

		clearPV(ply);

		value = search(alpha, beta, depth - (2 * one_ply), ply, true, false);

		// from hashtable or pv ?
		hashmove = pv.getMove(ply, 0);
		//hashmove = hashTable.getBestMove(board.getHashKey());
	}
#endif

	if ((threads.mustStop()) || (thread->cutoff())) {
		return 0;
	}

	clearPV(ply);

	// init futility pruning
#ifdef USE_FUTILITY_PRUNING
	bool futilityPruning = false;
	int futilityMargin = 0;

	if ((!nodepv)
		&& (!incheck)
		&& (depth < (futility_pruning_depth * one_ply))
		&& (!isGamePhasePawnEnding(board.gamePhase()))) {

		if (evalpos == eval_infinity) {
			evalpos = eval.evaluate(board, thread->hashTablePawn, alpha, beta);
		}

		if ((evalpos + futility_margin[rdepth]) <= alpha) {
			futilityPruning = true;
			futilityMargin = evalpos + futility_margin[rdepth];
		}
	}
#endif

	bool condLMR = ((depth >= (lmr_min_depth * one_ply))
					&& (!incheck)
					&& (engine.usingLMR()));

	std::array<ChessMove, max_history_quiets> searchedMoves;
	int countSearched = 0;

	ChessMove bestMove(0);

#ifdef USE_COUNTER_MOVE
	ChessMove lastmove = searchStack[ply - 1].move;
#endif

	ChessMoveOrder moveorder(board, thread->history,
				(incheck) ? ordermove_hash_check : ordermove_hash,
				 ply, hashmove
 #ifdef USE_COUNTER_MOVE
				 , lastmove
 #endif
				 );

	const HashKey hk = board.getHashKey();

	ChessMoveRec rec;
	ChessMove move;

	// legal moves count
	int num = 0;
	int movePhase = 0;

	while ((movePhase = moveorder.getNextMove(board, move)) != ordermove_end) {

		if (needCheckNodes && (!engine.checkNodes())) {
			return 0;
		}

		board.makeMove(move, rec);

		if (!board.isPositionLegal()) {
			board.unMakeMove(move, rec);
			continue;
		}

		searchStack[ply].move = move;

		bool chk = board.inCheck();

		// search extensions
		int ext = 0;

#ifdef USE_EXTENSION_CHECK
		if (chk) {
			ext += extend_check;
		}
#endif

#ifdef USE_EXTENSION_PAWNON7TH
		if (move.isMovePawnOn7th()) {
			ext += extend_pawnon7th;
		}
#endif

		// limit extension to 1 ply
		ext = std::min<int>(ext, (one_ply * 1));

		int newdepth = depth + ext - one_ply;

		// Futility Prunning
#ifdef USE_FUTILITY_PRUNING

		bool pruning = false;

		if (futilityPruning) {

			if ((num > 0)
				&& (!chk)
				&& (ext == 0)
				&& (!move.isMovePawnPast4th(~board.side()))
				&& (!move.isCaptureOrPromotion())) {

				if (futilityMargin <= alpha) {
					pruning = true;
				}
			}
		}

		// late moves pruning (LMP)
#ifdef USE_LATE_MOVE_PRUNING
		if ((movePhase == ordermove_next_quiet)
			&& (!incheck)
			&& (ext == 0)
			&& (!chk)
			&& (!nodepv)
			&& (!move.isMovePawnPast4th(~board.side()))
			&& (depth < lmp_max_depth * one_ply)) {

			if (num > lmp_count[rdepth]) {
				pruning = true;
			}
		}
#endif
		if (pruning) {
			++num;
			board.unMakeMove(move, rec);
			continue;
		}
#endif

		// increase number of nodes searched (after pruning)
		++thread->countNodes;

		repetition.push(hk);

		// Principal Variation Search (PVS)
		if (num > 0) {

			// Late Move Reduction (LMR)
#ifdef USE_LATE_MOVE_REDUCTION
			value = alpha + 1;
			if ((condLMR)
				&& (num >= lmr_min_moves)
				&& (!chk)
				&& (movePhase == ordermove_next_quiet)
				&& (ext == 0)) {

				uint32_t lr = lmr_reduction[std::min(rdepth,
					lmr_max_depth - 1)][std::min(num, lmr_max_moves - 1)];
				if (!nodepv) {
					lr++;
				}

				if (lr) {

					int ld = newdepth - lr * one_ply;

					// LMR search
					value = -search(-alpha - 1, -alpha, ld, ply + 1,
								false, false);
				}
			}

			if (value > alpha) {

#endif //USE_LATE_MOVE_REDUCTION

				// PVS search
				value = -search(-alpha - 1, -alpha, newdepth, ply + 1,
							false, chk);

#ifdef USE_LATE_MOVE_REDUCTION
			} 
#endif
		}

		// PVS re-search, or full window search for the first move
		if ((num == 0) ||
			((value > alpha) && (value < beta))) {
			value = -search(-beta, -alpha, newdepth, ply + 1, nodepv, chk);
		}

		++num;

		board.unMakeMove(move, rec);

		repetition.pop();

		if ((threads.mustStop()) || (thread->cutoff())) {
			return 0;
		}

		if (value > alpha) {

			alpha = value;
			bestMove = move;

			if (value >= beta) {
				break;
			}

			// update pv
			if (nodepv) {
				pv.insertPV(ply, move, pv);
			}
		}

		if ((!move.isCaptureOrPromotion()) &&
			(countSearched < max_history_quiets)) {
				searchedMoves[countSearched++] = move;
		}

		// YBWC : split the search
		if ((threads.getMaxThreads() > 1)
			&& (thread->smp == SMPMode::ybwc)
			&& (depth > engine.getMinSplitDepth())
			&& (thread->canSplit())
			&& (threads.isThreadAvailable(thread))) {
			if (ChessSearchYBWC::splitSearch(this, &moveorder,
					alpha, beta, depth, ply, num, true, incheck, false,
#ifdef USE_FUTILITY_PRUNING
					futilityPruning, futilityMargin,
#else
					0, 0,
#endif
					&alpha, &bestMove)) {

				if ((threads.mustStop()) || (thread->cutoff())) {
					return 0;
				}

				break;
			}
	   }
	}

	// update killer move + history heuristic
	if (alpha >= beta) {

		if ((!bestMove.isCaptureOrPromotion())
			&& (!incheck))
		{
			thread->history.addKillerMove(bestMove, ply);

			thread->history.add(bestMove, board.side(), rdepth);

			for (int i=0; i<countSearched; i++) {
				thread->history.sub(searchedMoves[i], board.side(), rdepth);
			}

#ifdef USE_COUNTER_MOVE
			thread->history.setHistoryMove(board.side(), lastmove, bestMove);
#endif
		}
	}

	// if no moves possible : mate or pat
	if (num == 0) {
		clearPV(ply);
		return (incheck) ? (-mate_value + ply) : engine.getDrawValue(board);
	}

	// save result in hash table
	hashTable.addNode(board.getHashKey(),
		bestMove.isEmpty() ? hashmove : bestMove,
		alpha, depth, ply,
		(alpha >= beta) ? HashNodeType::hash_beta :
			bestMove.isEmpty() ? HashNodeType::hash_alpha :
								 HashNodeType::hash_exact);

	return alpha;
}

////////////////////////////////////////////////////////////////////////////////
// alphabeta quiescence search
int	ChessSearch::searchQuiescent(int alpha, int beta, int depth, int ply,
	bool nodepv, bool incheck)
{
	assert(depth <= 0);
	assert(alpha < beta);
	assert((nodepv) || (alpha == beta - 1));
	assert(alpha >= -mate_value - 1);
	assert(beta <= mate_value + 1);
	assert(incheck == board.inCheck());

	if (ply >= (max_depth - 1)) {
		return engine.getDrawValue(board);
	}

	// fix problem with wrong moves in pv
	pv.clear(ply);

	// check time and stop conditions
	if (thread->isMainThread()) {
		if (!engine.checkSearchTime()) {
			return 0;
		}
	} else {
		if (threads.mustStop()) {
			return 0;
		}
	}

	// test draw
	if (board.evaluateDraw()) {
		return engine.getDrawValue(board);
	}

	// fifty moves rule
	if (board.fiftyRule() >= 100) {
		// mate is possible
		if (!engine.isMate(board)) {
			return engine.getDrawValue(board);
		}
	}

	// repetitions
	if (repetition.check(board.getHashKey(), board.fiftyRule())) {
		return engine.getDrawValue(board);
	}

	int value = 0;

	if (!incheck) {

		// evaluate position : stand pat
		value = eval.evaluate(board, thread->hashTablePawn, alpha, beta);

		if (value > alpha) {
			if (value >= beta) {
				return value;
			}
			alpha = value;
		}
	}

#ifdef USE_QUIESCENCE_CHECKS
	ChessMoveOrder moveorder(board, thread->history,
		(incheck) ? ordermove_init_quiescence_evasion :
					ordermove_init_quiescence_capture,
			(!incheck) &
			(depth > depth_checks_quiescence));
#else
		(incheck) ? ordermove_init_quiescence_evasion :
					ordermove_init_quiescence_capture,
			false);
#endif

#ifdef USE_DELTA_PRUNING
	bool usepruning = false;
	int value_pruning;
	if ((!nodepv) &&
		(!incheck) &&
		(isGamePhaseBeforeEnding(board.gamePhase))
	) {
		usepruning = true;
		value_pruning = value + delta_pruning_margin;
	}
#endif

	const HashKey hk = board.getHashKey();

	ChessMove move;
	ChessMoveRec rec;

	int num = 0;

	// for each moves
	while (moveorder.getNextMove(board, move) != ordermove_end) {

		board.makeMove(move, rec);
		if (!board.isPositionLegal()) {
			board.unMakeMove(move, rec);
			continue;
		}

		searchStack[ply].move = move;

		++num;

		++thread->countNodes;

		bool chk = board.inCheck();

#ifdef USE_DELTA_PRUNING
		if ((usepruning)
			&& (num > 0)
			&& (!incheck)
			&& (!chk)
			&& (!move.isPromotion())
		) {
			int v = value_pruning + pieces_value[move.capture()];
			if (v <= alpha) {
				board.unMakeMove(move, rec);
				continue;
			}
		}
#endif

		repetition.push(hk);

		value = -searchQuiescent(-beta, -alpha, depth - 1, ply + 1, nodepv,
			chk);

		board.unMakeMove(move, rec);

		repetition.pop();

		if (threads.mustStop()) {
			return 0;
		}

		if (value > alpha) {

			if (value >= beta) {
				return value;
			}

			if (nodepv) {
				pv.insertPV(ply, move, pv);
			}

			alpha = value;
		}
	}

	if ((incheck) && (num == 0)) {
		pv.clear(ply);
		return (-mate_value + ply);
	}

	return alpha;
}

#ifdef USE_TEST_PV
////////////////////////////////////////////////////////////////////////////////
void ChessSearch::testPV(ChessMove *moves, int sz)
{
	std::array<ChessMoveRec, max_depth_pv> rec;
	int n;
	for (n=0; n<sz; n++) {
		if (board.validMove(moves[n])) {
			board.makeMove(moves[n], rec[n]);
			if (!board.isPositionLegal()) {
				LOG_DEBUG << "error move : " << moves[n].text();
				++n;
				break;
			}
		} else {
			LOG_DEBUG << "invalid move : " << moves[n].text();
			break;
		}
	}
	--n;
	while (n >= 0) {
		board.unMakeMove(moves[n], rec[n]);
		--n;
	}
}
#endif

} // namespace Cheese

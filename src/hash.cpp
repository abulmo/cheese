////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "hash.h"

#include "util/logfile.h"

#include <iomanip>
#include <iostream>

#include <cstdlib>
#include <cstring>

#ifdef WIN32
#include <malloc.h>
#endif

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
void *allocateMemory(std::size_t size, std::size_t alignment)
{	
#ifdef USE_CACHE_ALIGNMENT
#ifdef __APPLE__
	return aligned_alloc(alignment, size);
#elif defined(WIN32)
	return _mm_malloc(size, alignment);
#else
	return std::aligned_alloc(alignment, size);
#endif
#else
	return std::malloc(size);
#endif
}

////////////////////////////////////////////////////////////////////////////////
void freeMemory(void *ptr)
{
#ifdef USE_CACHE_ALIGNMENT
#ifdef __APPLE__
	free(ptr);
#elif defined(WIN32)
	_mm_free(ptr);
#else
	std::free(ptr);
#endif
#else
	free(ptr);
#endif
}

////////////////////////////////////////////////////////////////////////////////
ChessHashNode::ChessHashNode()
{
}

////////////////////////////////////////////////////////////////////////////////
ChessHashNode::ChessHashNode(HashKey k, ChessMove mv, HashNodeType type,
	int depth, int score, int age) : key(k)
{
	const auto sc = static_cast<uint16_t>(score);
	data = (static_cast<uint64_t>(mv.data()) << shift_move) |
		   (static_cast<uint64_t>(sc) << shift_score) |
		   (static_cast<uint64_t>(age) << shift_age) |
		   (static_cast<uint64_t>(depth) << shift_depth) |
		   (static_cast<uint64_t>(type) << shift_type);
}

////////////////////////////////////////////////////////////////////////////////
ChessHashTable::~ChessHashTable()
{
	release();
}

////////////////////////////////////////////////////////////////////////////////
// create hash keys, and init hash table in Mb
bool ChessHashTable::init(int sizehash)
{
	std::size_t sz = std::clamp(sizehash, hashtable_size_min,
		hashtable_size_max);

	// init hash table
	if (hashtable != nullptr) {
		freeMemory(hashtable);
	}
	hashtable = nullptr;

	age = 0;

	// allocate to the next power of two size
	std::size_t n = 1;
	std::size_t asize = 1;

	while ((n < sz) && (n <= hashtable_size_max)) {
		n = (n << 1);
		if (n >= sz) {
			asize = n;
		}
	}

	constexpr std::size_t sznode = sizeof(ChessHashNode);
	constexpr std::size_t one_mb = 1024 * 1024;

	LOG_INFO << "Init main hash table : " << sz << " Mb";

	LOG_DEBUG << "allocated size = " << asize << " Mb";
	LOG_DEBUG << "hash node size = " << sznode << " bytes";

	size = (asize * one_mb) / sznode;
	sizeModulo = size - nb_hash_cluster;

	LOG_DEBUG << "size = " << size;
	LOG_DEBUG << "modulo = " << sizeModulo;

	hashtable = static_cast<ChessHashNode *>(allocateMemory(size * sznode,
		cache_aligned_value));

	LOG_INFO << "size hash node = " << sznode;

	if (hashtable == nullptr) {
		LOG_ERROR << "Hash table : out of memory";
		return false;
	}

	clear();

#ifdef HASH_STATS
	statExact = 0;
	statAlpha = 0;
	statBeta = 0;
	statMiss = 0;
	statMissKey = 0;
	statMissDepth = 0;
	statStore = 0;
	for (n=0; n<nb_hash_cluster; n++) {
		statStoreBucket[n] = 0;
	}
#endif

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// free hash table and keys
void ChessHashTable::release()
{
	if (hashtable != nullptr) {

#ifdef HASH_STATS
		LOG_DEBUG << "hash exact      = " << statExact;
		LOG_DEBUG << "hash alpha      = " << statAlpha;
		LOG_DEBUG << "hash beta       = " << statBeta;
		LOG_DEBUG << "hash miss       = " << statMiss;
		LOG_DEBUG << "hash miss key   = " << statMissKey;
		LOG_DEBUG << "hash miss depth = " << statMissDepth;
		LOG_DEBUG << "hash store      = " << statStore;

		for (int n=0; n<nb_hash_cluster; n++) {
			LOG_DEBUG << "hash store [" << n << "] = "
					  << statStoreBucket[n];
		}

		LOG_DEBUG << "hash efficiency = "
				  << static_cast<double>(statExact + statAlpha + statBeta) *
					 100.0 / static_cast<double>(statExact + statAlpha +
					 statBeta + statMiss)
				  << " %";
#endif

		freeMemory(hashtable);
		hashtable = nullptr;
	}
}

////////////////////////////////////////////////////////////////////////////////
// clear hash table
void ChessHashTable::clear()
{
	age = 0;
	if (hashtable != nullptr) {
		std::memset(static_cast<void *>(hashtable), 0, 
			size * sizeof(ChessHashNode));
	}
}

////////////////////////////////////////////////////////////////////////////////
// probe hash table
HashNodeType ChessHashTable::findNode(HashKey key, int depth, int ply,
	int alpha, int beta, ChessMove &cm, int &v)
{
	ChessHashNode *hash = getHashNode(key);

	ChessHashNode node;
	int n;
	for (n=0; n<nb_hash_cluster; n++) {
		hash[n].read(node);
		if (node.getKey() == key) {
			break;
		}
	}

	if (n < nb_hash_cluster) {

		cm = node.getMove();

		int hd = node.getDepth();

		if (hd >= depth) {

			int ha = node.getAge();

			// adjust mate values because they depend on ply
			int hs = adjustScoreFrom(node.getScore(), ply);

			v = hs;

			HashNodeType htype = node.getType();

			switch (htype) {

				// lower bound
				case	HashNodeType::hash_beta:
						if (hs >= beta) {
#ifdef HASH_STATS
							++statBeta;
#endif
							// refresh entry age
							if (age != ha) {
								node.setAge(age);
								hash[n].write(node);
							}

							return HashNodeType::hash_beta;
						}
						break;

				// upper bound
				case	HashNodeType::hash_alpha:
						if (hs <= alpha) {
#ifdef HASH_STATS
							++statAlpha;
#endif
							// refresh entry age
							if (age != ha) {
								node.setAge(age);
								hash[n].write(node);
							}

							return HashNodeType::hash_alpha;
						}
						break;

				// exact value
				case	HashNodeType::hash_exact:

#ifdef HASH_STATS
						++statExact;
#endif
						// refresh entry age
						if (age != ha) {
							node.setAge(age);
							hash[n].write(node);
						}

						return HashNodeType::hash_exact;
						break;

				default:
					break;
			}

		} else {
#ifdef HASH_STATS
			++statMissDepth;
#endif
		}
#ifdef HASH_STATS
	} else {
		++statMissKey;
	}
#else
	}
#endif

#ifdef HASH_STATS
	++statMiss;
#endif

	return HashNodeType::hash_notfound;
}

////////////////////////////////////////////////////////////////////////////////
// add new hash entry in the hash table
void ChessHashTable::addNode(HashKey key, ChessMove move, int score,
	int depth, int ply, HashNodeType type)
{
	ChessHashNode *hash = getHashNode(key);

#ifdef HASH_STATS
	int pb = 0;
#endif

	ChessHashNode *hashnew = nullptr;

	// find same hashkey or empty
	for (int n=0; n<nb_hash_cluster; n++) {
		HashKey k = hash[n].readKey();
		if ((k == 0) || (k == key)) {
		//if (k == key) {
			hashnew = &hash[n];
#ifdef HASH_STATS
			pb = n;
#endif
			break;
		}
	}

	// if not found, try to replace previous age lowest depth,
	// or lowest depth available
	if (hashnew == nullptr) {

		int mindepth = 2048 + 1024;

		for (int n=0; n<nb_hash_cluster; n++) {
			int hd = hash[n].getDepth();
			int ha = hash[n].getAge();
			int d = hd + ((ha == age) ? 2048 : 0);
			if (d < mindepth) {
				hashnew = &hash[n];
				mindepth = d;
#ifdef HASH_STATS
				pb = n;
#endif
			}
		}
	}

	// adjust mate values because they depend on ply
	ChessHashNode node(key, move, type, depth,
		adjustScoreTo(score, ply),
		age);

	// replace
	hashnew->write(node);

#ifdef HASH_STATS
	++statStore;
	++statStoreBucket[pb];
#endif
}

////////////////////////////////////////////////////////////////////////////////
ChessMove ChessHashTable::getBestMove(HashKey key) const
{
	ChessHashNode *hash = getHashNode(key);

	ChessHashNode node;
	int n;
	for (n=0; n<nb_hash_cluster; n++) {
		hash[n].read(node);
		if (node.getKey() == key) {
			break;
		}
	}

	return (n < nb_hash_cluster) ? (node.getMove()) : (ChessMove(0));
}

#ifdef USE_HASHFULL
////////////////////////////////////////////////////////////////////////////////
uint64_t ChessHashTable::getHashFull() const
{
	// approximation on 1000 nodes
	uint64_t count = 0;
	for (int k=0; k<hashfull_max; k++) {
		ChessHashNode *hash = getHashNode(k);
		ChessHashNode node;
		for (int n=0; n<nb_hash_cluster; n++) {
			hash[n].read(node);
			if ((node.getDepth() != 0) && (node.getAge() == age)) {
				++count;
			}
		}
	}
	return count / nb_hash_cluster;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// clear hash table
void ChessPawnHashTable::clear()
{
	ChessPawnHashNode node;
	node.key = 0;
	node.passed[0] = 0;
	node.passed[1] = 0;
	node.evalmid = 0;
	node.evalend = 0;
	hashtable.fill(node);

#ifdef HASH_STATS
	statFound = 0;
	statMiss = 0;
	statStore = 0;
#endif
}

#ifdef HASH_STATS
////////////////////////////////////////////////////////////////////////////////
void ChessPawnHashTable::printStat()
{
	LOG_DEBUG << "hash pawn found     = " << statFound;
	LOG_DEBUG << "hash pawn miss      = " << statMiss;
	LOG_DEBUG << "hash pawn store     = " << statStore;
	LOG_DEBUG << "hash paw efficiency = " <<
		static_cast<double>(statFound) * 100.0 /
		static_cast<double>(statFound + statMiss) << " %";
}
#endif

} // namespace Cheese

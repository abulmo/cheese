////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "searchthread.h"

#include "engine.h"
#include "lazysmp.h"
#include "moveorder.h"
#include "search.h"
#include "ybwc.h"

#include <numeric>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
ChessSearchThread::ChessSearchThread(int id, SMPMode mode,
	ChessSearchThreadList &th, ChessEngine &e) : smp(mode), stid(id),
	threads(th), engine(e), countNodes(0), maxPly(0), tbhits(0),
	searching(false), thinking(false), bestMove(0), ponderMove(0),
	searchDepth(0), bestScore(0), hashTablePawn()
{
	hashTablePawn.clear();
}

////////////////////////////////////////////////////////////////////////////////
bool ChessSearchThread::isAvailable(
	[[maybe_unused]] ChessSearchThread *mainthread)
{
	return (!searching);
}

////////////////////////////////////////////////////////////////////////////////
bool ChessSearchThread::cutoff() const
{
	return false;
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThread::resetNodeCount()
{
	countNodes = 0;
	maxPly = 0;
	tbhits = 0;
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThread::initSearch()
{
	searchDepth = 0;
	bestMove.clear();
	ponderMove.clear();
	bestScore = 0;
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThread::updateSearch(int depth, ChessMove bm, ChessMove pm,
	int score)
{
	searchDepth = depth;
	bestMove = bm;
	ponderMove = pm;
	bestScore = score;
}

////////////////////////////////////////////////////////////////////////////////
ChessSearchThreadList::~ChessSearchThreadList()
{
	destroy();
}

////////////////////////////////////////////////////////////////////////////////
ChessSearchThread *ChessSearchThreadList::createThread(SMPMode mode, int n,
	ChessEngine &engine)
{
	switch (mode) {
		case SMPMode::ybwc:
			return new ChessSearchThreadYBWC(n, *this, engine);
			break;
		case SMPMode::lazy:
			return new ChessSearchThreadLazy(n, *this, engine);
			break;
		default:
			LOG_ERROR << "unknonw thread type : " << static_cast<int>(mode);
			return nullptr;
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////
// create all search threads
void ChessSearchThreadList::create(ChessEngine &engine)
{
	destroy();

	threads.reserve(maxThread);

	for (int n=0; n<maxThread; n++) {
		auto *thread = createThread(engine.getSMPMode(), n, engine);
		if (thread != nullptr) {
			threads.push_back(thread);
			thread->create();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThreadList::destroy()
{
	for (ChessSearchThread *th : threads) {
		th->stop();

#ifdef HASH_STATS
		LOG_DEBUG << "Thread " << th->stid << " : ";
		th->hashTablePawn.printStat();
#endif

		delete th;
	}
	threads.clear();
}

////////////////////////////////////////////////////////////////////////////////
ChessSearchThread *ChessSearchThreadList::findAvailableThread(
	ChessSearchThread *thread)
{
	for (auto *th : threads) {
		if (th->isAvailable(thread)) {
			return th;
		}
	}
	return nullptr;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessSearchThreadList::isThreadAvailable(ChessSearchThread *thread)
{
	for (auto *th : threads) {
		if ((th->getId() != thread->getId()) &&
			(th->isAvailable(thread))) {
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
uint64_t ChessSearchThreadList::getNodeCount() const
{
	return std::accumulate(threads.begin(), threads.end(), 0ULL,
		[](uint64_t v, auto *th) { return v + th->countNodes; });
}

////////////////////////////////////////////////////////////////////////////////
int ChessSearchThreadList::getMaxPly() const
{
	int maxply = 0;
	for (auto *th : threads) {
		const int p = th->maxPly;
		if (p > maxply) {
			maxply = p;
		}
	}
	return maxply;
}

////////////////////////////////////////////////////////////////////////////////
uint64_t ChessSearchThreadList::getTBHits() const
{
	return std::accumulate(threads.begin(), threads.end(), 0ULL,
		[](uint64_t v, auto *th) { return v + th->tbhits; });
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThreadList::clearHistory()
{
	for (auto *th : threads) {
		th->clearHistory();
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThreadList::resetNodeCount()
{
	for (auto *th : threads) {
		th->resetNodeCount();
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThreadList::setMaxThreads(int m, ChessEngine &engine)
{
	maxThread = std::clamp(m, 1, (engine.getSMPMode() == SMPMode::ybwc) ?
		max_threads_ybwc : max_threads);

	// create threads
	create(engine);
}

////////////////////////////////////////////////////////////////////////////////
void ChessSearchThreadList::waitEndSearch()
{
	std::unique_lock<std::mutex> lk(mutex);
	cond.wait(lk, [&]{ return !getMainThread()->thinking; });
}

} // namespace Cheese

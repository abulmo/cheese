////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "bitbase.h"

#ifdef USE_BITBASE

#include "util/logfile.h"
#include "util/timer.h"

#include <array>
#include <iomanip>
#include <iostream>

#ifndef GENERATE_BITBASE
#include "bitbasekpk.h"
#endif

namespace Cheese::BitBases {

////////////////////////////////////////////////////////////////////////////////
enum BitBaseScore {
	kpk_draw,
	kpk_win,
	kpk_invalid,
	kpk_unknown,
	kpk_loss
};

#ifdef GENERATE_BITBASE
std::array<uint32_t, 8192> bitbase_kpk;
#endif

////////////////////////////////////////////////////////////////////////////////
inline void decodeIndexKPK(uint32_t i, Side &s, int &p, int &kw, int &kb)
{
	s = static_cast<Side>((i >> 18) & 1);
	p = static_cast<int>((i >> 12) & 0x3f);
	kw = static_cast<int>((i >> 6) & 0x3f);
	kb = static_cast<int>(i & 0x3f);
}

////////////////////////////////////////////////////////////////////////////////
inline auto encodeIndexKPK(Side s, int p, int kw, int kb)
{
	return (static_cast<uint32_t>(s) << 18) |
		   (static_cast<uint32_t>(p) << 12) |
		   (static_cast<uint32_t>(kw) << 6) |
		   static_cast<uint32_t>(kb);
}

////////////////////////////////////////////////////////////////////////////////
inline auto encodeIndexBitKPK(Side s, int p, int kw, int kb)
{
	return (static_cast<uint32_t>(s) << 17) |
		   (static_cast<uint32_t>(p) << 12) |
		   (static_cast<uint32_t>(kw) << 6) |
		   static_cast<uint32_t>(kb);
}

////////////////////////////////////////////////////////////////////////////////
// generate KPK table
//
// count iteration = 20
// kpk win         = 124960 (23.8%)
// kpk loss        = 123574 (23.6%)
// kpk draw        = 110778 (21.1%)
// kpk invalid     = 164976 (31.5%)
//
#ifdef GENERATE_BITBASE

void generateKPK()
{
	constexpr uint32_t total = nb_sides * nb_squares * nb_squares * nb_squares;

	std::array<uint32_t, 5> countKPK;
	std::array<char, total> kpk;

	// index format : 32 bits / 19 bits used

	// ........ .....x.. ........ ........ : side (0 = black, 1 = white)
	// ........ ......xx xxxx.... ........ : white pawn square
	// ........ ........ ....xxxx xx...... : white king square
	// ........ ........ ........ ..xxxxxx : black king square
	// 00000000 00000... ........ ........ : unused

	// init to unknown
	kpk.fill(kpk_unknown);

	int kw;
	int kb;
	int p;
	Side side;
	int adr;
	uint32_t i;

	// invalid positions
	// + set known mate and draw positions
	for (i=0; i<total; i++) {

		decodeIndexKPK(i, side, p, kw, kb);

		// impossible positions
		if ((kw == p) || (kb == p) ||
			(p < a2) ||
			((p > h7) && (side == white)) ||
			(squareDistance(kw, kb) < 2)) {
			kpk[i] = kpk_invalid;
		}

		if (side == white) {
			// white pawn attack black king
			if ((squareRank(kb) == (squareRank(p) + 1)) &&
				((squareFile(kb) == (squareFile(p) + 1)) ||
				(squareFile(kb) == (squareFile(p) - 1)))) {
				kpk[i] = kpk_invalid;
			}
		}

		if (kpk[i] != kpk_invalid) {

			if (side == black) {

				// if the pawn is taken it's a draw
				if ((squareDistance(kb, p) == 1) &&
					(squareDistance(kw, p) > 1)) {
					kpk[i] = kpk_draw;
				}

				// promoted pawn is loss for black
				if ((squareRank(p) == rank_8) &&
					((squareDistance(kb, p) > 1) ||
						((squareDistance(kb, p) == 1) &&
						(squareDistance(kw, p) == 1)))) {

					kpk[i] = kpk_loss;
				}
			}
		}
	}

	// repeat until no changes are possible
	uint64_t countIteration = 0;
	int changed = 1;
	while (changed) {

		changed = 0;

		int x;
		int y;

		// better to test all black moves, then white
		for (int s=1; s>=0; s--) {

			side = static_cast<Side>(s);

			for (p=a1; p<=h8; p++) {
			for (kw=a1; kw<=h8; kw++) {
			for (kb=a1; kb<=h8; kb++) {

			i = encodeIndexKPK(side, p, kw, kb);

			if (kpk[i] == kpk_unknown) {

				std::array<int, 5> countpos;

				countpos[kpk_draw] = 0;
				countpos[kpk_win] = 0;
				countpos[kpk_loss] = 0;
				countpos[kpk_invalid] = 0;
				countpos[kpk_unknown] = 0;

				int count = 0;

				// generate all successor positions (legal moves)
				if (side == white) {

					// pawn move 1
					if (squareRank(p) < rank_8) {
						adr = encodeIndexKPK(~side , p + dir_up, kw, kb);
						if (kpk[adr] != kpk_invalid) {
							++countpos[kpk[adr]];
							++count;
						}
					}

					// pawn move 2
					if (squareRank(p) == rank_2) {
						adr = encodeIndexKPK(~side, p + dir_up_2, kw, kb);
						if (((p + dir_up) != kw) && ((p + dir_up) != kb) &&
						    (kpk[adr] != kpk_invalid)) {
							++countpos[kpk[adr]];
							++count;
						}
					}

					// move white king
					for (y=squareRank(kw)-1; y<=squareRank(kw)+1; y++) {
						for (x=squareFile(kw)-1; x<=squareFile(kw)+1; x++) {
							int sq = makeSquare(x, y);
							adr = encodeIndexKPK(~side, p, sq, kb);
							if ((x >= file_a) && (x <= file_h) &&
								(y >= rank_1) && (y <= rank_8) &&
								(sq != kw) &&
								(kpk[adr] != kpk_invalid)) {

								++countpos[kpk[adr]];
								++count;
							}
						}
					}

				} else {

					// move black king
					for (y=squareRank(kb)-1; y<=squareRank(kb)+1; y++) {
						for (x=squareFile(kb)-1; x<=squareFile(kb)+1; x++) {
							int sq = makeSquare(x, y);
							adr = encodeIndexKPK(~side, p, kw, sq);
							if ((x >= file_a) && (x <= file_h) &&
								(y >= rank_1) && (y <= rank_8) &&
								(sq != kb) &&
								(kpk[adr] != kpk_invalid)) {

								++countpos[kpk[adr]];
								++count;
							}
						}
					}
				}

				if (count != 0) {

					// if any successor is LOSS, set to WIN
					// if all successor are WIN, set to LOSS
					// if all successor are DRAW, set to DRAW
					if (countpos[kpk_loss] != 0) {
						kpk[i] = kpk_win;
						changed++;
					} else
					if (countpos[kpk_win] == count) {
						kpk[i] = kpk_loss;
						changed++;
					} else
					if (countpos[kpk_draw] == count) {
						kpk[i] = kpk_draw;
						changed++;
					}

				}
			}

			}
			}
			}
		}

		++countIteration;
	}

	// end : set all unknown to draw
	for (i=0; i<total; i++) {
		if (kpk[i] == kpk_unknown) {
			kpk[i] = kpk_draw;
		}
	}

	countKPK[kpk_draw] = 0;
	countKPK[kpk_win] = 0;
	countKPK[kpk_loss] = 0;
	countKPK[kpk_invalid] = 0;
	countKPK[kpk_unknown] = 0;

	// count
	for (i=0; i<total; i++) {
		++countKPK[kpk[i]];
	}

	uint32_t totalbyte = (nb_sides * (nb_squares / 2) * nb_squares *
		nb_squares) / 32;

	// create bitbase table
	bitbase_kpk.fill(0);

	for (i=0; i<total; i++) {

		if (kpk[i] != kpk_invalid) {

			decodeIndexKPK(i, side, p, kw, kb);

			if (squareFile(p) <= file_d) {

				p = ((squareRank(p) << 2) | squareFile(p));

				uint32_t adrbit = encodeIndexBitKPK(side, p, kw, kb);
				uint32_t adrbyte = adrbit >> 5;
				uint32_t posbit = adrbit & 31;
				uint32_t mask = 1 << posbit;

				if ((kpk[i] == kpk_win) || (kpk[i] == kpk_loss)) {
					bitbase_kpk[adrbyte] |= mask;
				}
			}
		}
	}

	// output
	for (i=0; i<totalbyte/8; i++) {
		std::cout << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8] << ", "
				  << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8 + 1] << ", "
				  << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8 + 2] << ", "
				  << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8 + 3] << ", "
				  << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8 + 4] << ", "
				  << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8 + 5] << ", "
				  << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8 + 6] << ", "
				  << "0x" << std::setfill('0') << std::setw(8) << std::hex
				  << bitbase_kpk[i * 8 + 7] << ", " << std::endl;
	}

#ifdef USELOG_DEBUG
	LOG_DEBUG << "kpk total = " << total;
	LOG_DEBUG << "count bytes = " << totalbyte * 4;
	LOG_DEBUG << "count iteration = " << countIteration;
	LOG_DEBUG << "kpk win = " << countKPK[kpk_win] << " "
			  << static_cast<double>(countKPK[kpk_win]) * 100.0 /
				 static_cast<double>(total);
	LOG_DEBUG << "kpk loss = " << countKPK[kpk_loss] << " "
			  << static_cast<double>(countKPK[kpk_loss]) * 100.0 /
				 static_cast<double>(total);
	LOG_DEBUG << "kpk draw = " << countKPK[kpk_draw] << " "
			  << static_cast<double>(countKPK[kpk_draw]) * 100.0 /
				 static_cast<double>(total);
	LOG_DEBUG << "kpk invalid = " << countKPK[kpk_invalid] << " "
			  << static_cast<double>(countKPK[kpk_invalid]) * 100.0 /
				 static_cast<double>(total);
	LOG_DEBUG << "kpk unknown = " << countKPK[kpk_unknown] << " "
			  << static_cast<double>(countKPK[kpk_unknown]) * 100.0 /
				 static_cast<double>(total);
#endif
}

#endif

////////////////////////////////////////////////////////////////////////////////
int readKPK(Side side, int kw, int kb, int p)
{
	// pawn symmetry
	if (squareFile(p) > file_d) {
		kw = kw ^ 7;
		kb = kb ^ 7;
		p = p ^ 7;
	}

	// recalc pawn square offset with the symmetry
	// because file now takes 2 bits
	p = ((squareRank(p) << 2) | squareFile(p));

	uint32_t adrbit = encodeIndexBitKPK(side, p, kw, kb);
	uint32_t adrbyte = adrbit >> 5;
	uint32_t posbit = adrbit & 31;
	uint32_t mask = 1 << posbit;

	return (bitbase_kpk[adrbyte] & mask) ? 1 : 0;
}

#ifdef GENERATE_BITBASE
////////////////////////////////////////////////////////////////////////////////
void generate()
{
	LOG_DEBUG << "generate KPK";

	Timer timer;
	timer.start();

	generateKPK();

	uint64_t timesearch = timer.getElapsedTime();
	LOG_DEBUG << "time " << Timer::timeToString(timesearch) << " ms";
}
#endif

} // namespace Cheese::BitBases

#endif //USE_BITBASE

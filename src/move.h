////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_MOVE_H_
#define CHEESE_MOVE_H_

#include "config.h"

#include <algorithm>
#include <array>
#include <string>

namespace Cheese {

constexpr int nb_squares = 64;
constexpr int nb_ranks = 8;
constexpr int nb_files = 8;
constexpr int nb_sides = 2;
constexpr int nb_pieces_type = 8;
constexpr int max_square_distance = 8;
constexpr int nb_pieces_name = 16;

// Squares
enum Square {
	a1, b1, c1, d1, e1, f1, g1, h1,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a7, b7, c7, d7, e7, f7, g7, h7,
	a8, b8, c8, d8, e8, f8, g8, h8
};

// Squares ranks
enum Rank {
	rank_1,
	rank_2,
	rank_3,
	rank_4,
	rank_5,
	rank_6,
	rank_7,
	rank_8
};

// Squares files
enum File {
	file_a,
	file_b,
	file_c,
	file_d,
	file_e,
	file_f,
	file_g,
	file_h
};

// Directions from square to square (from black side point of view)
enum SquareDir : signed {
	dir_none  		= 0,
	dir_up    		=  nb_files,
	dir_down  		= -nb_files,
	dir_right 		=  1,
	dir_left  		= -1,
	dir_up_2 		= dir_up * 2,
	dir_down_2 		= dir_down * 2,
	dir_up_right   	= dir_up + dir_right,
	dir_up_left    	= dir_up + dir_left,
	dir_down_right 	= dir_down + dir_right,
	dir_down_left  	= dir_down + dir_left
};

// Playing side
enum Side : uint32_t {
	black,
	white
};

// flip side
constexpr Side operator~(Side s)
{
	return static_cast<Side>(static_cast<uint32_t>(s) ^ 1);
}

// Chess Pieces type values
enum Piece {
	no_piece,
	pawn,
	knight,
	bishop,
	rook,
	queen,
	king
};

// castling rights
enum CastlingRight {

	// white short castle enable
	castle_white_short = 1,

	// white long castle enable
	castle_white_long = 2,

	// black short castle enable
	castle_black_short = 4,

	// black long castle enable
	castle_black_long = 8,

	// white default castle rights
	castle_white_init = (castle_white_short | castle_white_long),

	// black default castle enable
	castle_black_init = (castle_black_short | castle_black_long),

	// default castling rights
	castle_init = (castle_black_init | castle_white_init)
};

struct ChessMoveSort;

////////////////////////////////////////////////////////////////////////////////
// a chess move packed in 32 bits
struct ChessMove
{
private:

	enum {
		shift_src       = 0,
		shift_dst       = 6,
		shift_moved     = 12,
		shift_captured  = 15,
		shift_promotion = 18,
		shift_castle    = 21,
		shift_enpassant = 22,

		mask_src 	   = 0x3f,
		mask_dst 	   = 0x3f,
		mask_moved     = 0x07,
		mask_captured  = 0x07,
		mask_promotion = 0x07,
		mask_castle	   = 0x01,
		mask_enpassant = 0x01
	};

	// move bit format (32 bits / 23 bits used)
	//
	// 00000000 00000000 00000000 00xxxxxx : src square	(y*8+x) : bits  1 -  6
	// 00000000 00000000 0000xxxx xx000000 : dst square	(y*8+x) : bits  7 - 12
	// 00000000 00000000 0xxx0000 00000000 : moved piece        : bits 13 - 15
	// 00000000 000000xx x0000000 00000000 : captured piece     : bits 16 - 18
	// 00000000 000xxx00 00000000 00000000 : promotion piece    : bits 18 - 20
	// 00000000 00x00000 00000000 00000000 : castle             : bit  21
	// 00000000 0x000000 00000000 00000000 : en passant         : bit  22
	// ???????? ?0000000 00000000 00000000 : not used           : bits 23 - 31

	uint32_t flag;

public:

	ChessMove()
	{}

	ChessMove(uint32_t data) : flag(data)
	{}

	ChessMove(int src, int dst, int mv, int cap = 0, int pro = 0, int cas = 0,
		int ep = 0);

	// get move raw data
	uint32_t data() const;

	// set move data
	void set(uint32_t f);

	// clear move
	void clear();

	// test if move is empty
	bool isEmpty() const;

	bool operator== (const ChessMove &m) const;

	bool operator!= (const ChessMove &m) const;

	bool isCastle() const;

	bool isCapture() const;

	bool isCaptureOrPromotion() const;

	bool isPromotion() const;

	bool isEnPassant() const;

	bool isPawnTwice() const;

	// pawn moving on 7th rank
	bool isMovePawnOn7th() const;

	// pawn moving past 4th rank
	bool isMovePawnPast4th(Side side) const;

	// test if the move is reversible
	// not a pawn move, castling, or capture
	bool isReversible() const;

	// get source square
	int src() const;

	// get destination square
	int dst() const;

	// get moved piece
	int piece() const;

	// get captured piece
	int capture() const;

	// get promotion piece
	int promotion() const;

	// get short SAN notation
	std::string textShort(ChessMoveSort *moves, int count);

	std::string text();

	std::string textMove();

	static std::string strSquare(int sq);

	void print();
};

////////////////////////////////////////////////////////////////////////////////
// sortable chess move
struct ChessMoveSort {

	// chess move
	ChessMove move;

	// order value
	int32_t value;

	ChessMoveSort()
	{}

	ChessMove getMove() const;

	int32_t getScore() const;

	void setMove(ChessMove m);

	void setScore(int32_t v);
};

////////////////////////////////////////////////////////////////////////////////
// calculate square offset
constexpr int makeSquare(int x, int y)
{
	return static_cast<int>(static_cast<unsigned int>(y) * nb_files +
							static_cast<unsigned int>(x));
}

////////////////////////////////////////////////////////////////////////////////
// get rank from square offset
constexpr int squareRank(int sq)
{
	return static_cast<int>(static_cast<unsigned int>(sq) / nb_files);
}

////////////////////////////////////////////////////////////////////////////////
// get file from square offset
constexpr int squareFile(int sq)
{
	constexpr unsigned int f = nb_files - 1;
	return static_cast<int>(static_cast<unsigned int>(sq) & f);	
}

////////////////////////////////////////////////////////////////////////////////
// get file distance
inline int fileDistance(int sqa, int sqb)
{
	return std::abs(squareFile(sqa) - squareFile(sqb));
}

////////////////////////////////////////////////////////////////////////////////
// get rank distance
inline int rankDistance(int sqa, int sqb)
{
	return std::abs(squareRank(sqa) - squareRank(sqb));
}

////////////////////////////////////////////////////////////////////////////////
// calculate square distance
inline int squareDistance(int sqa, int sqb)
{
	return std::max(fileDistance(sqa, sqb), rankDistance(sqa, sqb));
}

////////////////////////////////////////////////////////////////////////////////
// relative rank from player position
template<Side side>
constexpr int relRank(int r)
{
	return (side == white) ? r : (rank_8 - r);
}

////////////////////////////////////////////////////////////////////////////////
// relative square rank from player position
template<Side side>
constexpr int relSquareRank(int sq)
{
	return relRank<side>(squareRank(sq));
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareUp(int sq)
{
	return sq + ((side == white) ? dir_up : dir_down);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareUp2(int sq)
{
	return sq + ((side == white) ? dir_up_2 : dir_down_2);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareDown(int sq)
{
	return sq + ((side == white) ? dir_down : dir_up);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareDown2(int sq)
{
	return sq + ((side == white) ? dir_down_2 : dir_up_2);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareUpLeft(int sq)
{
	return sq + ((side == white) ? dir_up_left : dir_down_right);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareUpRight(int sq)
{
	return sq + ((side == white) ? dir_up_right : dir_down_left);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareDownRight(int sq)
{
	return sq + ((side == white) ? dir_down_right : dir_up_left);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr int relSquareDownLeft(int sq)
{
	return sq + ((side == white) ? dir_down_left : dir_up_right);
}

////////////////////////////////////////////////////////////////////////////////
// test if moving 2 squares up or down
constexpr bool isMoveTwiceUp(int src, int dst)
{
	return (src ^ dst) == dir_up_2;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove::ChessMove(int src, int dst, int mv, int cap, int pro, int cas,
	int ep) : flag ((static_cast<uint32_t>(src) << shift_src) |
					(static_cast<uint32_t>(dst) << shift_dst) |
					(static_cast<uint32_t>(mv)  << shift_moved) |
					(static_cast<uint32_t>(cap) << shift_captured) |
					(static_cast<uint32_t>(pro) << shift_promotion) |
					(static_cast<uint32_t>(cas) << shift_castle) |
					(static_cast<uint32_t>(ep)  << shift_enpassant))
{
}

////////////////////////////////////////////////////////////////////////////////
inline uint32_t ChessMove::data() const
{
	return flag;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessMove::set(uint32_t f)
{
	flag = f;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessMove::clear()
{
	flag = 0;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isEmpty() const
{
	return (flag == 0);
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::operator== (const ChessMove &m) const
{
	return (flag == m.flag);
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::operator!= (const ChessMove &m) const
{
	return (flag != m.flag);
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isCastle() const
{
	return (flag & (mask_castle << shift_castle)) != 0;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isCapture() const
{
	return (flag & (mask_captured << shift_captured)) != 0;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isPromotion() const
{
	return (flag & (mask_promotion << shift_promotion)) != 0;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isCaptureOrPromotion() const
{
	return (flag & ((mask_captured << shift_captured) |
					(mask_promotion << shift_promotion))) != 0;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isEnPassant() const
{
	return (flag & (mask_enpassant << shift_enpassant)) != 0;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isPawnTwice() const
{
	return ((piece() == pawn) &&
			isMoveTwiceUp(src(), dst()));
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isMovePawnOn7th() const
{
	int r = squareRank(dst());
	return ((piece() == pawn) &&
			((r == rank_2) || (r == rank_7)));
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isMovePawnPast4th(Side side) const
{
	int r = (side == white) ?
				squareRank(src()) :
				(rank_8 - squareRank(src()));			
	return ((piece() == pawn) && (r > rank_4));
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessMove::isReversible() const
{
	return ((piece() != pawn) &&
			(!isCapture()) &&
			(!isCastle()));
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessMove::src() const
{
	return static_cast<int>((flag >> shift_src) & mask_src);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessMove::dst() const
{
	return static_cast<int>((flag >> shift_dst) & mask_dst);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessMove::piece() const
{
	return static_cast<int>((flag >> shift_moved) & mask_moved);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessMove::capture() const
{
	return static_cast<int>((flag >> shift_captured) & mask_captured);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessMove::promotion() const
{
	return static_cast<int>((flag >> shift_promotion) & mask_promotion);
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessMoveSort::getMove() const
{
	return move;
}

////////////////////////////////////////////////////////////////////////////////
inline int32_t ChessMoveSort::getScore() const
{
	return value;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessMoveSort::setMove(ChessMove m)
{
	move = m;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessMoveSort::setScore(int32_t v)
{
	value = v;
}

// flip vertically a square on the board
extern const std::array<int, nb_squares> flip_square;

extern const std::array<char, nb_pieces_name> pieces_name;

} // namespace Cheese

#endif //CHEESE_MOVE_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "fen.h"

#include "util/logfile.h"
#include "move.h"

#include <array>
#include <fstream>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// convert ASCII letter to piece type
const std::array<char, 60> convert_ascii_to_piece = {
	0, 0, bishop + 8, 0, 0, 0, 0, 0, 0, 0,
	0, king + 8, 0, 0, knight + 8, 0, pawn + 8, queen + 8, rook + 8, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, bishop, 0, 0, 0, 0, 0,
	0, 0, 0, king, 0, 0, knight, 0, pawn, queen,
	rook, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

////////////////////////////////////////////////////////////////////////////////
ChessFENParser::ChessFENParser(ChessBoard &b) : board(b)
{
}

////////////////////////////////////////////////////////////////////////////////
bool ChessFENParser::load(const std::string &filename,
	std::vector<std::string> &fens)
{
	std::ifstream ifs(filename, std::ifstream::in);
	if (!ifs.is_open()) {
		return false;
	}

	std::string str;
	while (std::getline(ifs, str)) {
		if (str.size() > 2) {
			fens.emplace_back(str);
		}
	}

	ifs.close();

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessFENParser::set(const std::string &fen)
{
	init();

	auto ptr = fen.begin();
	auto ptrend = fen.end();
	char c = 0;

	// pieces
    int x = 0;
	int y = 7;
	while (ptr < ptrend) {

		c = *(ptr++);

		if (c == ' ') {
			break;
		} else
		if ((c >= '1') && (c <= '8')) {
			x += static_cast<int>(c) - static_cast<int>('0');
		} else
		if (c == '/') {
			x = 0;
			--y;
		} else {
			int o = static_cast<int>(c) - static_cast<int>('@');
			if ((o >= 0) && (o < 58) &&
				(x < 8) && (y >= 0) &&
				(convert_ascii_to_piece[o] != 0)) {
                setPiece(x++, y, convert_ascii_to_piece[o]);
			} else {
				return false;
			}
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	// side
	c = *(ptr++);

    Side side = white;
	if (c == 'w') {
		side = white;
	} else
	if (c == 'b') {
		side = black;
	} else {
		return false;
	}

    setSide(side);

	if (ptr == ptrend) {
		return false;
	}

	c = *(ptr++);
	if (c != ' ') {
		return false;
	}

	// castle
    int p = 0;
	while (ptr < ptrend) {

		c = *(ptr++);

		if (c == ' ') {
			break;
		}
		if ((++p) > 4) {
			return false;
		}
		switch (c) {
			case 	'-':
					break;
			case	'K':
					setCastle(white, 0);
					break;
			case	'Q':
					setCastle(white, 1);
					break;
			case	'k':
					setCastle(black, 0);
					break;
			case	'q':
					setCastle(black, 1);
					break;
			default:
				// FRC support
				if ((c >= 'A') && (c <= 'H')) {
					setCastleFRC(white, static_cast<int>(c) -
										static_cast<int>('A'));
				} else
				if ((c >= 'a') && (c <= 'h')) {
					setCastleFRC(black, static_cast<int>(c) -
										static_cast<int>('a'));
				} else {
					return false;
				}
				break;
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	// en passant
	c = *(ptr++);
	if (c != '-') {

		if ((c >= 'a') && (c <= 'h')) {

			int ep = static_cast<int>(c) - static_cast<int>('a') + 1;

			if (ptr == ptrend) {
				return false;
			}

			c = *(ptr++);

			if ((c >= '1') && (c <= '8')) {

				y = static_cast<int>(c) - static_cast<int>('1');
				if ((side == black) && (y != 2)) {
					return false;
				}
				if ((side == white) && (y != 5)) {
					return false;
				}

				setEnPassant(ep);

				if (ptr == ptrend) {
					return false;
				}

			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	c = *(ptr++);
	if (c != ' ') {
		return false;
	}

	// fifty
	if ((ptr < ptrend) && (*ptr >= '0') && (*ptr <= '9')) {

        int v = 0;
		while (ptr < ptrend) {

			c = *(ptr++);

			if (c == ' ') {
				setFifty(v);
				break;
			} else
			if ((c >= '0') && (c <= '9')) {
				v = v * 10 + static_cast<int>(c) - static_cast<int>('0');
			} else {
				break;
			}
		}

		// moves count
		if ((ptr < ptrend) && (*ptr >= '0') && (*ptr <= '9')) {

            v = 0;
			while (ptr < ptrend) {

				c = *(ptr++);

				if (c == ' ') {
                    setMoves(v);
					break;
				}
				if ((c >= '0') && (c <= '9')) {
					v = v * 10 + static_cast<int>(c) - static_cast<int>('0');
				} else {
					break;
				}
			}
		}
	}

	end();

	return true;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessFENParser::get()
{
	std::string fen;
	int	nb = 0;

	for (int y=7; y>=0; y--) {
		for (int x=0; x<8; x++) {

			int p = getPiece(x, y);

			int pside = (p < 0) ? black : white;
			if (p < 0) {
				p = -p;
			}

			if (p == 0) {
				++nb;
			} else {
				if (nb != 0) {
					fen += '0' + static_cast<char>(nb);
					nb = 0;
				}
				fen += pieces_name[pside * 8 + p];
			}
		}

		if (nb != 0) {
			fen += '0' + static_cast<char>(nb);
			nb = 0;
		}

		if (y != 0) {
			fen += '/';
		}
	}

	fen += ' ';

	Side side = getSide();
	if (side == white) {
		fen += 'w';
	} else {
		fen += 'b';
	}

	fen += ' ';

	auto p = fen.size();

	if (getCastle(white, 0)) {
		fen += 'K';
	}
	if (getCastle(white, 1)) {
		fen += 'Q';
	}
	if (getCastle(black, 0)) {
		fen += 'k';
	}
	if (getCastle(black, 1)) {
		fen += 'q';
	}

	if (fen.size() == p) {
		fen += '-';
	}

	fen += ' ';

	int ep = getEnPassant();
	if (ep) {

		fen += 'a' + static_cast<char>(ep - 1);

		if (side == black) {
			fen += '3';
		} else {
			fen += '6';
		}

	} else {
		fen += '-';
	}

	fen += ' ';

	// clock moves
	fen += std::to_string(getFifty());

	fen += ' ';

	// full moves
	fen += std::to_string(getMoves());

	return fen;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessFENParser::check(const std::string &fen) const
{
	Side side = white;
	int x = 0;
	int y = 7;

	auto ptr = fen.begin();
	auto ptrend = fen.end();
	char c = 0;

	// pieces
	while (ptr < ptrend) {

		c = *(ptr++);

		if (c == ' ') {
			break;
		} else
		if ((c >= '1') && (c <= '8')) {
			int m = static_cast<int>(c) - static_cast<int>('0');
			x += m;
		} else
		if (c == '/') {
			x = 0;
			--y;
		} else {
			int o = c - '@';
			if ((o >=0) && (o < 58)) {
				char pn = convert_ascii_to_piece[o];
				if (pn == 0) {
					return false;
				}
				++x;
			} else {
				return false;
			}
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	// side
	c = *(ptr++);

	if (c == 'w') {
		side = white;
	} else
	if (c == 'b') {
		side = black;
	} else {
		return false;
	}

	if (ptr == ptrend) {
		return false;
	}

	c = *(ptr++);
	if (c != ' ') {
		return false;
	}

	// castle
    int p = 0;
	while (ptr < ptrend) {

		c = *(ptr++);

		if (c == ' ') {
			break;
		} else {
			if ((++p) > 4) {
				return false;
			}
			switch (c) {
				case 	'-':
				case	'K':
				case	'Q':
				case	'k':
				case	'q':
						break;
				default:
					// FRC support
					if (((c >= 'A') && (c <= 'H')) ||
					    ((c >= 'a') && (c <= 'h'))) {
					} else {
						return false;
					}
					break;
			}
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	// en passant
	c = *(ptr++);

	if (c == '-') {
		if (ptr == ptrend) {
			return false;
		}
		++ptr;
	} else {

		if ((c >= 'a') && (c <= 'h')) {

			if (ptr == ptrend) {
				return false;
			}

			c = *(ptr++);

			if ((c >= '1') && (c <= '8')) {

				y = c - '1';
				if ((side == black) && (y != 2)) {
					return false;
				}
				if ((side == white) && (y != 5)) {
					return false;
				}

			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	// fifty
	p = 0;
	while (ptr < ptrend) {

		c = *(ptr++);

		if (c == ' ') {
			break;
		} else
		if ((c >= '0') && (c <= '9')) {
			if (p >= 2) {
				return false;
			}
		} else {
			return true;
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	// moves count
	p = 0;
	while (ptr < ptrend) {

		c = *(ptr++);

		if (c == ' ') {
			break;
		} else
		if ((c >= '0') && (c <= '9')) {
			if (p >= 4) {
				return false;
			}
		} else {
			return true;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// reverse a FEN string :
//
// - flip board verticaly
// - flip colour
// - flip side to move
// - flip castle
// - flip en passant
//
bool ChessFENParser::reverse(const std::string &str, std::string &newfen)
{
	auto ptr = str.begin();
	auto ptrend = str.end();

	while ((ptr != ptrend) && (*ptr != ' ')) {
		++ptr;
	}
	if (ptr == ptrend) {
		return false;
	}

    newfen = "";

    auto ps = str.begin();
	auto pe = ptr;
	auto p = ptr - 1;
	while (ptr >= ps) {
		if ((ptr == ps) || (*(ptr - 1) == '/')) {
			for (int i=0; i<=(p - ptr); i++) {
				char c = *(ptr + i);
				if ((c >= 'a') && (c <= 'z')) {
					c = c + static_cast<char>('A' - 'a');
				} else if ((c >= 'A') && (c <= 'Z')) {
					c = c + static_cast<char>('a' - 'A');
				} else if ((c < '0') || (c > '9')) {
					return false;
				}
				newfen += c;
			}
			if (ptr > ps) {
				newfen += '/';
				p = ptr - 2;
				--ptr;
			}
		}
		--ptr;
	}

	ptr = pe;
	if (*ptr != ' ') {
		return false;
	}
	newfen += ' ';
	++ptr;
	if (ptr == ptrend) {
		return false;
	}

	Side side = black;
	if (*ptr == 'w') {
		side = white;
		newfen += 'b';
	} else
	if (*ptr == 'b') {
		side = black;
		newfen += 'w';
	} else {
		return false;
	}

	++ptr;
	if (ptr == ptrend) {
		return true;
	}
	if (*ptr != ' ') {
		return false;
	}
	newfen += ' ';
	++ptr;

	// castle
	char c = 0;
    int n = 0;
	bool cwk = false;
	bool cwq = false;
	bool cbk = false;
	bool cbq = false;
	while (ptr < ptrend) {

		c = *(ptr++);

		if (c == ' ') {

			if (cbk) {
				newfen += 'K';
			}
			if (cbq) {
				newfen += 'Q';
			}
			if (cwk) {
				newfen += 'k';
			}
			if (cwq) {
				newfen += 'q';
			}

			newfen += ' ';
			break;
		} else {
			if ((++n) > 4) {
				return false;
			}
			switch (c) {
				case 	'-':
						newfen += '-';
						break;
				case	'K':
						cwk = true;
						break;
				case	'Q':
						cwq = true;
						break;
				case	'k':
						cbk = true;
						break;
				case	'q':
						cbq = true;
						break;
				default:
					// TODO : FRC support
					if ((c >= 'A') && (c <= 'H')) {
						newfen += (c - 'A' + 'a');
					} else
					if ((c >= 'a') && (c <= 'h')) {
						newfen += (c - 'a' + 'A');
					} else {
						return false;
					}
					break;
			}
		}
	}

	if (ptr == ptrend) {
		return false;
	}

	// en passant
	c = *(ptr++);

	if (c == '-') {
		newfen += '-';
		if (ptr == ptrend) {
			return false;
		}
	} else {

		if ((c >= 'a') && (c <= 'h')) {

			if (ptr == ptrend) {
				return false;
			}

			newfen += c;
			c = *(ptr++);

			if ((c >= '1') && (c <= '8')) {

				int y = c - '1';
				if ((side == black) && (y != 2)) {
					return false;
				}
				if ((side == white) && (y != 5)) {
					return false;
				}

				if (y == 2) {
					newfen += '6';
				} else {
					newfen += '3';
				}

			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	while (ptr < ptrend) {
		newfen += *(ptr++);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessFENParser::getOption(const std::string &fen,
							     const std::string &name)
{
	auto p = fen.find(name);
	if ((p != std::string::npos) &&
		(p != 0U) &&
		((fen[p - 1] == ' ') || (fen[p - 1] == ';')) &&
		((p + name.size()) < fen.size()) &&
		(fen[p + name.size()] == ' ')) {

		auto p1 = fen.find_first_of(';', p + name.size());
		if (p1 != std::string::npos) {
			return fen.substr(p + name.size() + 1, p1 - p - name.size() - 1);
		} else {
			return fen.substr(p + name.size(), fen.size() - p - name.size());
		}
	}
	return "";
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::init()
{
	board.clear();
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::end()
{
	board.initCastlingRights();
	board.hashKey = board.createHashKey();
	board.hashKeyPawns = board.createHashKeyPawns();
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::setPiece(int x, int y, int pc)
{
	int p = (pc < 8) ? pc : (pc - 8);
	Side s = (pc < 8) ? black : white;
	int sq = makeSquare(x, y);

	board.setBoardPiece(s,sq, p);
	board.setPiece(sq, static_cast<uint8_t>(p));

	board.evalMaterial[s] += pieces_value[p];
	board.matCount += material_count_mask[s][p];
	board.phaseCount[s] += piece_game_phase[p];

	if (p == king) {
		board.posKing[s] = sq;
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::setSide(Side side)
{
	board.player = side;
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::setCastle(Side side, int type)
{
	board.castle |= (side == white) ?
		((type == 0) ? castle_white_short : castle_white_long) :
		((type == 0) ? castle_black_short : castle_black_long);
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::setCastleFRC(Side side, int f)
{
	if (f > board.kingPosition(board.side())) {
		board.castle |= static_cast<uint8_t>((side == white) ?
			castle_white_short : castle_black_short);
	} else {
		board.castle |= static_cast<uint8_t>((side == white) ?
			castle_white_long : castle_black_long);
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::setEnPassant(int ep)
{
	board.enpassant = static_cast<uint8_t>(ep);
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::setMoves([[maybe_unused]] int count)
{
}

////////////////////////////////////////////////////////////////////////////////
void ChessFENParser::setFifty(int fifty)
{
	board.fifty = static_cast<uint8_t>(fifty);
}

////////////////////////////////////////////////////////////////////////////////
int ChessFENParser::getPiece(int x, int y)
{
	if (board.bitboardColor(white) & BitBoards::square[makeSquare(x, y)]) {
		return static_cast<int>(board.piece(makeSquare(x, y))) + 8;
	} else {
		return static_cast<int>(board.piece(makeSquare(x, y)));
	}
}

////////////////////////////////////////////////////////////////////////////////
Side ChessFENParser::getSide()
{
	return board.player;
}

////////////////////////////////////////////////////////////////////////////////
int ChessFENParser::getCastle(Side side, int type)
{
	return (side == white) ?
			((type == 0) ? board.castle & castle_white_short :
						   board.castle & castle_white_long) :
			((type == 0) ? board.castle & castle_black_short :
						   board.castle & castle_black_long);
}

////////////////////////////////////////////////////////////////////////////////
int ChessFENParser::getEnPassant()
{
	return board.enpassant;
}

////////////////////////////////////////////////////////////////////////////////
int ChessFENParser::getMoves()
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
int ChessFENParser::getFifty()
{
	return board.fifty;
}

} // namespace Cheese

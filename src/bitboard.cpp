////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "bitboard.h"

#include "util/logfile.h"
#include "move.h"
#include "util/timer.h"

#include <iomanip>
#include <iostream>
#include <limits>
#include <random>

namespace Cheese::BitBoards {

#ifndef SYSTEM64BIT

#ifndef FIND_MAGIC_KEYS
const std::array<BitBoard, nb_squares> magic_rook_mask =
{
	0x000101010101017eULL, 0x000202020202027cULL, 0x000404040404047aULL, 0x0008080808080876ULL,
	0x001010101010106eULL, 0x002020202020205eULL, 0x004040404040403eULL, 0x008080808080807eULL,
	0x0001010101017e00ULL, 0x0002020202027c00ULL, 0x0004040404047a00ULL, 0x0008080808087600ULL,
	0x0010101010106e00ULL, 0x0020202020205e00ULL, 0x0040404040403e00ULL, 0x0080808080807e00ULL,
	0x00010101017e0100ULL, 0x00020202027c0200ULL, 0x00040404047a0400ULL, 0x0008080808760800ULL,
	0x00101010106e1000ULL, 0x00202020205e2000ULL, 0x00404040403e4000ULL, 0x00808080807e8000ULL,
	0x000101017e010100ULL, 0x000202027c020200ULL, 0x000404047a040400ULL, 0x0008080876080800ULL,
	0x001010106e101000ULL, 0x002020205e202000ULL, 0x004040403e404000ULL, 0x008080807e808000ULL,
	0x0001017e01010100ULL, 0x0002027c02020200ULL, 0x0004047a04040400ULL, 0x0008087608080800ULL,
	0x0010106e10101000ULL, 0x0020205e20202000ULL, 0x0040403e40404000ULL, 0x0080807e80808000ULL,
	0x00017e0101010100ULL, 0x00027c0202020200ULL, 0x00047a0404040400ULL, 0x0008760808080800ULL,
	0x00106e1010101000ULL, 0x00205e2020202000ULL, 0x00403e4040404000ULL, 0x00807e8080808000ULL,
	0x007e010101010100ULL, 0x007c020202020200ULL, 0x007a040404040400ULL, 0x0076080808080800ULL,
	0x006e101010101000ULL, 0x005e202020202000ULL, 0x003e404040404000ULL, 0x007e808080808000ULL,
	0x7e01010101010100ULL, 0x7c02020202020200ULL, 0x7a04040404040400ULL, 0x7608080808080800ULL,
	0x6e10101010101000ULL, 0x5e20202020202000ULL, 0x3e40404040404000ULL, 0x7e80808080808000ULL
};

const std::array<BitBoard, nb_squares> magic_rook_magic =
{
	0x0018008450202280ULL, 0x0600180000106140ULL, 0x0081780000801260ULL, 0x0080180000804408ULL,
	0x020c008000800204ULL, 0x2002245040400902ULL, 0x08440900010002a3ULL, 0x000020c000400241ULL,
	0x21c0012008004040ULL, 0x1260080820002020ULL, 0x1082004008020804ULL, 0x0010448110940408ULL,
	0x18000c2001020112ULL, 0x0402007011004104ULL, 0x0623081400010091ULL, 0x1044028112410101ULL,
	0x0020408100060404ULL, 0x04412010020a0808ULL, 0x1080440021020208ULL, 0x0024180214010104ULL,
	0x0008408002413008ULL, 0x00804449010a0041ULL, 0x1086060401010102ULL, 0x0880040100104201ULL,
	0x48410029204300aeULL, 0x0924449421c10021ULL, 0x016001c002201109ULL, 0x001408600cc02a06ULL,
	0x3004200800101a02ULL, 0x01a6215002000882ULL, 0x1012050008800401ULL, 0x0100209222040263ULL,
	0x0040202805820e44ULL, 0x0040620000100323ULL, 0x00802088074410a0ULL, 0x4040440008900048ULL,
	0x0100088000001005ULL, 0x0100040429400803ULL, 0x0100050050010406ULL, 0x0040088800000141ULL,
	0x4200100e00a1300cULL, 0x4640400000002018ULL, 0x004200802800201aULL, 0x24240808440020c1ULL,
	0x081200200d00094eULL, 0x040408083030600aULL, 0x088042000c006801ULL, 0x40240208002c0043ULL,
	0x002020400100620dULL, 0x0a89010000424060ULL, 0x0011104002004030ULL, 0x40602202200100a2ULL,
	0x0201404040000906ULL, 0x04060200400b140aULL, 0x0403110005080401ULL, 0x00004040010800c1ULL,
	0x0900301900005280ULL, 0x0482483100500891ULL, 0x0008230108004030ULL, 0x0004402660200c12ULL,
	0x4010020508004108ULL, 0x010028014020140fULL, 0x11300a0102002097ULL, 0x001424d11000409bULL
};

std::array<BitBoard *, nb_squares> magic_rook_ptr;

const std::array<unsigned int, nb_squares> magic_rook_shift =
{
	20, 21, 21, 21, 21, 21, 21, 20,
	21, 22, 22, 22, 22, 22, 22, 21,
	21, 22, 22, 22, 22, 22, 22, 21,
	21, 22, 22, 22, 22, 22, 22, 21,
	21, 22, 22, 22, 22, 22, 22, 21,
	21, 22, 22, 22, 22, 22, 22, 21,
	21, 22, 22, 22, 22, 22, 22, 21,
	20, 21, 21, 21, 21, 21, 21, 20
};
#endif //FIND_MAGIC_KEYS

#ifndef FIND_MAGIC_KEYS
const std::array<BitBoard, nb_squares> magic_bishop_mask =
{
	0x0040201008040200ULL, 0x0000402010080400ULL, 0x0000004020100a00ULL, 0x0000000040221400ULL,
	0x0000000002442800ULL, 0x0000000204085000ULL, 0x0000020408102000ULL, 0x0002040810204000ULL,
	0x0020100804020000ULL, 0x0040201008040000ULL, 0x00004020100a0000ULL, 0x0000004022140000ULL,
	0x0000000244280000ULL, 0x0000020408500000ULL, 0x0002040810200000ULL, 0x0004081020400000ULL,
	0x0010080402000200ULL, 0x0020100804000400ULL, 0x004020100a000a00ULL, 0x0000402214001400ULL,
	0x0000024428002800ULL, 0x0002040850005000ULL, 0x0004081020002000ULL, 0x0008102040004000ULL,
	0x0008040200020400ULL, 0x0010080400040800ULL, 0x0020100a000a1000ULL, 0x0040221400142200ULL,
	0x0002442800284400ULL, 0x0004085000500800ULL, 0x0008102000201000ULL, 0x0010204000402000ULL,
	0x0004020002040800ULL, 0x0008040004081000ULL, 0x00100a000a102000ULL, 0x0022140014224000ULL,
	0x0044280028440200ULL, 0x0008500050080400ULL, 0x0010200020100800ULL, 0x0020400040201000ULL,
	0x0002000204081000ULL, 0x0004000408102000ULL, 0x000a000a10204000ULL, 0x0014001422400000ULL,
	0x0028002844020000ULL, 0x0050005008040200ULL, 0x0020002010080400ULL, 0x0040004020100800ULL,
	0x0000020408102000ULL, 0x0000040810204000ULL, 0x00000a1020400000ULL, 0x0000142240000000ULL,
	0x0000284402000000ULL, 0x0000500804020000ULL, 0x0000201008040200ULL, 0x0000402010080400ULL,
	0x0002040810204000ULL, 0x0004081020400000ULL, 0x000a102040000000ULL, 0x0014224000000000ULL,
	0x0028440200000000ULL, 0x0050080402000000ULL, 0x0020100804020000ULL, 0x0040201008040200ULL
};

const std::array<BitBoard, nb_squares> magic_bishop_magic =
{
	0x141840c004ea7063ULL, 0x0700402304101106ULL, 0x00400100006440e4ULL, 0x0000000011080861ULL,
	0x4008000100040420ULL, 0x2e24000000020112ULL, 0x2420000402224544ULL, 0x08040410420a08b2ULL,
	0x1004410206001b27ULL, 0x0803004210004a84ULL, 0x04850a0211200c8cULL, 0x031000000340460aULL,
	0x2c00400006280112ULL, 0x5008048009300c98ULL, 0x0405601900141254ULL, 0x00842104200a10d3ULL,
	0x20022200080602a0ULL, 0x282170c0580810e4ULL, 0x080011000006080cULL, 0x0620424006202002ULL,
	0x02420092044a6024ULL, 0x1520410020021001ULL, 0x01a8040000040001ULL, 0x0401042000022271ULL,
	0x1010419600044400ULL, 0x0910014010011201ULL, 0x0c08002840012204ULL, 0x0401000210052210ULL,
	0x2810140000105040ULL, 0x0430040108082188ULL, 0x0048040002040080ULL, 0x0024060104011242ULL,
	0x00180500080c2424ULL, 0x2050230020420225ULL, 0x0048002000040044ULL, 0x3020210002480401ULL,
	0x0009100200186904ULL, 0x2004280000010302ULL, 0x200201000c011801ULL, 0x00b10041220204c2ULL,
	0x4444080140080414ULL, 0x0800038000220813ULL, 0x0102180203010422ULL, 0x0121210006000021ULL,
	0x0240040000040811ULL, 0x0889004000101010ULL, 0x0408110520b00109ULL, 0x08250048000802ccULL,
	0x241f10021001080aULL, 0x092a101044020455ULL, 0x0604118114040445ULL, 0x608a210002430081ULL,
	0x020900000010480eULL, 0x4403201d00203020ULL, 0x0a03400000081104ULL, 0x0079300021041022ULL,
	0x00e049210c10262bULL, 0x2091100040020061ULL, 0x0e05040040221001ULL, 0x0884040608002018ULL,
	0x5002022004201084ULL, 0x111012400a080025ULL, 0x23040c5400491002ULL, 0x07c540a004082101ULL
};

std::array<BitBoard *, nb_squares> magic_bishop_ptr;

const std::array<unsigned int, nb_squares> magic_bishop_shift =
{
	26, 27, 27, 27, 27, 27, 27, 26,
	27, 27, 27, 27, 27, 27, 27, 27,
	27, 27, 25, 25, 25, 25, 27, 27,
	27, 27, 25, 23, 23, 25, 27, 27,
	27, 27, 25, 23, 23, 25, 27, 27,
	27, 27, 25, 25, 25, 25, 27, 27,
	27, 27, 27, 27, 27, 27, 27, 27,
	26, 27, 27, 27, 27, 27, 27, 26
};
#endif //FIND_MAGIC_KEYS

#else

#ifndef FIND_MAGIC_KEYS
const std::array<BitBoard, nb_squares> magic_rook_mask =
{
	0x000101010101017eULL, 0x000202020202027cULL, 0x000404040404047aULL, 0x0008080808080876ULL,
	0x001010101010106eULL, 0x002020202020205eULL, 0x004040404040403eULL, 0x008080808080807eULL,
	0x0001010101017e00ULL, 0x0002020202027c00ULL, 0x0004040404047a00ULL, 0x0008080808087600ULL,
	0x0010101010106e00ULL, 0x0020202020205e00ULL, 0x0040404040403e00ULL, 0x0080808080807e00ULL,
	0x00010101017e0100ULL, 0x00020202027c0200ULL, 0x00040404047a0400ULL, 0x0008080808760800ULL,
	0x00101010106e1000ULL, 0x00202020205e2000ULL, 0x00404040403e4000ULL, 0x00808080807e8000ULL,
	0x000101017e010100ULL, 0x000202027c020200ULL, 0x000404047a040400ULL, 0x0008080876080800ULL,
	0x001010106e101000ULL, 0x002020205e202000ULL, 0x004040403e404000ULL, 0x008080807e808000ULL,
	0x0001017e01010100ULL, 0x0002027c02020200ULL, 0x0004047a04040400ULL, 0x0008087608080800ULL,
	0x0010106e10101000ULL, 0x0020205e20202000ULL, 0x0040403e40404000ULL, 0x0080807e80808000ULL,
	0x00017e0101010100ULL, 0x00027c0202020200ULL, 0x00047a0404040400ULL, 0x0008760808080800ULL,
	0x00106e1010101000ULL, 0x00205e2020202000ULL, 0x00403e4040404000ULL, 0x00807e8080808000ULL,
	0x007e010101010100ULL, 0x007c020202020200ULL, 0x007a040404040400ULL, 0x0076080808080800ULL,
	0x006e101010101000ULL, 0x005e202020202000ULL, 0x003e404040404000ULL, 0x007e808080808000ULL,
	0x7e01010101010100ULL, 0x7c02020202020200ULL, 0x7a04040404040400ULL, 0x7608080808080800ULL,
	0x6e10101010101000ULL, 0x5e20202020202000ULL, 0x3e40404040404000ULL, 0x7e80808080808000ULL
};

const std::array<BitBoard, nb_squares> magic_rook_magic =
{
	0x0180001420804001ULL, 0x2040200010004000ULL, 0x6100082001001441ULL, 0x2100180420100100ULL,
	0x6200040200d00820ULL, 0x1600110200041008ULL, 0x4880408001000200ULL, 0x1180008000c02100ULL,
	0x0102002200410082ULL, 0x10c1002140010180ULL, 0x0111002000704100ULL, 0x0802002200401008ULL,
	0x0242003422001008ULL, 0x03120008041a0150ULL, 0x4042000102000408ULL, 0x12550001000a40a2ULL,
	0x0008688000401180ULL, 0x221000c010406008ULL, 0x4050008011200080ULL, 0x0801010060100008ULL,
	0x0442020008902044ULL, 0x0101010002040018ULL, 0x0020040008021001ULL, 0x1402020004006195ULL,
	0x0280184040002000ULL, 0x0101008d004000a0ULL, 0x0001004300200012ULL, 0x0100100100202902ULL,
	0x0002100500080100ULL, 0x0045004900040082ULL, 0x0010020400101548ULL, 0x00012042000c0081ULL,
	0x08a0244000800080ULL, 0x00d00a2010400042ULL, 0x0010040802200020ULL, 0x000200200a005040ULL,
	0x0018001101000428ULL, 0x1002009002002c18ULL, 0x0200100204000809ULL, 0x0001011142002084ULL,
	0x048000a000404010ULL, 0x0120100040044020ULL, 0x38a5002002490010ULL, 0x0050050010210008ULL,
	0x0044000800110100ULL, 0x2102001004020008ULL, 0x18a2008805020014ULL, 0x0001468100460004ULL,
	0x1082708000400080ULL, 0x0010004002200840ULL, 0x4020044110210100ULL, 0x0010081000210100ULL,
	0x0080101500080100ULL, 0x010100080c000300ULL, 0x2001001402000100ULL, 0x0401000153920900ULL,
	0x0080204111860102ULL, 0x0203002140081281ULL, 0x0200408873002001ULL, 0x20400410010008a1ULL,
	0x0801001004280083ULL, 0x0002001008010482ULL, 0x0001000400d20005ULL, 0x4004008401014022ULL
};

std::array<BitBoard *, nb_squares> magic_rook_ptr;

const std::array<unsigned int, nb_squares> magic_rook_shift =
{
	52, 53, 53, 53, 53, 53, 53, 52,
	53, 54, 54, 54, 54, 54, 54, 53,
	53, 54, 54, 54, 54, 54, 54, 53,
	53, 54, 54, 54, 54, 54, 54, 53,
	53, 54, 54, 54, 54, 54, 54, 53,
	53, 54, 54, 54, 54, 54, 54, 53,
	53, 54, 54, 54, 54, 54, 54, 53,
	52, 53, 53, 53, 53, 53, 53, 52
};
#endif //FIND_MAGIC_KEYS

#ifndef FIND_MAGIC_KEYS
const std::array<BitBoard, nb_squares> magic_bishop_mask =
{
	0x0040201008040200ULL, 0x0000402010080400ULL, 0x0000004020100a00ULL, 0x0000000040221400ULL,
	0x0000000002442800ULL, 0x0000000204085000ULL, 0x0000020408102000ULL, 0x0002040810204000ULL,
	0x0020100804020000ULL, 0x0040201008040000ULL, 0x00004020100a0000ULL, 0x0000004022140000ULL,
	0x0000000244280000ULL, 0x0000020408500000ULL, 0x0002040810200000ULL, 0x0004081020400000ULL,
	0x0010080402000200ULL, 0x0020100804000400ULL, 0x004020100a000a00ULL, 0x0000402214001400ULL,
	0x0000024428002800ULL, 0x0002040850005000ULL, 0x0004081020002000ULL, 0x0008102040004000ULL,
	0x0008040200020400ULL, 0x0010080400040800ULL, 0x0020100a000a1000ULL, 0x0040221400142200ULL,
	0x0002442800284400ULL, 0x0004085000500800ULL, 0x0008102000201000ULL, 0x0010204000402000ULL,
	0x0004020002040800ULL, 0x0008040004081000ULL, 0x00100a000a102000ULL, 0x0022140014224000ULL,
	0x0044280028440200ULL, 0x0008500050080400ULL, 0x0010200020100800ULL, 0x0020400040201000ULL,
	0x0002000204081000ULL, 0x0004000408102000ULL, 0x000a000a10204000ULL, 0x0014001422400000ULL,
	0x0028002844020000ULL, 0x0050005008040200ULL, 0x0020002010080400ULL, 0x0040004020100800ULL,
	0x0000020408102000ULL, 0x0000040810204000ULL, 0x00000a1020400000ULL, 0x0000142240000000ULL,
	0x0000284402000000ULL, 0x0000500804020000ULL, 0x0000201008040200ULL, 0x0000402010080400ULL,
	0x0002040810204000ULL, 0x0004081020400000ULL, 0x000a102040000000ULL, 0x0014224000000000ULL,
	0x0028440200000000ULL, 0x0050080402000000ULL, 0x0020100804020000ULL, 0x0040201008040200ULL
};

const std::array<BitBoard, nb_squares> magic_bishop_magic =
{
	0x0da0200a428908b0ULL, 0x0203100c01007003ULL, 0x0208080102300b24ULL, 0x0088204040600000ULL,
	0x01042c2068804080ULL, 0x4106170420190200ULL, 0x6084122964201028ULL, 0x2022482207304000ULL,
	0x0400224a0a280122ULL, 0x4c02058818010070ULL, 0x4042104c41902040ULL, 0x4200084841000200ULL,
	0x01000c0b08220400ULL, 0x300b0104606d0020ULL, 0x608400a404200e00ULL, 0x0400010328020290ULL,
	0x0021001002100104ULL, 0x0108005001630421ULL, 0x0020401005014012ULL, 0x4008000120404000ULL,
	0x00040102060a0002ULL, 0x020a0041004202a0ULL, 0x4104204202866801ULL, 0x5081400824040400ULL,
	0x0809200040040104ULL, 0x0c04200002080900ULL, 0x1008045006020200ULL, 0x0044010000200880ULL,
	0x20600a0000c05000ULL, 0x2001004102082002ULL, 0x4001020000e80c08ULL, 0x0064004020821082ULL,
	0x0004108500404404ULL, 0x0002109000445b22ULL, 0x12020082001021e4ULL, 0x0864201100880184ULL,
	0x008c040400025100ULL, 0x0001080220020211ULL, 0x0002008100022804ULL, 0x0222048304212400ULL,
	0x0004100c100c0400ULL, 0x08004c26204008c4ULL, 0x1006010402000100ULL, 0x0601044010460a02ULL,
	0x0108280500400400ULL, 0x0004250042000100ULL, 0x68083014009240c0ULL, 0x0803014062040100ULL,
	0x002201100ad201c4ULL, 0x010025052821002cULL, 0x4000004602d02000ULL, 0x3000000042020000ULL,
	0x0800003002020000ULL, 0x0100187021421014ULL, 0x103b1010009128c0ULL, 0x1020028c10c03208ULL,
	0x484a486610100402ULL, 0x1840321c60980880ULL, 0x0000200064260834ULL, 0x10000102089c0400ULL,
	0x0000060010020200ULL, 0x040200c006ac0d10ULL, 0x08801420360240a0ULL, 0x0042440f28020288ULL
};

std::array<BitBoard *, nb_squares> magic_bishop_ptr;

const std::array<unsigned int, nb_squares> magic_bishop_shift =
{
	58, 59, 59, 59, 59, 59, 59, 58,
	59, 59, 59, 59, 59, 59, 59, 59,
	59, 59, 57, 57, 57, 57, 59, 59,
	59, 59, 57, 55, 55, 57, 59, 59,
	59, 59, 57, 55, 55, 57, 59, 59,
	59, 59, 57, 57, 57, 57, 59, 59,
	59, 59, 59, 59, 59, 59, 59, 59,
	58, 59, 59, 59, 59, 59, 59, 58
};
#endif //FIND_MAGIC_KEYS

#endif // SYSTEM64BIT

// size of the magic keys
#ifdef SYSTEM64BIT
constexpr int magic_key_size = 64;
#else
constexpr int magic_key_size = 32;
#endif

// bishop magic table size
constexpr int magictable_bishop_size = 5248;

// rook magic table size
constexpr int magictable_rook_size = 102400;

// magic tables
std::array<BitBoard, magictable_rook_size> magicTableRook;

std::array<BitBoard, magictable_bishop_size> magicTableBishop;

// pawn attacks
std::array<std::array<BitBoard, nb_squares>, nb_sides> pawn_attack;

// knight attacks
std::array<BitBoard, nb_squares> knight_attack;

// king attacks
std::array<BitBoard, nb_squares> king_attack;

// set mask (1 << n)
const std::array<BitBoard, nb_squares> square = {
	0x0000000000000001ULL, 0x0000000000000002ULL, 0x0000000000000004ULL, 0x0000000000000008ULL,
	0x0000000000000010ULL, 0x0000000000000020ULL, 0x0000000000000040ULL, 0x0000000000000080ULL,
	0x0000000000000100ULL, 0x0000000000000200ULL, 0x0000000000000400ULL, 0x0000000000000800ULL,
	0x0000000000001000ULL, 0x0000000000002000ULL, 0x0000000000004000ULL, 0x0000000000008000ULL,
	0x0000000000010000ULL, 0x0000000000020000ULL, 0x0000000000040000ULL, 0x0000000000080000ULL,
	0x0000000000100000ULL, 0x0000000000200000ULL, 0x0000000000400000ULL, 0x0000000000800000ULL,
	0x0000000001000000ULL, 0x0000000002000000ULL, 0x0000000004000000ULL, 0x0000000008000000ULL,
	0x0000000010000000ULL, 0x0000000020000000ULL, 0x0000000040000000ULL, 0x0000000080000000ULL,
	0x0000000100000000ULL, 0x0000000200000000ULL, 0x0000000400000000ULL, 0x0000000800000000ULL,
	0x0000001000000000ULL, 0x0000002000000000ULL, 0x0000004000000000ULL, 0x0000008000000000ULL,
	0x0000010000000000ULL, 0x0000020000000000ULL, 0x0000040000000000ULL, 0x0000080000000000ULL,
	0x0000100000000000ULL, 0x0000200000000000ULL, 0x0000400000000000ULL, 0x0000800000000000ULL,
	0x0001000000000000ULL, 0x0002000000000000ULL, 0x0004000000000000ULL, 0x0008000000000000ULL,
	0x0010000000000000ULL, 0x0020000000000000ULL, 0x0040000000000000ULL, 0x0080000000000000ULL,
	0x0100000000000000ULL, 0x0200000000000000ULL, 0x0400000000000000ULL, 0x0800000000000000ULL,
	0x1000000000000000ULL, 0x2000000000000000ULL, 0x4000000000000000ULL, 0x8000000000000000ULL
};

// all diagonal 45 R
std::array<BitBoard, nb_squares> diag_45r;

// all diagonal 45 L
std::array<BitBoard, nb_squares> diag_45l;

std::array<std::array<BitBoard, nb_squares>, nb_squares> between;

// direction from a square to another
std::array<std::array<int8_t, nb_squares>, nb_squares> direction;

// all files
const std::array<BitBoard, nb_files> file = {
	0x0101010101010101ULL,
	0x0202020202020202ULL,
	0x0404040404040404ULL,
	0x0808080808080808ULL,
	0x1010101010101010ULL,
	0x2020202020202020ULL,
	0x4040404040404040ULL,
	0x8080808080808080ULL
};

// all ranks
const std::array<BitBoard, nb_ranks> rank = {
	0x00000000000000ffULL,
	0x000000000000ff00ULL,
	0x0000000000ff0000ULL,
	0x00000000ff000000ULL,
	0x000000ff00000000ULL,
	0x0000ff0000000000ULL,
	0x00ff000000000000ULL,
	0xff00000000000000ULL
};

// files around
const std::array<BitBoard, nb_files> files_around = {
	0x0202020202020202ULL,
	0x0505050505050505ULL,
	0x0a0a0a0a0a0a0a0aULL,
	0x1414141414141414ULL,
	0x2828282828282828ULL,
	0x5050505050505050ULL,
	0xa0a0a0a0a0a0a0a0ULL,
	0x4040404040404040ULL
};

// all white squares
const BitBoard white_squares = 0x55aa55aa55aa55aaULL;

// all black squares
const BitBoard black_squares = 0xaa55aa55aa55aa55ULL;

#ifdef FIND_MAGIC_KEYS
std::array<BitBoard, nb_squares> magic_bishop_magic;
std::array<BitBoard, nb_squares> magic_rook_magic;
std::array<BitBoard, nb_squares> magic_bishop_mask;
std::array<BitBoard, nb_squares> magic_rook_mask;
std::array<unsigned int, nb_squares> magic_bishop_shift;
std::array<unsigned int, nb_squares> magic_rook_shift;
std::array<BitBoard *, nb_squares> magic_bishop_ptr;
std::array<BitBoard *, nb_squares> magic_rook_ptr;

constexpr BitBoard BitBoardFull = std::numeric_limits<uint64_t>::max();

#endif //FIND_MAGIC_KEYS

////////////////////////////////////////////////////////////////////////////////
uint64_t RookAttack(int sq, uint64_t occ)
{
	constexpr int nb_dir = 4;
	static constexpr std::array<int, nb_dir> rstep = {
		dir_right, dir_left, dir_up, dir_down };

	const std::array<int, nb_dir> rlimit = {
		file_h - squareFile(sq),
		         squareFile(sq),
		rank_8 - squareRank(sq),
 		         squareRank(sq)
	};

	uint64_t bb = 0ULL;
	for (int i=0; i<nb_dir; i++) {
		int p = sq;
		for (int n=0; n<rlimit[i]; n++) {
			p += rstep[i];
			bb |= square[p];
			if ((occ & square[p]) != 0ULL) {
				break;
			}
		}
	}

	return bb;
}

////////////////////////////////////////////////////////////////////////////////
// generate bishop attack without using bitboard (slow)
uint64_t BishopAttack(int sq, uint64_t occ)
{
	constexpr int nb_dir = 4;
	static constexpr std::array<int, nb_dir> bstep = {
		dir_up_right, dir_up_left, dir_down_right, dir_down_left };

	const std::array<int, nb_dir> blimit = {
		std::min(file_h - squareFile(sq), rank_8 - squareRank(sq)),
		std::min(         squareFile(sq), rank_8 - squareRank(sq)),
		std::min(file_h - squareFile(sq),          squareRank(sq)),
		std::min(         squareFile(sq),          squareRank(sq))
	};

	uint64_t bb = 0ULL;
	for (int i=0; i<nb_dir; i++) {
		int p = sq;
		for (int n=0; n<blimit[i]; n++) {
			p += bstep[i];
			bb |= square[p];
			if ((occ & square[p]) != 0ULL) {
				break;
			}
		}
	}

	return bb;
}

////////////////////////////////////////////////////////////////////////////////
// Generate magic table from magic keys & shifts
void createMagicTable()
{
	magicTableBishop = {};
	uint64_t maxIndex = 0;
	unsigned int dbOffset = 0;

	// bishop
	for (int n=0; n<nb_squares; n++) {

		magic_bishop_ptr[n] = magicTableBishop.data() + dbOffset;
		BitBoard occ = 0;
		maxIndex = 0;
		BitBoard oset = magic_bishop_mask[n];

		do {
			uint64_t index = magicAttack(occ, oset, magic_bishop_magic[n],
				magic_bishop_shift[n]);

			if ((index + dbOffset) > maxIndex) {
				maxIndex = (index + dbOffset);
			}

			*(magic_bishop_ptr[n] + index) = BishopAttack(n, occ);
			occ = (occ - oset) & oset;

		} while (occ != 0);

		dbOffset += (1 << (magic_key_size - magic_bishop_shift[n]));
	}

	LOG_DEBUG << "Max Bishop Index = " << maxIndex;

	magicTableRook = {};
	dbOffset = 0;

	// rook
	for (int n=0; n<nb_squares; n++) {

		magic_rook_ptr[n] = magicTableRook.data() + dbOffset;
		BitBoard occ = 0;
		maxIndex = 0;
		BitBoard oset = magic_rook_mask[n];

		do {
			uint64_t index = magicAttack(occ, oset, magic_rook_magic[n],
				magic_rook_shift[n]);

			if ((index + dbOffset) > maxIndex) {
				maxIndex = (index + dbOffset);
			}

			*(magic_rook_ptr[n] + index) = RookAttack(n, occ);
			occ = (occ - oset) & oset;

		} while (occ != 0);

		dbOffset += (1 << (magic_key_size - magic_rook_shift[n]));
	}

	LOG_DEBUG << "Max Rook Index = " << maxIndex;
}

#ifdef FIND_MAGIC_KEYS
////////////////////////////////////////////////////////////////////////////////
constexpr uint64_t filterBishopAttack(uint64_t occ)
{
	return occ & ~(file[file_a] | file[file_h] | rank[rank_1] | rank[rank_8]);
}

////////////////////////////////////////////////////////////////////////////////
constexpr uint64_t filterRookAttack(uint64_t occ, int sq)
{
	uint64_t o = occ;
	if (squareFile(sq) != file_a) {
		o &= ~file[file_a];
	}
	if (squareFile(sq) != file_h) {
		o &= ~file[file_h];
	}
	if (squareRank(sq) != rank_1) {
		o &= ~rank[rank_1];
	}
	if (squareRank(sq) != rank_8) {
		o &= ~rank[rank_8];
	}
	return o;
}

////////////////////////////////////////////////////////////////////////////////
bool testMagicKey(int sq, uint64_t magickey, int numbit,int type,
	int *testcount, int *testcollision, uint64_t *maxvalue)
{
	constexpr std::size_t max_index = (1 << 12);

	// max size = 1 << (64 - numbit)
	std::array<uint64_t, max_index>	indexList;
	indexList.fill(BitBoardFull);

	uint64_t occ = 0;
	uint64_t oset;

	// possible attack on the square for the piece on an empty board
	// (but we can remove borders for sliding pieces)
	if (type == bishop) {
		oset = filterBishopAttack(BishopAttack(sq, occ));
	} else {
		oset = filterRookAttack(RookAttack(sq, occ), sq);
	}

	uint64_t maxval = 0;
	unsigned int collision = 0;
	unsigned int count = 0;

	do {

		uint64_t index = magicAttack(occ, oset, magickey, numbit);

		if (index > maxval) {
			maxval = index;
		}

		uint64_t bAtt = (type == bishop)  ?
						BishopAttack(sq, occ) :
						RookAttack(sq, occ);

		if (indexList[index] == BitBoardFull) {
			indexList[index] = bAtt;
			++count;
		} else
		if (indexList[index] == bAtt) {
			++collision;
		} else {
			*maxvalue = maxval;
			return false;
		}

		occ = (occ - oset) & oset;

	} while (occ != 0);

	*testcount = count;
	*testcollision = collision;
	*maxvalue = maxval;

	return true;
}

////////////////////////////////////////////////////////////////////////////////
int findMagicKey(int sq, unsigned int nb, int nbbit, int type,
	std::mt19937_64 &rnd)
{
	uint64_t bestKey = 0;
	uint64_t maxval = 0;
	uint64_t bestmaxvalue = 0;
	int testcount = 0;
	int testcollision = 0;
	int bestNumBit = 0;
	int bestCount = 0;
	int bestCollision = -1;
	int	numbit = magic_key_size - nbbit;

	std::uniform_int_distribution<uint64_t> dist;

	while (numbit >= (magic_key_size - 12)) {

		unsigned int i = 0;

		while (i < nb) {

			uint64_t magickey = dist(rnd) & dist(rnd) & dist(rnd);

			// test the magic key & bit
			if (testMagicKey(sq, magickey, numbit, type, &testcount,
				&testcollision, &maxval) == 1) {

				if (maxval > bestmaxvalue) {
					bestmaxvalue = maxval;
				}

				if (testcollision > bestCollision) {
					bestKey = magickey;
					bestNumBit = numbit;
					bestCount = testcount;
					bestCollision = testcollision;
				}
			}

			++i;
		}

		if (bestCollision != -1) {
			break;
		}

		numbit--;
	}

	if (type == bishop) {
		magic_bishop_magic[sq] = bestKey;
		magic_bishop_shift[sq] = bestNumBit;
	} else {
		magic_rook_magic[sq] = bestKey;
		magic_rook_shift[sq] = bestNumBit;
	}
	LOG_DEBUG << "Square "
			  << ChessMove::strSquare(sq)
			  << " : "
			  << "0x" << std::setfill('0') << std::setw(16) << std::hex
			  << bestKey << std::dec
			  << " with " << bestNumBit << " bits (count = "
			  << bestCount << ", collision = " << bestCollision << ")";

	// return the size we need in the table
	return (1 << (magic_key_size - bestNumBit));
}
#endif

////////////////////////////////////////////////////////////////////////////////
void initPawnAttacks()
{
	for (int y=0; y<nb_ranks; y++) {
		for (int x=0; x<nb_files; x++) {

			unsigned int ofs = makeSquare(x, y);

			if (y < rank_8) {
				if (x > file_a) {
					pawn_attack[white][ofs] |= square[ofs + dir_up_left];
				}
				if (x < file_h) {
					pawn_attack[white][ofs] |= square[ofs + dir_up_right];
				}
			}

			if (y > rank_1) {
				if (x > file_a) {
					pawn_attack[black][ofs] |= square[ofs + dir_down_left];
				}
				if (x < file_h) {
					pawn_attack[black][ofs] |= square[ofs + dir_down_right];
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void initKnightAttacks()
{
	for (int y=0; y<nb_ranks; y++) {
		for (int x=0; x<nb_files; x++) {

			unsigned int ofs = makeSquare(x, y);

			// knight
			if ((x > file_a) && (y < rank_7)) {
				knight_attack[ofs] |= square[ofs + dir_up_2 + dir_left];
			}
			if ((x < file_h) && (y < rank_7)) {
				knight_attack[ofs] |= square[ofs + dir_up_2 + dir_right];
			}
			if ((x > file_a) && (y > rank_2)) {
				knight_attack[ofs] |= square[ofs + dir_down_2 + dir_left];
			}
			if ((x < file_h) && (y > rank_2)) {
				knight_attack[ofs] |= square[ofs + dir_down_2 + dir_right];
			}
			if ((x < file_g) && (y > rank_1)) {
				knight_attack[ofs] |= square[ofs + dir_down + dir_right * 2];
			}
			if ((x < file_g) && (y < rank_8)) {
				knight_attack[ofs] |= square[ofs + dir_up + dir_right * 2];
			}
			if ((x > file_b) && (y > rank_1)) {
				knight_attack[ofs] |= square[ofs + dir_down + dir_left * 2];
			}
			if ((x > file_b) && (y < rank_8)) {
				knight_attack[ofs] |= square[ofs + dir_up + dir_left * 2];
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void initKingAttacks()
{
	for (int y=0; y<nb_ranks; y++) {
		for (int x=0; x<nb_files; x++) {

			unsigned int ofs = makeSquare(x, y);

			// king
			if ((x > file_a) && (y > rank_1)) {
				king_attack[ofs] |= square[ofs + dir_down + dir_left];
			}
			if ((y > rank_1)) {
				king_attack[ofs] |= square[ofs + dir_down];
			}
			if ((x < file_h) && (y > rank_1)) {
				king_attack[ofs] |= square[ofs + dir_down + dir_right];
			}
			if ((x > file_a) && (y < rank_8)) {
				king_attack[ofs] |= square[ofs + dir_up + dir_left];
			}
			if ((y < rank_8)) {
				king_attack[ofs] |= square[ofs + dir_up];
			}
			if ((x < file_h) && (y < rank_8)) {
				king_attack[ofs] |= square[ofs + dir_up + dir_right];
			}
			if ((x > file_a)) {
				king_attack[ofs] |= square[ofs + dir_left];
			}
			if ((x < file_h)) {
				king_attack[ofs] |= square[ofs + dir_right];
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void initDiagonals()
{
	constexpr int ur = dir_up + dir_right;
	constexpr int ul = dir_up + dir_left;

	for (int p=0; p<nb_squares; p++) {

		diag_45r[p] = square[p];
		diag_45l[p] = square[p];

		// up right
		for (int n=p+ur; ((n < nb_squares) && (squareFile(n) > squareFile(p)));
			n+=ur) {
			diag_45r[p] |= square[n];
		}

		// down left
		for (int n=p-ur; ((n >= 0) && (squareFile(n) < squareFile(p))); n-=ur) {
			diag_45r[p] |= square[n];
		}

		// up left
		for (int n=p-ul; ((n >= 0) && (squareFile(n) > squareFile(p))); n-=ul) {
			diag_45l[p] |= square[n];
		}

		// down right
		for (int n=p+ul; ((n < nb_squares) && (squareFile(n) < squareFile(p)));
			n+=ul) {
			diag_45l[p] |= square[n];
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void initDirections()
{
	for (int x=0; x<nb_squares; x++) {
		for (int y=0; y<nb_squares; y++) {

			int	dx = squareFile(x) - squareFile(y);
			int	dy = squareRank(x) - squareRank(y);

			// default = no direction
			int8_t d = dir_none;

			if ((dx == 0) && (dy > 0)) {
				d = dir_up;
			}
			if ((dx > 0) && (dy == 0)) {
				d = dir_right;
			}
			if ((dx == 0) && (dy < 0)) {
				d = dir_up;
			}
			if ((dx < 0) && (dy == 0)) {
				d = dir_right;
			}

			if (std::abs(dx) == std::abs(dy)) {

				if ((dx > 0) && (dy > 0)) {
					d = dir_up_right;
				}
				if ((dx > 0) && (dy < 0)) {
					d = dir_up_left;
				}
				if ((dx < 0) && (dy < 0)) {
					d = dir_up_right;
				}
				if ((dx < 0) && (dy > 0)) {
					d = dir_up_left;
				}
			}

			direction[y][x] = d;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void initSquaresBetween()
{
	// squares between two squares on the same diagonal / file / rank
	for (int y=0; y<nb_squares; y++) {

		int yr = squareRank(y);
		int yf = squareFile(y);

		for (int x=0; x<nb_squares; x++) {

			int xr = squareRank(x);
			int xf = squareFile(x);
			int	dx = xf - yf;
			int	dy = xr - yr;

			between[y][x] = 0;

			if ((dx != 0) || (dy != 0)) {

				if (std::abs(dx) == std::abs(dy)) {

					if (dx > 0) {
						if (dy > 0) {
							for (int n=1; n<dx; n++) {
								between[y][x] |=
									square[makeSquare(yf + n, yr + n)];
							}
						} else {
							for (int n=1; n<dx; n++) {
								between[y][x] |=
									square[makeSquare(yf + n, yr - n)];
							}
						}
					} else {
						if (dy > 0) {
							for (int n=1; n<-dx; n++) {
								between[y][x] |=
									square[makeSquare(yf - n, yr + n)];
							}
						} else {
							for (int n=1; n<-dx; n++) {
								between[y][x] |=
									square[makeSquare(yf - n, yr - n)];
							}
						}
					}

				} else {

					if (dx == 0) {
						if (dy > 0) {
							for (int n=yr+1; n<xr; n++) {
								between[y][x] |= square[makeSquare(xf, n)];
							}
						} else {
							for (int n=xr+1; n<yr; n++) {
								between[y][x] |= square[makeSquare(xf, n)];
							}
						}
					} else
					if (dy == 0) {
						if (dx > 0) {
							for (int n=yf+1; n<xf; n++) {
								between[y][x] |= square[makeSquare(n, xr)];
							}
						} else {
							for (int n=xf+1; n<yf; n++) {
								between[y][x] |= square[makeSquare(n, xr)];
							}
						}
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
#ifdef FIND_MAGIC_KEYS
void buildMagicTables()
{
	std::mt19937_64 mtrnd(0x434845455345);

	unsigned int szb = 0;
	unsigned int szr = 0;

	// bishop :
	LOG_DEBUG << "Bishop magic keys :";
	for (int p=0; p<nb_squares; p++) {
		szb += findMagicKey(p, 1000000, 5, bishop, mtrnd);
	}

	// rook
	LOG_DEBUG << "Rook magic keys :";
	for (int p=0; p<nb_squares; p++) {
		szr += findMagicKey(p, 1000000, 10, rook, mtrnd);
	}

	for (int n=0; n<nb_squares; n++) {
		magic_bishop_mask[n] = filterBishopAttack(BishopAttack(n, 0ULL));
		magic_rook_mask[n] = filterRookAttack(RookAttack(n, 0ULL), n);
	}

	// output magic + shift
	std::cout << "bishop table size = " << std::dec << szb << std::endl;

	std::cout << "bishop magic : " << std::endl;
	for (int p=0; p<nb_squares; p+=4) {
		std::cout << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_magic[p] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_magic[p + 1] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_magic[p + 2] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_magic[p + 3] << "ULL, " << std::endl;
	}

	std::cout << "bishop shift : " << std::dec << std::endl;
	for (int p=0; p<nb_squares; p+=8) {
		std::cout << magic_bishop_shift[p]
				  << ", " << magic_bishop_shift[p + 1]
				  << ", " << magic_bishop_shift[p + 2]
				  << ", " << magic_bishop_shift[p + 3]
				  << ", " << magic_bishop_shift[p + 4]
				  << ", " << magic_bishop_shift[p + 5]
  				  << ", " << magic_bishop_shift[p + 6]
				  << ", " << magic_bishop_shift[p + 7] << std::endl;
	}

	// output bishop masks
	std::cout << "bishop mask : " << std::endl;
	for (int p=0; p<nb_squares; p+=4) {
		std::cout << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_mask[p] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_mask[p + 1] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_mask[p + 2] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_bishop_mask[p + 3] << "ULL, " << std::endl;
	}

	std::cout << "rook table size = " << std::dec << szr << std::endl;

	std::cout << "rook magic : " << std::endl;
	for (int p=0; p<nb_squares; p+=4) {
		std::cout << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_magic[p] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_magic[p + 1] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_magic[p + 2] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_magic[p + 3] << "ULL, " << std::endl;
	}

	std::cout << "rook shift : " << std::dec << std::endl;
	for (int p=0; p<nb_squares; p+=8) {
		std::cout << magic_rook_shift[p]
				  << ", " << magic_rook_shift[p + 1]
				  << ", " << magic_rook_shift[p + 2]
				  << ", " << magic_rook_shift[p + 3]
				  << ", " << magic_rook_shift[p + 4]
				  << ", " << magic_rook_shift[p + 5]
  				  << ", " << magic_rook_shift[p + 6]
				  << ", " << magic_rook_shift[p + 7] << std::endl;
	}

	// output rook masks
	std::cout << "rook mask : " << std::endl;
	for (int p=0; p<nb_squares; p+=4) {
		std::cout << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_mask[p] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_mask[p + 1] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_mask[p + 2] << "ULL, "
				  << "0x" << std::setfill('0') << std::setw(16) << std::hex
				  << magic_rook_mask[p + 3] << "ULL, " << std::endl;
	}
}
#endif // FIND_MAGIC_KEYS

////////////////////////////////////////////////////////////////////////////////
void init()
{
#ifdef DEBUG
	LOG_DEBUG << "Init Bitboards";

	Timer timer;
	timer.start();
#endif
	initPawnAttacks();
	initKnightAttacks();
	initKingAttacks();
	initDiagonals();
	initDirections();
	initSquaresBetween();

#ifdef FIND_MAGIC_KEYS
	buildMagicTables();
#endif

	createMagicTable();

#ifdef DEBUG
	uint64_t timesearch = timer.getElapsedTime();
	LOG_DEBUG << "time " << Timer::timeToString(timesearch) << " ms";
#endif
}

////////////////////////////////////////////////////////////////////////////////
void printBitBoard(BitBoard bb, std::ostream &stream)
{
	stream << std::setfill('0') << std::setw(16) << std::hex << bb;
	std::string tmp;
	stream << "\n|---|---|---|---|---|---|---|---|\n";
	for (int y=0; y<nb_ranks; y++) {
		tmp = '|';
		for (int x=0; x<nb_files; x++) {
			tmp += ' ';
			int sq = makeSquare(x, rank_8 - y);
			if ((bb & BitBoards::square[sq]) != 0ULL) {
				tmp += 'x';
			} else {
				tmp += ' ';
			}
			tmp += ' ';
			tmp += '|';
		}
		tmp += '\n';
		stream << tmp;
		stream << "|---|---|---|---|---|---|---|---|\n";
	}
	stream << std::endl;
}

} // namespace Cheese::BitBoards

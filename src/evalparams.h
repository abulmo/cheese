////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_EVALPARAMS_H_
#define CHEESE_EVALPARAMS_H_

#include "config.h"

#include "move.h"

#include <array>

namespace Cheese {

// evaluation phases (middle game, end game)
constexpr int nb_eval_phases = 2;

// Default parameters for evaluation function

////////////////////////////////////////////////////////////////////////////////
// pst data black
//
//   + warning : inverted on y axis !
//
const std::array<std::array<int32_t, nb_squares>, nb_pieces_type>
	param_psqt_mid = {

	{
	// not used
	{},
	// pawn
	{
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,  10,  10,   0,   0,   0,
	   -4,  -2,  -2,  20,  20,  -2,  -2,  -4,
	   -2,  -1,  -1,  10,  10,  -1,  -1,  -2,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0
	},
	// knight
	{
  	  -32, -16,  -8,  -8,  -8,  -8, -16, -32,
	  -16,  -8,   0,   0,   0,   0,  -8, -16,
	   -8,   2,   2,   2,   2,   2,   2,  -8,
	   -8,   2,   4,   4,   4,   4,   2,  -8,
	   -8,   2,   8,   8,   8,   8,   2,  -8,
	   -8,   2,   8,   8,   8,   8,   2,  -8,
	  -16,  -8,   4,   4,   4,   4,  -8, -16,
	  -32, -16,  -8,  -8,  -8,  -8, -16, -32
	},
	// bishop
	{
	  -32, -16,  -8,  -8,  -8,  -8, -16, -32,
	  -16,   0,   0,   0,   0,   0,   0, -16,
	   -8,   2,   2,   2,   2,   2,   2,  -8,
	   -8,   2,   4,   4,   4,   4,   2,  -8,
	   -8,   2,   8,   8,   8,   8,   2,  -8,
	   -8,   2,   8,   8,   8,   8,   2,  -8,
	  -16,   8,   4,   4,   4,   4,   8, -16,
	  -32, -16,  -8,  -8,  -8,  -8, -16, -32
	},
	// rook
	{
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4,
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4,
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4,
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4,
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4,
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4,
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4,
	   -4,  -2,  -1,   0,   0,  -1,  -2,  -4
	},
	// queen
	{
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0
	},
	// king
	{
	  -32, -32, -32, -32, -32, -32, -32, -32,
	  -32, -32, -32, -32, -32, -32, -32, -32,
	  -32, -32, -32, -32, -32, -32, -32, -32,
	  -32, -32, -32, -32, -32, -32, -32, -32,
	  -32, -32, -32, -32, -32, -32, -32, -32,
	  -32, -32, -32, -32, -32, -32, -32, -32,
	  -16, -16, -16, -16, -16, -16, -16, -16,
	  -8,  32,  -8,   0,   0,  -8,  32,  -8
	},
	// not used
	{}
	}
};

// pst data black
const std::array<std::array<int32_t, nb_squares>, nb_pieces_type>
	param_psqt_end = {

	{
	// not used
	{},
	// pawn
	{
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0
	},
	// knight
	{
   	  -12,  -6,   0,   6,   6,   0,  -6, -12,
	   -6,   0,   6,  12,  12,   6,   0,  -6,
	    0,   6,  12,  18,  18,  12,   6,   0,
	    6,  12,  18,  24,  24,  18,  12,   6,
	    6,  12,  18,  24,  24,  18,  12,   6,
	    0,   6,  12,  18,  18,  12,   6,   0,
	   -6,   0,   6,  12,  12,   6,   0,  -6,
  	  -12,  -6,   0,   6,   6,   0,  -6, -12
	},
	// bishop
	{
      -12,  -8,  -4,   0,   0,  -4,  -8, -12,
	   -8,  -4,   0,   4,   4,   0,  -4,  -8,
	   -4,   0,   4,   8,   8,   4,   0,  -4,
	    0,   4,   8,  12,  12,   8,   4,   0,
	    0,   4,   8,  12,  12,   8,   4,   0,
	   -4,   0,   4,   8,   8,   4,   0,  -4,
	   -8,  -4,   0,   4,   4,   0,  -4,  -8,
	  -12,  -8,  -4,   0,   0,  -4,  -8, -12

	},
	// rook
	{
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   0,   0,   0,   0,   0,   0
	},
	// queen
	{
		 0,  2,  4,  6,  6,  4,  2,  0,
		 2,  4,  6,  8,  8,  6,  4,  2,
		 4,  6,  8, 10, 10,  8,  6,  4,
		 6,  8, 10, 12, 12, 20,  8,  6,
		 6,  8, 10, 12, 12, 20,  8,  6,
		 4,  6,  8, 10, 10,  8,  6,  4,
		 2,  4,  6,  8,  8,  6,  4,  2,
		 0,  2,  4,  6,  6,  4,  2,  0

	},
	// king
	{
	  -50, -30, -30, -30, -30, -30, -30, -50,
	  -30,   0,   0,   0,   0,   0,   0, -30,
	  -30,   0,  16,  16,  16,  16,   0, -30,
	  -30,   0,  16,  32,  32,  16,   0, -30,
	  -30,   0,  16,  32,  32,  16,   0, -30,
	  -30,   0,  16,  16,  16,  16,   0, -30,
	  -30,   0,   0,   0,   0,   0,   0, -30,
	  -50, -30, -30, -30, -30, -30, -30, -50
	},
	// not used
	{}
	}
};

// knight outpost for black
const std::array<int32_t, nb_squares> param_eval_outpost_knight = {
    0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0,
	0,   8,  16,  24,  24,  16,   8,   0,
    0,   8,  16,  24,  24,  16,   8,   0,
    0,   8,  16,  16,  16,  16,   8,   0,
    0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0
};

// bishop outpost for black
const std::array<int32_t, nb_squares> param_eval_outpost_bishop = {
    0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0,
    0,   4,   8,  12,  12,   8,   4,   0,
    0,   4,   8,  12,  12,   8,   4,   0,
    0,   4,   8,   8,   8,   8,   4,   0,
    0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0
};

// material value
const std::array<int32_t, nb_pieces_type> param_piece_capture_value = {
	0,
	100,		// pawn
	325,		// knight
	325,		// bishop
	500,		// rook
	975,		// queen
	0,			// king
	0
};

// malus for doubled pawns
const std::array<int32_t, nb_eval_phases> param_eval_doubled_pawn =
	{ 10, 20 };

// isolated pawn
const std::array<int32_t, nb_eval_phases> param_eval_isolated_pawn =
	{ 10, 15 };

// backward pawn
const std::array<int32_t, nb_eval_phases> param_eval_backward_pawn =
	{ 15, 15 };

// open file
const std::array<int32_t, nb_eval_phases> param_eval_rook_open_file =
	{ 30, 10 };

// semi open file
const std::array<int32_t, nb_eval_phases> param_eval_rook_semi_open_file =
	{ 20, 10 };

// rook on 7th file
const std::array<int32_t, nb_eval_phases> param_eval_rook_on_7th =
	{ 10, 30 };

// bishop pair (bonus = half a pawn)
const std::array<int32_t, nb_eval_phases> param_eval_bishop_pair =
	{ 50, 50 };

const int32_t param_eval_passed_base_mid = 3;

const int32_t param_eval_passed_base_end = 5;

const int32_t param_eval_passed_free = 11;

const int32_t param_eval_passed_king_opp = 4;

const int32_t param_eval_passed_king_own = 2;

// mobility tables
const std::array<std::array<int32_t, nb_pieces_type>, nb_eval_phases>
	param_mob_translate = {{
		{ 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0 }
}};

const std::array<std::array<int32_t, nb_pieces_type>, nb_eval_phases>
	param_mob_scale = {{
		{ 0, 0,  6 * 8,  6 * 8,  4 * 8,  2 * 8,  0, 0 },
		{ 0, 0,  5 * 8,  5 * 8,  5 * 8,  3 * 8,  0, 0 }
}};

// scale king safety attacks by pieces
const std::array<int32_t, nb_pieces_type> param_king_safety_scale =  {
	0, 0, 2, 2, 3, 5, 0, 0
};

// king safety attacks table
const std::array<int32_t, 32> param_king_attack_table = {
	  0,  2,    4,  8,   11,  16,  23,  31,
	 41,  54,  69,  89, 113, 142, 176, 216,
	262, 314, 371, 431, 494, 556, 616, 673,
	725, 771, 811, 845, 874, 898, 918, 933
};

// eval for missing or moved pawn shield
const std::array<int32_t, nb_ranks> param_eval_pawn_shield = {
	-50, 0, 0, -25, -25, -25, -25, 0
};

const std::array<int32_t, nb_ranks> param_eval_pawn_storm = {
	0, 25, 20, 10,  0,  0,  0,  0
};

// blocked pawns at D2,E2 / D7,E7
const std::array<int32_t, nb_eval_phases> param_eval_blocked_pawn =
	{ 40, 0 };

// blocked bishops
const std::array<int32_t, nb_eval_phases> param_eval_trapped_bishop =
	{ 100, 0 };

// blocked knights
const std::array<int32_t, nb_eval_phases> param_eval_trapped_knight =
	{ 100, 0 };

// blocked rooks
const std::array<int32_t, nb_eval_phases> param_eval_blocked_rook =
	{ 50, 0 };

// tempo bonus
const std::array<int32_t, nb_eval_phases> param_eval_tempo =
	{ 5,  5 };

} // namespace Cheese

#endif //CHEESE_EVALPARAMS_H_

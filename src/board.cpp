////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "board.h"

#include "util/logfile.h"

#include <cassert>

#include <iostream>
#include <string>

namespace Cheese {

// offset to en passant square for each side (for the side who move)
const std::array<int, nb_sides> enpassant_offset = { dir_up, dir_down };

const std::array<uint8_t, nb_sides> castle_default = {
	castle_black_init,
	castle_white_init
};

////////////////////////////////////////////////////////////////////////////////
// default pieces positions used by setStartPosition
static const std::array<char, nb_squares> default_position_pieces = {
	 rook + 8,  knight + 8,  bishop + 8,  queen + 8,
	 king + 8,  bishop + 8,  knight + 8,  rook + 8,
	 pawn + 8,  pawn + 8,    pawn + 8,    pawn + 8,
	 pawn + 8,  pawn + 8,    pawn + 8,    pawn + 8,
	 0,     0,       0,       0,      0,     0,       0,       0,
	 0,     0,       0,       0,      0,     0,       0,       0,
	 0,     0,       0,       0,      0,     0,       0,       0,
	 0,     0,       0,       0,      0,     0,       0,       0,
	pawn, pawn,   pawn,   pawn,  pawn, pawn,   pawn,   pawn,
	rook, knight, bishop, queen, king, bishop, knight, rook
};

// material values for SEE
const std::array<int, nb_pieces_type> piece_value_see = {
	0,			// EMPTY
	100,		// pawn
	325,		// knight
	325,		// bishop
	500,		// rook
	975,		// queen
	5000,		// king
	0
};

////////////////////////////////////////////////////////////////////////////////
ChessBoard::ChessBoard() {
	clear();
	rules = ChessRules::classic;
}

////////////////////////////////////////////////////////////////////////////////
// clear board
void ChessBoard::clear()
{
	hashKey = 0;
	hashKeyPawns = 0;
	bbAllPiece = 0;
	bbColorPiece = {};
	bbPiece = {};
	pieces = {};
	evalMaterial = {};
	player = black;
	phaseCount = {};
	matCount = 0;
	posKing = {};
	castle = 0;
	enpassant = 0;
	fifty = 0;
	squaresCastleShort = {};
	squaresCastleLong = {};
	sqKingStart = {};
	sqCastleRookShort = {};
	sqCastleRookLong = {};
}

////////////////////////////////////////////////////////////////////////////////
// set chess default position
// FEN : rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
void ChessBoard::setStartPosition()
{
	setPosition(default_position_pieces, white, castle_init, 0, 0);
}

////////////////////////////////////////////////////////////////////////////////
void ChessBoard::setPosition(const std::array<char, nb_squares> &pc, Side s,
	int cst, int ep, int fft)
{
	bbAllPiece = 0;
	bbColorPiece = {};
	bbPiece = {};
	evalMaterial = {};
	phaseCount = {};
	matCount = 0;

	for (int y=0; y<nb_ranks; y++) {
		for (int x=0; x<nb_files; x++) {
			int sq = makeSquare(x, y);
			int p = static_cast<int>(pc[sq]);
			Side pside = (p < 8) ? black : white;
			p = (p < 8) ? p : (p - 8);
			if ((p >= pawn) && (p <= king)) {
				setBoardPiece(pside, sq, p);
				setPiece(sq, static_cast<uint8_t>(p));
				evalMaterial[pside] += pieces_value[p];
				matCount += material_count_mask[pside][p];
				phaseCount[pside] += piece_game_phase[p];
				if (p == king) {
					posKing[pside] = sq;
				}
			} else {
				clearPiece(sq);
			}
		}
	}

	player = s;
	castle = static_cast<uint8_t>(cst);
	enpassant = static_cast<uint8_t>(ep);
	fifty = static_cast<uint8_t>(fft);

	hashKey = createHashKey();
	hashKeyPawns = createHashKeyPawns();

	initCastlingRights();
}

////////////////////////////////////////////////////////////////////////////////
void ChessBoard::initCastlingRights()
{
	// king must be on start square
	// (should not happen with a valid FEN)
	if (squareRank(posKing[white]) != rank_1) {
		castle &= ~castle_white_init;
	}

	if (squareRank(posKing[black]) != rank_8) {
		castle &= ~castle_black_init;
	}

	// king start position
	// if the king didn't move we know he is on his start square
	// else suppose it was the default chess start position
	if (canCastle(castle_black_init)) {
		sqKingStart[black] = posKing[black];
	} else {
		sqKingStart[black] = e8;
	}

	if (canCastle(castle_white_init)) {
		sqKingStart[white] = posKing[white];
	} else {
		sqKingStart[white] = e1;
	}

	// default
	sqCastleRookShort[black] = h8;
	sqCastleRookLong[black] = a8;
	sqCastleRookShort[white] = h1;
	sqCastleRookLong[white] = a1;

	// * PB : KQkq : if other rook moved between this rook and the king !
	bool found = false;
	if (canCastle(castle_black_short)) {
		for (int i=squareFile(posKing[black])+1; i<=file_h; i++) {
			if (bitboardPiece(black, rook) &
				BitBoards::square[makeSquare(i, rank_8)]) {
				sqCastleRookShort[black] = makeSquare(i, rank_8);
				found = true;
			}
		}
		if (!found) {
			castle &= ~castle_black_short;
		}
	}

	found = false;
	if (canCastle(castle_black_long)) {
		for (int i=squareFile(posKing[black])-1; i>=file_a; i--) {
			if (bitboardPiece(black, rook) &
				BitBoards::square[makeSquare(i, rank_8)]) {
				sqCastleRookLong[black] = makeSquare(i, rank_8);
				found = true;
			}
		}
		if (!found) {
			castle &= ~castle_black_long;
		}
	}

	found = false;
	if (canCastle(castle_white_short)) {
		for (int i=squareFile(posKing[white])+1; i<=file_h; i++) {
			if (bitboardPiece(white, rook) &
				BitBoards::square[makeSquare(i, rank_1)]) {
				sqCastleRookShort[white] = makeSquare(i, rank_1);
				found = true;
			}
		}
		if (!found) {
			castle &= ~castle_white_short;
		}
	}

	found = false;
	if (canCastle(castle_white_long)) {
		for (int i=squareFile(posKing[white])-1; i>=file_a; i--) {
			if (bitboardPiece(white, rook) &
				BitBoards::square[makeSquare(i, rank_1)]) {
				sqCastleRookLong[white] = makeSquare(i, rank_1);
				found = true;
			}
		}
		if (!found) {
			castle &= ~castle_white_long;
		}
	}

	squaresCastleShort[white] = 0ULL;
	if (canCastle(castle_white_short)) {
		for (int i=std::min(sqKingStart[white], static_cast<int>(g1));
			i<=std::max(sqKingStart[white], static_cast<int>(g1)); i++) {
			squaresCastleShort[white] |= BitBoards::square[i];
		}
		for (int i=std::min(sqCastleRookShort[white], static_cast<int>(f1));
			i<=std::max(sqCastleRookShort[white], static_cast<int>(f1)); i++) {
			squaresCastleShort[white] |= BitBoards::square[i];
		}
		squaresCastleShort[white] &= ~BitBoards::square[sqKingStart[white]];
		squaresCastleShort[white] &=
			~BitBoards::square[sqCastleRookShort[white]];
	}

	squaresCastleLong[white] = 0ULL;
	if (canCastle(castle_white_long)) {
		for (int i=std::min(sqKingStart[white], static_cast<int>(c1));
			i<=std::max(sqKingStart[white], static_cast<int>(c1)); i++) {
			squaresCastleLong[white] |= BitBoards::square[i];
		}
		for (int i=std::min(sqCastleRookLong[white], static_cast<int>(d1));
			i<=std::max(sqCastleRookLong[white], static_cast<int>(d1)); i++) {
			squaresCastleLong[white] |= BitBoards::square[i];
		}
		squaresCastleLong[white] &= ~BitBoards::square[sqKingStart[white]];
		squaresCastleLong[white] &= ~BitBoards::square[sqCastleRookLong[white]];
	}

	squaresCastleShort[black] = 0ULL;
	if (canCastle(castle_black_short)) {
		for (int i=std::min(sqKingStart[black], static_cast<int>(g8));
			i<=std::max(sqKingStart[black], static_cast<int>(g8)); i++) {
			squaresCastleShort[black] |= BitBoards::square[i];
		}
		for (int i=std::min(sqCastleRookShort[black], static_cast<int>(f8));
			i<=std::max(sqCastleRookShort[black], static_cast<int>(f8)); i++) {
			squaresCastleShort[black] |= BitBoards::square[i];
		}
		squaresCastleShort[black] &= ~BitBoards::square[sqKingStart[black]];
		squaresCastleShort[black] &=
			~BitBoards::square[sqCastleRookShort[black]];
	}

	squaresCastleLong[black] = 0ULL;
	if (canCastle(castle_black_long)) {
		for (int i=std::min(sqKingStart[black], static_cast<int>(c8));
			i<=std::max(sqKingStart[black], static_cast<int>(c8)); i++) {
			squaresCastleLong[black] |= BitBoards::square[i];
		}
		for (int i=std::min(sqCastleRookLong[black], static_cast<int>(d8));
			i<=std::max(sqCastleRookLong[black], static_cast<int>(d8)); i++) {
			squaresCastleLong[black] |= BitBoards::square[i];
		}
		squaresCastleLong[black] &= ~BitBoards::square[sqKingStart[black]];
		squaresCastleLong[black] &= ~BitBoards::square[sqCastleRookLong[black]];
	}

#ifdef USELOG_DEBUG
	LOG_DEBUG << "White castle : "
			  << (canCastle(castle_white_short) ? 'K' : ' ')
			  << (canCastle(castle_white_long) ? 'Q' : ' ');
	LOG_DEBUG << "Black castle : "
			  << (canCastle(castle_black_short) ? 'k' : ' ')
			  << (canCastle(castle_black_long) ? 'q' : ' ');
	LOG_DEBUG << "White King position = "
			  << ChessMove::strSquare(sqKingStart[white]);
	LOG_DEBUG << "Black King position = "
			  << ChessMove::strSquare(sqKingStart[black]);
	LOG_DEBUG << "White Rook short = "
			  << ChessMove::strSquare(sqCastleRookShort[white]);
	LOG_DEBUG << "White Rook long = "
			  << ChessMove::strSquare(sqCastleRookLong[white]);
	LOG_DEBUG << "Black Rook short = "
			  << ChessMove::strSquare(sqCastleRookShort[black]);
	LOG_DEBUG << "Black Rook long = "
			  << ChessMove::strSquare(sqCastleRookLong[black]);
#endif
}

////////////////////////////////////////////////////////////////////////////////
// Static Exchange Evaluation (SEE)
int ChessBoard::staticExchangeEvaluation(ChessMove move) const
{
	// attack type : 1 = bishop like, 2 = rook like
	static const std::array<int, nb_pieces_type> attType =
		{ 0, 1, 0, 1, 2, 3, 0, 0 };

	std::array<int, 32> capture;

	int src = move.src();
	int dst = move.dst();

	// find pieces attacking & defending the square
	// generate attacks bitboard, on dst square for all sides
	BitBoard bbQueenBishop = bitboardPiece(white, queen) |
							 bitboardPiece(white, bishop) |
							 bitboardPiece(black, queen) |
							 bitboardPiece(black, bishop);

	BitBoard bbQueenRook = bitboardPiece(white, queen) |
						   bitboardPiece(white, rook) |
						   bitboardPiece(black, queen) |
						   bitboardPiece(black, rook);

	BitBoard bbAttack =
		((bitboardPiece(black, pawn) & BitBoards::attackPawn(white, dst)) |
		 (bitboardPiece(white, pawn) & BitBoards::attackPawn(black, dst)));
	bbAttack |= (bitboardPiece(white, knight) |
			     bitboardPiece(black, knight)) & BitBoards::attackKnight(dst);
	bbAttack |= (bbQueenBishop & BitBoards::attackBishop(dst, bitboardAll()));
	bbAttack |= (bbQueenRook & BitBoards::attackRook(dst, bitboardAll()));
	bbAttack |= (bitboardPiece(white, king) | bitboardPiece(black, king)) &
				 BitBoards::attackKing(dst);

	BitBoard bbOcc = bitboardAll();

	int ncap = 0;

	// current piece value (the next piece captured)
	int cPiece = move.piece();

	capture[ncap] = piece_value_see[move.capture()];

	int pro = move.promotion();
	if (pro != no_piece) {
		cPiece = pro;
		capture[ncap] += piece_value_see[pro] - piece_value_see[pawn];
	}

	++ncap;

	// remove first attacker
	bbOcc &= ~BitBoards::square[src];

	// e.p : remove the captures piece !
	// a rook or queen in front of the e.p captured pawn could capture
	// the moved piece
	int ep = move.isEnPassant();
	if (ep != 0) {
		int sqe = dst + enpassant_offset[player];
		bbOcc &= ~BitBoards::square[sqe];
	}

	// discovered attacks
	if (attType[cPiece] & 1) {
		bbAttack |= (BitBoards::attackBishop(dst, bbOcc) & bbQueenBishop);
	}

	// note : 1st pawn move can discover file attack (promotion, en passant)
	if ((cPiece == pawn) || (attType[cPiece] & 2)) {
		bbAttack |= (BitBoards::attackRook(dst, bbOcc) & bbQueenRook);
	}

	bbAttack &= bbOcc;

	Side side = ~player;

	// loop all possible captures
	while (bbAttack) {

		BitBoard bbTmp;

		// find the least valuable attacker
		int n;
		for (n=pawn; n<=king; n++) {
			bbTmp = (bbAttack & bitboardPiece(side, n));
			if (bbTmp) {
				break;
			}
		}

		if (n > king) {
			break;
		}

		// remove attacker
		bbOcc ^= (bbTmp & ~(bbTmp - 1));

		// discovered attacks
		if (attType[n] & 1) {
			bbAttack |= (BitBoards::attackBishop(dst, bbOcc) & bbQueenBishop);
		}

		if (attType[n] & 2) {
			bbAttack |= (BitBoards::attackRook(dst, bbOcc) & bbQueenRook);
		}

		bbAttack &= bbOcc;

		capture[ncap] = -capture[ncap - 1] + piece_value_see[cPiece];

		++ncap;

		// stop after king capture ?
		if (cPiece == king) {
			break;
		}

		cPiece = n;

		// stop here : doesn't change the result ?
		//if ((capture[ncap - 1]  - piece_value_see[cPiece]) > 0) {
		//	break;
		//}

		side = ~side;
	}

	while (--ncap) {
		capture[ncap - 1] = -std::max(-capture[ncap - 1], capture[ncap]);
	}

	return capture[0];
}

////////////////////////////////////////////////////////////////////////////////
// Make a move
void ChessBoard::makeMove(ChessMove move, ChessMoveRec &rec)
{
	// decode move
	const int src = move.src();
	int dst = move.dst();
	const int mp = move.piece();
	const int cp = move.capture();
	const int pp = move.promotion();
	const bool cst = move.isCastle();

	// save flags
	rec.hashKey = hashKey; // save old hash key
	rec.hashKeyPawns = hashKeyPawns;
	rec.castle = castle;
	rec.enpassant = enpassant;
	rec.fifty = fifty;

	// fifty moves rule : pawn move or capture = reset
	if ((mp == pawn) || (cp != no_piece)) {
		fifty = 0;
	} else {
		++fifty;
	}

	int rsrc = 0;
	int rdst = 0;

	if (cst) {
		rsrc = dst;
		if (player == white) {
			rdst = (dst > src) ? f1 : d1;
			dst = (dst > src) ? g1 : c1;
		} else {
			rdst = (dst > src) ? f8 : d8;
			dst = (dst > src) ? g8 : c8;
		}
	}

	// clear source piece
	clearBoardPiece(player, src, mp);
	clearPiece(src);

	const Side notplayer = ~player;

	// clear destination piece (captured) if needed
	if (cp) {

		// update material count
		matCount -= material_count_mask[notplayer][cp];
		evalMaterial[notplayer] -= pieces_value[cp];
		phaseCount[notplayer] -= piece_game_phase[cp];

		int cdst = dst;

		// en passant
		if (move.isEnPassant()) {
			cdst += enpassant_offset[player];
		}

		clearBoardPiece(notplayer, cdst, cp);
		clearPiece(cdst);

		hashKey ^= Zobrist::piece[notplayer][cp][cdst];

		if (cp == pawn) {
			hashKeyPawns ^= Zobrist::piece[notplayer][pawn][cdst];
		}

		// if the rook is captured and we have not castled
		if (cp == rook) {

			if (player == black) {
				if (dst == sqCastleRookShort[white]) {
					castle &= ~castle_white_short;
				} else
				if (dst == sqCastleRookLong[white]) {
					castle &= ~castle_white_long;
				}
			} else {
				if (dst == sqCastleRookShort[black]) {
					castle &= ~castle_black_short;
				} else
				if (dst == sqCastleRookLong[black]) {
					castle &= ~castle_black_long;
				}
			}
		}
	}

	if (mp == king) {

		// castling
		if (cst) {

			// move rook
			// FRC : need to clear king and rook square
			// before adding king and rook to their new squares
			clearBoardPiece(player, rsrc, rook);
			setBoardPiece(player, rdst, rook);

			clearPiece(rsrc);
			setPiece(rdst, rook);

			hashKey ^= (Zobrist::piece[player][rook][rsrc] ^
						Zobrist::piece[player][rook][rdst]);
		}

		// king move : change position, and disable castling
		posKing[player] = dst;

		castle &= ~castle_default[player];

	} else
	if (mp == rook) { // rook move : disable castling on this side

		if (player == white) {
			if (src == sqCastleRookShort[white]) {
				castle &= ~castle_white_short;
			} else
			if (src == sqCastleRookLong[white]) {
				castle &= ~castle_white_long;
			}
		} else {
			if (src == sqCastleRookShort[black]) {
				castle &= ~castle_black_short;
			} else
			if (src == sqCastleRookLong[black]) {
				castle &= ~castle_black_long;
			}
		}
	}

	// new value of piece
	int np = mp;

	if (pp) {
		np = pp;

		// update material count
		matCount += (material_count_mask[player][pp] -
					 material_count_mask[player][pawn]);
		evalMaterial[player] += (pieces_value[pp] -
								 pieces_value[pawn]);
		phaseCount[player] += piece_game_phase[pp];
	}

	// move new piece to destination
	setBoardPiece(player, dst, np);
	setPiece(dst, static_cast<uint8_t>(np));

	hashKey ^= (Zobrist::piece[player][mp][src] ^
				Zobrist::piece[player][np][dst]);

	enpassant = 0;	// clear en passant file for next move
	if (mp == pawn) {

		if (isMoveTwiceUp(src, dst)) {
			enpassant = static_cast<uint8_t>(squareFile(dst) + 1);
		}

		hashKeyPawns ^= Zobrist::piece[player][pawn][src];
		if (!pp) {
			hashKeyPawns ^= Zobrist::piece[player][pawn][dst];
		}
	}

	if (rec.enpassant != enpassant) {
		hashKey ^= (Zobrist::enpassant[rec.enpassant] ^
					Zobrist::enpassant[enpassant]);
	}

	// replace castling rights in hash key
	if (castle != rec.castle) {
		hashKey ^= (Zobrist::castling[rec.castle] ^
					Zobrist::castling[castle]);
	}

	// change side
	hashKey ^= Zobrist::side;

	// next player to move
	player = notplayer;
}

////////////////////////////////////////////////////////////////////////////////
// unMake a move
void ChessBoard::unMakeMove(ChessMove move, const ChessMoveRec &rec)
{
	// decode move
	const int src = move.src();
	int dst = move.dst();
	const int mp = move.piece();
	const int cp = move.capture();
	const int pp = move.promotion();
	const bool cst = move.isCastle();

	// previous player to move
	const Side notplayer = player;
	player = ~player;

	int rsrc = 0;
	int rdst = 0;

	if (cst) {

		rdst = dst;

		if (player == white) {
			rsrc = (dst > src) ? f1 : d1;
			dst = (dst > src) ? g1 : c1;
		} else {
			rsrc = (dst > src) ? f8 : d8;
			dst = (dst > src) ? g8 : c8;
		}
	}

	// clear destination piece
	int np = mp;
	if (pp) {
		np = pp;

		// update material count
		matCount += (material_count_mask[player][pawn] -
					 material_count_mask[player][pp]);
		evalMaterial[player] += (pieces_value[pawn] -
								 pieces_value[pp]);
		phaseCount[player] -= piece_game_phase[pp];
	}

	clearBoardPiece(player, dst, np);
	clearPiece(dst);

	// set old destination piece if it was a capture
	if (cp) {

		// update material count
		matCount += material_count_mask[notplayer][cp];
		evalMaterial[notplayer] += pieces_value[cp];
		phaseCount[notplayer] += piece_game_phase[cp];

		// en passant
		int cdst = dst;

		if (move.isEnPassant()) {
			cdst += enpassant_offset[player];
		}

		setBoardPiece(notplayer, cdst, cp);
		setPiece(cdst, static_cast<uint8_t>(cp));
	}

	if (mp == king)  {

		// castling
		if (cst) {

			// move rook
			clearBoardPiece(player, rsrc, rook);
			setBoardPiece(player, rdst, rook);

			clearPiece(rsrc);
			setPiece(rdst, rook);
		}

		posKing[player] = src;
	}

	// set old source piece
	setBoardPiece(player, src, mp);
	setPiece(src, static_cast<uint8_t>(mp));

	// change ep flag + castling flags
	hashKey	  = rec.hashKey;
	hashKeyPawns = rec.hashKeyPawns;
	castle    = rec.castle;
	enpassant = rec.enpassant;
	fifty     = rec.fifty;
}

////////////////////////////////////////////////////////////////////////////////
// make a null move
void ChessBoard::makeNullMove(ChessMoveRec &rec)
{
	rec.hashKey = hashKey;
	rec.hashKeyPawns = hashKeyPawns;
	rec.enpassant = enpassant;
	rec.fifty = fifty;

	fifty = 0;

	if (enpassant != 0) {
		hashKey ^= Zobrist::enpassant[enpassant];
		enpassant = 0;
	}

	hashKey ^= Zobrist::side;
	player = ~player;
}

////////////////////////////////////////////////////////////////////////////////
// unmake a null move
void ChessBoard::unMakeNullMove(const ChessMoveRec &rec)
{
	player = ~player;
	hashKey = rec.hashKey;
	hashKeyPawns = rec.hashKeyPawns;
	enpassant = rec.enpassant;
	fifty = rec.fifty;
}

////////////////////////////////////////////////////////////////////////////////
// Test move validity : but don't test if the move leave us in check
bool ChessBoard::validMove(ChessMove move) const
{
	if (move.isEmpty()) {
        return false;
	}

	const int src = move.src();
	const int dst = move.dst();
	const int mp  = move.piece();
	const int cp  = move.capture();

	// test source square = moved piece
    if (!(bitboardPiece(player, mp) & BitBoards::square[src])) {
		return false;
	}

	// test destination square
    if ((cp != no_piece) && (!move.isEnPassant())) {
		if ((cp == king) ||
			(!(bitboardPiece(~player, cp) & BitBoards::square[dst]))) {
			return false;
		}
    } else {
		if ((!move.isCastle()) &&
			(piece(dst) != no_piece)) {
			return false;
		}
	}

	switch (mp) {

		case	pawn:
				if (move.isPromotion()) {
					int n = (player == white) ? rank_8 : rank_1;
					if (squareRank(dst) != n) {
						return false;
					}
				}
				if (!move.isEnPassant()) {
					int n = (player == white) ? dir_down : dir_up;
					if (move.isPawnTwice()) {
						if ((piece(dst + n) != no_piece) ||
							((dst + n + n) != src)) {
							return false;
						}
					} else {
						if (cp == no_piece) {
							if ((dst + n) != src) {
								return false;
							}
						} else {
							if (((dst + n + dir_left) != src) &&
								((dst + n + dir_right) != src)) {
								return false;
							}
						}
					}
				} else {
					if (enpassant == (squareFile(dst) + 1)) {

						int n = (player == white) ? dir_down : dir_up;

						if (!(bitboardPiece(~player, pawn) &
							BitBoards::square[dst + n])) {
							return false;
						}

					} else {
						return false;
					}
				}
				break;

		case	bishop:
				if (!(BitBoards::attackBishop(src, bitboardAll()) &
					BitBoards::square[dst])) {
					return false;
				}
				break;

		case	rook:
				if (!(BitBoards::attackRook(src, bitboardAll()) &
					BitBoards::square[dst])) {
					return false;
				}
				break;

		case	queen:
				if (!(BitBoards::attackQueen(src, bitboardAll()) &
					BitBoards::square[dst])) {
					return false;
				}
				break;

		case	king:
				if (move.isCastle()) {

					if (piece(sqKingStart[player]) != king) {
						return false;
					}

					if (player == white) {

						if (dst == sqCastleRookShort[white]) {

							if (!canCastle(castle_white_short)) {
								return false;
							}

							if (!(bitboardPiece(white, rook) &
								BitBoards::square[sqCastleRookShort[white]])) {
								return false;
							}

							if (bitboardAll() & squaresCastleShort[white]) {
								return false;
							}

							for (int i=std::min(sqKingStart[white],
								static_cast<int>(g1));
								i<=std::max(sqKingStart[white],
								static_cast<int>(g1)); i++) {
								if (isAttacked(i, black)) {
									return false;
								}
							}

						} else
						if (dst == sqCastleRookLong[white]) {

							if (!canCastle(castle_white_long)) {
								return false;
							}

							if (!(bitboardPiece(white, rook) &
								BitBoards::square[sqCastleRookLong[white]])) {
								return false;
							}

							if (bitboardAll() & squaresCastleLong[white]) {
								return false;
							}

							for (int i=std::min(sqKingStart[white],
								static_cast<int>(c1));
								i<=std::max(sqKingStart[white],
								static_cast<int>(c1)); i++) {
								if (isAttacked(i, black)) {
									return false;
								}
							}

						} else {
							return false;
						}

					} else {

						if (dst == sqCastleRookShort[black]) {

							if (!canCastle(castle_black_short)) {
								return false;
							}

							if (!(bitboardPiece(black, rook) &
								BitBoards::square[sqCastleRookShort[black]])) {
								return false;
							}

							if (bitboardAll() & squaresCastleShort[black]) {
								return false;
							}

							for (int i=std::min(sqKingStart[black],
								static_cast<int>(g8));
								i<=std::max(sqKingStart[black],
								static_cast<int>(g8)); i++) {
								if (isAttacked(i, white)) {
									return false;
								}
							}

						} else
						if (dst == sqCastleRookLong[black]) {

							if (!canCastle(castle_black_long)) {
								return false;
							}

							if (!(bitboardPiece(black, rook) &
								BitBoards::square[sqCastleRookLong[black]]))
							{
								return false;
							}

							if (bitboardAll() & squaresCastleLong[black]) {
								return false;
							}

							for (int i=std::min(sqKingStart[black],
								static_cast<int>(c8));
								i<=std::max(sqKingStart[black],
								static_cast<int>(c8)); i++) {
								if (isAttacked(i, white)) {
									return false;
								}
							}

						} else {
							return false;
						}
					}

				} else {
					// * king destination square
					if (!(BitBoards::attackKing(src) &
						BitBoards::square[dst])) {
						return false;
					}
				}
				break;

		case	knight:
				// * test : dst = knight move
				if (!(BitBoards::attackKnight(src) & BitBoards::square[dst])) {
					return false;
				}
				break;

		default:
			return false;
			break;
	}

	// here, the king can be in check but it is tested in the search

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// check if a square is attacked by a given color
// sq = square to test
// color = color of the attacker
bool ChessBoard::isAttacked(int sq, Side s) const
{
	return ((BitBoards::attackPawn(~s, sq) & bitboardPiece(s, pawn)) ||
			(BitBoards::attackKnight(sq) & bitboardPiece(s, knight)) ||
			(BitBoards::attackBishop(sq, bitboardAll()) &
				(bitboardPiece(s, bishop) | bitboardPiece(s, queen))) ||
			(BitBoards::attackRook(sq, bitboardAll()) &
				(bitboardPiece(s, rook) | bitboardPiece(s, queen))) ||
			(BitBoards::attackKing(sq) & bitboardPiece(s, king)));
}

////////////////////////////////////////////////////////////////////////////////
// same as isAttacked but with modified occupency
// (to see through the king for example)
bool ChessBoard::isAttackedOcc(int sq, Side s, BitBoard occ) const
{
	return ((BitBoards::attackPawn(~s, sq) & bitboardPiece(s, pawn)) ||
			(BitBoards::attackKnight(sq) & bitboardPiece(s, knight)) ||
			(BitBoards::attackBishop(sq, occ) &
				(bitboardPiece(s, bishop) | bitboardPiece(s, queen))) ||
			(BitBoards::attackRook(sq, occ) &
				(bitboardPiece(s, rook) | bitboardPiece(s, queen))) ||
			(BitBoards::attackKing(sq) & bitboardPiece(s, king)));
}

////////////////////////////////////////////////////////////////////////////////
// initialize Zobrist HashKey from current position
HashKey ChessBoard::createHashKey()
{
	HashKey key = 0ULL;

	for (int sq=0; sq<nb_squares; sq++) {
		if (piece(sq) != no_piece) {
			if (bitboardColor(white) & BitBoards::square[sq]) {
				key ^= Zobrist::piece[white][piece(sq)][sq];
			} else
			if (bitboardColor(black) & BitBoards::square[sq]) {
				key ^= Zobrist::piece[black][piece(sq)][sq];
			}
		}
	}

	key ^= Zobrist::enpassant[enpassant];
	key ^= Zobrist::castling[castle];

	if (player == black) {
		key ^= Zobrist::side;
	}

	return key;
}

////////////////////////////////////////////////////////////////////////////////
// initialize Zobrist HashKey for pawns from current position
HashKey ChessBoard::createHashKeyPawns()
{
	HashKey key = 0ULL;

	for (int sq=0; sq<nb_squares; sq++) {
		if (piece(sq) == pawn) {
			if (bitboardColor(white) & BitBoards::square[sq]) {
				key ^= Zobrist::piece[white][pawn][sq];
			} else
			if (bitboardColor(black) & BitBoards::square[sq]) {
				key ^= Zobrist::piece[black][pawn][sq];
			}
		}
	}

	return key;
}

////////////////////////////////////////////////////////////////////////////////
void ChessBoard::print(std::ostream &stream) const
{
	std::string tmp;
	stream << "\n|---|---|---|---|---|---|---|---|\n";
	for (int y=0; y<nb_ranks; y++) {
		tmp = '|';
		for (int x=0; x<nb_files; x++) {
			tmp += ' ';
			int p = makeSquare(x, rank_8 - y);
			if (bitboardColor(white) & BitBoards::square[p]) {
				tmp += pieces_name[piece(p) + 8];
			} else {
				tmp += pieces_name[piece(p)];
			}
			tmp += ' ';
			tmp += '|';
		}
		tmp += '\n';
		stream << tmp;
		stream << "|---|---|---|---|---|---|---|---|\n";
	}

	if (player == white) {
		stream << "White to play" << std::endl;
	} else {
		stream << "Black to play" << std::endl;
	}
}

} // namespace Cheese

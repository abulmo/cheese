////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_THREAD_H_
#define CHEESE_THREAD_H_

#include "config.h"

#ifdef WIN32
#include <process.h>
#include <windows.h>
#else
#include <pthread.h>
#include <unistd.h>
#endif

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

namespace Cheese {

// max stack size for threads
constexpr std::size_t thread_max_stack_size = 8 * 1024 * 1024;

////////////////////////////////////////////////////////////////////////////////
// Note : can't use std::thread, because we need to change threads stack size 
class Thread {

private:

#ifdef WIN32
	HANDLE handle;

	uint32_t tid;
#else
	pthread_t tid;
#endif

	// thread internal id
	uint32_t id;

	bool initialized;

protected:

	std::condition_variable cond;

	std::mutex mutex;

	std::atomic_bool terminate;

public:

	static uint32_t countThread;

public:

	Thread();

	virtual ~Thread();

	// create system thread
	void create();

	void stop();

	// get internal thread id
	uint32_t getId() const;

#ifdef WIN32
	static unsigned int __stdcall funcThread(void *param);
#else
	static void *funcThread(void *param);
#endif

	// thread main function
	virtual void func()
	{ }

};

////////////////////////////////////////////////////////////////////////////////
inline uint32_t Thread::getId() const
{
	return id;
}

} // namespace Cheese

#endif //CHEESE_THREAD_H_

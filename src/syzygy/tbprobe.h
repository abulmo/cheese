
#ifndef TBPROBE_H
#define TBPROBE_H

#include "board.h"
#include "move.h"
#include "repetitions.h"
#include "movegen.h"

namespace Cheese::TableBases {

// 5 if 5-piece tables, 6 if 6-piece tables were found.
extern int TBlargest;
extern int TBnum_piece;
extern int TBnum_pawn;

void init_tablebases(const char *path);

int probe_wdl(ChessBoard &board, int *success);

int probe_dtz(ChessBoard &board, int *success);

int root_probe(ChessBoard &board, int& TBScore, ChessMoveSortList &rootmoves,
	int count, ChessRepetitions &rep, int *newcount);

int root_probe_wdl(ChessBoard &board, int& TBScore,
	ChessMoveSortList &rootmoves, int count, int *newcount);

int probe_wdl_table(ChessBoard &board, int *success);

int probe_dtz_table(ChessBoard &board, int wdl, int *success);

void prt_str(ChessBoard &board, char *str, int mirror);

uint64_t calc_key(ChessBoard &board, int mirror);

} // namespace Cheese::TableBases

#endif

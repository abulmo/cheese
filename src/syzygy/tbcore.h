/*
  Copyright (c) 2011-2015 Ronald de Man
*/

// Original code by Ronald de Man, modified for Cheese

#ifndef TBCORE_H
#define TBCORE_H

#define USE_STD_MUTEX

#ifdef USE_STD_MUTEX
#include <atomic>
#include <mutex>
#endif

#ifndef WIN32
#include <pthread.h>
#else
#include <windows.h>
#endif

#if defined(_WIN64) || defined(_LP64)
#define DECOMP64
#endif

#ifdef USE_STD_MUTEX
#define LOCK_T std::mutex
#define LOCK_INIT(x)
#define LOCK_DESTROY(x)
#define LOCK(x) x.lock()
#define UNLOCK(x) x.unlock()
#else
#ifndef WIN32
#define LOCK_T pthread_mutex_t
#define LOCK_INIT(x) pthread_mutex_init(&(x), NULL)
#define LOCK_DESTROY(x) pthread_mutex_destroy(&(x))
#define LOCK(x) pthread_mutex_lock(&(x))
#define UNLOCK(x) pthread_mutex_unlock(&(x))
#else
#define LOCK_T HANDLE
#define LOCK_INIT(x) do { x = CreateMutex(NULL, FALSE, NULL); } while (0)
#define LOCK_DESTROY(x) CloseHandle(x)
#define LOCK(x) WaitForSingleObject(x, INFINITE)
#define UNLOCK(x) ReleaseMutex(x)
#endif
#endif // USE_STD_MUTEX

namespace Cheese::TableBases {

#ifndef WIN32
const char SEP_CHAR = ':';
typedef int FD;
const int FD_ERR = -1;
#else
const char SEP_CHAR = ';';
typedef HANDLE FD;
const HANDLE FD_ERR = INVALID_HANDLE_VALUE;
#endif

const int TBPIECES = 6;

const int TBHASHBITS = 10;

const int TBMAX_PIECE = 254;

const int TBMAX_PAWN = 256;

const int HSHMAX = 5;

const int DTZ_ENTRIES = 64;

struct TBHashEntry;

#ifdef DECOMP64
typedef uint64_t base_t;
#else
typedef uint32_t base_t;
#endif

struct PairsData {
	char *indextable;
	uint16_t *sizetable;
	uint8_t *data;
	uint16_t *offset;
	uint8_t *symlen;
	uint8_t *sympat;
	int blocksize;
	int idxbits;
	int min_len;
	base_t base[1]; // C++ complains about base[]...
};

struct TBEntry {
	char *data;
	uint64_t key;
	uint64_t mapping;

	#ifdef USE_STD_MUTEX
	std::atomic_uint8_t ready;	// sizeof ??
	#else
	uint8_t ready;
	#endif

	uint8_t num;
	uint8_t symmetric;
	uint8_t has_pawns;
};

struct TBEntry_piece {
	char *data;
	uint64_t key;
	uint64_t mapping;
	uint8_t ready;
	uint8_t num;
	uint8_t symmetric;
	uint8_t has_pawns;
	uint8_t enc_type;
	struct PairsData *precomp[2];
	int factor[2][TBPIECES];
	uint8_t pieces[2][TBPIECES];
	uint8_t norm[2][TBPIECES];
};

struct TBEntry_pawn {
	char *data;
	uint64_t key;
	uint64_t mapping;
	uint8_t ready;
	uint8_t num;
	uint8_t symmetric;
	uint8_t has_pawns;
	uint8_t pawns[2];
	struct {
		struct PairsData *precomp[2];
		int factor[2][TBPIECES];
		uint8_t pieces[2][TBPIECES];
		uint8_t norm[2][TBPIECES];
	} file[4];
};

struct DTZEntry_piece {
	char *data;
	uint64_t key;
	uint64_t mapping;
	uint8_t ready;
	uint8_t num;
	uint8_t symmetric;
	uint8_t has_pawns;
	uint8_t enc_type;
	struct PairsData *precomp;
	int factor[TBPIECES];
	uint8_t pieces[TBPIECES];
	uint8_t norm[TBPIECES];
	uint8_t flags; // accurate, mapped, side
	uint16_t map_idx[4];
	uint8_t *map;
};

struct DTZEntry_pawn {
	char *data;
	uint64_t key;
	uint64_t mapping;
	uint8_t ready;
	uint8_t num;
	uint8_t symmetric;
	uint8_t has_pawns;
	uint8_t pawns[2];
	struct {
		struct PairsData *precomp;
		int factor[TBPIECES];
		uint8_t pieces[TBPIECES];
		uint8_t norm[TBPIECES];
	} file[4];
	uint8_t flags[4];
	uint16_t map_idx[4][4];
	uint8_t *map;
};

struct TBHashEntry {
	uint64_t key;
	struct TBEntry *ptr;
};

struct DTZTableEntry {
	uint64_t key1;
	uint64_t key2;
	struct TBEntry *entry;
};

extern struct TBHashEntry TB_hash[1 << TBHASHBITS][HSHMAX];
extern struct DTZTableEntry DTZ_table[DTZ_ENTRIES];
extern LOCK_T TB_mutex;

extern const char pchr[6];
extern const int wdl_to_map[5];
extern const uint8_t pa_flags[5];

int init_table_wdl(struct TBEntry *entry, char *str);

uint64_t encode_piece(struct TBEntry_piece *ptr, uint8_t *norm, int *pos,
	int *factor);

uint8_t decompress_pairs(struct PairsData *d, uint64_t idx);

int pawn_file(struct TBEntry_pawn *ptr, int *pos);

uint64_t encode_pawn(struct TBEntry_pawn *ptr, uint8_t *norm, int *pos,
	int *factor);

void free_dtz_entry(struct TBEntry *entry);

void load_dtz_table(char *str, uint64_t key1, uint64_t key2);

} // namespace Cheese::TableBases

#endif

/*
  Copyright (c) 2013-2015 Ronald de Man
  This file may be redistributed and/or modified without restrictions.

  tbprobe.cpp contains the Stockfish-specific routines of the
  tablebase probing code. It should be relatively easy to adapt
  this code to other chess engines.
*/

// Original code by Ronald de Man, modified for Cheese

// The probing code currently expects a little-endian architecture (e.g. x86).

// Define DECOMP64 when compiling for a 64-bit platform.
// 32-bit is only supported for 5-piece tables, because tables are mmap()ed
// into memory.

#include "config.h"

#include "board.h"
#include "bitboard.h"
#include "move.h"
#include "movegen.h"
#include "zobrist.h"

#include "tbcore.h"
#include "tbprobe.h"

namespace Cheese::TableBases {

////////////////////////////////////////////////////////////////////////////////
const int TB_VALUE_MATE = 32000;
const int TB_MAX_PLY = 128;
const int TB_PAWN_VALUE_ENDGAME = 100;

const int wdl_to_dtz[] = {
  -1, -101, 0, 101, 1
};

const int wdl_to_value[5] = {
  -TB_VALUE_MATE + TB_MAX_PLY + 1,
  TB_VALUE_MATE - 2,
  TB_VALUE_MATE,
  TB_VALUE_MATE + 2,
  TB_VALUE_MATE - TB_MAX_PLY - 1
};

extern int TBlargest;

////////////////////////////////////////////////////////////////////////////////
// Given a position with 6 or fewer pieces, produce a text string
// of the form KQPvKRP, where "KQP" represents the white pieces if
// mirror == 0 and the black pieces if mirror == 1.
// No need to make this very efficient.
void prt_str(ChessBoard &board, char *str, int mirror)
{
	Side side = (mirror == 0) ? white : black;
	for (int pt = king; pt >= pawn; pt--) {
		int nb = BitBoards::bitCount(board.bitboardPiece(side, pt));
		for (int n=nb; n>0; n--) {
			*str++ = pchr[6 - pt];
		}
	}
	*str++ = 'v';
	side = (side == white) ? black : white;
	for (int pt = king; pt >= pawn; pt--) {
		int nb = BitBoards::bitCount(board.bitboardPiece(side, pt));
		for (int n=nb; n>0; n--) {
			*str++ = pchr[6 - pt];
		}
	}
	*str++ = 0;
}

////////////////////////////////////////////////////////////////////////////////
// Given a position, produce a 64-bit material signature key.
// If the engine supports such a key, it should equal the engine's key.
// Again no need to make this very efficient.
uint64_t calc_key(ChessBoard &board, int mirror)
{
	uint64_t key = 0;
	Side side = (mirror == 0) ? white : black;

	for (int pt=pawn; pt<=king; pt++) {
		int n = BitBoards::bitCount(board.bitboardPiece(side, pt));
		for (int i=n; i>0; i--) {
			key ^= Zobrist::piece[white][pt][i - 1];
		}
	}

	side = (mirror == 0) ? black : white;
	for (int pt=pawn; pt<=king; pt++) {
		int n = BitBoards::bitCount(board.bitboardPiece(side, pt));
		for (int i=n; i>0; i--) {
			key ^= Zobrist::piece[black][pt][i - 1];
		}
	}

	return key;
}

////////////////////////////////////////////////////////////////////////////////
// Produce a 64-bit material key corresponding to the material combination
// defined by pcs[16], where pcs[1], ..., pcs[6] is the number of white
// pawns, ..., kings and pcs[9], ..., pcs[14] is the number of black
// pawns, ..., kings.
// Again no need to be efficient here.
uint64_t calc_key_from_pcs(int *pcs, int mirror)
{
	uint64_t key = 0;
	int color = (mirror == 0) ? 0 : 8;

	for (int pt=pawn; pt<=king; pt++) {
		for (int i=0; i<pcs[color + pt]; i++) {
			key ^= Zobrist::piece[white][pt][i];
		}
	}
	color ^= 8;
	for (int pt=pawn; pt<=king; pt++) {
		for (int i=0; i<pcs[color + pt]; i++) {
			key ^= Zobrist::piece[black][pt][i];
		}
	}

	return key;
}

////////////////////////////////////////////////////////////////////////////////
// calculate material key for a given position
uint64_t engine_calc_key(ChessBoard &board)
{
	uint64_t key = 0;

	// TODO : use engine material count ?

	for (int side=0; side<=1; side++) {
		for (int pt=pawn; pt<=king; pt++) {
			int n = BitBoards::bitCount(board.bitboardPiece(
				static_cast<Side>(side), pt));
			for (int i=n; i>0; i--) {
				key ^= Zobrist::piece[side][pt][i - 1];
			}
		}
	}

	return key;
}

////////////////////////////////////////////////////////////////////////////////
// probe_wdl_table and probe_dtz_table require similar adaptations.
int probe_wdl_table(ChessBoard &board, int *success)
{
	uint8_t res;

	// Obtain the position's material signature key.
	uint64_t key = engine_calc_key(board);

	// Test for KvK.
	if (key == (Zobrist::piece[white][king][0] ^
				Zobrist::piece[black][king][0])) {
		return 0;
	}

	struct TBHashEntry *ptr2 = TB_hash[key >> (64 - TBHASHBITS)];
	int i;
	for (i = 0; i < HSHMAX; i++) {
		if (ptr2[i].key == key) {
			break;
		}
	}
	if (i == HSHMAX) {
		*success = 0;
		return 0;
	}

	struct TBEntry *ptr = ptr2[i].ptr;
	if (!ptr->ready) {
		LOCK(TB_mutex);
		if (!ptr->ready) {
			char str[16];
			prt_str(board, str, ptr->key != key);
			if (!init_table_wdl(ptr, str)) {
				ptr2[i].key = 0ULL;
				*success = 0;
				UNLOCK(TB_mutex);
				return 0;
			}

			ptr->ready = 1;
		}
		UNLOCK(TB_mutex);
	}

	int bside, mirror, cmirror;
	if (!ptr->symmetric) {
		if (key != ptr->key) {
			cmirror = 8;
			mirror = 0x38;
			bside = (board.side() == white);
		} else {
			cmirror = mirror = 0;
			bside = !(board.side() == white);
		}
	} else {
		cmirror = board.side() == white ? 0 : 8;
		mirror = board.side() == white ? 0 : 0x38;
		bside = 0;
	}

	int p[TBPIECES];

	// p[i] is to contain the square 0-63 (A1-H8) for a piece of type
	// pc[i] ^ cmirror, where 1 = white pawn, ..., 14 = black king.
	// Pieces of the same type are guaranteed to be consecutive.
	Side s;
	if (!ptr->has_pawns) {
		struct TBEntry_piece *entry = (struct TBEntry_piece *)ptr;
		uint8_t *pc = entry->pieces[bside];
		for (i = 0; i < entry->num;) {
			s = static_cast<Side>(1 - ((pc[i] ^ cmirror) >> 3));
			BitBoard bb = board.bitboardPiece(s, pc[i] & 0x07);
			do {
				p[i++] = BitBoards::popFirstBit(bb);
			} while (bb);
		}
		uint64_t idx = encode_piece(entry, entry->norm[bside], p,
			entry->factor[bside]);
		res = decompress_pairs(entry->precomp[bside], idx);
	} else {
		struct TBEntry_pawn *entry = (struct TBEntry_pawn *)ptr;
		int k = entry->file[0].pieces[0][0] ^ cmirror;
		s = static_cast<Side>(1 - (k >> 3));
		BitBoard bb = board.bitboardPiece(s, k & 0x07);
		i = 0;
		do {
			int sq = BitBoards::popFirstBit(bb);
			p[i++] = sq ^ mirror;
		} while (bb);
		int f = pawn_file(entry, p);
		uint8_t *pc = entry->file[f].pieces[bside];
		for (; i < entry->num;) {
			s = static_cast<Side>(1 - ((pc[i] ^ cmirror) >> 3));
			bb = board.bitboardPiece(s,	pc[i] & 0x07);
			do {
				int sq = BitBoards::popFirstBit(bb);
				p[i++] = sq ^ mirror;
			} while (bb);
		}
		uint64_t idx = encode_pawn(entry, entry->file[f].norm[bside], p,
			entry->file[f].factor[bside]);
		res = decompress_pairs(entry->file[f].precomp[bside], idx);
	}

	return ((int)res) - 2;
}

////////////////////////////////////////////////////////////////////////////////
// The value of wdl MUST correspond to the WDL value of the position without
// en passant rights.
int probe_dtz_table(ChessBoard &board, int wdl, int *success)
{
	int i, res;
	struct TBEntry *ptr;

	// Obtain the position's material signature key.
	uint64_t key = engine_calc_key(board);

	if (DTZ_table[0].key1 != key && DTZ_table[0].key2 != key) {
		for (i = 1; i < DTZ_ENTRIES; i++) {
			if (DTZ_table[i].key1 == key || DTZ_table[i].key2 == key) {
				break;
			}
		}
		if (i < DTZ_ENTRIES) {
			struct DTZTableEntry table_entry = DTZ_table[i];
			for (; i > 0; i--) {
				DTZ_table[i] = DTZ_table[i - 1];
			}
			DTZ_table[0] = table_entry;
		} else {
			struct TBHashEntry *ptr2 = TB_hash[key >> (64 - TBHASHBITS)];
			for (i = 0; i < HSHMAX; i++) {
				if (ptr2[i].key == key) {
					break;
				}
			}
			if (i == HSHMAX) {
				*success = 0;
				return 0;
			}
			ptr = ptr2[i].ptr;
			char str[16];
			int mirror = (ptr->key != key);
			prt_str(board, str, mirror);
			if (DTZ_table[DTZ_ENTRIES - 1].entry) {
				free_dtz_entry(DTZ_table[DTZ_ENTRIES-1].entry);
			}
			for (i = DTZ_ENTRIES - 1; i > 0; i--) {
				DTZ_table[i] = DTZ_table[i - 1];
			}
			load_dtz_table(str, calc_key(board, mirror),
				calc_key(board, !mirror));
		}
	}

	ptr = DTZ_table[0].entry;
	if (!ptr) {
		*success = 0;
		return 0;
	}

	int bside, mirror, cmirror;
	if (!ptr->symmetric) {
		if (key != ptr->key) {
			cmirror = 8;
			mirror = 0x38;
			bside = (board.side() == white);
		} else {
			cmirror = mirror = 0;
			bside = !(board.side() == white);
		}
	} else {
		cmirror = board.side() == white ? 0 : 8;
		mirror = board.side() == white ? 0 : 0x38;
		bside = 0;
	}

	int p[TBPIECES];

	Side s;
	if (!ptr->has_pawns) {
		struct DTZEntry_piece *entry = (struct DTZEntry_piece *)ptr;
		if ((entry->flags & 1) != bside && !entry->symmetric) {
			*success = -1;
			return 0;
		}
		uint8_t *pc = entry->pieces;
		for (i = 0; i < entry->num;) {
			s = static_cast<Side>(1 - ((pc[i] ^ cmirror) >> 3));
			BitBoard bb = board.bitboardPiece(s, pc[i] & 0x07);
			do {
				p[i++] = BitBoards::popFirstBit(bb);
			} while (bb);
		}
		uint64_t idx = encode_piece((struct TBEntry_piece *)entry, entry->norm,
			p, entry->factor);
		res = decompress_pairs(entry->precomp, idx);

		if (entry->flags & 2) {
			res = entry->map[entry->map_idx[wdl_to_map[wdl + 2]] + res];
		}

		if (!(entry->flags & pa_flags[wdl + 2]) || (wdl & 1)) {
			res *= 2;
		}
	} else {
		struct DTZEntry_pawn *entry = (struct DTZEntry_pawn *)ptr;
		int k = entry->file[0].pieces[0] ^ cmirror;
		s = static_cast<Side>(1 - (k >> 3));
		BitBoard bb = board.bitboardPiece(s, k & 0x07);
		i = 0;
		do {
			p[i++] = BitBoards::popFirstBit(bb) ^ mirror;
		} while (bb);
		int f = pawn_file((struct TBEntry_pawn *)entry, p);
		if ((entry->flags[f] & 1) != bside) {
			*success = -1;
			return 0;
		}
		uint8_t *pc = entry->file[f].pieces;
		for (; i < entry->num;) {
			s = static_cast<Side>(1 - ((pc[i] ^ cmirror) >> 3));
			bb = board.bitboardPiece(s,	pc[i] & 0x07);
			do {
				p[i++] = BitBoards::popFirstBit(bb) ^ mirror;
			} while (bb);
		}
		uint64_t idx = encode_pawn((struct TBEntry_pawn *)entry,
			entry->file[f].norm, p, entry->file[f].factor);
		res = decompress_pairs(entry->file[f].precomp, idx);

		if (entry->flags[f] & 2) {
			res = entry->map[entry->map_idx[f][wdl_to_map[wdl + 2]] + res];
		}

		if (!(entry->flags[f] & pa_flags[wdl + 2]) || (wdl & 1)) {
			res *= 2;
		}
	}

	return res;
}

////////////////////////////////////////////////////////////////////////////////
int probe_ab(ChessBoard &board, int alpha, int beta, int *success)
{
	int nb = 0;

	ChessMoveSortList movelist;

	// Generate (at least) all legal captures including (under)promotions.
	// It is OK to generate more, as long as they are filtered out below.
	if (!board.inCheck()) {
		nb = ChessMoveGen::generateCaptures(board, movelist);
	} else {
		nb = ChessMoveGen::generateKingEvasions(board, movelist);
	}

	ChessMoveRec rec;

	int v;
	for (int n=0; n<nb; n++) {
		ChessMove move = movelist[n].move;

		if (!move.isCapture()) {
			continue;
		}

		board.makeMove(move, rec);

		if (board.isPositionLegal()) {
			v = -probe_ab(board, -beta, -alpha, success);
			board.unMakeMove(move, rec);
		} else {
			board.unMakeMove(move, rec);
			continue;
		}

		if (*success == 0) {
			return 0;
		}
		if (v > alpha) {
			if (v >= beta) {
				return v;
			}
			alpha = v;
		}
	}

	v = probe_wdl_table(board, success);

	return alpha >= v ? alpha : v;
}

////////////////////////////////////////////////////////////////////////////////
// Probe the WDL table for a particular position.
//
// If *success != 0, the probe was successful.
//
// If *success == 2, the position has a winning capture, or the position
// is a cursed win and has a cursed winning capture, or the position
// has an ep capture as only best move.
// This is used in probe_dtz().
//
// The return value is from the point of view of the side to move:
// -2 : loss
// -1 : loss, but draw under 50-move rule
//  0 : draw
//  1 : win, but draw under 50-move rule
//  2 : win
int probe_wdl(ChessBoard &board, int *success)
{
	*success = 1;

	ChessMoveSortList movelist;

	// Generate (at least) all legal captures including (under)promotions.
	int nb = 0;
	if (!board.inCheck()) {
		nb = ChessMoveGen::generateCaptures(board, movelist);
	} else {
		nb = ChessMoveGen::generateKingEvasions(board, movelist);
	}

	int best_cap = -3, best_ep = -3;

	// We do capture resolution, letting best_cap keep track of the best
	// capture without ep rights and letting best_ep keep track of still
	// better ep captures if they exist.

	ChessMoveRec rec;
	int n, v;
	for (n=0; n<nb; n++) {

		ChessMove move = movelist[n].move;

		if (!move.isCapture()) {
			continue;
		}

		board.makeMove(move, rec);
		if (board.isPositionLegal()) {
			v = -probe_ab(board, -2, -best_cap, success);
			board.unMakeMove(move, rec);
		} else {
			board.unMakeMove(move, rec);
			continue;
		}

		if (*success == 0) {
			return 0;
		}
		if (v > best_cap) {
			if (v == 2) {
				*success = 2;
				return 2;
			}
			if (!move.isEnPassant()) {
				best_cap = v;
			} else if (v > best_ep) {
				best_ep = v;
			}
		}
	}

	v = probe_wdl_table(board, success);
	if (*success == 0) {
		return 0;
	}

	// Now max(v, best_cap) is the WDL value of the position without ep rights.
	// If the position without ep rights is not stalemate or no ep captures
	// exist, then the value of the position is max(v, best_cap, best_ep).
	// If the position without ep rights is stalemate and best_ep > -3,
	// then the value of the position is best_ep (and we will have v == 0).

	if (best_ep > best_cap) {
		if (best_ep > v) { // ep capture (possibly cursed losing) is best.
			*success = 2;
			return best_ep;
		}
		best_cap = best_ep;
	}

	// Now max(v, best_cap) is the WDL value of the position unless
	// the position without ep rights is stalemate and best_ep > -3.

	if (best_cap >= v) {
		// No need to test for the stalemate case here: either there are
		// non-ep captures, or best_cap == best_ep >= v anyway.
		*success = 1 + (best_cap > 0);
		return best_cap;
	}

	// Now handle the stalemate case.
	if (best_ep > -3 && v == 0) {
		// Check for stalemate in the position with ep captures.
		for (n=0; n<nb; n++) {

			ChessMove move = movelist[n].move;

			if (move.isEnPassant()) {
				continue;
			}

			board.makeMove(move, rec);
			bool ok = board.isPositionLegal();
			board.unMakeMove(move, rec);

			if (ok) {
				break;
			}
		}

		if ((n == nb) && (!board.inCheck())) {

			movelist.clear();
			nb = ChessMoveGen::generateQuiets(board, movelist);

			for (n=0; n<nb; n++) {

				ChessMove move = movelist[n].move;

				board.makeMove(move, rec);
				bool ok = board.isPositionLegal();
				board.unMakeMove(move, rec);

				if (ok) {
					break;
				}
			}
		}
		// Stalemate detected.
		if (n == nb) {
			*success = 2;
			return best_ep;
		}
	}

	// Stalemate / en passant not an issue, so v is the correct value.

	return v;
}

////////////////////////////////////////////////////////////////////////////////
// Probe the DTZ table for a particular position.
// If *success != 0, the probe was successful.
// The return value is from the point of view of the side to move:
//         n < -100 : loss, but draw under 50-move rule
// -100 <= n < -1   : loss in n ply (assuming 50-move counter == 0)
//         0	    : draw
//     1 < n <= 100 : win in n ply (assuming 50-move counter == 0)
//   100 < n        : win, but draw under 50-move rule
//
// If the position is mate, -1 is returned instead of 0.
//
// The return value n can be off by 1: a return value -n can mean a loss
// in n+1 ply and a return value +n can mean a win in n+1 ply. This
// cannot happen for tables with positions exactly on the "edge" of
// the 50-move rule.
//
// This means that if dtz > 0 is returned, the position is certainly
// a win if dtz + 50-move-counter <= 99. Care must be taken that the engine
// picks moves that preserve dtz + 50-move-counter <= 99.
//
// If n = 100 immediately after a capture or pawn move, then the position
// is also certainly a win, and during the whole phase until the next
// capture or pawn move, the inequality to be preserved is
// dtz + 50-movecounter <= 100.
//
// In short, if a move is available resulting in dtz + 50-move-counter <= 99,
// then do not accept moves leading to dtz + 50-move-counter == 100.
//
int probe_dtz(ChessBoard &board, int *success)
{
	int wdl = probe_wdl(board, success);

	if (*success == 0) {
		return 0;
	}

	// If draw, then dtz = 0.
	if (wdl == 0) {
		return 0;
	}

	// Check for winning (cursed) capture or ep capture as only best move.
	if (*success == 2) {
		return wdl_to_dtz[wdl + 2];
	}

	ChessMoveSortList movelist;
	int nb = 0;

	// If winning, check for a winning pawn move.
	if (wdl > 0) {
		// Generate at least all legal non-capturing pawn moves
		// including non-capturing promotions.
		// (The call to generate<>() in fact generates all moves.)

		nb = ChessMoveGen::generateMoves(board, movelist, board.inCheck());

		ChessMoveRec rec;
		for (int n=0; n<nb; n++) {

			ChessMove move = movelist[n].move;

			if ((move.piece() != pawn) || move.isCapture()) {
				continue;
			}

			board.makeMove(move, rec);

			int v;
			if (board.isPositionLegal()) {
				v = -probe_wdl(board, success);
				board.unMakeMove(move, rec);
			} else {
				board.unMakeMove(move, rec);
				continue;
			}

			if (*success == 0) {
				return 0;
			}
			if (v == wdl) {
				return wdl_to_dtz[wdl + 2];
			}
		}
	}

	// If we are here, we know that the best move is not an ep capture.
	// In other words, the value of wdl corresponds to the WDL value of
	// the position without ep rights. It is therefore safe to probe the
	// DTZ table with the current value of wdl.

	int dtz = probe_dtz_table(board, wdl, success);
	if (*success >= 0) {
		return wdl_to_dtz[wdl + 2] + ((wdl > 0) ? dtz : -dtz);
	}

	// *success < 0 means we need to probe DTZ for the other side to move.
	int best;
	if (wdl > 0) {
		best = INT32_MAX;
		// If wdl > 0, we already generated all moves.
	} else {
		// If (cursed) loss, the worst case is a losing capture or pawn move
		// as the "best" move, leading to dtz of -1 or -101.
		// In case of mate, this will cause -1 to be returned.
		best = wdl_to_dtz[wdl + 2];

		nb = ChessMoveGen::generateMoves(board, movelist,
			board.inCheck());
	}

	ChessMoveRec rec;
	for (int n=0; n<nb; n++) {

		ChessMove move = movelist[n].move;

		// We can skip pawn moves and captures.
		// If wdl > 0, we already caught them. If wdl < 0, the initial value
		// of best already takes account of them.

		if ((move.piece() == pawn) || move.isCapture()) {
			continue;
		}

		board.makeMove(move, rec);

		int v;
		if (board.isPositionLegal()) {
			v = -probe_dtz(board, success);
			board.unMakeMove(move, rec);
		} else {
			board.unMakeMove(move, rec);
			continue;
		}

		if (*success == 0) {
			return 0;
		}

		if (wdl > 0) {
			if (v > 0 && v + 1 < best) {
				best = v + 1;
			}
		} else {
			if (v -1 < best) {
				best = v - 1;
			}
		}
	}

	return best;
}

////////////////////////////////////////////////////////////////////////////////
// Check whether there has been at least one repetition of positions
// since the last capture or pawn move.
bool has_repeated(ChessBoard &board, ChessRepetitions &rep)
{
	return rep.getCount(board.getHashKey(), board.fiftyRule()) != 0;
}

////////////////////////////////////////////////////////////////////////////////
// Use the DTZ tables to filter out moves that don't preserve the win or draw.
// If the position is lost, but DTZ is fairly high, only keep moves that
// maximise DTZ.
//
// A return value of 0 indicates that not all probes were successful and that
// no moves were filtered out.
int root_probe(ChessBoard &board, int &TBScore, ChessMoveSortList &rootmoves,
	int count, ChessRepetitions &rep, int *newcount)
{
	int success = 0;

	int dtz = probe_dtz(board, &success);

	if (!success) {
		return 0;
	}

	ChessMoveRec rec;

	// Probe each move.
	for (int i=0; i<count; i++) {

		ChessMove move = rootmoves[i].move;

		board.makeMove(move, rec);

		int v = 0;
		if (board.inCheck() && dtz > 0) {
			if (ChessMoveGen::countLegalMoves(board) == 0) {
				v = 1;
			}
		}
		if (!v) {
			if (board.fiftyRule() != 0) {
				v = -probe_dtz(board, &success);

				if (v > 0) {
					v++;
				} else if (v < 0) {
					v--;
				}
			} else {
				v = -probe_wdl(board, &success);
				v = wdl_to_dtz[v + 2];
			}
		}
		board.unMakeMove(move, rec);
		if (!success) {
			return 0;
		}

		rootmoves[i].setScore(v);
	}

	// Obtain 50-move counter for the root position.
	// In Stockfish there seems to be no clean way, so we do it like this:

	int cnt50 = board.fiftyRule();

	// Use 50-move counter to determine whether the root position is
	// won, lost or drawn.
	int wdl = 0;
	if (dtz > 0) {
		wdl = (dtz + cnt50 <= 100) ? 2 : 1;
	} else if (dtz < 0) {
		wdl = (-dtz + cnt50 <= 100) ? -2 : -1;
	}

	// Determine the score to report to the user.
	TBScore = wdl_to_value[wdl + 2];
	// If the position is winning or losing, but too few moves left, adjust the
	// score to show how close it is to winning or losing.
	// NOTE: int(PawnValueEg) is used as scaling factor in score_to_uci().
	if (wdl == 1 && dtz <= 100) {
		TBScore = (int)(((200 - dtz - cnt50) * TB_PAWN_VALUE_ENDGAME) / 200);
	} else if (wdl == -1 && dtz >= -100) {
		TBScore = -(int)(((200 + dtz - cnt50) * TB_PAWN_VALUE_ENDGAME) / 200);
	}

	// Now be a bit smart about filtering out moves.
	int j = 0;

	if (dtz > 0) { // winning (or 50-move rule draw)
		int best = 0xffff;
		for (int i = 0; i < count; i++) {
			int v = rootmoves[i].getScore();
			if (v > 0 && v < best) {
				best = v;
			}
		}
		int max = best;

		// If the current phase has not seen repetitions, then try all moves
		// that stay safely within the 50-move budget, if there are any.
		if (!has_repeated(board, rep) && best + cnt50 <= 99) {
			max = 99 - cnt50;
		}

		for (int i = 0; i < count; i++) {
			int v = rootmoves[i].getScore();
			if (v > 0 && v <= max) {
				rootmoves[j].setMove(rootmoves[i].getMove());
				rootmoves[j].setScore(rootmoves[i].getScore());
				++j;
			}
		}

	} else if (dtz < 0) {
		int best = 0;

		for (int i = 0; i < count; i++) {
			int v = rootmoves[i].getScore();
			if (v < best) {
				best = v;
			}
		}

		// Try all moves, unless we approach or have a 50-move rule draw.
		if (-best * 2 + cnt50 < 100) {
			return 1;
		}

		for (int i = 0; i < count; i++) {
			if (rootmoves[i].getScore() == best) {
				rootmoves[j].setMove(rootmoves[i].getMove());
				rootmoves[j].setScore(rootmoves[i].getScore());
				++j;
			}
		}
	} else { // drawing
		// Try all moves that preserve the draw.

		for (int i = 0; i < count; i++) {
			if (rootmoves[i].getScore() == 0) {
				rootmoves[j].setMove(rootmoves[i].getMove());
				rootmoves[j].setScore(rootmoves[i].getScore());
				++j;
			}
		}
	}

	*newcount = j;

	return 1;
}

////////////////////////////////////////////////////////////////////////////////
// Use the WDL tables to filter out moves that don't preserve the win or draw.
// This is a fallback for the case that some or all DTZ tables are missing.
//
// A return value of 0 indicates that not all probes were successful and that
// no moves were filtered out.
int root_probe_wdl(ChessBoard &board, int &TBScore,
	ChessMoveSortList &rootmoves, int count, int *newcount)
{
	int success = 0;

	int wdl = probe_wdl(board, &success);

	if (!success) {
		return false;
	}
	TBScore = wdl_to_value[wdl + 2];

	int best = -2;

	ChessMoveRec rec;

	// Probe each move.
	for (int i = 0; i < count; i++) {

		ChessMove move = rootmoves[i].getMove();

		board.makeMove(move, rec);

		int v = -probe_wdl(board, &success);

		board.unMakeMove(move, rec);

		if (!success) {
			return false;
		}

		rootmoves[i].setScore(v);
		if (v > best) {
			best = v;
		}
	}

	int j = 0;
	for (int i = 0; i < count; i++) {
		if (rootmoves[i].getScore() == best) {
			rootmoves[j].setMove(rootmoves[i].getMove());
			rootmoves[j].setScore(rootmoves[i].getScore());
			++j;
		}
	}

	*newcount = j;

	return 1;
}

} // namespace Cheese::TableBases

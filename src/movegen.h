////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_MOVEGEN_H_
#define CHEESE_MOVEGEN_H_

#include "config.h"

#include "bitboard.h"
#include "move.h"

namespace Cheese {

// move generation template parameter for promotions
enum class GenPromotion {
	gen_all,
	gen_queen,
	gen_under
};

// max number of moves each turn (maximum = 218)
constexpr int max_play_moves = 256;

class ChessBoard;

////////////////////////////////////////////////////////////////////////////////
class ChessMoveSortList {

private:

	std::array<ChessMoveSort, max_play_moves> moves;

	ChessMoveSort *pos;

public:

	ChessMoveSortList() : pos(moves.data())
	{}

	inline const auto *begin()
	{
		return moves.data();
	}

	inline const auto *end()
	{
		return pos;
	}

	inline auto *data()
	{
		return moves.data();
	}

	inline int size() const
	{
		return static_cast<int>(pos - moves.data());
	}

	inline bool empty() const
	{
		return (pos == moves.data());
	}

	inline void clear()
	{
		pos = moves.data();
	}

	inline auto & operator[](int index)
	{
		return moves[index];
	}

	inline auto & operator[](int index) const
	{
		return moves[index];
	}

	inline void push(const ChessMoveSort move)
	{
		*(pos++) = move;
	}

	inline void push(const ChessMove move)
	{
		// update only the move (speed)
		(pos++)->setMove(move);
	}

	inline void setScore(int index, int32_t v)
	{
		moves[index].setScore(v);
	}

	void sort(int p, int cnt);
};

////////////////////////////////////////////////////////////////////////////////
// Chess moves generation
namespace ChessMoveGen {

	// generate non capture moves (and no promotion)
	int generateQuiets(const ChessBoard &board, ChessMoveSortList &moves);

	// generate captures moves + promotion moves
	int generateCaptures(const ChessBoard &board, ChessMoveSortList &moves);

	int generateQuietCaptures(const ChessBoard &board,
		ChessMoveSortList &moves);

	// generate all legal moves when king must evade check
	int generateKingEvasions(const ChessBoard &board, ChessMoveSortList &moves);

	int generateQuietKingEvasions(const ChessBoard &board,
		ChessMoveSortList &moves);

	// Generate non capture moves that give check
	int generateQuietChecks(const ChessBoard &board, ChessMoveSortList &moves);

	// generate all legal moves for this position
	int generateMoves(const ChessBoard &board, ChessMoveSortList &moves,
		bool incheck);

	// count legal moves
	int countLegalMoves(ChessBoard &board);

	// generate legal moves
	int generateLegalMoves(ChessBoard &board, ChessMoveSortList &moves,
		bool incheck);
}

} // namespace Cheese

#endif //CHEESE_MOVEGEN_H_

//////////////////////////////////////////////////////////
// CMake configuration options (generated by cmake)
//////////////////////////////////////////////////////////

#ifndef CHEESE_MAKECONFIG_H_
#define CHEESE_MAKECONFIG_H_

// define to use hardware popcount
#define USE_POPCOUNT

// define to use PEXT instruction
/* #undef USE_PEXT */

// Use syzygy tablebases
#define USE_TABLEBASES

// project name and version
#define PROJECT_VER  "3.0.0"
#define PROJECT_VER_MAJOR "3"
#define PROJECT_VER_MINOR "0"
#define PROJECT_VER_PATCH "0"

#endif //CHEESE_MAKECONFIG_H_


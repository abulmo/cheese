////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_TUNE_H_
#define CHEESE_TUNE_H_

#include "config.h"

#include "util/logfile.h"

#include <string>
#include <vector>

//#define USE_TUNING

namespace Cheese {

#ifdef USE_TUNING

using TunableType = int32_t;

////////////////////////////////////////////////////////////////////////////////
struct Tunable {

	std::string name;

	TunableType *ptr {nullptr};

	TunableType def {0};

	TunableType min {0};

	TunableType max {0};
};

////////////////////////////////////////////////////////////////////////////////
class Tuning {

protected:

	std::vector<Tunable> params;

public:

	Tuning() = default;

	~Tuning() = default;

	void clear();

	void add(const std::string &name, TunableType *ptr, TunableType def,
		TunableType min, TunableType max);

	void set(const std::string &name, TunableType value);

	TunableType get(const std::string &name);

	auto &getParams();

};

////////////////////////////////////////////////////////////////////////////////
inline void Tuning::clear()
{
	params.clear();
}

////////////////////////////////////////////////////////////////////////////////
inline auto &Tuning::getParams()
{
	return params;
}

#endif //USE_TUNING

} // namespace Cheese

#endif //CHEESE_TUNE_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "movegen.h"

#include "board.h"

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
void ChessMoveSortList::sort(int p, int cnt)
{
	//auto cmp = [](const ChessMoveSort &a, const ChessMoveSort &b) {
	//	return a.getScore() > b.getScore(); };
	//std::stable_sort(moves.begin() + p, moves.begin() + p + cnt, cmp);

	// still faster than std::stable_sort
	const auto *ms = moves.data() + p;
	const auto *me = ms + cnt;
	auto *mi = moves.data() + p;
	while (++mi < me) {

		auto mm = mi->move.data();
		auto mv = mi->getScore();

		auto *mj = mi - 1;
		while ((mj >= ms) &&
			   (mj->getScore() < mv)) {
			*(mj + 1) = *mj;
			--mj;
		}

		(mj + 1)->move.set(mm);
		(mj + 1)->setScore(mv);
	}
}

namespace ChessMoveGen {

////////////////////////////////////////////////////////////////////////////////
// generate castling moves
template<Side side>
void generateMovesCastling(
	const ChessBoard &board, ChessMoveSortList &moves)
{
	if (!board.isAttacked(board.kingPosition(side), ~side)) {

		constexpr uint8_t csflag1 = (side == white) ? castle_white_short :
										              castle_black_short;
		constexpr int sqmax1 = (side == white) ? g1 : g8;

		if (board.canCastle(csflag1) &&
			(!(board.bitboardAll() & board.bitboardCastleShort(side)))) {

			bool safe = true;
			for (int i=board.squareKingStart(side) + 1; i<=sqmax1; i++) {
				if (board.isAttacked(i, ~side)) {
					safe = false;
					break;
				}
			}
			if (safe) {
				moves.push(ChessMove(board.squareKingStart(side),
					board.squareRookShort(side), king, 0, 0, 1));
			}
		}

		constexpr uint8_t csflag2 = (side == white) ? castle_white_long :
													  castle_black_long;
		constexpr int sqmax2 = (side == white) ? c1 : c8;

		if (board.canCastle(csflag2) &&
			(!(board.bitboardAll() & board.bitboardCastleLong(side)))) {

			bool safe = true;
			for (int i=sqmax2; i<board.squareKingStart(side); i++) {
				if (board.isAttacked(i, ~side)) {
					safe = false;
					break;
				}
			}
			if (safe) {
				moves.push(ChessMove(board.squareKingStart(side),
					board.squareRookLong(side), king, 0, 0, 1));
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
template<Side side, bool capture>
void generateMovesPieces(const ChessBoard &board,
	ChessMoveSortList &moves, BitBoard bbmask)
{
	BitBoard bbSrc = board.bitboardPiece(side, knight);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackKnight(src) & bbmask;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push((capture) ?
				ChessMove(src, dst, knight, board.piece(dst)) :
				ChessMove(src, dst, knight));
		}
	}

	bbSrc = board.bitboardPiece(side, bishop);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackBishop(src, board.bitboardAll()) &
						 bbmask;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push((capture) ?
				ChessMove(src, dst, bishop, board.piece(dst)) :
				ChessMove(src, dst, bishop));
		}
	}

	bbSrc = board.bitboardPiece(side, rook);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackRook(src, board.bitboardAll()) &
					     bbmask;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push((capture) ?
				ChessMove(src, dst, rook, board.piece(dst)) :
				ChessMove(src, dst, rook));
		}
	}

	bbSrc = board.bitboardPiece(side, queen);

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackQueen(src, board.bitboardAll()) &
						 bbmask;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push((capture) ?
				ChessMove(src, dst, queen, board.piece(dst)) :
				ChessMove(src, dst, queen));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
template<Side side, bool capture>
void generateMovesKing(const ChessBoard &board,
	ChessMoveSortList &moves, BitBoard bbmask)
{
	int src = board.kingPosition(side);
	BitBoard bbDst = BitBoards::attackKing(src) & bbmask;

	while (bbDst) {
		int dst = BitBoards::popFirstBit(bbDst);
		moves.push((capture) ?
			ChessMove(src, dst, king, board.piece(dst)) :
			ChessMove(src, dst, king));
	}
}

////////////////////////////////////////////////////////////////////////////////
template<Side side, bool evasion>
void generateMovesPawn(const ChessBoard &board,
	ChessMoveSortList &moves, BitBoard bbmask)
{
	// 1 up
	constexpr int r7 = relRank<side>(rank_7);
	BitBoard bbSrc = BitBoards::shiftUp<side>(board.bitboardPiece(side, pawn) &
					~BitBoards::rank[r7]) & ~board.bitboardAll();
	if (evasion) {
		bbSrc &= bbmask;
	}

	while (bbSrc) {
		int src = BitBoards::popFirstBit(bbSrc);
		moves.push(ChessMove(relSquareDown<side>(src), src, pawn));
	}

	// 2 up
	constexpr int r2 = relRank<side>(rank_2);
	bbSrc = BitBoards::shiftUp2<side>(board.bitboardPiece(side, pawn) &
			BitBoards::rank[r2]) & (~(board.bitboardAll() |
			BitBoards::shiftUp<side>(board.bitboardAll())));
	if (evasion) {
		bbSrc &= bbmask;
	}

	while (bbSrc) {
		int src = BitBoards::popFirstBit(bbSrc);
		moves.push(ChessMove(relSquareDown2<side>(src), src, pawn));
	}
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
void generateCapturesPawn(
	const ChessBoard &board, ChessMoveSortList &moves, BitBoard bbmask)
{
	// attack
	constexpr int r = relRank<side>(rank_7);
	const BitBoard bb = board.bitboardPiece(side, pawn) & ~BitBoards::rank[r];

	constexpr auto f1 = (side == white) ? file_h : file_a;
	BitBoard bbSrc = BitBoards::shiftUpLeft<side>(bb) &
		~BitBoards::file[f1] & bbmask;

	while (bbSrc) {
		int src = BitBoards::popFirstBit(bbSrc);
		moves.push(ChessMove(relSquareDownRight<side>(src), src, pawn,
									board.piece(src)));
	}

	constexpr auto f2 = (side == white) ? file_a : file_h;
	bbSrc = BitBoards::shiftUpRight<side>(bb) &
		~BitBoards::file[f2] & bbmask;

	while (bbSrc) {
		int src = BitBoards::popFirstBit(bbSrc);
		moves.push(ChessMove(relSquareDownLeft<side>(src), src, pawn,
									board.piece(src)));
	}

	// en passant
	if (board.isEnPassant()) {

		const int sqep = makeSquare(board.enPassantSquare(),
			relRank<side>(rank_6));

		if (BitBoards::square[relSquareDown<side>(sqep)] & bbmask) {
			bbSrc = board.bitboardPiece(side, pawn) &
					BitBoards::attackPawn(~side, sqep);
			while (bbSrc) {
				int src = BitBoards::popFirstBit(bbSrc);
				moves.push(ChessMove(src, sqep, pawn, pawn, 0, 0, 1));
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
template<Side side, GenPromotion mode>
void generateMovesPromotions(
	const ChessBoard &board, ChessMoveSortList &moves, BitBoard bbmask)
{
	constexpr int r = relRank<side>(rank_7);
	BitBoard bbSrc = BitBoards::shiftUp<side>(
						board.bitboardPiece(side, pawn) &
						BitBoards::rank[r]) & bbmask;
	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		int dst = relSquareDown<side>(src);

		if constexpr ((mode == GenPromotion::gen_all) ||
			(mode == GenPromotion::gen_queen)) {
			moves.push(ChessMove(dst, src, pawn, 0, queen));
		}
		if constexpr ((mode == GenPromotion::gen_all) ||
			(mode == GenPromotion::gen_under)) {
			moves.push(ChessMove(dst, src, pawn, 0, rook));
			moves.push(ChessMove(dst, src, pawn, 0, knight));
			moves.push(ChessMove(dst, src, pawn, 0, bishop));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
template<Side side, GenPromotion mode>
void generateCapturesPromotions(
	const ChessBoard &board, ChessMoveSortList &moves, BitBoard bbmask)
{
	// attack
	constexpr int r = relRank<side>(rank_7);
	const BitBoard bb = board.bitboardPiece(side, pawn) & BitBoards::rank[r];
	constexpr auto f1 = (side == white) ? file_h : file_a;
	BitBoard bbSrc = BitBoards::shiftUpLeft<side>(bb) &
		~BitBoards::file[f1] & bbmask;
	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		int dst = relSquareDownRight<side>(src);

		if constexpr ((mode == GenPromotion::gen_all) ||
			(mode == GenPromotion::gen_queen)) {
			moves.push(ChessMove(dst, src, pawn,
				board.piece(src), queen));
		}
		if constexpr ((mode == GenPromotion::gen_all) ||
			(mode == GenPromotion::gen_under)) {
			moves.push(ChessMove(dst, src, pawn,
				board.piece(src), rook));
			moves.push(ChessMove(dst, src, pawn,
				board.piece(src), knight));
			moves.push(ChessMove(dst, src, pawn,
				board.piece(src), bishop));
		}
	}

	constexpr auto f2 = (side == white) ? file_a : file_h;
	bbSrc = BitBoards::shiftUpRight<side>(bb) &
		~BitBoards::file[f2] & bbmask;
	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		int dst = relSquareDownLeft<side>(src);

		if constexpr ((mode == GenPromotion::gen_all) ||
			(mode == GenPromotion::gen_queen)) {
			moves.push(ChessMove(dst, src, pawn,
						board.piece(src), queen));
		}
		if constexpr ((mode == GenPromotion::gen_all) ||
			(mode == GenPromotion::gen_under)) {
			moves.push(ChessMove(dst, src, pawn,
				board.piece(src), rook));
			moves.push(ChessMove(dst, src, pawn,
				board.piece(src), knight));
			moves.push(ChessMove(dst, src, pawn,
				board.piece(src), bishop));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// generate non capture moves (and no promotion), and castling
template<Side side>
int generateAllQuiets(const ChessBoard &board,
	ChessMoveSortList &moves)
{
	int nb = moves.size();

	generateMovesCastling<side>(board, moves);

	BitBoard bb = ~board.bitboardAll();

	generateMovesPieces<side, false>(board, moves, bb);
	generateMovesKing<side, false>(board, moves, bb);
	generateMovesPawn<side, false>(board, moves, 0);

	return moves.size() - nb;
}

////////////////////////////////////////////////////////////////////////////////
// generate captures moves + promotion moves
template<Side side>
int generateMovesCaptureAll(const ChessBoard &board,
	ChessMoveSortList &moves)
{
	int nb = moves.size();

	const BitBoard bb = board.bitboardColor(~side);

	generateMovesPieces<side, true>(board, moves, bb);
	generateMovesKing<side, true>(board, moves, bb);
	generateCapturesPawn<side>(board, moves, bb);
	generateCapturesPromotions<side, GenPromotion::gen_all>(board,
		moves, bb);
	generateMovesPromotions<side, GenPromotion::gen_all>(board, moves,
			~board.bitboardAll());

	return moves.size() - nb;
}

////////////////////////////////////////////////////////////////////////////////
// generate captures moves + promotion moves for queen only
template<Side side>
int generateMovesCaptureQuiescenceAll(const ChessBoard &board,
	ChessMoveSortList &moves)
{
	int nb = moves.size();

	const BitBoard bb = board.bitboardColor(~side);

	generateMovesPieces<side, true>(board, moves, bb);
	generateMovesKing<side, true>(board, moves, bb);
	generateCapturesPawn<side>(board, moves, bb);
	generateCapturesPromotions<side, GenPromotion::gen_queen>(board,
		moves, bb);
	generateMovesPromotions<side, GenPromotion::gen_queen>(board, moves,
			~board.bitboardAll());

	return moves.size() - nb;
}

////////////////////////////////////////////////////////////////////////////////
// generate non capture moves (and no promotion), and castling
int generateQuiets(const ChessBoard &board,
	ChessMoveSortList &moves)
{
	return (board.side() == black) ?
			generateAllQuiets<black>(board, moves) :
			generateAllQuiets<white>(board, moves);
}

////////////////////////////////////////////////////////////////////////////////
// generate captures moves + promotion moves
int generateCaptures(const ChessBoard &board,
	ChessMoveSortList &moves)
{
	return (board.side() == black) ?
			generateMovesCaptureAll<black>(board, moves) :
			generateMovesCaptureAll<white>(board, moves);
}

////////////////////////////////////////////////////////////////////////////////
// generate captures moves + promotion moves for queen only
int generateQuietCaptures(const ChessBoard &board,
	ChessMoveSortList &moves)
{
	return (board.side() == black) ?
			generateMovesCaptureQuiescenceAll<black>(board, moves) :
			generateMovesCaptureQuiescenceAll<white>(board, moves);
}

////////////////////////////////////////////////////////////////////////////////
// generate all legal moves when king must evade check
template<GenPromotion mode>
int generateKingEvasions(const ChessBoard &board, ChessMoveSortList &moves)
{
	const Side notplayer = ~board.side();

	int nb = moves.size();

	const int src = board.kingPosition(board.side());

	// create a bitboard with pieces attacking the king
	BitBoard bbAtt = (BitBoards::attackPawn(board.side(), src) &
						board.bitboardPiece(notplayer, pawn)) |
					 (BitBoards::attackKnight(src) &
						board.bitboardPiece(notplayer, knight)) |
					 (BitBoards::attackRook(src, board.bitboardAll()) &
						(board.bitboardPiece(notplayer, rook) |
							board.bitboardPiece(notplayer, queen))) |
					 (BitBoards::attackBishop(src, board.bitboardAll()) &
						(board.bitboardPiece(notplayer, bishop) |
							board. bitboardPiece(notplayer, queen)));

	// * king move to evade check (by moving or attacking)
	//	- only solution when there are more than one attacker
	BitBoard bbDst = BitBoards::attackKing(src) &
					~(board.bitboardColor(board.side()));
	BitBoard bbAll = board.bitboardAll() ^
					BitBoards::square[board.kingPosition(board.side())];

	while (bbDst) {
		int dst = BitBoards::popFirstBit(bbDst);
		if (!board.isAttackedOcc(dst, notplayer, bbAll)) {
			moves.push(ChessMove(src, dst, king, board.piece(dst)));
		}
	}

	// more than one attacker, the king must move
	int nbattacker = BitBoards::bitCount(bbAtt);
	if (nbattacker != 1) {
		return moves.size() - nb;
	}

	// get attacker type and position :
	int attdst = BitBoards::getFirstBit(bbAtt);

	// move a piece between sliding attacker and king, or capture the attacker

	// bitboard of squares between attacker and king + the attacker
	BitBoard bbAttKing =
		BitBoards::between[board.kingPosition(board.side())][attdst];

	BitBoard bbmask = ~(board.bitboardColor(board.side())) &
		(bbAttKing | bbAtt);

	if (board.side() == black) {
		generateMovesPieces<black, true>(board, moves, bbmask);

		generateCapturesPawn<black>(board, moves, bbAtt);
		generateCapturesPromotions<black, mode>(board, moves, bbAtt);

		generateMovesPawn<black, true>(board, moves, bbAttKing);
		generateMovesPromotions<black, mode>(board, moves,
				~board.bitboardAll() & bbAttKing);
	} else {
		generateMovesPieces<white, true>(board, moves, bbmask);

		generateCapturesPawn<white>(board, moves, bbAtt);
		generateCapturesPromotions<white, mode>(board, moves, bbAtt);

		generateMovesPawn<white, true>(board, moves, bbAttKing);
		generateMovesPromotions<white, mode>(board, moves,
				~board.bitboardAll() & bbAttKing);
	}

	return moves.size() - nb;
}

////////////////////////////////////////////////////////////////////////////////
// generate all legal moves when king must evade check
int generateKingEvasions(const ChessBoard &board, ChessMoveSortList &moves)
{
	return generateKingEvasions<GenPromotion::gen_all>(board, moves);
}

////////////////////////////////////////////////////////////////////////////////
// generate all legal moves when king must evade check
int generateQuietKingEvasions(const ChessBoard &board, ChessMoveSortList &moves)
{
	return generateKingEvasions<GenPromotion::gen_queen>(board, moves);
}

////////////////////////////////////////////////////////////////////////////////
// Generate non capture moves that give check
template<Side side>
int generateQuietChecks(const ChessBoard &board, ChessMoveSortList &moves)
{
	int nb = moves.size();

	// discovered checks
	// moving one of our piece pinned on ennemy king

	// pinned piece will not give direct check in 2nd part
	BitBoard bbPinned = 0ULL;

	const int pk = board.kingPosition(~side);

	// possible pinner rook & bishop & queen
	BitBoard bbPinner = (BitBoards::file[squareFile(pk)] |
						BitBoards::rank[squareRank(pk)]) &
						(board.bitboardPiece(side, rook) |
						board.bitboardPiece(side, queen));
	bbPinner |= (BitBoards::diag_45r[pk] | BitBoards::diag_45l[pk]) &
				(board.bitboardPiece(side, bishop) |
				board.bitboardPiece(side, queen));

	while (bbPinner) {

		int pp = BitBoards::popFirstBit(bbPinner);

		// path between pinner and king
		BitBoard bbp = BitBoards::between[pk][pp];

		// pinned pieces
		BitBoard bb = bbp & board.bitboardAll();

		// one piece behind king and pinner
		if ((bb) && (!BitBoards::bitCountMoreThanOne(bb))) {

			bb = bbp & board.bitboardColor(side);
			if (bb) {

				int src = BitBoards::getFirstBit(bb);

				BitBoard bbAtt = ~(bbp | board.bitboardAll());

				BitBoard bbDst;

				switch (board.piece(src)) {

					case	pawn:

							// 1 up
							bbDst = BitBoards::shiftUp<side>(bb &
								~BitBoards::rank[relRank<side>(rank_7)]) &
								bbAtt;
							if (bbDst) {
								moves.push(ChessMove(src,
									relSquareUp<side>(src), pawn));
							}

							// 2 up
							bbDst = BitBoards::shiftUp2<side>(bb &
								BitBoards::rank[relRank<side>(rank_2)]) &
								bbAtt & ~(BitBoards::shiftUp<side>(
								board.bitboardAll()));
							if (bbDst) {
								moves.push(ChessMove(src,
									relSquareUp2<side>(src), pawn));
							}
							bbPinned |= bb;
							break;

					case	knight:
							bbDst = BitBoards::attackKnight(src) & ~bbp &
								~board.bitboardAll();
							while (bbDst) {
								int dst = BitBoards::popFirstBit(bbDst);
								moves.push(ChessMove(src, dst, knight));
							}
							bbPinned |= bb;
							break;

					case	bishop:
							bbDst = BitBoards::attackBishop(src,
								board.bitboardAll()) & bbAtt;
							while (bbDst) {
								int dst = BitBoards::popFirstBit(bbDst);
								moves.push(ChessMove(src, dst, bishop));
							}
							bbPinned |= bb;
							break;

					case	rook:
							bbDst = BitBoards::attackRook(src,
								board.bitboardAll()) & bbAtt;
							while (bbDst) {
								int dst = BitBoards::popFirstBit(bbDst);
								moves.push(ChessMove(src, dst, rook));
							}
							bbPinned |= bb;
							break;

					case	queen:
							bbDst = BitBoards::attackQueen(src,
								board.bitboardAll()) & bbAtt;
							while (bbDst) {
								int dst = BitBoards::popFirstBit(bbDst);
								moves.push(ChessMove(src, dst, queen));
							}
							bbPinned |= bb;
							break;

					case	king:
							bbDst = BitBoards::attackKing(src) & bbAtt;
							while (bbDst) {
								int dst = BitBoards::popFirstBit(bbDst);
								moves.push(ChessMove(src, dst, king));
							}
							bbPinned |= bb;
							break;

					default:
						break;
				}
			}
		}
	}

	// direct checks

	// knights
	BitBoard bbAtt = BitBoards::attackKnight(pk) & ~board.bitboardAll();
	BitBoard bbSrc = board.bitboardPiece(side, knight) & ~bbPinned;

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackKnight(src) & bbAtt;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push(ChessMove(src, dst, knight));
		}
	}

	// bishops
	bbAtt = BitBoards::attackBishop(pk, board.bitboardAll()) &
			~board.bitboardAll();
	bbSrc = board.bitboardPiece(side, bishop) & ~bbPinned;

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackBishop(src,
			board.bitboardAll()) & bbAtt;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push(ChessMove(src, dst, bishop));
		}
	}

	// rooks
	bbAtt = BitBoards::attackRook(pk, board.bitboardAll()) &
			~board.bitboardAll();
	bbSrc = board.bitboardPiece(side, rook) & ~bbPinned;

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackRook(src,
				board.bitboardAll()) & bbAtt;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push(ChessMove(src, dst, rook));
		}
	}

	// queens
	bbAtt = BitBoards::attackQueen(pk, board.bitboardAll()) &
			~board.bitboardAll();
	bbSrc = board.bitboardPiece(side, queen) & ~bbPinned;

	while (bbSrc) {

		int src = BitBoards::popFirstBit(bbSrc);
		BitBoard bbDst = BitBoards::attackQueen(src, board.bitboardAll()) &
				bbAtt;

		while (bbDst) {
			int dst = BitBoards::popFirstBit(bbDst);
			moves.push(ChessMove(src, dst, queen));
		}
	}

	// pawns

	const BitBoard bbfilea = (side == white) ? ~BitBoards::file[file_a] :
										  ~BitBoards::file[file_h];
	const BitBoard bbfileh = (side == white) ? ~BitBoards::file[file_h] :
										  ~BitBoards::file[file_a];

	bbAtt = ((BitBoards::shiftUpLeft<~side>(BitBoards::square[pk]) & bbfilea)
		| (BitBoards::shiftUpRight<~side>(BitBoards::square[pk]) & bbfileh))
		& ~board.bitboardAll();

	// 1 up
	bbSrc = BitBoards::shiftUp<side>(board.bitboardPiece(side, pawn) &
			~BitBoards::rank[relRank<side>(rank_7)]) & bbAtt & ~bbPinned;

	while (bbSrc) {
		int src = BitBoards::popFirstBit(bbSrc);
		moves.push(ChessMove(relSquareDown<side>(src) , src, pawn));
	}

	// 2 up
	bbSrc = BitBoards::shiftUp2<side>(board.bitboardPiece(side, pawn) &
			BitBoards::rank[relRank<side>(rank_2)]) & bbAtt &
			~BitBoards::shiftUp<side>(board.bitboardAll()) & ~bbPinned;

	while (bbSrc) {
		int src = BitBoards::popFirstBit(bbSrc);
		moves.push(ChessMove(relSquareDown2<side>(src), src, pawn));
	}

	// TODO : castle : rook can check the ennemy king

	return moves.size() - nb;
}

////////////////////////////////////////////////////////////////////////////////
// Generate non capture moves that give check
int generateQuietChecks(const ChessBoard &board, ChessMoveSortList &moves)
{
	return (board.side() == black) ?
		generateQuietChecks<black>(board, moves) :
		generateQuietChecks<white>(board, moves);
}

////////////////////////////////////////////////////////////////////////////////
// generate all possible moves (pseudo legal moves)
int generateMoves(const ChessBoard &board, ChessMoveSortList &moves,
	bool incheck)
{
	if (incheck) {
		return generateKingEvasions(board, moves);
	} else {
		int nb = generateCaptures(board, moves);
		nb += generateQuiets(board, moves);
		return nb;
	}
}

////////////////////////////////////////////////////////////////////////////////
// count legal moves
int countLegalMoves(ChessBoard &board)
{
	ChessMoveSortList mlist;

	bool incheck = board.inCheck();
	int countmoves = generateMoves(board, mlist, incheck);

	int nblegalmoves = 0;
	int nm = 0;

	ChessMoveRec rec;
	while (nm < countmoves) {
		board.makeMove(mlist[nm].move, rec);
		if (board.isPositionLegal()) {
			++nblegalmoves;
		}
		board.unMakeMove(mlist[nm].move, rec);
		++nm;
	}

	return nblegalmoves;
}

////////////////////////////////////////////////////////////////////////////////
// generate legal moves
int generateLegalMoves(ChessBoard &board,
	ChessMoveSortList &moves, bool incheck)
{
	ChessMoveSortList mlist;

	moves.clear();
	int cnt = generateMoves(board, mlist, incheck);

	ChessMoveRec rec;
	for (int n=0; n<cnt; n++) {
		board.makeMove(mlist[n].move, rec);
		if (board.isPositionLegal()) {
			moves.push(mlist[n].move);
		}
		board.unMakeMove(mlist[n].move, rec);
	}

	return moves.size();
}

} // namespace ChessMoveGen

} // namespace Cheese

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_BOARD_H_
#define CHEESE_BOARD_H_

#include "config.h"

#include "eval.h"
#include "fen.h"
#include "move.h"
#include "zobrist.h"

#include <array>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
enum class ChessRules {
	// classic chess rules
	classic,

	// Fischer Random Chess (Chess 960)
	frc
};

// game phases
//
//	value : pawns and king = 0
//			knight  = 1
//			bishop  = 1
//			rook    = 2
//			queen   = 4
//
enum ChessGamePhaseLimit {

	// pawns ending (0)
	gamephase_ending_pawns = 0,

	// ending (1 .. 8)
	gamephase_ending       = 1,

	// middle game (9 .. 20)
	gamephase_middle_game  = 9,

	// opening (21 .. 31)
	gamephase_opening      = 21,

	// start value (31)
	gamephase_start        = 31
};

////////////////////////////////////////////////////////////////////////////////
// Move undo information used in make/unmake
struct ChessMoveRec {

	// hashkey
	HashKey hashKey;

	HashKey hashKeyPawns;

	// castling flags
	uint8_t castle;

	// en passant file + 1
	uint8_t enpassant;

	// fifty move rule
	uint8_t fifty;
};

////////////////////////////////////////////////////////////////////////////////
// Define a position on the Chess board
class ChessBoard {

	friend class ChessFENParser;

private:

	// current position hashkey
	HashKey hashKey;

	// current position pawns hashkey
	HashKey hashKeyPawns;

	// all pieces bitboard
	BitBoard bbAllPiece;

	// all black & whites pieces bitboards
	std::array<BitBoard, nb_sides> bbColorPiece;

	// pieces positions bitboards : for each color, each pieces
	std::array<std::array<BitBoard, nb_pieces_type>, nb_sides> bbPiece;

	// needed to fast find piece type
	// note : possible to add piece color
	std::array<uint8_t, nb_squares> pieces;

	// incremental material evaluation
	std::array<int32_t, nb_sides> evalMaterial;

	// current playing side
	Side player;

	// game phase count for each side
	std::array<uint32_t, nb_sides> phaseCount;

	// piece count mask
	uint64_t matCount;

	// kings positions
	std::array<uint32_t, nb_sides> posKing;

	// castle possibility for white & black
	uint8_t castle;

	// en passant file + 1, or 0 if none
	uint8_t enpassant;

	// fifty moves rule
	uint8_t fifty;

	// * FRC :

	// squares between rook initial and final position
	std::array<BitBoard, nb_sides> squaresCastleShort;

	std::array<BitBoard, nb_sides> squaresCastleLong;

	// Initial position of king
	std::array<int, nb_sides> sqKingStart;

	// Initial position of King rook
	std::array<int, nb_sides> sqCastleRookShort;

	// Initial position of Queen rook
	std::array<int, nb_sides> sqCastleRookLong;

	// classic vs FRC support
	ChessRules rules;

public:

	ChessBoard();

	ChessBoard(const ChessBoard &) = default;

	ChessBoard & operator= (const ChessBoard &) = default;

	// clear board
	void clear();

	BitBoard bitboardPiece(Side s, int p) const;

	BitBoard bitboardColor(Side s) const;

	BitBoard bitboardAll() const;

	BitBoard bitboardPieceType(int p) const;

	uint8_t piece(int sq) const;

	int32_t materialScore(Side s) const;

	uint64_t materialCount() const;

	int kingPosition(Side s) const;

	bool canCastle(uint8_t f) const;

	// if en passant is possible for the current siode
	bool isEnPassant() const;

	// return en passant square
	int enPassantSquare() const;

	void initCastlingRights();

	void clearBoardPiece(Side s, int sq, int pc);

	void setBoardPiece(Side s, int sq, int pc);

	void moveBoardPiece(Side s, int src, int dst, int pc);

	void setPiece(int sq, uint8_t pc);

	void clearPiece(int sq);

	uint32_t gamePhaseCount(Side s) const;

	// get current game phase
	uint32_t gamePhase() const;

	// set chess default position
	void setStartPosition();

	// set custom position
	void setPosition(const std::array<char, nb_squares> &pc, Side s, int cst,
		int ep, int fft);

	// print board
	void print(std::ostream &stream) const;

	// check if a move is valid
	bool validMove(ChessMove move) const;

	// create Zobrist HashKey from current position
	HashKey createHashKey();

	// create Zobrist Hashkey for pawns
	HashKey createHashKeyPawns();

	// make a move
	void makeMove(ChessMove move, ChessMoveRec &rec);

	// unmake a move
	void unMakeMove(ChessMove move, const ChessMoveRec &rec);

	// make a null move
	void makeNullMove(ChessMoveRec &rec);

	// unmake a null move
	void unMakeNullMove(const ChessMoveRec &rec);

	// draw by insufficient material
	// return true if insufficient material to win
	bool evaluateDraw() const;

	int squareKingStart(Side s) const;

	BitBoard bitboardCastleShort(Side s) const;

	BitBoard bitboardCastleLong(Side s) const;

	int squareRookShort(Side s) const;

	int squareRookLong(Side s) const;

	// return true if the king is attacked
	bool inCheck() const;

	// get current position hashkey
	HashKey getHashKey() const;

	// get current position pawns hashkey
	HashKey getHashKeyPawns() const;

	// get current playing side
	Side side() const;

	uint8_t fiftyRule() const;

	// check if a square is attacked by a given color
	bool isAttacked(int sq, Side s) const;

	bool isAttackedOcc(int sq, Side s, BitBoard occ) const;

	// SEE : Static Exchange Evalutaion
	int staticExchangeEvaluation(ChessMove move) const;

	// test if the current position is legal after playing a move
	// (opponent king not in check)
	// (used after makemove)
	bool isPositionLegal() const;

	void setChessRules(ChessRules r);

	ChessRules getChessRules() const;

};

////////////////////////////////////////////////////////////////////////////////
inline bool isGamePhaseOpening(int p)
{
	return (p >= gamephase_opening);
}

////////////////////////////////////////////////////////////////////////////////
inline bool isGamePhaseMiddle(int p)
{
	return ((p >= gamephase_middle_game) &&
			(p < gamephase_opening));
}

////////////////////////////////////////////////////////////////////////////////
inline bool isGamePhaseEnding(int p)
{
	return ((p >= gamephase_ending) &&
			(p < gamephase_middle_game));
}

////////////////////////////////////////////////////////////////////////////////
inline bool isGamePhasePawnEnding(int p)
{
	return (p == gamephase_ending_pawns);
}

////////////////////////////////////////////////////////////////////////////////
inline bool isGamePhaseBeforeEnding(int p)
{
	return (p >= gamephase_middle_game);
}

////////////////////////////////////////////////////////////////////////////////
inline bool isGamePhaseAfterMiddle(int p)
{
	return (p < gamephase_middle_game);
}

////////////////////////////////////////////////////////////////////////////////
inline BitBoard ChessBoard::bitboardPiece(Side s, int p) const
{
	return bbPiece[s][p];
}

////////////////////////////////////////////////////////////////////////////////
inline BitBoard ChessBoard::bitboardColor(Side s) const
{
	return bbColorPiece[s];
}

////////////////////////////////////////////////////////////////////////////////
inline BitBoard ChessBoard::bitboardAll() const
{
	return bbAllPiece;
}

////////////////////////////////////////////////////////////////////////////////
inline BitBoard ChessBoard::bitboardPieceType(int p) const
{
	return (bbPiece[black][p] | bbPiece[white][p]);
}

////////////////////////////////////////////////////////////////////////////////
inline uint8_t ChessBoard::piece(int sq) const
{
	return pieces[sq];
}

////////////////////////////////////////////////////////////////////////////////
inline int32_t ChessBoard::materialScore(Side s) const
{
	return evalMaterial[s];
}

////////////////////////////////////////////////////////////////////////////////
inline uint64_t ChessBoard::materialCount() const
{
	return matCount;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessBoard::kingPosition(Side s) const
{
	return static_cast<int>(posKing[s]);
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessBoard::canCastle(uint8_t f) const
{
	return ((castle & f) != 0);
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessBoard::isEnPassant() const
{
	return (enpassant != 0);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessBoard::enPassantSquare() const
{
	return static_cast<int>(enpassant) - 1;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessBoard::clearBoardPiece(Side s, int sq, int pc)
{
	const uint64_t bb = ~BitBoards::square[sq];
	bbAllPiece &= bb;
	bbColorPiece[s] &= bb;
	bbPiece[s][pc] &= bb;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessBoard::setBoardPiece(Side s, int sq, int pc)
{
	const uint64_t bb = BitBoards::square[sq];
	bbAllPiece |= bb;
	bbColorPiece[s] |= bb;
	bbPiece[s][pc] |= bb;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessBoard::moveBoardPiece(Side s, int src, int dst, int pc)
{
	// assume the piece is on src square
	const uint64_t bb = BitBoards::square[src] | BitBoards::square[dst];
	bbAllPiece ^= bb;
	bbColorPiece[s] ^= bb;
	bbPiece[s][pc] ^= bb;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessBoard::setPiece(int sq, uint8_t pc)
{
	pieces[sq] = pc;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessBoard::clearPiece(int sq)
{
	pieces[sq] = no_piece;
}

////////////////////////////////////////////////////////////////////////////////
inline uint32_t ChessBoard::gamePhaseCount(Side s) const
{
	return phaseCount[s];
}

////////////////////////////////////////////////////////////////////////////////
inline uint32_t ChessBoard::gamePhase() const
{
	return (phaseCount[black] + phaseCount[white]);
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessBoard::evaluateDraw() const
{
	// no pawns and one knight/bishop is draw
	return ((!havePawns(matCount)) &&
			((evalMaterial[black] + evalMaterial[white]) <=
				pieces_value[bishop]));
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessBoard::squareKingStart(Side s) const
{
	return sqKingStart[s];
}

////////////////////////////////////////////////////////////////////////////////
inline BitBoard ChessBoard::bitboardCastleShort(Side s) const
{
	return squaresCastleShort[s];
}

////////////////////////////////////////////////////////////////////////////////
inline BitBoard ChessBoard::bitboardCastleLong(Side s) const
{
	return squaresCastleLong[s];
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessBoard::squareRookShort(Side s) const
{
	return sqCastleRookShort[s];
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessBoard::squareRookLong(Side s) const
{
	return sqCastleRookLong[s];
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessBoard::inCheck() const
{
	return isAttacked(kingPosition(player), ~player);
}

////////////////////////////////////////////////////////////////////////////////
inline HashKey ChessBoard::getHashKey() const
{
	return hashKey;
}

////////////////////////////////////////////////////////////////////////////////
inline HashKey ChessBoard::getHashKeyPawns() const
{
	return hashKeyPawns;
}

////////////////////////////////////////////////////////////////////////////////
inline Side ChessBoard::side() const
{
	return player;
}

////////////////////////////////////////////////////////////////////////////////
inline uint8_t ChessBoard::fiftyRule() const
{
	return fifty;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessBoard::isPositionLegal() const
{
	return !isAttacked(kingPosition(~player), player);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessBoard::setChessRules(ChessRules r)
{
	rules = r;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessRules ChessBoard::getChessRules() const
{
	return rules;
}

} // namespace Cheese

#endif //CHEESE_BOARD_H_

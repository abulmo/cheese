////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

// Zobrist 64 bits Hashing, using Mersenne Twister for random number generation

#ifndef CHEESE_ZOBRIST_H_
#define CHEESE_ZOBRIST_H_

#include "config.h"

#include "move.h"

#include <array>
#include <string>

// define to generate zobrist keys
//#define GENERATE_ZOBRIST_KEYS

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// Zobrist keys used for hash tables
//
//	need :   pieces		: 2 * 6 * 64 = 768
//			 side		: 1
//			 enpassant	: 8
//			 castling	: 4 castling rights
//
//			 total		: 781

// 64 bits hash key used for zobrist hash
using HashKey = uint64_t;

////////////////////////////////////////////////////////////////////////////////
namespace Zobrist {

	constexpr int nb_castling_hash = 16;

	extern const std::array<std::array<std::array<HashKey, nb_squares>,
		nb_pieces_type>, nb_sides> piece;

	extern const std::array<HashKey, nb_castling_hash> castling;

	extern const std::array<HashKey, nb_files + 1> enpassant;

	extern const HashKey side;
	
#ifdef GENERATE_ZOBRIST_KEYS
	// generate all zobrist hashkeys
	void generate();
#endif

	// convert hashkey to string
	std::string hashKeyToString(HashKey key);
}

} // namespace Cheese

#endif //CHEESE_ZOBRIST_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "moveorder.h"

#include "board.h"
#include "engine.h"
#include "util/logfile.h"

#include <iostream>
#include <utility>

#include <cassert>

namespace Cheese {

// MVV-LVA table [captured][moved]
const std::array<std::array<int32_t, nb_pieces_type>, nb_pieces_type> mvv_lva =
{{
	{ 0,    0,    0,    0,    0,    0,    0, 0 },
	{ 0,   63,   61,   61,   59,   55,   32, 0 },
	{ 0,  191,  189,  189,  187,  183,  160, 0 },
	{ 0,  191,  189,  189,  187,  183,  160, 0 },
	{ 0,  319,  317,  317,  315,  311,  288, 0 },
	{ 0,  575,  573,  573,  571,  567,  544, 0 },
	{ 0, 2047, 2045, 2045, 2043, 2039, 2016, 0 },
	{ 0,    0,    0,    0,    0,    0,    0, 0 }
}};

#ifdef PRECALC_MVVLVA
const std::array<int32_t, nb_pieces_type> mvv_lva_pieces =
	{ 0, 1, 3, 3, 5, 9, 32 };

constexpr int scale_mvv_lva = 16;
#endif

////////////////////////////////////////////////////////////////////////////////
ChessMoveOrder & ChessMoveOrder::operator=(const ChessMoveOrder &mo)
{
	// note : don't copy history reference !

	phaseNext = mo.phaseNext;
	position = mo.position;
	pos_capture = mo.pos_capture;
	count = mo.count;
	checks = mo.checks;
	moveHash = mo.moveHash;
	moveKiller1 = mo.moveKiller1;
	moveKiller2 = mo.moveKiller2;
#ifdef USE_COUNTER_MOVE
	moveCounter = mo.moveCounter;
#endif
	movelist = mo.movelist;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMoves(const ChessBoard &board,
		ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		int cp = move.capture();
		int pp = move.promotion();

		if ((cp != no_piece) || (pp == queen)) {

			int	mp = move.piece();
			int sc = mvv_lva[cp][mp];
			moves[n].setScore(sortmove_capture_good + sc);

			assert(((sortmove_capture_good + sc) >= sortmove_capture_good) &&
				    ((sortmove_capture_good + sc) < sortmove_hash));

		} else
		if (move == moveKiller1) {
			moves[n].setScore(sortmove_killer1);
		} else
		if (move == moveKiller2) {
			moves[n].setScore(sortmove_killer2);
		} else {
			int hh = history.get(move, board.side());
			moves[n].setScore(sortmove_history + hh);

			assert(((sortmove_history + hh) >= sortmove_history) &&
				    ((sortmove_history + hh) < sortmove_killer2));
		}
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesEvasion(const ChessBoard &board,
	ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		int cp = move.capture();
		int pp = move.promotion();

		if ((cp != no_piece) || (pp == queen)) {

			int	mp = move.piece();
			int sc = mvv_lva[cp][mp];

			moves[n].setScore(sortmove_capture_good + sc);

			assert(((sortmove_capture_good + sc) >= sortmove_capture_good) &&
				    ((sortmove_capture_good + sc) < sortmove_hash));

		} else {

			int hh = history.get(move, board.side());

			moves[n].setScore(sortmove_history + hh);

			assert(((sortmove_history + hh) >= sortmove_history) &&
				    ((sortmove_history + hh) < sortmove_killer2));
		}
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesCaptures(ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		int cp = move.capture();
		int pp = move.promotion();

		if ((cp != no_piece) || (pp == queen)) {

			int	mp = move.piece();

			int sc = mvv_lva[cp][mp];

			moves[n].setScore(sortmove_capture_good + sc);

			assert(((sortmove_capture_good + sc) >= sortmove_capture_good) &&
				    ((sortmove_capture_good + sc) < sortmove_hash));

		} else {
			moves[n].setScore(sortmove_history);
		}
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesQuiet(const ChessBoard &board,
	ChessMoveSortList &moves, int pos, int nbmoves)
{
	for (int n=pos; n<(pos + nbmoves); n++) {
		int hh = history.get(moves[n].move, board.side());
		moves[n].setScore(sortmove_history + hh);
	}

	moves.sort(pos, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
int ChessMoveOrder::orderMovesRoot(ChessSearch *search, ChessBoard &board,
	ChessMoveSortList &moves, int nbmoves, int nply,
	[[maybe_unused]] bool incheck)
{
	ChessMoveRec rec;
	for (int n=0; n<nbmoves; n++) {

		// sort by quiescence
#ifdef SORT_ROOT_QUIESCENCE		
		search->getRepetitions().push(board.getHashKey());
		board.makeMove(moves[n].move, rec);

		bool chk = board.inCheck();
		moves[n].setScore(-search->searchQuiescent(-mate_value,
			mate_value, depth_checks_quiescence, nply + 1, true, chk));

		board.unMakeMove(moves[n].move, rec);
		search->getRepetitions().pop();
#else	
		// sort by castling + captures/see + quiets sorted by eval
		int cp = moves[n].move.capture();
		int pp = moves[n].move.promotion();

		if ((cp != no_piece) || (pp == queen)) {

			int	mp = moves[n].move.piece();
			int sc = mvv_lva[cp][mp];

			if (pieces_value[cp] < pieces_value[mp]) {
				int see = board.staticExchangeEvaluation(moves[n].move);
				if (see < 0) {
					moves[n].setScore(sortmove_capture_bad + sc);

					assert(((sortmove_capture_bad + sc) >= 
					sortmove_capture_bad) && ((sortmove_capture_bad + sc) < 
					sortmove_history));

				} else {
					moves[n].setScore(sortmove_capture_good + sc);

					assert(((sortmove_capture_good + sc) >= 
						sortmove_capture_good) && ((sortmove_capture_good + sc) 
						< sortmove_hash));
				}
			} else {
				moves[n].setScore(sortmove_capture_good + sc);

				assert(((sortmove_capture_good + sc) >= 
					sortmove_capture_good) && ((sortmove_capture_good + sc) < 
					sortmove_hash));
			}

		} else {
			if (moves[n].move.isCastle()) {
				moves[n].setScore(sortmove_hash);
			} else {
				board.makeMove(moves[n].move, rec);
				int hh = -search->evaluate(-mate_value, mate_value);
				moves[n].setScore(sortmove_history + hh);

				assert(((sortmove_history + hh) > sortmove_capture_bad) &&
				    ((sortmove_history + hh) < sortmove_killer2));

				board.unMakeMove(moves[n].move, rec);
			}
		}
#endif //SORT_ROOT_QUIESCENCE
	}

	moves.sort(0, nbmoves);

	return nbmoves;
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesQuiescent(ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		int cp = move.capture();
		int mp = move.piece();

		if (pieces_value[cp] >= pieces_value[mp]) {

			int sc = mvv_lva[cp][mp];

			moves[n].setScore(sortmove_capture_good + sc);

			assert(((sortmove_capture_good + sc) >= sortmove_capture_good) &&
				    ((sortmove_capture_good + sc) < sortmove_hash));

		} else {
			moves[n].setScore(sortmove_capture_bad);
		}
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesQuiescentChecks(const ChessBoard &board,
	ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		int cp = move.capture();
		int pp = move.promotion();

		if ((cp != no_piece) || (pp == queen)) {

			int	mp = move.piece();

			int sc = mvv_lva[cp][mp];

			moves[n].setScore(sortmove_capture_good + sc);

			assert(((sortmove_capture_good + sc) >= sortmove_capture_good) &&
				    ((sortmove_capture_good + sc) < sortmove_hash));

		} else {
			int hh = history.get(move, board.side());
			moves[n].setScore(sortmove_history + hh);

			assert(((sortmove_history + hh) >= sortmove_history) &&
				((sortmove_history + hh) < sortmove_killer2));
		}
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
ChessMoveOrder::ChessMoveOrder(const ChessBoard &board,
	ChessHistory &h, int ph, int p, ChessMove mh, ChessMove lastmove) :
	history(h), phaseNext(ph), position(0), pos_capture(0), count(0),
	checks(false),
	moveHash(mh)
{
	moveKiller1 = history.getKiller1(p);
	moveKiller2 = history.getKiller2(p);

#ifdef USE_COUNTER_MOVE
	moveCounter = history.getHistoryMove(board.side(), lastmove);
#endif
}

////////////////////////////////////////////////////////////////////////////////
ChessMoveOrder::ChessMoveOrder([[maybe_unused]] const ChessBoard &board,
	ChessHistory &h, int ph, bool genchecks) : history(h), phaseNext(ph),
	position(0), pos_capture(0), count(0), checks(genchecks)
{
}

////////////////////////////////////////////////////////////////////////////////
int ChessMoveOrder::getNextMove(const ChessBoard &board, ChessMove &move)
{
	bool done = false;
	int cnt = 0;
	int phase = 0;

	do {

		phase = phaseNext;

		switch (phaseNext) {

			case	ordermove_hash:
					++phaseNext;
					if (board.validMove(moveHash)) {
						move = moveHash;
						return phase;
					}
					moveHash.clear();
					
					[[fallthrough]];

			case	ordermove_init_capture:
					++phaseNext;
					position = 0;
					pos_capture = 0;
					count = ChessMoveGen::generateCaptures(board, movelist);
					if (count > 0) {
						orderMovesCaptures(movelist, count);
					} else {
						++phaseNext;
					}
					break;

			case	ordermove_next_capture:
					if (position < count) {
						move = movelist[position++].move;
						// do SEE now, and push move at the end of the list
						// if see < 0
						if (pieces_value[move.capture()] <
							pieces_value[move.piece()]) {
							if (board.staticExchangeEvaluation(move) < 0) {
								// use begining of the list for bad captures
								// pos_capture will never overwrite not
								// tested captures
								movelist[pos_capture++].move = move;
								break;
							}
						}
						if (move != moveHash) {
							done = true;
						}
					} else {
						++phaseNext;
					}
					break;

			case	ordermove_killer1:
					++phaseNext;
					if ((moveKiller1 != moveHash) &&
						(board.validMove(moveKiller1))) {
						move = moveKiller1;
						return phase;
					}
					
					[[fallthrough]];

			case	ordermove_killer2:
					++phaseNext;
					if ((moveKiller2 != moveHash) &&
						(board.validMove(moveKiller2))) {
						move = moveKiller2;
						return phase;
					}

					[[fallthrough]];

#ifdef USE_COUNTER_MOVE
			case	ordermove_countermove:
					++phaseNext;
					if ((moveCounter != moveHash) &&
						(moveCounter != moveKiller1) &&
						(moveCounter != moveKiller2) &&
						(board.validMove(moveCounter))) {
						move = moveCounter;
						return phase;
					}

					[[fallthrough]];
#endif

			case	ordermove_init_quiet:
					++phaseNext;
					cnt = ChessMoveGen::generateQuiets(board,
						movelist);
					if (cnt > 0) {
						orderMovesQuiet(board, movelist, count, cnt);
						count += cnt;
					} else {
						++phaseNext;
						position = 0;
						count = pos_capture;
					}
					break;

			case	ordermove_next_quiet:
					if (position < count) {
						move = movelist[position++].move;
						if ((move != moveHash)
							&& (move != moveKiller1)
							&& (move != moveKiller2)
#ifdef USE_COUNTER_MOVE
							&& (move != moveCounter)
#endif
						) {
							done = true;
						}
					} else {
						++phaseNext;
						position = 0;
						count = pos_capture;
					}
					break;

			case	ordermove_next_bad_capture:
					if (position < count) {
						move = movelist[position++].move;
						if (move != moveHash) {
							done = true;
						}
					} else {
						phaseNext = ordermove_end;
						return ordermove_end;
					}
					break;

			case	ordermove_hash_check:
					++phaseNext;
					if (board.validMove(moveHash)) {
						move = moveHash;
						return phase;
					}
					moveHash.clear();
					
					[[fallthrough]];

			case	ordermove_init_check:
					++phaseNext;
					position = 0;
					count = ChessMoveGen::generateKingEvasions(board,
						movelist);
					if (count > 0) {
						orderMoves(board, movelist, count);
					} else {
						phaseNext = ordermove_end;
						return ordermove_end;
					}
					break;

			case	ordermove_next_check:
					if (position < count) {
						move = movelist[position++].move;
						if (move != moveHash) {
							done = true;
						}
					} else {
						phaseNext = ordermove_end;
						return ordermove_end;
					}
					break;

			case	ordermove_init_quiescence_capture:
					position = 0;
					count = ChessMoveGen::generateQuietCaptures(board,
							movelist);
					++phaseNext;
					if (count > 0) {
						orderMovesQuiescent(movelist, count);
					} else {
						phaseNext = ordermove_init_quiescence_check;
					}
					break;

			case	ordermove_next_quiescence_capture:
					if (position < count) {
						move = movelist[position].move;
						int value = movelist[position++].getScore();
						if ((value > sortmove_capture_bad) ||
							(board.staticExchangeEvaluation(move) >= 0)) {
							done = true;
						}
					} else {
						phaseNext = ordermove_init_quiescence_check;
					}
					break;

			case	ordermove_init_quiescence_evasion:
					position = 0;
					count = ChessMoveGen::generateQuietKingEvasions(board,
						movelist);
					if (count > 0) {
						orderMovesQuiescentChecks(board, movelist,
							count);
						++phaseNext;
					} else {
						phaseNext = ordermove_init_quiescence_check;
					}
					break;

			case	ordermove_next_quiescence_evasion:
					if (position < count) {
						move = movelist[position++].move;
						done = true;
					} else {
						phaseNext = ordermove_init_quiescence_check;
					}
					break;

			case	ordermove_init_quiescence_check:
					if (!checks) {
						phaseNext = ordermove_end;
					 	return ordermove_end;
					} else {
						position = 0;
						movelist.clear();
						count = ChessMoveGen::generateQuietChecks(board,
							movelist);
						if (count > 0) {
							++phaseNext;
						} else {
							phaseNext = ordermove_end;
							return ordermove_end;
						}
					}
					break;
					
			case	ordermove_next_quiescence_check:
					if (position < count) {
						move = movelist[position++].move;
						if (board.staticExchangeEvaluation(move) >= 0) {
							done = true;
						}
					} else {
						phaseNext = ordermove_end;
						return ordermove_end;
					}
					break;

			default:
				phaseNext = ordermove_end;
				return ordermove_end;
				break;
		}

	} while (!done);

	return phase;
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::initSearchRoot(ChessSearch *search, ChessBoard &board)
{
	count = orderMovesRoot(search, board, movelist, count, 0, board.inCheck());
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::updateRootMove(const ChessMove &pvmove)
{
	if ((count > 1) &&
		(!pvmove.isEmpty())) {

		// find last pv move
		int num = 0;
		for (int n=0; n<count; n++) {
			if (movelist[n].move == pvmove) {
				num = n;
				break;
			}
		}

		// move to the top of the list
		if (num > 0) {
			ChessMoveSort move = movelist[num];
			for (int n=num; n>0; n--) {
				movelist[n] = movelist[n - 1];
			}
			movelist[0] = move;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::copyMoves(ChessMoveOrder &moveorder)
{
	count = moveorder.getMoveCount();
	for (int n=0; n<count; n++) {
		movelist[n] = moveorder.getMoveSort(n);
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::excludeMove(ChessMove move)
{
	if (count > 0) {
		int i = 0;
		for (i=0; i<count; i++) {
			if (movelist[i].move == move) {
				break;
			}
		}
		if (i < count) {
			for (int n=i+1; n<count; n++) {
				movelist[n - 1] = movelist[n];
			}
			--count;
		}
	}
}

#ifdef PRECALC_MVVLVA
////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::precalcMVVLVA()
{
	for (int y=0; y<nb_pieces_type; y++) {
		for (int x=0; x<nb_pieces_type; x++) {
			int v = 0;
			if (((y >= pawn) && (y <= king)) &&
				((x >= pawn) && (x <= king))) {
				v = mvv_lva_pieces[y] * scale_mvv_lva - mvv_lva_pieces[x];
			}
			std::cout << v << ", ";
		}
		std::cout << std::endl;
	}
}
#endif

} // namespace Cheese

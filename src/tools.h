////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_TOOLS_H_
#define CHEESE_TOOLS_H_

#include "config.h"

#include "engine.h"
#include "movegen.h"

#include <array>
#include <fstream>
#include <string>
#include <vector>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// information used in divide function
struct ChessDivide {

	uint64_t count;

	std::string name;

	ChessDivide() : count(0ULL)
	{
	}
};

class ChessTools;

////////////////////////////////////////////////////////////////////////////////
struct ChessSearchResult {

	uint64_t countNodes;

	uint64_t searchTime;

	ChessMove bestMove;

	ChessMove ponderMove;

	int maxDepth;

	int maxPly;
};

////////////////////////////////////////////////////////////////////////////////
// all tools to test / benchmark
class ChessTools : public ChessEngineListener
{
public:

	// pointer to the chess engine
	ChessEngine &engine;

	// pointer to the board position
	ChessBoard &board;

	std::array<ChessDivide, max_play_moves> listDivide;

	int nbDivide;

	uint64_t countnode;

	uint64_t countcapture;

	uint64_t countep;

	uint64_t countpromotion;

	uint64_t countcastle;

	uint64_t countcheck;

	uint64_t countmate;

	// results from last search
	ChessSearchResult searchResult;

	bool muted;

public:

	ChessTools(ChessEngine &e, ChessBoard &b);

	~ChessTools() override;

	void divide(int depth);

	uint64_t searchAllMoves(int depth);

	void perftd(int depth);

	void perft(int depth);

	void doPerft(int maxdepth);

	// run a test suite, from a .EPD file
	bool runTestSuite(const std::string &filename);

	void runBenchmark(const std::vector<std::string> &fens, int mode);

	static std::string formatNumber(uint64_t v);

	void onEndSearch(ChessEngineNotifier &notifier,
		ChessMove bm, ChessMove pm, int depth, int ply, uint64_t cntnodes,
		uint64_t searchtime) override;

	void onSendMultiPV(ChessEngineNotifier &notifier, int num, int depth,
		int score, ChessPVMoveList &mlist, int count, uint64_t cntnodes,
		uint64_t searchtime, int maxply, int bound, uint64_t tbhits) override;

	void onSendHashFull([[maybe_unused]] ChessEngineNotifier &notifier,
		[[maybe_unused]] uint64_t v) override
	{}

	void onSendInfoDepth([[maybe_unused]] ChessEngineNotifier &notifier,
		[[maybe_unused]] int d) override
	{}

	void onSearchRootMove([[maybe_unused]]ChessEngineNotifier &notifier,
		[[maybe_unused]] ChessMove move, [[maybe_unused]] int num) override
	{}

	void onSendInfoMessage([[maybe_unused]] ChessEngineNotifier &notifier,
		[[maybe_unused]] const std::string &msg) override
	{}

};

} // namespace Cheese

#endif //CHEESE_TOOLS_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_ENGINE_H_
#define CHEESE_ENGINE_H_

#include "config.h"

#include "board.h"
#include "polyglot/book.h"
#include "eval.h"
#include "hash.h"
#include "listener.h"
#include "pv.h"
#include "repetitions.h"
#include "search.h"
#include "thread.h"
#include "util/timer.h"
#include "tune.h"

#include <array>
#include <string>
#include <vector>

// UCI limit strength option
#define USE_LIMIT_STRENGTH

namespace Cheese {

// maximum strength
constexpr int max_strength_elo = 3200;

// minimum strength
constexpr int min_strength_elo = 1000;

// reduce ELO by limiting speed or nodes
enum class ELOLimitMode {
	speed,
	nodes
};

////////////////////////////////////////////////////////////////////////////////
// Chess engine main class
class ChessEngine : public ChessEngineNotifier {

	friend class ChessSearch;

private:

	// chess board current position (not in search)
	ChessBoard board;

	// evaluation functions
	ChessEval eval;

	// main hash table
	ChessHashTable hashTable;

	// repetition table	for current game (not in search)
	ChessRepetitions repetition;

	// move list for current game
	std::vector<ChessMove> gameMoves;

	// undo move list, to be able to take back
	std::vector<ChessMoveRec> undoMoves;

	// threads used by the search
	ChessSearchThreadList threads;

	// opening book
	ChessOpeningBook openingBook;

	// timer
	Timer timerSearch;

	// number of moves in the game from FEN position
	int moveNumberFEN;

	// engine playing side
	Side engineSide;

	// multipv number of moves (1 = disable)
	int multipv;

	// search mode
	SearchMode searchMode;

	// search time by moves
	uint64_t ideepTimeByMove;

	// max depth to search
	int ideepMaxDepth;

	// number of nodes between time checks
	uint64_t ideepNodeCheckTime;

	// node count for time checks
	uint64_t ideepNodeCountTime;

	// count nodes before Sleep to reduce speed
	uint64_t nodeCountSleep;

	// how many nodes before sleep to reduce speed
	// 0 = disable
	uint64_t reduceSpeedNodes;

	// limit number of nodes to reduce strength
	uint64_t eloNodesLimit;

	ELOLimitMode eloLimitMode;

	uint64_t fixedNodesValue;

	uint64_t fixedTimeValue;

	// remaining moves (for tournament mode, if used)
	int remainingmoves;

	// time increment for each side (should be the same ?)
	std::array<int, nb_sides> timeIncrement;

	// number of moves of the game out of book moves
	int movesOutOfBook;

	// max depth for current search
	int thinkDepth;

	int timeGameLength;

	// maximum number of threads used by the search
	int maxThread;

	// SMP algorithm
	SMPMode smpmode;

	// minimum split depth for YBWC
	int minSplitDepth;

	// time for white & black (ms)
	std::array<int, nb_sides> timeGame;

	// pondering is in use for the current search
	std::atomic_bool pondering;

	// enable null moves prunning
	bool useNullMoves;

	// enable Late moves reduction
	bool useLMR;

	// set when running testsuites
	bool runtestsuite;

	// use opening book
	bool useOpeningBook;

#ifdef USE_LIMIT_STRENGTH
	int strengthELO;

	bool useCustomStrength;
#endif

#ifdef USE_TABLEBASES
	int TBProbeDepth;

	int TBProbeLimit;

	int TBCardinality;

	bool TB50MoveRule;
#endif

public:

	ChessEngine();

	~ChessEngine() override = default;

	// precalc tables for bitboards, search, ...
	// must be called once
	static void precalcTables();

	void free();

	void init(int hashsize);

#ifdef USE_TUNING
	void initTuning();

	auto getTuningParams();
#endif

	ChessBoard &getBoardRef();

	ChessEval &getEval();

	ChessHashTable &getHashTable();

	ChessRepetitions &getRepetitions();

	ChessSearchThreadList &getThreads();

	bool mustCheckNodes() const;

	bool isRunningTestSuite() const;

	void setModeRunTestSuite(bool b);

	bool usingLMR() const;

	bool usingNullMoves() const;

	SMPMode getSMPMode() const;

	// set algorithm for SMP (use it before creating threads)
	void setSMPMode(SMPMode m);

	// start a new game
	void newGame();

	void setRemainingMoves(int r);

	void setTimeIncrement(Side side, int increment);

	void enableNullMoves(bool b);

	void enableLMR(bool b);

	void setMultiPV(int n);

	bool isPondering() const;

#ifdef USE_LIMIT_STRENGTH
	// return if custom strength is used
	bool usingCustomStrength() const;

	// return current ELO strength
	int getStrengthELO() const;

	// set custom ELO strength
	void setCustomStrength(int e);
#endif

#ifdef USE_LIMIT_STRENGTH
	void setStrengthELO(int e);
#endif

	// get current search time
	auto getSearchTime() const;

	// return the number of legal moves on current position
	int countLegalMoves(ChessBoard &pos);

	// test if the player is mate (slow)
	bool isMate(ChessBoard &pos);

	// return draw value for current player
	int getDrawValue(ChessBoard &pos);

	// set contempt factor, returned as draw value
	void setDrawValue(int v);

	// update piece board
	void updatePieces();

	// init time management and stop conditions
	void initSearch();

	// start a new search for current position
	void startSearch(bool ponder);

	// start searching
	bool search(ChessMoveOrder &moveOrderRoot);

	void waitEndSearch();

	// calculate mate value
	int calcMateValue(int value);

	// extract pv from hash
	void extractPV(int depth);

	// test if the engine must end current search
	bool mustStopSearch() const;

	// when start seearching a move at root
	void searchingRootMove(ChessMove &mv, int num);

	// set tuning parameter
#ifdef USE_TUNING
	void setTuneValue(const std::string &name, const std::string &v);
#endif

	// evaluate current position
	int evaluate(int alpha, int beta);

	// clear the main hash table
	void clearHashTable();

	void setHashTableSize(int sz);

	void reduceSpeed();

	bool isReducingSpeed() const;

	// get current search nodes count
	// smp : for all threads
	uint64_t getSearchNodeCount();

	uint64_t getSearchElapsedTime() const;

	// find if the current search must end
	bool checkSearchTime();

	// get current playing side
	Side side() const;

	void errorMoveFromGUI(const std::string &text);

	// set move with no piece information (used by UCI engines)
	// ex: "e2e4"
	bool setMoveSquareList(const std::string &text);

	// take back one move
	bool takeBack();

	// play a move (must be legal)
	bool playMove(ChessMove move);

	// init board with start position
	void setStartPosition();

	// init board with FEN position
	bool setFENPosition(const std::string &str);

	// get FEN position from board
	std::string getFENPosition(ChessBoard &pos);

	void setEngineSearchMode(SearchMode type, int value1, int value2);

	void setMaxThreads(int m);

	void setMinSplitDepth(int d);

	int getMinSplitDepth() const;

	uint64_t getReduceSpeedNodes() const;

#ifdef USE_PERSONALITY
	void setPersonality(const std::string &fname);
#endif

	SearchMode getSearchMode() const;

	// return true if the engine is searching
	bool isThinking();

	// force the current search to stop
	void stopThinking();

	// the opponent played the expected move,
	// continue thinking in normal mode
	void ponderHit();

	// test nodes searched
	// must be called in the move loop for exact node count
	bool checkNodes();

	void setELOLimitMode(ELOLimitMode m);

	// get number of moves in current game
	int getGameMoveCount() const;

	// get move in current game
	ChessMove getGameMove(int n) const;

	ChessMove getGameLastMove() const;

	// load opening book
	bool loadOpeningBook(const std::string &filebook);

	// unload opening book
	void unloadOpeningBook();

	// show available opening book moves for current position
	void showBookMoves();

	// get current time
	int getTime(Side p) const;

	std::string getTextMove(const ChessMove &move);

	std::string getTextPV(const ChessMove *mlist, int count);

	void setPlayerTime(Side p, int tm);

	void resetTime();

	void forceCheckTime();

#ifdef USE_TABLEBASES
	int getTBProbeDepth() const;

	void setTBProbeDepth(int d);

	int getTBProbeLimit() const;

	void setTBProbeLimit(int d);

	int getTBCardinality() const;

	bool getTB50MoveRule() const;

	void setTB50MoveRule(bool b);
#endif

};

////////////////////////////////////////////////////////////////////////////////
inline ChessBoard &ChessEngine::getBoardRef()
{
	return board;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessEval &ChessEngine::getEval()
{
	return eval;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessHashTable &ChessEngine::getHashTable()
{
	return hashTable;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::isPondering() const
{
	return pondering;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::waitEndSearch()
{
	threads.waitEndSearch();
}

////////////////////////////////////////////////////////////////////////////////
inline ChessRepetitions &ChessEngine::getRepetitions()
{
	return repetition;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessSearchThreadList &ChessEngine::getThreads()
{
	return threads;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::mustCheckNodes() const
{
	return ((searchMode == SearchMode::fixed_nodes) ||
			(eloNodesLimit != 0));
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::isRunningTestSuite() const
{
	return runtestsuite;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setModeRunTestSuite(bool b)
{
	runtestsuite = b;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::usingLMR() const
{
	return useLMR;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::usingNullMoves() const
{
	return useNullMoves;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setRemainingMoves(int r)
{
	remainingmoves = r;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setTimeIncrement(Side side, int increment)
{
	timeIncrement[side] = increment;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::enableNullMoves(bool b)
{
	useNullMoves = b;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::enableLMR(bool b)
{
	useLMR = b;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setMultiPV(int n)
{
	multipv = std::clamp(n, 1, max_multipv);
}

#ifdef USE_LIMIT_STRENGTH
////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::usingCustomStrength() const
{
	return useCustomStrength;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getStrengthELO() const
{
	return strengthELO;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setCustomStrength(int e)
{
	strengthELO = std::clamp(e, min_strength_elo, max_strength_elo);
	useCustomStrength = (strengthELO < max_strength_elo);
}
#endif

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getDrawValue(ChessBoard &pos)
{
	return (pos.side() == engineSide) ? -eval.getDrawValue() :
		eval.getDrawValue();
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setDrawValue(int v)
{
	eval.setDrawValue(v);
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::evaluate(int alpha, int beta)
{
	return eval.evaluate(board, threads.getMainThread()->hashTablePawn, alpha,
		beta);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::clearHashTable()
{
	hashTable.clear();
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setHashTableSize(int sz)
{
	hashTable.init(sz);
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::isReducingSpeed() const
{
	return (reduceSpeedNodes != 0);
}

////////////////////////////////////////////////////////////////////////////////
inline uint64_t ChessEngine::getSearchNodeCount()
{
	return threads.getNodeCount();
}

////////////////////////////////////////////////////////////////////////////////
inline uint64_t ChessEngine::getSearchElapsedTime() const
{
	return timerSearch.getElapsedTime();
}

////////////////////////////////////////////////////////////////////////////////
inline Side ChessEngine::side() const
{
	return board.side();
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setMaxThreads(int m)
{
	threads.setMaxThreads(m, *this);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setMinSplitDepth(int d)
{
	minSplitDepth = d * one_ply;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getMinSplitDepth() const
{
	return minSplitDepth;
}

////////////////////////////////////////////////////////////////////////////////
inline uint64_t ChessEngine::getReduceSpeedNodes() const
{
	return reduceSpeedNodes;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::isThinking()
{
	return threads.isThinking();
}

////////////////////////////////////////////////////////////////////////////////
inline SearchMode ChessEngine::getSearchMode() const
{
	return searchMode;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setELOLimitMode(ELOLimitMode m)
{
	eloLimitMode = m;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getGameMoveCount() const
{
	return static_cast<int>(gameMoves.size());
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessEngine::getGameMove(int n) const
{
	return (n < getGameMoveCount()) ? gameMoves[n] : ChessMove(0);
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessEngine::getGameLastMove() const
{
	return gameMoves.back();
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getTime(Side p) const
{
	return timeGame[p];
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setPlayerTime(Side p, int tm)
{
	timeGame[p] = tm;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::isMate(ChessBoard &pos)
{
	return (pos.inCheck() &&
		    (countLegalMoves(pos) == 0));
}

////////////////////////////////////////////////////////////////////////////////
inline SMPMode ChessEngine::getSMPMode() const
{
	return smpmode;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::forceCheckTime()
{
	ideepNodeCountTime = ideepNodeCheckTime;
}

////////////////////////////////////////////////////////////////////////////////
#ifdef USE_TUNING
inline auto ChessEngine::getTuningParams()
{
	return eval.getTuning().getParams();
}
#endif

////////////////////////////////////////////////////////////////////////////////
inline auto ChessEngine::getSearchTime() const
{
	return timerSearch.getElapsedTime();
}

#ifdef USE_TABLEBASES
////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getTBProbeDepth() const
{
	return TBProbeDepth;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setTBProbeDepth(int d)
{
	TBProbeDepth = d;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getTBProbeLimit() const
{
	return TBProbeLimit;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setTBProbeLimit(int d)
{
	TBProbeLimit = d;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessEngine::getTBCardinality() const
{
	return TBCardinality;
}

////////////////////////////////////////////////////////////////////////////////
inline bool ChessEngine::getTB50MoveRule() const
{
	return TB50MoveRule;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessEngine::setTB50MoveRule(bool b)
{
	TB50MoveRule = b;
}
#endif //USE_TABLEBASES

} // namespace Cheese

#endif //CHEESE_ENGINE_H_

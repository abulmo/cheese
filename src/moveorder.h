////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_MOVEORDER_H_
#define CHEESE_MOVEORDER_H_

#include "config.h"

#include "history.h"
#include "move.h"
#include "movegen.h"

#include "util/logfile.h"

#include <array>

// generate MVVLVA table
//#define PRECALC_MVVLVA

// sort root moves by quiescence search result
#define SORT_ROOT_QUIESCENCE

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// move ordering phases
enum MoveOrderPhase {

	// end of move ordering
	ordermove_end,

	// normal search
	ordermove_hash,
	ordermove_init_capture,
	ordermove_next_capture,
	ordermove_killer1,
	ordermove_killer2,
#ifdef USE_COUNTER_MOVE
	ordermove_countermove,
#endif
	ordermove_init_quiet,
	ordermove_next_quiet,
	ordermove_next_bad_capture,

	// normal search in check
	ordermove_hash_check,
	ordermove_init_check,
	ordermove_next_check,

	// quiescence search captures only
	ordermove_init_quiescence_capture,
	ordermove_next_quiescence_capture,

	// quiescence search check evasion
	ordermove_init_quiescence_evasion,
	ordermove_next_quiescence_evasion,

	// quiescence search moves that check
	ordermove_init_quiescence_check,
	ordermove_next_quiescence_check
};

////////////////////////////////////////////////////////////////////////////////
// sort moves values
const int32_t sortmove_hash 		= 100000;
const int32_t sortmove_capture_good =  80000;
const int32_t sortmove_killer1      =  70001;
const int32_t sortmove_killer2      =  70002;
const int32_t sortmove_history      =   4000;
const int32_t sortmove_capture_bad  =   2000;

class ChessEngine;
class ChessSearch;
class ChessBoard;

////////////////////////////////////////////////////////////////////////////////
// moves generation and ordering
class ChessMoveOrder {

private:

	ChessHistory &history;

	// next phase
	int phaseNext;

	// current position in move list
	int position;

	// current position in capture list
	int pos_capture;

	// number of moves in current phase
	int count;

	bool checks;

	// hash move
	ChessMove moveHash;

	// first killer move
	ChessMove moveKiller1;

	// second killer move
	ChessMove moveKiller2;

#ifdef USE_COUNTER_MOVE
	ChessMove moveCounter;
#endif

	// sorted move list
	ChessMoveSortList movelist;

public:

	ChessMoveOrder(ChessHistory &h) : history(h)
	{
		// note: no init for speed, because we allocate movelist in the
		// stack in search function
	}

	ChessMoveOrder(const ChessBoard &board, ChessHistory &h, int ph,
		int p, ChessMove mh, ChessMove lastmove);

	// constructor for quiescence search
	ChessMoveOrder(const ChessBoard &board, ChessHistory &h, int ph,
		bool genchecks);

	ChessMoveOrder & operator=(const ChessMoveOrder &mo);

	ChessMoveSortList &getMoveList();

	int getMoveCount() const;

	// copy move list from another ChessMoveOrder object
	void copyMoves(ChessMoveOrder &moveorder);

	// remove a move from the list
	void excludeMove(ChessMove move);

	void setMoveCount(int n);

	ChessMoveSort getMoveSort(int n) const;

	ChessMove getMove(int n) const;

	int32_t getMoveScore(int n) const;

	void initSearchRoot(ChessSearch *search, ChessBoard &board);

	void updateRootMove(const ChessMove &pvmove);

	void orderMoves(const ChessBoard &board, ChessMoveSortList &moves,
		int nbmoves);

	void orderMovesCaptures(ChessMoveSortList &moves, int nbmoves);

	void orderMovesQuiet(const ChessBoard &board, ChessMoveSortList &moves,
		int pos, int nbmoves);

	void orderMovesEvasion(const ChessBoard &board, ChessMoveSortList &moves,
		int nbmoves);

	int orderMovesRoot(ChessSearch *search, ChessBoard &board,
		ChessMoveSortList &moves, int nbmoves, int nply, bool incheck);

	void orderMovesQuiescent(ChessMoveSortList &moves, int nbmoves);

	void orderMovesQuiescentChecks(const ChessBoard &board,
		ChessMoveSortList &moves, int nbmoves);

	int getNextMove(const ChessBoard &board, ChessMove &move);

#ifdef PRECALC_MVVLVA
	static void precalcMVVLVA();
#endif
};

////////////////////////////////////////////////////////////////////////////////
inline ChessMoveSortList &ChessMoveOrder::getMoveList()
{
	return movelist;
}

////////////////////////////////////////////////////////////////////////////////
inline int ChessMoveOrder::getMoveCount() const
{
	return count;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessMoveOrder::setMoveCount(int n)
{
	count = n;
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMoveSort ChessMoveOrder::getMoveSort(int n) const
{
	return movelist[n];
}

////////////////////////////////////////////////////////////////////////////////
inline ChessMove ChessMoveOrder::getMove(int n) const
{
	return movelist[n].getMove();
}

////////////////////////////////////////////////////////////////////////////////
inline int32_t ChessMoveOrder::getMoveScore(int n) const
{
	return movelist[n].getScore();
}

} // namespace Cheese

#endif //CHEESE_MOVEORDER_H_

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_BITBOARD_H_
#define CHEESE_BITBOARD_H_

#include "config.h"

#include "move.h"

#include <array>

#include <cassert>

#ifdef _MSC_VER
#ifdef SYSTEM64BIT
#include <intrin.h>
#pragma intrinsic(_BitScanForward64)
#pragma intrinsic(_BitScanReverse64)
#pragma intrinsic(__popcnt64)
#else
#include <intrin.h>
#pragma intrinsic(_BitScanForward)
#pragma intrinsic(_BitScanReverse)
#endif
#endif

#ifdef USE_PEXT
#include <immintrin.h>
#endif

// used to create magic keys
//#define FIND_MAGIC_KEYS

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// 8x8 BitBoard (64 bits)
using BitBoard = uint64_t;

namespace BitBoards {

#ifdef FIND_MAGIC_KEYS
extern std::array<BitBoard, nb_squares> magic_rook_mask;
extern std::array<BitBoard, nb_squares> magic_rook_magic;
extern std::array<BitBoard *, nb_squares> magic_rook_ptr;
extern std::array<unsigned int, nb_squares> magic_rook_shift;
extern std::array<BitBoard, nb_squares> magic_bishop_mask;
extern std::array<BitBoard, nb_squares> magic_bishop_magic;
extern std::array<BitBoard *, nb_squares> magic_bishop_ptr;
extern std::array<unsigned int, nb_squares> magic_bishop_shift;
#else
extern const std::array<BitBoard, nb_squares> magic_rook_mask;
extern const std::array<BitBoard, nb_squares> magic_rook_magic;
extern std::array<BitBoard *, nb_squares> magic_rook_ptr;
extern const std::array<unsigned int, nb_squares> magic_rook_shift;
extern const std::array<BitBoard, nb_squares> magic_bishop_mask;
extern const std::array<BitBoard, nb_squares> magic_bishop_magic;
extern std::array<BitBoard *, nb_squares> magic_bishop_ptr;
extern const std::array<unsigned int, nb_squares> magic_bishop_shift;
#endif

// pawn attacks
extern std::array<std::array<BitBoard, nb_squares>, nb_sides> pawn_attack;

// knight attacks
extern std::array<BitBoard,nb_squares> knight_attack;

// king attacks
extern std::array<BitBoard, nb_squares> king_attack;

extern const std::array<BitBoard, nb_squares> square;

// all diagonal 45 R
extern std::array<BitBoard, nb_squares> diag_45r;

// all diagonal 45 L
extern std::array<BitBoard, nb_squares> diag_45l;

extern std::array<std::array<BitBoard, nb_squares>, nb_squares> between;

// direction from a square to another
extern std::array<std::array<int8_t, nb_squares>, nb_squares> direction;

extern const std::array<BitBoard, nb_files> file;

extern const std::array<BitBoard, nb_ranks> rank;

extern const std::array<BitBoard, nb_files> files_around;

// all white squares
extern const BitBoard white_squares;

// all black squares
extern const BitBoard black_squares;

#ifdef FIND_MAGIC_KEYS
extern std::array<BitBoard, nb_squares> magic_bishop_magic;
extern std::array<BitBoard, nb_squares> magic_rook_magic;
extern std::array<BitBoard, nb_squares> magic_bishop_mask;
extern std::array<BitBoard, nb_squares> magic_rook_mask;
extern std::array<unsigned int, nb_squares> magic_bishop_shift;
extern std::array<unsigned int, nb_squares> magic_rook_shift;
#endif

// bitboard attacks functions

////////////////////////////////////////////////////////////////////////////////
// compute knight attack
inline BitBoard	attackKnight(int sq)
{
	return knight_attack[sq];
}

////////////////////////////////////////////////////////////////////////////////
// compute king attack
inline BitBoard	attackKing(int sq)
{
	return king_attack[sq];
}

////////////////////////////////////////////////////////////////////////////////
// compute pawn attack
inline BitBoard	attackPawn(int color, int sq)
{
	return pawn_attack[color][sq];
}

////////////////////////////////////////////////////////////////////////////////
// compute sliding attack index
inline uint64_t magicAttack(BitBoard occ, BitBoard mask, BitBoard magic,
	unsigned int shift)
{
#ifdef USE_PEXT
	return _pext_u64(occ, mask);
#else

#ifndef SYSTEM64BIT
	const BitBoard bb = occ & mask;
	uint32_t lo = static_cast<unsigned>(static_cast<int32_t>(bb) *
										static_cast<int32_t>(magic));
	uint32_t hi = static_cast<unsigned>(static_cast<int32_t>(bb >> 32) *
										static_cast<int32_t>(magic >> 32));
	return ((lo ^ hi) >> shift);
#else
	return ((occ & mask) * magic) >> shift;
#endif // SYSTEM64BIT

#endif //USE_BITBOARD_PEXT
}

////////////////////////////////////////////////////////////////////////////////
// compute rook attack
inline BitBoard	attackRook(int sq, BitBoard occ)
{
	return *(magic_rook_ptr[sq] +
		magicAttack(occ, magic_rook_mask[sq],
			magic_rook_magic[sq],
			magic_rook_shift[sq]));
}

////////////////////////////////////////////////////////////////////////////////
// compute bishop attack
inline BitBoard	attackBishop(int sq, BitBoard occ)
{
	return *(magic_bishop_ptr[sq] +
		magicAttack(occ, magic_bishop_mask[sq],
			magic_bishop_magic[sq],
			magic_bishop_shift[sq]));
}

////////////////////////////////////////////////////////////////////////////////
// compute queen attack
inline BitBoard	attackQueen(int sq, BitBoard occ)
{
	return (attackRook(sq, occ) | attackBishop(sq, occ));
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr BitBoard shiftUp(BitBoard bb)
{
	return (side == white) ? (bb << dir_up) : (bb >> dir_up);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr BitBoard shiftUp2(BitBoard bb)
{
	return (side == white) ? (bb << dir_up_2) : (bb >> dir_up_2);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr BitBoard shiftUpLeft(BitBoard bb)
{
	return (side == white) ? (bb << dir_up_left) : (bb >> dir_up_left);
}

////////////////////////////////////////////////////////////////////////////////
template<Side side>
constexpr BitBoard shiftUpRight(BitBoard bb)
{
	return (side == white) ? (bb << dir_up_right) : (bb >> dir_up_right);
}

////////////////////////////////////////////////////////////////////////////////
// fill all bits up
inline BitBoard fillUp(BitBoard bb)
{
	BitBoard b = bb;
	b |= (b << 8);
	b |= (b << 16);
	b |= (b << 32);
	return b;
}

////////////////////////////////////////////////////////////////////////////////
// fill all bits down
inline BitBoard fillDown(BitBoard bb)
{
	BitBoard b = bb;
	b |= (b >> 8);
	b |= (b >> 16);
	b |= (b >> 32);
	return b;
}

////////////////////////////////////////////////////////////////////////////////
// tell if we have more than 1 bit set in the bitboard
// bb must not be empty !
constexpr bool bitCountMoreThanOne(BitBoard bb)
{
	return (bb & (bb - 1)) != 0ULL;
}

////////////////////////////////////////////////////////////////////////////////
inline int bitCountSoft(BitBoard bb)
{
#ifdef SYSTEM64BIT
	constexpr BitBoard k1 = 0x5555555555555555ULL;
	constexpr BitBoard k2 = 0x3333333333333333ULL;
	constexpr BitBoard k4 = 0x0f0f0f0f0f0f0f0fULL;
	constexpr BitBoard kf = 0x0101010101010101ULL;

	BitBoard b =  bb - ((bb >> 1)  & k1);
    b = (b & k2) + ((b >> 2)  & k2);
    b = (b       +  (b >> 4)) & k4 ;
    b = (b * kf) >> 56;

	return static_cast<int>(b);
#else
	constexpr BitBoard bitcount_m1 = 0x5555555555555555ULL;
	constexpr BitBoard bitcount_m2 = 0x3333333333333333ULL;

	BitBoard a = bb - ((bb >> 1) & bitcount_m1);
	BitBoard c = (a & bitcount_m2) + ((a >> 2) & bitcount_m2);

	auto n = static_cast<uint32_t>(c) +
			 static_cast<uint32_t>(c >> 32);
	n = (n & 0x0F0F0F0F) + ((n >> 4) & 0x0F0F0F0F);
	n = (n & 0xFFFF) + (n >> 16);
	n = (n & 0xFF) + (n >> 8);

	return static_cast<int>(n);
#endif
}

////////////////////////////////////////////////////////////////////////////////
// bitboard bitcount
inline int bitCount(BitBoard bb)
{
#ifdef SYSTEM64BIT
#ifdef USE_POPCOUNT
#ifdef _MSC_VER
	return static_cast<int>(__popcnt64(bb));
#else
	return static_cast<int>(__builtin_popcountll(bb));
#endif // _MSC_VER
#else
	return bitCountSoft(bb);
#endif // USE_POPCOUNT
#else
	return bitCountSoft(bb);
#endif // SYSTEM64BIT
}

////////////////////////////////////////////////////////////////////////////////
// test : get the first bit of a bitboard
// warning ! don't work for bb = 0
inline int getFirstBit(BitBoard bb)
{
	assert(bb != 0ULL);
#ifdef _MSC_VER
#ifdef SYSTEM64BIT
	unsigned long index;
	_BitScanForward64(&index, bb);
	return static_cast<int>(index);
#else
	unsigned long index;
	if (static_cast<uint32_t>(bb)) {
		_BitScanForward(&index, static_cast<uint32_t>(bb));
		return static_cast<int>(index);
	} else {
		_BitScanForward(&index, static_cast<uint32_t>(bb >> 32));
		return static_cast<int>(index) + 32;
	}
#endif // SYSTEM64BIT
#else
	return static_cast<int>(__builtin_ctzll(bb));
#endif // _MSC_VER
}

////////////////////////////////////////////////////////////////////////////////
inline int getLastBit(BitBoard bb)
{
	assert(bb != 0ULL);
#ifdef _MSC_VER
#ifdef SYSTEM64BIT
	unsigned long index;
	_BitScanReverse64(&index, bb);
	return static_cast<int>(index);
#else
	unsigned long index;
	if (static_cast<uint32_t>(bb >> 32)) {
		_BitScanReverse(&index, static_cast<uint32_t>(bb >> 32));
		return static_cast<int>(index) + 32;
	} else {
		_BitScanReverse(&index, static_cast<uint32_t>(bb));
		return static_cast<int>(index);
	}
#endif // SYSTEM64BIT
#else
	return 63 - static_cast<int>(__builtin_clzll(bb));
#endif // _MSC_VER
}

////////////////////////////////////////////////////////////////////////////////
// get the first bit and remove it from bitboard
inline int popFirstBit(BitBoard &bb)
{
	int x = getFirstBit(bb);
	bb &= (bb - 1);
	return x;
}

// init bitboard tables
void init();

// print a bitboard using logfile
void printBitBoard(BitBoard bb, std::ostream &stream);

} // namespace BitBoards

} // namespace Cheese

#endif // CHEESE_BITBOARD_H_

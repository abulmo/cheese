////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "move.h"

#include "util/logfile.h"

#include <iomanip>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// flip vertically a square on the board
const std::array<int, nb_squares> flip_square = {
	a8, b8, c8, d8, e8, f8, g8, h8,
	a7, b7, c7, d7, e7, f7, g7, h7,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a1, b1, c1, d1, e1, f1, g1, h1
};

const std::array<char, nb_pieces_name> pieces_name = {
	' ', 'p', 'n', 'b', 'r', 'q', 'k', ' ',
	' ', 'P', 'N', 'B', 'R', 'Q', 'K', ' '
};

////////////////////////////////////////////////////////////////////////////////
constexpr auto makeCharFile(int x)
{
	constexpr auto f = static_cast<int>('a');
	return static_cast<char>(f + x);
}

////////////////////////////////////////////////////////////////////////////////
constexpr auto makeCharRank(int y)
{
	constexpr auto r = static_cast<int>('1');
	return static_cast<char>(r + y);
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::textShort(ChessMoveSort *moves, int count)
{
	std::string str;

	if (flag == 0) {
		return str;
	}

	int sqsrc = src();
	int sqdst = dst();
	int sx = squareFile(sqsrc);
	int sy = squareRank(sqsrc);
	int dx = squareFile(sqdst);
	int dy = squareRank(sqdst);
	int mp = piece();

	bool ss = false;
	bool sf = false;
	bool sr = false;

	// find if we have another piece of the same type able to move to
	// destination
	if ((mp != pawn) && (mp != king)) {
		for (int n=0; n<count; n++) {

			const ChessMove &move = moves[n].getMove();

			// regarde si on a la meme piece qui peut atteindre la destination
			if ((move.piece() == mp) &&
				(move.dst() == sqdst) &&
				(move.src() != sqsrc)) {

				ss = true;

				int sqx = squareFile(move.src());
				int sqy = squareRank(move.src());

				if (sqx == sx) {
					sf = true;
				}
				if (sqy == sy) {
					sr = true;
				}

				if ((!sf) && (!sr)) {
					sr = true;
				}
			}
		}
	}

	if (isCastle()) {
		str += 'O';
		str += '-';
		str += 'O';

		if (dx < sx) {
			str += '-';
			str += 'O';
		}
	} else {

		// source
		if (mp != pawn) {
			str += pieces_name[mp + 8];
		}

		if (ss) {
			if (sr) {
				str += makeCharFile(sx);
			}
			if (sf) {
				str += makeCharRank(sy);
			}
		}

		// action
		int mcp = capture();
		if (mcp != no_piece) {
			if (mp == 1) {
				str += makeCharFile(sx);
			}
			str += 'x';
		}

		// destination
		str += makeCharFile(dx);
		str += makeCharRank(dy);

		// promotion
		int pro = promotion();
		if (pro != no_piece) {
			str += '=';
			str += pieces_name[pro + 8];
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::text()
{
	std::string str;

	if (flag == 0) {
		return str;
	}

	int sqsrc = src();
	int sqdst = dst();
	int sx = squareFile(sqsrc);
	int sy = squareRank(sqsrc);
	int dx = squareFile(sqdst);
	int dy = squareRank(sqdst);

	if (isCastle()) {
		str += 'O';
		str += '-';
		str += 'O';
		if (dx < sx) {
			str += '-';
			str += 'O';
		}
	} else {

		// piece
		int mp = piece();
		if (mp != pawn) {
			str += pieces_name[mp + 8];
		}

		// source
		str += makeCharFile(sx);
		str += makeCharRank(sy);

		// action
		if (capture() != no_piece) {
			str += 'x';
		} else {
			str += '-';
		}

		// destination
		str += makeCharFile(dx);
		str += makeCharRank(dy);

		// promotion
		int pro = promotion();
		if (pro != no_piece) {
			str += '=';
			str += pieces_name[pro + 8];
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::strSquare(int sq)
{
	std::string str;
	str += makeCharFile(squareFile(sq));
	str += makeCharRank(squareRank(sq));
	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::textMove()
{
	int sqsrc = src();
	int sqdst = dst();

	std::string str;
	str += makeCharFile(squareFile(sqsrc));
	str += makeCharRank(squareRank(sqsrc));
	str += makeCharFile(squareFile(sqdst));
	str += makeCharRank(squareRank(sqdst));
	return str;
}

////////////////////////////////////////////////////////////////////////////////
void ChessMove::print()
{
	#ifdef USELOG_DEBUG
	LOG_DEBUG << "move (" << textMove() << " 0x"
			  << std::setfill('0') << std::setw(8) << std::hex << flag
			  << ") : src "
			  << strSquare(src())
			  << ", dst "
			  << strSquare(dst())
			  << ", mv (" << pieces_name[piece() + 8] << ')'
			  << ", cap (" << pieces_name[capture() + 8] << ')'
			  << ", pro (" << pieces_name[promotion() + 8] << ')'
			  << ", cas " << isCastle()
			  << ", ep " << isEnPassant();
	#endif
}

} // namespace Cheese

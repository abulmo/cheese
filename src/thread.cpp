////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "thread.h"

#include "util/logfile.h"

#ifdef WIN32
#include <cerrno>
#endif

namespace Cheese {

uint32_t Thread::countThread = 0;

#ifdef WIN32
////////////////////////////////////////////////////////////////////////////////
Thread::Thread() : handle(nullptr), tid(0), initialized(false), terminate(false)
{
	id = countThread++;
}
#else
////////////////////////////////////////////////////////////////////////////////
Thread::Thread() : tid(), initialized(false), terminate(false)
{
	id = countThread++;
}
#endif

////////////////////////////////////////////////////////////////////////////////
Thread::~Thread()
{
	stop();
}

////////////////////////////////////////////////////////////////////////////////
void Thread::create()
{
	terminate = false;
#ifdef WIN32
	handle = (HANDLE)_beginthreadex(nullptr, thread_max_stack_size,
							Thread::funcThread, this, 0, &tid);
	if (handle == nullptr) {
		LOG_ERROR << "Thread " << id << " creation error : " << errno;
	} else {
		LOG_DEBUG << "Create system thread " << id;
		initialized = true;
	}
#else

	// set max stack size
	pthread_attr_t tattr;
	pthread_attr_init(&tattr);
	std::size_t szstack = 0;
	if (pthread_attr_getstacksize(&tattr, &szstack) == 0) {
		if (pthread_attr_setstacksize(&tattr, thread_max_stack_size)) {
			LOG_WARNING << "unable to set thread stack limit";
		}
	} else {
		LOG_WARNING << "unable to get thread stack limit";
	}

	int r = pthread_create(&tid, &tattr, Thread::funcThread, this);
	if (r != 0) {
		LOG_ERROR << "Thread " << id << " creation error : " << r;
	} else {
		LOG_DEBUG << "Create system thread " << id;
		initialized = true;
	}
#endif
}

////////////////////////////////////////////////////////////////////////////////
void Thread::stop()
{
#ifdef WIN32
	if (initialized) {

		mutex.lock();
		terminate = true;
		mutex.unlock();

		cond.notify_one();

		WaitForSingleObject(handle, INFINITE);
		CloseHandle(handle);

		LOG_DEBUG << "terminate system thread " << id;
		handle = nullptr;
		tid = 0;
		initialized = false;
	}
#else
	if (initialized) {

		mutex.lock();
		terminate = true;
		mutex.unlock();

		cond.notify_one();

		pthread_join(tid, nullptr);

		LOG_DEBUG << "terminate system thread " << id;
		tid = 0;
		initialized = false;
	}
#endif
}

#ifdef WIN32
////////////////////////////////////////////////////////////////////////////////
unsigned int __stdcall Thread::funcThread(void *param)
{
	static_cast<Thread *>(param)->func();
	_endthreadex(0);
	return 0;
}
#else
////////////////////////////////////////////////////////////////////////////////
void *Thread::funcThread(void *param)
{
	static_cast<Thread *>(param)->func();
	return nullptr;
}
#endif

} // namespace Cheese

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "tune.h"

namespace Cheese {

#ifdef USE_TUNING

////////////////////////////////////////////////////////////////////////////////
void Tuning::add(const std::string &name, TunableType *ptr, TunableType def,
	TunableType min, TunableType max)
{
	Tunable p;
	p.name = name;
	p.ptr = ptr;
	p.def = def;
	p.min = min;
	p.max = max;
	params.emplace_back(p);
}

////////////////////////////////////////////////////////////////////////////////
void Tuning::set(const std::string &name, TunableType value)
{
	for (auto &p : params) {
		if (p.name == name) {
			*(p.ptr) = value;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
TunableType Tuning::get(const std::string &name)
{
	for (auto &p : params) {
		if (p.name == name) {
			return *(p.ptr);
		}
	}
	return 0;
}

#endif //USE_TUNING

} // namespace Cheese

////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2021 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>

#include <algorithm>
#include <tuple>
#include <string>
#include <vector>

#include "bitboard.h"
#include "board.h"
#include "endgame.h"
#include "engine.h"
#include "eval.h"
#include "movegen.h"
#include "moveorder.h"
#include "tools.h"

using namespace Cheese;

////////////////////////////////////////////////////////////////////////////////
// test basic bitboards functions
TEST(BitBoards, TestBitBoards) {
	EXPECT_EQ(BitBoards::bitCount(0ULL), 0);
	EXPECT_EQ(BitBoards::bitCount(0xFFFFFFFFFFFFFFFFULL), 64);
	EXPECT_EQ(BitBoards::getFirstBit(1ULL), 0);
	EXPECT_EQ(BitBoards::getFirstBit(0x8000000000000000ULL), 63);	
	EXPECT_EQ(BitBoards::getFirstBit(0x80000000ULL), 31);		
	
	EXPECT_EQ(BitBoards::shiftUp<white>(BitBoards::square[e2]), 
		   BitBoards::square[e3]);
	EXPECT_EQ(BitBoards::shiftUp2<white>(BitBoards::square[e2]), 
		   BitBoards::square[e4]);
	EXPECT_EQ(BitBoards::shiftUpLeft<white>(BitBoards::square[e2]), 
		   BitBoards::square[d3]);
	EXPECT_EQ(BitBoards::shiftUpRight<white>(BitBoards::square[e2]), 
		   BitBoards::square[f3]);
}

////////////////////////////////////////////////////////////////////////////////
// test chess move format
TEST(ChessMove, TestChessMove) {
	for (int src=a1; src<=h8; src++) {
		for (int dst=a1; dst<=h8; dst++) {
			for (int mv=pawn; mv<=king; mv++) {
				for (int cap=no_piece; cap<=king; cap++) {
					for (int pro=knight; pro<=queen; pro++) {
						for (int cas=0; cas<2; cas++) {
							for (int ep=0; ep<2; ep++) {
								ChessMove move(src, dst, mv,cap, pro, cas, ep);
								EXPECT_EQ(move.src(), src);
								EXPECT_EQ(move.dst(), dst);
								EXPECT_EQ(move.piece(), mv);
								EXPECT_EQ(move.capture(), cap);
								EXPECT_EQ(move.promotion(), pro);
								EXPECT_EQ(move.isCastle(), (cas == 1));
								EXPECT_EQ(move.isEnPassant(), (ep == 1));
							}
						}
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// test basic move functions
TEST(ChessMove, TestMoveFunctions) {
	
	EXPECT_EQ(relSquareUp<white>(e4), e5);
	EXPECT_EQ(relSquareUp<black>(e5), e4);
	
	EXPECT_EQ(relSquareDown<white>(e4), e3);
	EXPECT_EQ(relSquareDown<black>(e5), e6);
	
	EXPECT_EQ(relSquareUpLeft<white>(e4), d5);
	EXPECT_EQ(relSquareUpLeft<black>(e5), f4);
	
	EXPECT_EQ(relSquareUpRight<white>(e4), f5);
	EXPECT_EQ(relSquareUpRight<black>(e5), d4);
	
	EXPECT_EQ(relSquareRank<white>(e4), rank_4);
	EXPECT_EQ(relSquareRank<black>(e5), rank_4);
}

////////////////////////////////////////////////////////////////////////////////
// test move generation
TEST(MoveGen, TestMoveGen1) {
	ChessEngine engine;
	engine.init(1);	
	ChessEngine::precalcTables();
	ChessBoard &board = engine.getBoardRef();
	ChessMoveSortList moves;
	engine.setFENPosition("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ");
	int r = ChessMoveGen::generateLegalMoves(board, moves, board.inCheck());
	EXPECT_EQ(r, 20);
	engine.setFENPosition("R6R/3Q4/1Q4Q1/4Q3/2Q4Q/Q4Q2/pp1Q4/kBNN1KB1 w - - 0 1");
	r = ChessMoveGen::generateLegalMoves(board, moves, board.inCheck());
	EXPECT_EQ(r, 218);
	engine.setFENPosition("3qr1Qk/pbpp2pp/1p5N/6b1/2P1P3/P7/1PP2PPP/R4RK1 b - - 1 1 ");
	r = ChessMoveGen::generateLegalMoves(board, moves, board.inCheck());
	EXPECT_EQ(r, 1);
}

////////////////////////////////////////////////////////////////////////////////
// test fen reverse function 
TEST(FEN, TestFEN) {
	ChessEngine engine;
	engine.init(1);	
	ChessEngine::precalcTables();
	ChessBoard &board = engine.getBoardRef();
	
	ChessFENParser fen(board);
	
	std::vector<std::pair<std::string, std::string>> positions = {
		{"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ",
		 "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1 "},
		 		
		{"r4rk1/1b1n1pb1/3p2p1/1p1Pq1Bp/2p1P3/2P2RNP/1PBQ2P1/5R1K w - - 0 1 ",
		 "5r1k/1pbq2p1/2p2rnp/2P1p3/1P1pQ1bP/3P2P1/1B1N1PB1/R4RK1 b - - 0 1 "},
		 
		{"r1bqkbr1/pppp1ppp/2n2n2/4p3/4P3/2N2N2/PPPP1PPP/1RBQKB1R w Kq - 0 1 ",		 
		 "1rbqkb1r/pppp1ppp/2n2n2/4p3/4P3/2N2N2/PPPP1PPP/R1BQKBR1 b Qk - 0 1 "}
	};
	
	for (const auto &p : positions) {
		std::string strtmp;
		int r = fen.reverse(p.first, strtmp);
		EXPECT_EQ(r, true);
		EXPECT_EQ(strtmp, p.second);
		r = fen.reverse(p.second, strtmp);
		EXPECT_EQ(r, true);
		EXPECT_EQ(strtmp, p.first);
	}	
}

/////////////////////////////////////////////////////////////////////////////////
// test staged move generation
TEST(MoveOrder, TestMoveOrder1) {
	
	ChessEngine engine;
	engine.init(1);	
	ChessEngine::precalcTables();
	ChessBoard &board = engine.getBoardRef();
	
	ChessMoveSortList moves;
	
	std::string fen = 
		"r4rk1/1b1n1pb1/3p2p1/1p1Pq1Bp/2p1P3/2P2RNP/1PBQ2P1/5R1K w - - 0 1 ";
	engine.setFENPosition(fen);
	ChessMoveGen::generateLegalMoves(board, moves, board.inCheck());
		
	ChessMoveOrder moveorder(board, 
		engine.getThreads().getMainThread()->history, ordermove_hash, 1, 
		ChessMove(g3, f5, knight, no_piece, 0, 0, 0), ChessMove(0));
		
	ChessMoveSortList moves2;
	ChessMove move;
	ChessMoveRec rec;
	
	while (moveorder.getNextMove(board, move) != ordermove_end) {
		
		board.makeMove(move, rec);

		if (!board.isPositionLegal()) {
			board.unMakeMove(move, rec);
			continue;
		}

		moves2.push(move);
		
		board.unMakeMove(move, rec);
	}
	
	EXPECT_EQ(moves.size(), moves2.size());
	
	for (int i=0; i<moves.size(); i++) {
		bool f = false;
		for (int j=0; j<moves2.size(); j++) {
			if (moves2[j].move == moves[i].move) {
				f = true;
				break;
			}
		}
		EXPECT_EQ(f, true);
	}	
}

////////////////////////////////////////////////////////////////////////////////
// test eval symmetry
TEST(ChessEval, TestEval1) {
	ChessEngine engine;
	engine.init(1);	
	ChessEngine::precalcTables();
	ChessBoard &board = engine.getBoardRef();	
	
	ChessFENParser fen(board);
	
	std::vector<std::string> positions = {
		"r1bqkbnr/1ppp1ppp/p1n5/4p3/B3P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 1 4 ",
		"8/6p1/P1b1pp2/2p1p3/1k4P1/3PP3/1PK5/5B2 w - - 0 1 ",
		"2rr3k/2qnbppp/p1n1p3/1p1pP3/3P1N2/1Q1BBP2/PP3P1P/1KR3R1 w - - 0 1 ",
		"rb3qk1/pQ3ppp/4p3/3P4/8/1P3N2/1P3PPP/3R2K1 w - - 0 1 ",
		"kr2R3/p4r2/2pq4/2N2p1p/3P2p1/Q5P1/5P1P/5BK1 w - - 0 1 "
	};
	
	for (const auto &p : positions) {
		engine.setFENPosition(p);
		int v = engine.evaluate(min_score_mate, max_score_mate);		
		std::string rstr;
		int r = fen.reverse(p, rstr);
		EXPECT_EQ(r, true);
		engine.setFENPosition(rstr);
		int rv = engine.evaluate(min_score_mate, max_score_mate);	
		EXPECT_EQ(v, rv);
	}
}

////////////////////////////////////////////////////////////////////////////////
// test SEE
TEST(ChessBoard, TestSEE) {
	ChessEngine engine;
	engine.init(1);	
	ChessEngine::precalcTables();
	ChessBoard &board = engine.getBoardRef();	
	
	ChessFENParser fen(board);
	
	std::vector<std::tuple<std::string, ChessMove, int>> positions = {
		{ "rb3qk1/pQ3ppp/4p3/3P4/8/1P3N2/1P3PPP/3R2K1 w - - 0 1 ", 
			ChessMove(b7, a8, queen, rook), 
			pieces_value[rook] },
		{ "2b3k1/4rrpp/p2p4/2pP2RQ/1pP1Pp1N/1P3P1P/1q6/6RK w - - 0 1 ",
			ChessMove(h5, h7, queen, pawn), 
			pieces_value[pawn] - pieces_value[queen] },
		{ "r4rk1/pp2ppbp/1qp2np1/n2P4/1Q2P3/2N2B2/PP3PPP/R1B2RK1 w - - 0 1 ",
			ChessMove(b4, b6, queen, queen),
			0 }
			
	};
	
	for (auto p : positions) {
		engine.setFENPosition(std::get<0>(p));
		int r = board.staticExchangeEvaluation(std::get<1>(p));		
		EXPECT_EQ(r, std::get<2>(p));
	}
}

////////////////////////////////////////////////////////////////////////////////
// test bitbases
TEST(ChessBitBases, TestBitBases1) {
	ChessEngine engine;
	engine.init(1);	
	ChessEngine::precalcTables();
	ChessBoard &board = engine.getBoardRef();	
	
	ChessFENParser fen(board);
	
	std::vector<std::pair<std::string, bool>> positions = {
		{ "4k3/8/8/8/P7/8/8/K7 w - - 0 1 ", false },
		{ "k7/8/8/p7/8/8/8/4K3 b - - 0 1 ", false },
		{ "4k3/8/2K5/8/8/8/3P4/8 w - - 0 1 ", true },
		{ "8/3p4/8/8/8/2k5/8/4K3 b - - 0 1 ", true }
	};
	
	for (const auto &p : positions) {
		engine.setFENPosition(p.first);
			
		int v = ChessEndGames::evalKPK(static_cast<Side>(board.side()), board);
		
		if (p.second) {
			EXPECT_NE(v, 0);
		} else {
			EXPECT_EQ(v, 0);
		}				
	}
}

// TODO : test tablebases 

// TODO : test FRC castling

////////////////////////////////////////////////////////////////////////////////
// test perft counts
uint64_t testPerft(const std::string &str, int d)
{
	ChessEngine engine;
	engine.init(1);	
	ChessEngine::precalcTables();
	engine.setFENPosition(str);		
	ChessTools tools(engine, engine.getBoardRef());
	tools.perft(d);
	return tools.countnode;
}

TEST(Perft, TestPerft1) {	
    EXPECT_EQ(119060324, testPerft(
	"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ", 6));
}

TEST(Perft, TestPerft2) {
	EXPECT_EQ(193690690, testPerft(
	"r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - ", 5));
}

TEST(Perft, TestPerft3) {
	EXPECT_EQ(178633661, testPerft(
	"8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - ", 7));
}

TEST(Perft, TestPerft4) {
	EXPECT_EQ(15833292, testPerft(
	"r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1 ", 5));
}

TEST(Perft, TestPerft5) {
	EXPECT_EQ(15833292, testPerft(
	"r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1 ", 5));
}

TEST(Perft, TestPerft6) {
	EXPECT_EQ(89941194, testPerft(
	"rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8  ", 5));	
}

////////////////////////////////////////////////////////////////////////////////
// test hash table entry format : type
TEST(Hash, TestHashType) {
	ChessEngine engine;
	ChessHashTable &ht = engine.getHashTable();
	engine.init(1);	
	ChessEngine::precalcTables();
	
	for (int i=0; i<3; i++) {
		for (int n=0; n<4; n++) {
			ChessHashNode *hash = ht.getHashNode(n);
			int depth = 0;
			int score = 0;
			int age = 0;
			if (i == 1) {
				depth = 255;
				score = 32767;
				age = 63;
			} 
			if (i == 2) {
				depth = 255;
				score = -32768;
				age = 63;
			} 
			ChessHashNode node(static_cast<HashKey>(n), ChessMove(0), 
				static_cast<HashNodeType>(n), depth, score, age);
			hash->write(node);
			ChessHashNode node2;
			hash->read(node2);
			EXPECT_EQ(node.getKey(), static_cast<HashKey>(n));
			EXPECT_EQ(node.getMove(), ChessMove(0));
			EXPECT_EQ(node.getType(), static_cast<HashNodeType>(n));
			EXPECT_EQ(node.getDepth(), depth);
			EXPECT_EQ(node.getScore(), score);
			EXPECT_EQ(node.getAge(), age);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// test hash table entry format : depth
TEST(Hash, TestHashDepth) {
	ChessEngine engine;
	ChessHashTable &ht = engine.getHashTable();
	engine.init(1);	
	ChessEngine::precalcTables();

	for (int i=0; i<3; i++) {
		for (int n=0; n<256; n++) {
			ChessHashNode *hash = ht.getHashNode(n);
			HashNodeType type = HashNodeType::hash_exact;
			int score = 0;
			int age = 0;
			if (i == 1) {
				type = HashNodeType::hash_notfound;
				score = 32767;
				age = 63;
			}
			if (i == 2) {
				type = HashNodeType::hash_notfound;
				score = -32768;
				age = 63;
			}
			ChessHashNode node(static_cast<HashKey>(n), ChessMove(0), 
				type, n, score, age);
			hash->write(node);
			ChessHashNode node2;
			hash->read(node2);
			EXPECT_EQ(node.getKey(), static_cast<HashKey>(n));
			EXPECT_EQ(node.getMove(), ChessMove(0));
			EXPECT_EQ(node.getType(), type);
			EXPECT_EQ(node.getDepth(), n);
			EXPECT_EQ(node.getScore(), score);
			EXPECT_EQ(node.getAge(), age);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// test hash table entry format : score
TEST(Hash, TestHashScore) {
	ChessEngine engine;
	ChessHashTable &ht = engine.getHashTable();
	engine.init(1);	
	ChessEngine::precalcTables();
	
	for (int i=0; i<2; i++) {
		for (int n=-32768; n<32767; n++) {
			ChessHashNode *hash = ht.getHashNode(n);
			HashNodeType type = HashNodeType::hash_exact;
			int depth = 0; 
			int age = 0;
			if (i == 1) {
				type = HashNodeType::hash_notfound;
				depth = 255;
				age = 63;
			}
			ChessHashNode node(static_cast<HashKey>(n), ChessMove(0), 
				type, depth, n, age);
			hash->write(node);
			ChessHashNode node2;
			hash->read(node2);
			EXPECT_EQ(node.getKey(), static_cast<HashKey>(n));
			EXPECT_EQ(node.getMove(), ChessMove(0));
			EXPECT_EQ(node.getType(), type);
			EXPECT_EQ(node.getDepth(), depth);
			EXPECT_EQ(node.getScore(), n);
			EXPECT_EQ(node.getAge(), age);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// test hash table entry format : age
TEST(Hash, TestHashAge) {
	ChessEngine engine;
	ChessHashTable &ht = engine.getHashTable();
	engine.init(1);	
	ChessEngine::precalcTables();
	
	for (int i=0; i<3; i++) {
		for (int n=0; n<nb_squares; n++) {
			ChessHashNode *hash = ht.getHashNode(n);
			HashNodeType type = HashNodeType::hash_exact;
			int depth = 0; 
			int score = 0;
			if (i == 1) {
				type = HashNodeType::hash_notfound;
				depth = 255;
				score = 32767;
			}
			if (i == 2) {
				type = HashNodeType::hash_notfound;
				depth = 255;
				score = -32768;
			}
			ChessHashNode node(static_cast<HashKey>(n), ChessMove(0), 
				type, depth, score, n);
			hash->write(node);
			ChessHashNode node2;
			hash->read(node2);
			EXPECT_EQ(node.getKey(), static_cast<HashKey>(n));
			EXPECT_EQ(node.getMove(), ChessMove(0));
			EXPECT_EQ(node.getType(), type);
			EXPECT_EQ(node.getDepth(), depth);
			EXPECT_EQ(node.getScore(), score);
			EXPECT_EQ(node.getAge(), n);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
